#include "SpaghettiHeap.h"

template <class T>
SpaghettiHeap::SpaghettiHeap()
{
	size = 0;
}
template <class T>
SpaghettiHeap::~SpaghettiHeap()
{
	delete left;
	delete right;
}
template <class T>
bool SpaghettiHeap::insert(T* data)
{
	if(data==0) this.data = data;
	else
	{
		if(*data > *(this.data))
		{
			T* temp = this.data;
			this.data = data;
			data = temp;
		}
		if(!left)
		{
			left = new SpaghettiHeap();
			left->insert(data);
		}
		else if(!right)
		{
			right = new SpaghettiHeap();
			right->insert(data);
		}
		else if(left->size() <= right->size())
			left->insert(data);
		else right->insert(data);
	}
	size++;
	return true;
}
template <class T>
int SpaghettiHeap::size()
{
	return size;
}
template <class T>
T* SpaghettiHeap::pull()
{
	T* result = data;
	if(left->data&&right->data)
	{
		if(*(left->data) > *(right->data))
			data = left->pull();
		else
			data = right->pull();
	}
	else if(left->data)
		data = left->pull()
	else if(right->data)
		data = right->pull()
	else data = 0;
	return result;
}
template <class T>
void SpaghettiHeap::print()
{
	printf("%d ",*data);
	if(left&&left->data)
	{
		printf("LEFT: ");
		left->print();
	}
	if(right&&right->data)
	{
		printf("RIGHT: ");
		right->print();
	}
	printf("UP.");
}
