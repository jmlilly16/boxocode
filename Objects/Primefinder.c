#include <stdlib.h>
#include <stdio.h>

typedef struct Counter
{
	int prime;
	int count;
} Counter;

typedef struct CounterNode
{
	Counter* counter;
	CounterNode* next;
} CounterNode;

typedef struct PrimeNode
{
	int prime;
	primerNode* next;
} PrimeNode;

static int isPrime;
CounterNode* counterTracer,lastCounter;
PrimeNode* lastPrime;

int main()
{
	int x = 3;	//The number you're checking;
	//output 2,3 to file
	CounterNode* counterList = malloc(sizeof(CounterNode));
	counterList -> counter = malloc(sizeof(Counter));
	counterList -> counter -> prime = 3;
	counterList -> counter -> count = 0;
	counterList -> next = 0;

	PrimeNode* primeList = malloc(sizeof(PrimeNode));
	primeList -> prime = 3;
	primeList -> next = 0;
	lastPrime = primeList;
	lastCounter = counterList;

	while(x<1000)
	{
		isPrime = 1;
		x+=2;
		if(primeList -> prime == sqrt(X))
		{	
			lastCounter -> next = malloc(sizeof(CounterNode));
			lastCounter = lastCounter -> next;
			lastCounter -> counter = malloc(sizeof(Counter));
			lastCounter -> counter -> prime = primeList -> prime;
			lastCounter -> counter -> count = 0;
		}
			
		counterTracer = counterList;
		while(counterTracer != 0)
		{
			counterTracer -> counter -> count += 2;
			if(counterTracer -> counter -> count == counterTracer -> counter -> prime)
				isPrime = 0;
			if(counterTracer -> counter -> count > counterTracer -> counter -> prime)
				counterTracer -> counter -> count %= counterTracer -> counter -> prime;
			counterTracer = counterTracer -> next;
		}
		if(isPrime)
		{
			lastPrime -> next = malloc(sizeof(PrimeNode));
			lastPrime = lastPrime -> next;
			lastPrime -> prime = x;
			lastPrime -> next = 0;
		}
	}
	return 0;
}
