#ifndef MODINT
#define MODINT
class ModInt
{
	private:
		int modNumber;
		int value;
	public:
		ModInt(int);
		~ModInt(void);

		int operator(int)(void) const;

		ModInt operator+(const int) const;
		ModInt operator*(const int) const;
		ModInt operator/(const int) const;
		ModInt operator-(const int) const;

		ModInt& operator=(const int);
		ModInt& operator+=(const int);
		ModInt& operator-=(const int);
		ModInt& operator*=(const int);
		ModInt& operator/=(const int);
		ModInt& operator++(void);
		ModInt& operator--(void);

		bool operator==(const int) const;
		bool operator!=(const int) const;
		bool operator<(const int) const;
		bool operator>(const int) const;
		bool operator<=(const int) const;
		bool operator>=(const int) const;

		int modNumber() const;
		int value() const;
}
#endif
