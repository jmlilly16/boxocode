#include "ModInt.h"

ModInt::ModInt(int mod)
{
	modNumber = mod;
}
ModInt::~ModInt(void)
{
}

int ModInt::operator(int)(void) const
{
	return value;
}

ModInt& ModInt::operator=(const int newval) const
{
	value = newval%modNumber;
	return this;
}

ModInt ModInt::operator+(const int i) const
{
	ModInt result(modNumber) = (value + i);
	return result;
}

ModInt ModInt::operator*(const int i) const
{
	ModInt result(modNumber) = (value * i);
	return result;
}
		ModInt operator/(const int) const;
		ModInt operator-(const int) const;

		ModInt& operator=(const int);
		ModInt& operator+=(const int);
		ModInt& operator-=(const int);
		ModInt& operator*=(const int);
		ModInt& operator/=(const int);
		ModInt& operator++(void);
		ModInt& operator--(void);

		bool operator==(const int) const;
		bool operator!=(const int) const;
		bool operator<(const int) const;
		bool operator>(const int) const;
		bool operator<=(const int) const;
		bool operator>=(const int) const;

		int modNumber() const;
		int value() const;
