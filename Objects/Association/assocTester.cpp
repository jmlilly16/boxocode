#include "assoc.h"
#include <iostream>
#include <typeinfo>
using namespace std;

int main()
{
	assoc<char,int> alphabet{{'a',1},{'b',2},{'c',3}};
	cout << "alphabet.contains() = " << alphabet.contains(1) << endl;
	alphabet.set(alphabet['a'],'d');
	cout << "alphabet.contains() = " << alphabet.contains(1) << endl;
	for(auto& i: (alphabet.left))
	{
		cout << i << endl;
	}
	return 0;
}
