template <class T>
class SpaghettiHeap<T>
{
	private:
	SpaghettiHeap<T>* left,right;
	int size;

	public:
	T* data;
	SpaghettiHeap<T>();
	~SpaghettiHeap<T>();

	int size();
	bool insert(T);
	T pull();
	void print();
}
