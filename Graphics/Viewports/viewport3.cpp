/********************************************************************
 FILE: viewport3.cpp
 NAME: T. Garrett
 DATE LAST MODIFIED: 01/22/2015
 DESCRIPTION:
	Demonstrates using viewports within the window
		- uses  vec4 for colors
		- color is stored along with position in struct VertexData  
		- uses a function call to draw the triangle

 KEYS:  ESC to exit program
		w - wireframe
		f - filled

 Assumes all header files and shader files (vshader_gouraud.glsl, fshader_gouraud.glsl) 
 	are in the same directory
********************************************************************/

 #ifdef __APPLE__       
 #include <GLUT/glut.h>
 #else		       
 #include <GL/glut.h>         
 #endif

#include "Angel.h"  //Provides InitShader, vec4

//GLOBAL VARIABLES:
int window_width=800, window_height=800;
bool wireframe = false;
bool filled = true;
GLuint vPosition, vColor;

void init();									
void display();									
void keyboard(unsigned char key, int x, int y);	

void drawTriangle();				

int main(int argc, char** argv) 
{
  glutInit(&argc,argv); 
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(window_width,window_height);
  glutInitWindowPosition(0,0);  
  glutCreateWindow("Gouraud Shading: press 'w' for Wireframe and 'f' for Filled");
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);  
  init();
  glutMainLoop();

}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  glClear(GL_COLOR_BUFFER_BIT); 
  glViewport(0,0,400,400);
  drawTriangle();
  glViewport(400,0,400,400);
  drawTriangle();
  glViewport(0,400,400,400);
  drawTriangle();
  glViewport(400,400,400,400);
  drawTriangle();

  glFlush(); 
}


void init() 
{
	struct VertexData {
		vec4 color;
		vec4 position;
	};
	
  // Specify the vertices for a triangle, along with a color for each:
  	VertexData vertices[3] = {
	    //color, position:
		{vec4(1.0, 0.0, 0.0, 1.0),	vec4(-1.0, -1.0, 0.0, 1.0)},  	//red
		{vec4(0.0, 1.0, 0.0, 1.0),	vec4(0.0, 1.0, 0.0, 1.0)},		//green
		{vec4(0.0, 0.0, 1.0, 1.0),	vec4(1.0, -1.0, 0.0, 1.0)}		//blue
	};

  // Create a vertex array object
  GLuint vao[1];
  #ifdef __APPLE__       // For use with OS X
    glGenVertexArraysAPPLE(1, vao );
    glBindVertexArrayAPPLE(vao[0] );
  #else		       // Other (Linux)
    glGenVertexArrays(1, vao );
    glBindVertexArray(vao[0] );       
  #endif
  
  // Create and initialize a buffer object
  GLuint buffer;
  glGenBuffers( 1, &buffer );
  glBindBuffer( GL_ARRAY_BUFFER, buffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW );
  GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
  glUseProgram( program );
  
  // Initialize the vertex color attribute
  GLuint color_location = glGetAttribLocation( program, "vColor" );
  glEnableVertexAttribArray(color_location);
  glVertexAttribPointer(color_location, 4, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(0) );

  // Initialize the vertex position attribute
  GLuint position_location = glGetAttribLocation( program, "vPosition");
  glEnableVertexAttribArray(position_location);
  glVertexAttribPointer(position_location, 4, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(sizeof(vertices[0].color)));
  
  glEnableVertexAttribArray(vColor);
  glEnableVertexAttribArray(vPosition);
  
  /* set clear color to black */
  glClearColor (0.0, 0.0, 0.0, 0.0);

 
  //std::cout<<glGetString(GL_SHADING_LANGUAGE_VERSION) ;

}

void drawTriangle()
{
	if (wireframe)
	  {
			glLineWidth(5.0);
		  	glDrawArrays(GL_LINE_LOOP, 0, 3);
	  };
	  if (filled)
	  {
			glDrawArrays(GL_TRIANGLES, 0,3);
	  };
}
void keyboard(unsigned char key, int x, int y)
{
   switch (key) {
      case 27:     //ESC
         exit(0);
         break;
  	  case 'w':   //wireframe
		wireframe = true;
		filled = false;
		glutPostRedisplay();
		break;
	  case 'f':   //filled
		wireframe = false;
		filled = true;
		glutPostRedisplay();
		break;
   }
}


