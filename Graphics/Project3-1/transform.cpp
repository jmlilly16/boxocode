/********************************************************************
 FILE: viewport1.cpp
 NAME: T. Garrett
 DATE LAST MODIFIED: 01/22/2015
 DESCRIPTION:
	Demonstrates using viewports within the window
		- uses  vec4 for colors
		- color is stored along with position in struct VertexData  

 KEYS:  ESC to exit program
		w - wireframe
		f - filled

 Assumes all header files and shader files (vshader_gouraud.glsl, fshader_gouraud.glsl) 
 	are in the same directory
********************************************************************/

 #ifdef __APPLE__       
 #include <GLUT/glut.h>
 #else		       
 #include <GL/glut.h>         
 #endif

#include "Angel.h"  //Provides InitShader, vec4
#include <iostream>
using namespace std;

//GLOBAL VARIABLES:
int window_width=800, window_height=800;
bool wireframe = false;
bool filled = true;
GLuint vPosition, vColor,viewMatLoc;
mat4 viewMat;

struct VertexData {
	vec4 color;
	vec4 position;
};
	
// Specify the vertices for a triangle, along with a color for each:
VertexData vertices[3] = {
	//color, position:
	{vec4(1.0, 0.0, 0.0, 1.0),	vec4(-1.0, -1.0, 0.0, 1.0)},  		//red
	{vec4(0.0, 1.0, 0.0, 1.0),	vec4(0.0, 1.0, 0.0, 1.0)},		//green
	{vec4(0.0, 0.0, 1.0, 1.0),	vec4(1.0, -1.0, 0.0, 1.0)}		//blue
};

void init();									
void display();									
void keyboard(unsigned char key, int x, int y);
void keyboardSpecial(int, int, int);				

int main(int argc, char** argv) 
{
  glutInit(&argc,argv); 
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
  glutInitWindowSize(window_width,window_height);
  glutInitWindowPosition(0,0);  
  glutCreateWindow("Move the triangle around!");
  glutDisplayFunc(display);
  glutKeyboardFunc(keyboard);
  glutSpecialFunc(keyboardSpecial);
  init();
  glutMainLoop();

}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
	glClear(GL_COLOR_BUFFER_BIT);
	glUniformMatrix4fv(viewMatLoc,1,GL_TRUE,viewMat);
	if (wireframe)
	{
		glLineWidth(5.0);
		glDrawArrays(GL_LINE_LOOP, 0, 3);
	};
	if (filled)
	{
		glDrawArrays(GL_TRIANGLES, 0,3);
	};
	glFlush(); 
}


void init() 
{
  
  // Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers( 1, &buffer );
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW );
	GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
	glUseProgram( program );
  
	// Initialize the vertex color attribute
	GLuint color_location = glGetAttribLocation( program, "vColor" );
	glEnableVertexAttribArray(color_location);
	glVertexAttribPointer(color_location, 4, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(0) );

	// Initialize the vertex position attribute
	GLuint position_location = glGetAttribLocation( program, "vPosition");
	glEnableVertexAttribArray(position_location);
	glVertexAttribPointer(position_location, 4, GL_FLOAT, GL_FALSE, sizeof(VertexData), BUFFER_OFFSET(sizeof(vertices[0].color)));
	
	glEnableVertexAttribArray(vColor);
	glEnableVertexAttribArray(vPosition);
  
	/* set clear color to black */
	glClearColor (0.0, 0.0, 0.0, 0.0);
	
	transMat = mat4();
}

void keyboard(unsigned char key, int x, int y)
{
	cout << key << endl;
	switch (key)
	{
		case 27:     //ESC
			exit(0);
			break;
		case 'w':   //wireframe
			wireframe = true;
			filled = false;
			glutPostRedisplay();
			break;
		case 'f':   //filled
			wireframe = false;
			filled = true;
			glutPostRedisplay();
			break;
		case '+':
			transMat = RotateZ(5.0) * transMat;
			break;
		case 'x':
			transMat = Scale(0.8,1.0,1.0) * transMat;
			break;
		case 'y':
			transMat = Scale(1.0,0.8,1.0) * transMat;
			break;
		case 'X':
			transMat = Scale(1.25,1.0,1.0) * transMat;
			break;
		case 'Y':
			transMat = Scale(1.0,1.25,1.0) * transMat;
			break;
		case ' ':
			transMat = mat4();
	}
	cout << transMat << endl;
	glutPostRedisplay();
}

void keyboardSpecial(int key, int x, int y)
{
	switch(key)
	{
		case GLUT_KEY_LEFT:
			transMat = Translate(-0.1,0.0,0.0) * transMat;
			break;
		case GLUT_KEY_DOWN:
			transMat = Translate(0.0,-0.1,0.0) * transMat;
			break;
		case GLUT_KEY_RIGHT:
			transMat = Translate(0.1,0.0,0.0) * transMat;
			break;
		case GLUT_KEY_UP:
			transMat = Translate(0.0,0.1,0.0) * transMat;
			break;
	}
	glutPostRedisplay();
}
