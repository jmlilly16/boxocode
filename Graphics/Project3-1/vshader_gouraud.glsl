attribute vec4 vColor, vPosition;
varying vec4 color;
uniform mat4 M;

void main()
{
	color = vColor;
	gl_Position = M * vPosition;
}
