//FILE: drawArraysDemo.cpp
//NAME:  T. Garrett
//DATE LAST MODIFIED:  01/14/2015
//  This program demonstrates the "mode" parameter in glDrawArrays function
//  Modes: POINTS, TRIANGLES, TRIANGLE_STRIP, TRIANGLE_FAN,
//         LINES, LINE_STRIP, LINE_LOOP 
//
//  Keyboard:
//		ESC to terminate program
//
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
 #ifdef __APPLE__       // For use with OS X
 #include <GLUT/glut.h>
 #else		       // Other (Linux)
 #include <GL/glut.h>         
 #endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
using namespace std;

void init();		
void display();		
void keyboard( unsigned char key, int x, int y );

int main(int argc, char** argv) {
	
//Set up the windowing context:

  //Initialize Window, display mode (singly buffered window, RGB mode).
  glutInit(&argc,argv); 
  glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

  //Window Size in pixels
  glutInitWindowSize(800,600);

  //Window Position (upper left corner of the screen).
  glutInitWindowPosition(0,0); 

  glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
  //glutCreateWindow("First OpenGL Program!");

//Pass our display function to the context
  glutDisplayFunc(display);

//Pass our keyboard function to the context
   glutKeyboardFunc( keyboard );

//Call our initialization function
  init();  //called only once, at the beginning

//Entger the GLUT event processing loop.
  glutMainLoop();

}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  // clear window 
  	glClear(GL_COLOR_BUFFER_BIT); 
  
  // specify if polygons are to be filled or not.  Default is filled.
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);	//fills the polygon
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);	//outlines the polygon
	
	//glPointSize(5);
	//glEnable(GL_POINT_SMOOTH);
	//glLineWidth(3.0);
	//glEnable(GL_LINE_SMOOTH);
	
	
  //Fill the buffer with ONE of the following:
	//glDrawArrays(GL_POINTS, 0, 6);
	//glDrawArrays(GL_LINES, 0, 6);
  	//glDrawArrays(GL_LINE_STRIP, 0, 6);
  	//glDrawArrays(GL_LINE_LOOP, 0, 6);
  	//glDrawArrays(GL_TRIANGLES, 0, 6);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 6);
	//glDrawArrays(GL_TRIANGLE_FAN, 0, 6);

  //flush the buffer (draw it on the screen)
   glFlush(); 

}

void init() 
{
  // Specify the vertices for a triangle
  // Alternative to using Angel's vec2 type

  	GLfloat points[6][2] = {
    	{0.0, 0.0},{-0.5, -0.5}, {-0.35, 0.75}, 
		{0.5, 0.5}, {0.8, -0.3}, {0.3, -0.75}
  	};

  // Create a Vertex Buffer Object
  GLuint VBO[1];
  #ifdef __APPLE__       // For use with OS X
    glGenVertexArraysAPPLE(1, VBO );
    glBindVertexArrayAPPLE(VBO[0] );
  #else		       // Other (Linux)
    glGenVertexArrays(1, VBO );
    glBindVertexArray(VBO[0] );       
  #endif
  
  // Create and initialize a buffer object
  GLuint buffer;
  glGenBuffers( 1, &buffer );
  glBindBuffer( GL_ARRAY_BUFFER, buffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  
  // Load shaders and use the resulting shader program
  GLuint program = InitShader( "vshader_demo.glsl", "fshader_demo.glsl" );
  glUseProgram( program );
  
  // Initialize the vertex position attribute from the vertex shader
  GLuint loc = glGetAttribLocation( program, "vPosition" );
  glEnableVertexAttribArray( loc );
  glVertexAttribPointer( loc, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
  
  /* set clear color to green */
  glClearColor (0.3, 1.0, 1.0, 0.0);
  
}

void keyboard( unsigned char key, int x, int y )
{
    switch ( key ) {
    case 033:   //ESC key
        exit( EXIT_SUCCESS );
        break;
    }
}

