//TODO: Fill this out
//FILE: morph.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  iunno
//  This program <blahblahblah>
//  
//
//  Keyboard:
//		<your keys here>
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
#include "Colors.h"
#include "Tweener.h"
#include "ParametricSet.h"
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 600;
const int MAXPOINTS = 256;
const int STEPFRAMES = 500;
const string INFILENAME1 = "closed.dat", INFILENAME2 = "half.dat", INFILENAME3 = "open.dat";

Tweener<GLfloat> openTweener1,openTweener2;
bool pauseSet=false,paused=false;
int currentWidth=WIN_WIDTH;
int currentHeight=WIN_HEIGHT;

void init();		
void display();		
void keyboard( unsigned char key, int x, int y );
//void mouse(int button, int state, int x, int y);
void idle();
void reshape(int width, int height);
void setAutoVP();

vec2 pix2dev(int x, int y);
vec4 pix2dev4(int x, int y);

typedef struct point
{
	vec4 pos;
	vec4 col;
} point;	

GLfloat umbrellaHeight = 0.0;

point points[MAXPOINTS];
int numCoords=0,numPoints=0;

int main(int argc, char** argv) {
	ifstream inFile1(INFILENAME1.data()),
		 inFile2(INFILENAME2.data()),
		 inFile3(INFILENAME3.data());
	int numOpen=0,numHalf=0,numClosed=0;
	
	vec4 openCoords[MAXPOINTS];
	vec4 halfCoords[MAXPOINTS];
	vec4 closedCoords[MAXPOINTS];
	
	string line;
	while(getline(inFile1,line).good())
		closedCoords[numClosed++] = 
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	while(getline(inFile2,line).good())
		halfCoords[numHalf++] =
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	while(getline(inFile3,line).good())
		openCoords[numOpen++] =
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	inFile1.close();
	inFile2.close();
	inFile3.close();
	
	numCoords = min(numOpen,(min(numHalf,numClosed)));

	cout << "Coord files read" << endl;
	
	//TODO: Put an error check here to make sure the number of points is good.
	numPoints = 0;
	//Background
	points[numPoints++] = {closedCoords[0],LIGHTGRAY};
	points[numPoints++] = {closedCoords[1],LIGHTBLUE};
	points[numPoints++] = {closedCoords[2],LIGHTGRAY};
	points[numPoints++] = {closedCoords[1],LIGHTBLUE};
	points[numPoints++] = {closedCoords[2],LIGHTGRAY};
	points[numPoints++] = {closedCoords[3],LIGHTBLUE};
	//Shaft
	points[numPoints++] = {closedCoords[8],DARKGRAY};
	points[numPoints++] = {closedCoords[9],DARKGRAY};
	points[numPoints++] = {closedCoords[10],DARKGRAY};
	points[numPoints++] = {closedCoords[10],DARKGRAY};
	points[numPoints++] = {closedCoords[11],DARKGRAY};
	points[numPoints++] = {closedCoords[8],DARKGRAY};
	//Handle
	points[numPoints++] = {closedCoords[4],mixColors(DARKGRAY,BROWN)};
	points[numPoints++] = {closedCoords[5],mixColors(DARKGRAY,BROWN)};
	points[numPoints++] = {closedCoords[6],BROWN};
	points[numPoints++] = {closedCoords[6],BROWN};
	points[numPoints++] = {closedCoords[7],BROWN};
	points[numPoints++] = {closedCoords[4],BROWN};
	//Folds
	points[numPoints++] = {closedCoords[12],WHITE};
	points[numPoints++] = {closedCoords[13],LIGHTGRAY};
	points[numPoints++] = {closedCoords[14],WHITE};
	points[numPoints++] = {closedCoords[14],RED};
	points[numPoints++] = {closedCoords[15],mixColors(RED,BLACK)};
	points[numPoints++] = {closedCoords[16],RED};
	points[numPoints++] = {closedCoords[16],WHITE};
	points[numPoints++] = {closedCoords[17],LIGHTGRAY};
	points[numPoints++] = {closedCoords[18],WHITE};
	points[numPoints++] = {closedCoords[18],RED};
	points[numPoints++] = {closedCoords[19],mixColors(RED,BLACK)};
	points[numPoints++] = {closedCoords[20],RED};

	point closedPoints[numPoints],halfPoints[numPoints],openPoints[numPoints];
	for(int i=0;i<numPoints;i++)
		closedPoints[i] = points[i];

	numPoints = 0;
	//Background
	halfPoints[numPoints++] = {halfCoords[0],GRAY};
	halfPoints[numPoints++] = {halfCoords[1],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[2],GRAY};
	halfPoints[numPoints++] = {halfCoords[1],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[2],GRAY};
	halfPoints[numPoints++] = {halfCoords[3],DARKGRAY};
	//Shaft
	halfPoints[numPoints++] = {halfCoords[8],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[9],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[10],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[10],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[11],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[8],DARKGRAY};
	//Handle
	halfPoints[numPoints++] = {halfCoords[4],mixColors(DARKGRAY,BROWN)};
	halfPoints[numPoints++] = {halfCoords[5],mixColors(DARKGRAY,BROWN)};
	halfPoints[numPoints++] = {halfCoords[6],BROWN};
	halfPoints[numPoints++] = {halfCoords[6],BROWN};
	halfPoints[numPoints++] = {halfCoords[7],BROWN};
	halfPoints[numPoints++] = {halfCoords[4],BROWN};
	//Folds
	halfPoints[numPoints++] = {halfCoords[12],WHITE};
	halfPoints[numPoints++] = {halfCoords[13],LIGHTGRAY};
	halfPoints[numPoints++] = {halfCoords[14],WHITE};
	halfPoints[numPoints++] = {halfCoords[14],RED};
	halfPoints[numPoints++] = {halfCoords[15],mixColors(RED,BLACK)};
	halfPoints[numPoints++] = {halfCoords[16],RED};
	halfPoints[numPoints++] = {halfCoords[16],WHITE};
	halfPoints[numPoints++] = {halfCoords[17],LIGHTGRAY};
	halfPoints[numPoints++] = {halfCoords[18],WHITE};
	halfPoints[numPoints++] = {halfCoords[18],RED};
	halfPoints[numPoints++] = {halfCoords[19],mixColors(RED,BLACK)};
	halfPoints[numPoints++] = {halfCoords[20],RED};

	numPoints = 0;
	//Background
	openPoints[numPoints++] = {openCoords[0],GRAY};
	openPoints[numPoints++] = {openCoords[1],DARKGRAY};
	openPoints[numPoints++] = {openCoords[2],GRAY};
	openPoints[numPoints++] = {openCoords[1],DARKGRAY};
	openPoints[numPoints++] = {openCoords[2],GRAY};
	openPoints[numPoints++] = {openCoords[3],DARKGRAY};
	//Shaft
	openPoints[numPoints++] = {openCoords[8],DARKGRAY};
	openPoints[numPoints++] = {openCoords[9],DARKGRAY};
	openPoints[numPoints++] = {openCoords[10],DARKGRAY};
	openPoints[numPoints++] = {openCoords[10],DARKGRAY};
	openPoints[numPoints++] = {openCoords[11],DARKGRAY};
	openPoints[numPoints++] = {openCoords[8],DARKGRAY};
	//Handle
	openPoints[numPoints++] = {openCoords[4],mixColors(DARKGRAY,BROWN)};
	openPoints[numPoints++] = {openCoords[5],mixColors(DARKGRAY,BROWN)};
	openPoints[numPoints++] = {openCoords[6],BROWN};
	openPoints[numPoints++] = {openCoords[6],BROWN};
	openPoints[numPoints++] = {openCoords[7],BROWN};
	openPoints[numPoints++] = {openCoords[4],BROWN};
	//Folds
	openPoints[numPoints++] = {openCoords[12],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[13],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[14],WHITE};
	openPoints[numPoints++] = {openCoords[14],RED};
	openPoints[numPoints++] = {openCoords[15],mixColors(RED,BLACK)};
	openPoints[numPoints++] = {openCoords[16],RED};
	openPoints[numPoints++] = {openCoords[16],WHITE};
	openPoints[numPoints++] = {openCoords[17],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[18],WHITE};
	openPoints[numPoints++] = {openCoords[18],RED};
	openPoints[numPoints++] = {openCoords[19],mixColors(RED,BLACK)};
	openPoints[numPoints++] = {openCoords[20],mixColors(RED,GRAY)};
	
	/*struct Triangle
	{
		point *p1,*p2,*p3;
	};
	struct
	{
		struct
		{
			Triangle t1 = {&points[0],&points[1],&points[2]};
			Triangle t2 = {&points[3],&points[4],&points[5]};
		} background;
		struct
		{
			Triangle t1 = {&points[6],&points[7],&points[8]};
			Triangle t2 = {&points[9],&points[10],&points[11]};
		} shaft;
		struct
		{
			Triangle t1 = {&points[12],&points[13],&points[14]};
			Triangle t2 = {&points[15,]&points[16],&points[17]};
		} handle;
		struct
		{
			Triangle f1 = {&points[18],&points[19],&points[20]};
			Triangle f2 = {&points[21],&points[22],&points[23]};
			Triangle f3 = {&points[24],&points[25],&points[26]};
			Triangle f4 = {&points[27],&points[28],&points[29]};
			Triangle f5 = {&points[30],&points[31],&points[32]};
		} folds;
	} umbrella;*/

	cout << "Scene point arrays constructed" << endl;
	
	vector<GLfloat*> targets;
	vector<pair<double,double>> openEndpoints1,openEndpoints2;
	for(int i=0;i<numPoints;i++)
	{
		targets.push_back(&points[i].pos.x);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.x,halfPoints[i].pos.x));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.x,openPoints[i].pos.x));
		targets.push_back(&points[i].pos.y);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.y,halfPoints[i].pos.y));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.y,openPoints[i].pos.y));
		targets.push_back(&points[i].pos.z);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.z,halfPoints[i].pos.z));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.z,openPoints[i].pos.z));
		targets.push_back(&points[i].pos.w);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.w,halfPoints[i].pos.w));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.w,openPoints[i].pos.w));
		targets.push_back(&points[i].col.x);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.x,halfPoints[i].col.x));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.x,openPoints[i].col.x));
		targets.push_back(&points[i].col.y);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.y,halfPoints[i].col.y));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.y,openPoints[i].col.y));
		targets.push_back(&points[i].col.z);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.z,halfPoints[i].col.z));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.z,openPoints[i].col.z));
		targets.push_back(&points[i].col.w);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.w,halfPoints[i].col.w));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.w,openPoints[i].col.w));
	}
	//Add the umbrellaEdge variable to the targets so it will be moved by the tweeners as well.
	targets.push_back(&umbrellaHeight);
	openEndpoints1.push_back(pair<double,double>(closedPoints[12].pos.y,halfPoints[12].pos.y));	//Pick a point on the lower edge of the umbrella whose y value to track
	openEndpoints2.push_back(pair<double,double>(halfPoints[12].pos.y,openPoints[12].pos.y));
	cout << "About to construct Tweeners" << endl;
	openTweener1 = Tweener<GLfloat>(targets,initLinearSet(openEndpoints1));
	openTweener2 = Tweener<GLfloat>(targets,initLinearSet(openEndpoints2));
	cout << "Tweeners constructed" << endl;
	cout << "openTweener1 isFoward() = " << openTweener1.isForward() << endl;
	openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES);

	cout << "Tweener started" << endl;
	/*for(int i=0;i<numPoints;i++)
		points[i] = {closedCoords[i],BLACK};
	*/
	
//Set up the windowing context:

  //Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

  //Window Size in pixels
	glutInitWindowSize(800,600);

  //Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 

	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
  //glutCreateWindow("First OpenGL Program!");

//Pass our display function to the context
	glutDisplayFunc(display);

//Pass our keyboard function to the context
	glutKeyboardFunc(keyboard);

	//Pass in the idle function
	glutIdleFunc(idle);

	//Pass in the reshape function
	glutReshapeFunc(reshape);

//Call our initialization function
	init();  //called only once, at the beginning

//Entger the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT);
	//cout << sizeof(points) << " " << sizeof(points[0]) << endl;
	//tweener.advance();
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  	glDrawArrays(GL_TRIANGLES, 0, numPoints);
	glFlush(); 
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers( 1, &buffer );
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  
	// Load shaders and use the resulting shader program
	GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
	glUseProgram( program );
  
	// Initialize the vertex position attribute from the vertex shader
	GLuint pos_loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(pos_loc);
	glVertexAttribPointer(pos_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(0));
	
	GLuint color_loc = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(color_loc);
	glVertexAttribPointer(color_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(points[0].pos)));
  
  /* set clear color to white */
	glClearColor (1.0, 1.0, 1.0, 0.0);  
}

void idle()
{
	static int i=0;
	if(i++==10000)
	{
		if(openTweener1.isRunning())
		{
			openTweener1.advance();
			if(!openTweener1.isRunning())
			{
				if(openTweener1.isForward())
					openTweener2.start(Tweener<GLfloat>::FORWARD,STEPFRAMES);
				else
				{
					if(pauseSet)
						paused = true;
					else openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES/2);
				}
			}
		}
		else if(openTweener2.isRunning())
		{
			openTweener2.advance();
			if(!openTweener2.isRunning())
			{
				if(openTweener2.isForward())
					openTweener2.start(Tweener<GLfloat>::BACKWARD,STEPFRAMES);
				else
					openTweener1.start(Tweener<GLfloat>::BACKWARD,STEPFRAMES/2);
			}
		}
		else if(paused&&!pauseSet)
		{
			openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES/2);
			paused = false;
		}
		glutPostRedisplay();
		i=0;
	}
};

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 033:   //ESC key
			exit( EXIT_SUCCESS );
			break;
		case 's':
			pauseSet = true;
			break;
		case 'g':
			pauseSet = false;
			break;
	}
}

void reshape(int width, int height)
{
	currentWidth = width;
	currentHeight = height;
	setAutoVP();
}
void setAutoVP()
{
	static const double frameRatio = (double)WIN_HEIGHT/(double)WIN_WIDTH;
	double winRatio = (double)currentHeight/(double)currentWidth;
	if(frameRatio<winRatio)
	{
		glViewport(0,(double(currentHeight-(frameRatio*currentWidth)))/((double)(2)),currentWidth,frameRatio*currentWidth);
	}
	else
	{
		glViewport((double)(currentWidth-((double)(currentHeight)/frameRatio))/2,0,(double)(currentHeight)/frameRatio,currentHeight);
	}
}

vec2 pix2dev(int x, int y)
{
	vec2 result;
	result.x = (((float)(2.0*x))/(WIN_WIDTH))-1.0;
	result.y = ((2.0*((float)y))/(WIN_HEIGHT))-1.0;
	return result;
}

vec4 pix2dev4(int x, int y)
{
	vec2 dev = pix2dev(x,y);
	return vec4(dev.x,dev.y,0.0,1.0);
}
