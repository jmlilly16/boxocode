#include "ParametricSet.h"
#include <typeinfo>

ParametricSet::~ParametricSet()
{
	for(ParametricEqn* eqn: equations)
		delete eqn;
}
void ParametricSet::addEquation(ParametricEqn* eqn)
{
	equations.push_back(eqn);
}
vector<double> ParametricSet::calc(double t)
{
	cout << "Calcing a parametric set" << endl;
	//cout << "ZING!" << endl;
	vector<double> result;
	for(ParametricEqn* eqn: equations)
	{
		//cout << "BING!" << endl;
		cout << "Calculating function at location " << eqn << endl;
		cout << " with size " << sizeof(*eqn) << endl;
		cout << " and type " << typeid(*eqn).name() << endl;
		cout << "With value " << eqn->fn(t) << endl;
		result.push_back(eqn->fn(t));
	}
	return result;
}

const int ParametricSet::size() const
{
	return equations.size();
}

void ParametricSet::echoEquations()
{
	for(ParametricEqn* eqn: equations)
		cout << eqn << " " << eqn->fn(0.5) << endl;
}

ParametricSet initLinearSet(vector<pair<double,double>> list)
{
	//cout << "Initting a linear parametric set" << endl;
	//cout << "LinearEqn class defined" << endl;
	ParametricSet result;
	//cout << "Base Parametric Set instanciated" << endl;
	for(auto& i: list)
	{
		ParametricEqn* eqn = new LinearEqn(i.first,i.second);
		//cout << "Formed a linear equation at " << eqn << endl;
		//cout << "Test calculation: fn(0.5) = " << eqn->fn(0.5) << endl;
		//cout << "eqn points at something " << sizeof(*eqn) << " bytes long" << endl;
		result.addEquation(eqn);
	}
	cout << "Calcing a linear set from within the constructor" << endl;
	result.calc(0.5);
	//result.echoEquations();
	//cout << "Equations added, returning" << endl;
	return result;
}
