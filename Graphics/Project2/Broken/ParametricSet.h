#ifndef PARAMSET
#define PARAMSET

#include <vector>
#include <utility>
#include <iostream>

using namespace std;

class ParametricEqn
{
	public:
	virtual double fn(double t)=0;
};

class ParametricSet
{
	private:
	vector<ParametricEqn*> equations;
	public:
	ParametricSet(){}
	~ParametricSet();
	void addEquation(ParametricEqn* eqn);
	vector<double> calc(double t);
	const int size() const;
	void echoEquations();
};

class LinearEqn : public ParametricEqn
{
	private:
	double start,diff;
	public:
	LinearEqn(double start, double end)
		:start(start)
	{diff = end-start;/*cout << "Constructed a linear equation" << endl;*/}
	double fn(double t)
	{
		//cout << "Calculating fn" << endl;
		return start+t*diff;
	}
};

ParametricSet initLinearSet(vector<pair<double,double>>);

#endif
