#ifndef TWEENER
#define TWEENER

#include <vector>
#include <utility>
#include <iostream>
using namespace std;

#include "ParametricSet.h"

template <class T>
class Tweener
{
	public:	
	enum Direction {FORWARD,BACKWARD};
	private:
	ParametricSet funcSet;
	vector<T*> targets;
	int duration, frameNum;
	bool running;
	Direction direction;
	int numTargets;
	void setTargets(double t);
	public:
	Tweener(vector<T*> targets, ParametricSet funcSet);
	Tweener();
	~Tweener();
	void start(Direction direction, int duration);
	bool stop();
	bool advance();
	const Direction getDirection() const;
	const bool isForward() const;
	const bool isRunning() const;
};

template<class T>
Tweener<T>::Tweener(vector<T*> targets, ParametricSet funcSet)
	:targets(targets), funcSet(funcSet), duration(0), frameNum(0), running(false), direction(FORWARD)
{
	//cout << "Constructing a Tweener!" << endl;
	cout << "Constructing a tweener" << endl;
	numTargets = min(int(targets.size()),funcSet.size());
	cout << "Number of targets = " << numTargets << endl;
	cout << "Calcing a linear set from within the tweener constructor" << endl;
	//funcSet.echoEquations();
	funcSet.calc(0.5);
}

template<class T>
Tweener<T>::Tweener()
{
	//cout << "Constructing an empty Tweener!" << endl;
}

template<class T>
Tweener<T>::~Tweener()
{}

template<class T>
void Tweener<T>::start(Direction dir, int durationFrames)
{
	cout << "Tweener starting with direction " << (dir==FORWARD?"FORWARD":"BACKWARD") << " and duration " << durationFrames << endl;
	cout << "PING!" << endl;
	direction = dir;
	duration = durationFrames;
	running = true;
	cout << "Entering switch" << endl;
	switch(direction)
	{
		case FORWARD: setTargets(0.0); frameNum = 0;break;
		case BACKWARD: setTargets(1.0); frameNum = duration;break;
		default: break;
	}
	cout << "Tweener started" << endl;
}
template<class T>
bool Tweener<T>::stop()
{
	cout << "Tweener Stopped" << endl;
	if(!running) return false;
	running = false;
	return true;
}
template<class T>
bool Tweener<T>::advance()
{
	if(!running) {/*cout << "Tweener has stopped, return false" << endl;*/return false;}
	switch(direction)
	{
		case FORWARD:
			frameNum++;
			break;
		case BACKWARD:
			frameNum--;
			break;
		default: break;
	}
	//cout << "Tweener: frame " << frameNum << endl;
	double frac = (double)frameNum/(double)duration;
	setTargets(frac);
	if(direction==FORWARD&&frameNum==duration || direction==BACKWARD&&frameNum==0){running = false;/*cout << "Max frames reached, stopping" << endl;*/}
	return true;
}
template<class T>
void Tweener<T>::setTargets(double t)
{
	cout << "Setting targets" << endl;
	cout << "DING!" << endl;
	vector<double> vals = funcSet.calc(t);
	cout << "Got target values" << endl;
	for(int i=0;i<numTargets;i++)
		*(targets[i]) = vals[i];
}
template<class T>
const typename Tweener<T>::Direction Tweener<T>::getDirection() const
{
	return direction;
}
template<class T>
const bool Tweener<T>::isForward() const
{
	return direction==FORWARD;
}
template<class T>
const bool Tweener<T>::isRunning() const
{
	return running;
}
#endif
