#ifndef COLORS
#define COLORS

#include "Angel.h"

typedef vec4 Color;

const Color RED(1.0,0.0,0.0,1.0);
const Color GREEN(0.0,1.0,0.0,1.0);
const Color BLUE(0.0,0.0,1.0,1.0);
const Color WHITE(1.0,1.0,1.0,1.0);
const Color BLACK(0.0,0.0,0.0,1.0);
const Color CLEAR(0.0,0.0,0.0,0.0);
const Color DARKGRAY(0.25,0.25,0.25,1.0);
const Color BROWN(0.25,0.125,0.0,1.0);
const Color LIGHTBLUE(0.5,0.5,1.0,1.0);
const Color LIGHTGRAY(0.75,0.75,0.75,1.0);
const Color GRAY(0.5,0.5,0.5,1.0);

const Color mixColors(Color c1, Color c2)
{
	Color result((c1.x+c2.x)/2,(c1.y+c2.y)/2,(c1.z+c2.z)/2,(c1.w+c2.w)/2);
	return result;
}

#endif
