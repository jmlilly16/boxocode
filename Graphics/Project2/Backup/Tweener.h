//FILE: Tweener.h
//AUTHOR: Mason Lilly
//DATE LAST MODIFIED: 2/20/15

//This file defines the Tweener template.
//A Tweener is an object that advances evenly across values of t for a set of Parametric Equations
//Its duration and direction can be set, and it controls its target values directly through pointers.
//The type specified by the template is the type of value the Tweener will control.

#ifndef TWEENER
#define TWEENER

#include <vector>
#include <utility>
#include <iostream>

#include "ParametricSet.h"

template <class T>
class Tweener
{
	public:
	enum Direction {FORWARD,BACKWARD}; 	//What direction is the tweener moving in?
	private:
	LinearSet linearSet;			//The equations used (TODO: Make it work for ALL ParametricSets)
	vector<T*> targets;			//Pointers to the values to be controlled.
	int duration, frameNum;			//How long to go, the current frame
	bool running;				//Whether the tweener is going
	Direction direction;			//What direction the tweener is going in
	int numTargets;				//How many targets exist and match with an equation
	
	/*
	This function does the job of evaluating the parametric set for a given t and setting the controlled values to the results.
	Precondition: targets contains numTargets valid pointers-to-T and linearSet contains numTargets valid LinearEqns. 0.0<t<1.0.
	Postcondition: the values referenced in targets have been set to the results of the evaluation of the equation set. 
	*/
	void setTargets(double t);
	public:
	/*This constructor takes in a vector of pointers-to-T and a set of start-end pairs to initialize the LinearSet.
	Order matters - The nth entry in targets will be controlled by the nth entry in endpoints*/
	Tweener(vector<T*> targets, vector<pair<double,double>> endpoints);
	Tweener();
	~Tweener();
	/*Starts the tweener.
	This function sets the Tweener's running flag to true and initializes its duration, frameNum, and direction fields appropriately
	It also sets its targets to their start or end values, depending on the selected direction.
	Duration represents the total number of positions the tweener will assign over the course of its run;
	calling this function places the tweener into the first of these positions.
	The positions will be evenly spread between 0.0 and 1.0, inclusive
	*/
	void start(Direction direction, int duration);
	/*
	Attempts to stop the tweener by setting its running field to false.
	Returns false if the tweener was already stopped; true otherwise.
	*/
	bool stop();
	/*
	Advances the tweener by one frame.
	frameNum increases/decreases by 1 depending on direction
	A value of t is calculated and the equation set is evaluated on it.
	The targets are then set to the results.
	If calling this function again would place frameNum outside the range [0,duration], running gets set to false.
	Calls to advance while the tweener is not running result in a return value of false.
	All other cases return true.
	*/
	bool advance();
	/*
	Returns the current value of direction
	*/
	const Direction getDirection() const;
	/*
	Returns whether the current value of direction is FORWARD
	*/
	const bool isForward() const;
	/*
	Returns whether the tweener is running
	*/
	const bool isRunning() const;
};

template<class T>
Tweener<T>::Tweener(vector<T*> targets, vector<pair<double,double>> endpoints)
	:targets(targets), linearSet(endpoints), duration(0), frameNum(0), running(false), direction(FORWARD)
{
	//cout << "Constructing a Tweener!" << endl;
	numTargets = min(targets.size(),endpoints.size());
}

template<class T>
Tweener<T>::Tweener()
	:linearSet(vector<pair<double,double>>())
{
	//cout << "Constructing an empty Tweener!" << endl;
}

template<class T>
Tweener<T>::~Tweener()
{}

template<class T>
void Tweener<T>::start(Direction dir, int durationFrames)
{
	//cout << "Tweener started with direction " << (dir==FORWARD?"FORWARD":"BACKWARD") << " and duration " << durationFrames << endl;
	direction = dir;
	duration = durationFrames;
	running = true;
	switch(direction)
	{
		case FORWARD: setTargets(0.0); frameNum = 0;break;
		case BACKWARD: setTargets(1.0); frameNum = duration;break;
		default: break;
	}
}
template<class T>
bool Tweener<T>::stop()
{
	cout << "Tweener Stopped" << endl;
	if(!running) return false;
	running = false;
	return true;
}
template<class T>
bool Tweener<T>::advance()
{
	if(!running) {/*cout << "Tweener has stopped, return false" << endl;*/return false;}
	switch(direction)
	{
		case FORWARD:
			frameNum++;
			break;
		case BACKWARD:
			frameNum--;
			break;
		default: break;
	}
	//cout << "Tweener: frame " << frameNum << endl;
	double frac = (double)frameNum/(double)duration;
	setTargets(frac);
	if(direction==FORWARD&&frameNum==duration || direction==BACKWARD&&frameNum==0){running = false;/*cout << "Max frames reached, stopping" << endl;*/}
	return true;
}
template<class T>
void Tweener<T>::setTargets(double t)
{
	vector<double> vals = linearSet.calc(t);
	for(int i=0;i<numTargets;i++)
		*(targets[i]) = vals[i];
}
template<class T>
const typename Tweener<T>::Direction Tweener<T>::getDirection() const
{
	return direction;
}
template<class T>
const bool Tweener<T>::isForward() const
{
	return direction==FORWARD;
}
template<class T>
const bool Tweener<T>::isRunning() const
{
	return running;
}
#endif
