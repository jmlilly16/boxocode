//FILE: morph.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  2/20/15
//  This program draws an umbrella and animates it. The sky darkens and the umbrella opens and closes
//  
//
//  Keyboard:
//		ESC: close
//		s  : stop animation after its current cycle
//		g  : restart animation
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
#include "Colors.h"
#include "Tweener.h"
#include "ParametricSet.h"
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
#include <fstream>
#include <string>
using namespace std;

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 600;
const int MAXPOINTS = 256;
const int STEPFRAMES = 500; //'Frames' to do each step of the animation over
//NOTE: Time is not calculated in "draw" cycles; see implemetation of idle();
const string INFILENAME1 = "closed.dat", INFILENAME2 = "half.dat", INFILENAME3 = "open.dat";

Tweener<GLfloat> openTweener1,openTweener2; //Tweeners for each step of the animation
bool pauseSet=false,paused=false;
int currentWidth=WIN_WIDTH;
int currentHeight=WIN_HEIGHT;

void init();
void display();
void keyboard( unsigned char key, int x, int y );
//void mouse(int button, int state, int x, int y);
void idle();
void reshape(int width, int height);
void setAutoVP();

typedef struct point
{
	vec4 pos;
	vec4 col;
} point;

point points[MAXPOINTS];
int numCoords=0,numPoints=0;

int main(int argc, char** argv) {
	//***Initial Setup***//
	ifstream inFile1(INFILENAME1.data()),
		 inFile2(INFILENAME2.data()),
		 inFile3(INFILENAME3.data());
	int numOpen=0,numHalf=0,numClosed=0;
	
	vec4 openCoords[MAXPOINTS];
	vec4 halfCoords[MAXPOINTS];
	vec4 closedCoords[MAXPOINTS];
	
	//***Read in the point coordinates from the files***//
	string line;
	while(getline(inFile1,line).good())
		closedCoords[numClosed++] = 
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	while(getline(inFile2,line).good())
		halfCoords[numHalf++] =
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	while(getline(inFile3,line).good())
		openCoords[numOpen++] =
			vec4(atof(line.substr(0,line.find(" ")).data()),atof(line.substr(line.find(" ")+1).data()),0.0,1.0);
	inFile1.close();
	inFile2.close();
	inFile3.close();
	
	numCoords = min(numOpen,(min(numHalf,numClosed)));
	if(numCoords<21)
	{
		cout << "Missing some points in the .dat files" << endl;
		return 1;
	}

	//***Make one point array for each of the three positions***//
	//-Combine coordinates from the read-in arrays with hard-coded colors to create the designs-// 
	numPoints = 0;
	
	//First, set up the points array - the one that actually gets drawn - to the default start scene
	//Background
	points[numPoints++] = {closedCoords[0],LIGHTGRAY};
	points[numPoints++] = {closedCoords[1],LIGHTBLUE};
	points[numPoints++] = {closedCoords[2],LIGHTGRAY};
	points[numPoints++] = {closedCoords[1],LIGHTBLUE};
	points[numPoints++] = {closedCoords[2],LIGHTGRAY};
	points[numPoints++] = {closedCoords[3],LIGHTBLUE};
	//Shaft
	points[numPoints++] = {closedCoords[8],DARKGRAY};
	points[numPoints++] = {closedCoords[9],DARKGRAY};
	points[numPoints++] = {closedCoords[10],DARKGRAY};
	points[numPoints++] = {closedCoords[10],DARKGRAY};
	points[numPoints++] = {closedCoords[11],DARKGRAY};
	points[numPoints++] = {closedCoords[8],DARKGRAY};
	//Handle
	points[numPoints++] = {closedCoords[4],mixColors(DARKGRAY,BROWN)};
	points[numPoints++] = {closedCoords[5],mixColors(DARKGRAY,BROWN)};
	points[numPoints++] = {closedCoords[6],BROWN};
	points[numPoints++] = {closedCoords[6],BROWN};
	points[numPoints++] = {closedCoords[7],BROWN};
	points[numPoints++] = {closedCoords[4],BROWN};
	//Folds
	points[numPoints++] = {closedCoords[12],WHITE};
	points[numPoints++] = {closedCoords[13],LIGHTGRAY};
	points[numPoints++] = {closedCoords[14],WHITE};
	points[numPoints++] = {closedCoords[14],RED};
	points[numPoints++] = {closedCoords[15],mixColors(RED,BLACK)};
	points[numPoints++] = {closedCoords[16],RED};
	points[numPoints++] = {closedCoords[16],WHITE};
	points[numPoints++] = {closedCoords[17],LIGHTGRAY};
	points[numPoints++] = {closedCoords[18],WHITE};
	points[numPoints++] = {closedCoords[18],RED};
	points[numPoints++] = {closedCoords[19],mixColors(RED,BLACK)};
	points[numPoints++] = {closedCoords[20],RED};
	
	//Now make the three designs.
	point closedPoints[numPoints],halfPoints[numPoints],openPoints[numPoints];
	//First, copy what was just set up in the point array into the "closed" scene
	for(int i=0;i<numPoints;i++)
		closedPoints[i] = points[i];
	
	//Next, make the half-open scene - the sky darkens and the umbrella extends
	numPoints = 0;
	//Background
	halfPoints[numPoints++] = {halfCoords[0],GRAY};
	halfPoints[numPoints++] = {halfCoords[1],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[2],GRAY};
	halfPoints[numPoints++] = {halfCoords[1],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[2],GRAY};
	halfPoints[numPoints++] = {halfCoords[3],DARKGRAY};
	//Shaft
	halfPoints[numPoints++] = {halfCoords[8],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[9],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[10],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[10],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[11],DARKGRAY};
	halfPoints[numPoints++] = {halfCoords[8],DARKGRAY};
	//Handle
	halfPoints[numPoints++] = {halfCoords[4],mixColors(DARKGRAY,BROWN)};
	halfPoints[numPoints++] = {halfCoords[5],mixColors(DARKGRAY,BROWN)};
	halfPoints[numPoints++] = {halfCoords[6],BROWN};
	halfPoints[numPoints++] = {halfCoords[6],BROWN};
	halfPoints[numPoints++] = {halfCoords[7],BROWN};
	halfPoints[numPoints++] = {halfCoords[4],BROWN};
	//Folds
	halfPoints[numPoints++] = {halfCoords[12],WHITE};
	halfPoints[numPoints++] = {halfCoords[13],LIGHTGRAY};
	halfPoints[numPoints++] = {halfCoords[14],WHITE};
	halfPoints[numPoints++] = {halfCoords[14],RED};
	halfPoints[numPoints++] = {halfCoords[15],mixColors(RED,BLACK)};
	halfPoints[numPoints++] = {halfCoords[16],RED};
	halfPoints[numPoints++] = {halfCoords[16],WHITE};
	halfPoints[numPoints++] = {halfCoords[17],LIGHTGRAY};
	halfPoints[numPoints++] = {halfCoords[18],WHITE};
	halfPoints[numPoints++] = {halfCoords[18],RED};
	halfPoints[numPoints++] = {halfCoords[19],mixColors(RED,BLACK)};
	halfPoints[numPoints++] = {halfCoords[20],RED};

	//Finally, make the 'open' scene. The folds of the umbrella come apart and the edges darken slightly
	numPoints = 0;
	//Background
	openPoints[numPoints++] = {openCoords[0],GRAY};
	openPoints[numPoints++] = {openCoords[1],DARKGRAY};
	openPoints[numPoints++] = {openCoords[2],GRAY};
	openPoints[numPoints++] = {openCoords[1],DARKGRAY};
	openPoints[numPoints++] = {openCoords[2],GRAY};
	openPoints[numPoints++] = {openCoords[3],DARKGRAY};
	//Shaft
	openPoints[numPoints++] = {openCoords[8],DARKGRAY};
	openPoints[numPoints++] = {openCoords[9],DARKGRAY};
	openPoints[numPoints++] = {openCoords[10],DARKGRAY};
	openPoints[numPoints++] = {openCoords[10],DARKGRAY};
	openPoints[numPoints++] = {openCoords[11],DARKGRAY};
	openPoints[numPoints++] = {openCoords[8],DARKGRAY};
	//Handle
	openPoints[numPoints++] = {openCoords[4],mixColors(DARKGRAY,BROWN)};
	openPoints[numPoints++] = {openCoords[5],mixColors(DARKGRAY,BROWN)};
	openPoints[numPoints++] = {openCoords[6],BROWN};
	openPoints[numPoints++] = {openCoords[6],BROWN};
	openPoints[numPoints++] = {openCoords[7],BROWN};
	openPoints[numPoints++] = {openCoords[4],BROWN};
	//Folds
	openPoints[numPoints++] = {openCoords[12],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[13],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[14],WHITE};
	openPoints[numPoints++] = {openCoords[14],RED};
	openPoints[numPoints++] = {openCoords[15],mixColors(RED,BLACK)};
	openPoints[numPoints++] = {openCoords[16],RED};
	openPoints[numPoints++] = {openCoords[16],WHITE};
	openPoints[numPoints++] = {openCoords[17],LIGHTGRAY};
	openPoints[numPoints++] = {openCoords[18],WHITE};
	openPoints[numPoints++] = {openCoords[18],RED};
	openPoints[numPoints++] = {openCoords[19],mixColors(RED,BLACK)};
	openPoints[numPoints++] = {openCoords[20],mixColors(RED,GRAY)};


	//***Set up the tweeners, which will facilitate the morphing***//
	//targets contains the addresses of all the points in the point array to be controlled
	vector<GLfloat*> targets;
	//the Endpoints vectors contain the start and end values for the variables to be controlled
	vector<pair<double,double>> openEndpoints1,openEndpoints2;
	for(int i=0;i<numPoints;i++)
	{
		targets.push_back(&points[i].pos.x);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.x,halfPoints[i].pos.x));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.x,openPoints[i].pos.x));
		targets.push_back(&points[i].pos.y);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.y,halfPoints[i].pos.y));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.y,openPoints[i].pos.y));
		targets.push_back(&points[i].pos.z);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.z,halfPoints[i].pos.z));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.z,openPoints[i].pos.z));
		targets.push_back(&points[i].pos.w);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].pos.w,halfPoints[i].pos.w));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].pos.w,openPoints[i].pos.w));
		targets.push_back(&points[i].col.x);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.x,halfPoints[i].col.x));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.x,openPoints[i].col.x));
		targets.push_back(&points[i].col.y);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.y,halfPoints[i].col.y));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.y,openPoints[i].col.y));
		targets.push_back(&points[i].col.z);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.z,halfPoints[i].col.z));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.z,openPoints[i].col.z));
		targets.push_back(&points[i].col.w);
		openEndpoints1.push_back(pair<double,double>(closedPoints[i].col.w,halfPoints[i].col.w));
		openEndpoints2.push_back(pair<double,double>(halfPoints[i].col.w,openPoints[i].col.w));
	}
	//Instanciate the tweeners
	openTweener1 = Tweener<GLfloat>(targets,openEndpoints1);
	openTweener2 = Tweener<GLfloat>(targets,openEndpoints2);
	openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES/2);
	
	//Set up the windowing context:

	//Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

	//Window Size in pixels
	glutInitWindowSize(800,600);

	//Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 

	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
	
	//Pass our display function to the context
	glutDisplayFunc(display);

	//Pass our keyboard function to the context
	glutKeyboardFunc(keyboard);

	//Pass in the idle function
	glutIdleFunc(idle);

	//Pass in the reshape function
	glutReshapeFunc(reshape);

	//Call our initialization function
	init();  //called only once, at the beginning

	//Entger the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT);
	//cout << sizeof(points) << " " << sizeof(points[0]) << endl;
	//tweener.advance();
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  	glDrawArrays(GL_TRIANGLES, 0, numPoints);
	glFlush(); 
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers( 1, &buffer );
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  
	// Load shaders and use the resulting shader program
	GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
	glUseProgram( program );
  
	// Initialize the vertex position and color attributes from the vertex shader
	GLuint pos_loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(pos_loc);
	glVertexAttribPointer(pos_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(0));
	
	GLuint color_loc = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(color_loc);
	glVertexAttribPointer(color_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(points[0].pos)));

	/* set clear color to white */
	glClearColor (1.0, 1.0, 1.0, 0.0);  
}

//Idle callback function for glut
//This function controls the animation aspect of the program
//Every 10000 times it gets called, it tries to advance whatever tweener is currently active (1 frame)
//It will detect when the active tweener reaches the end of its run and will activate the next one accordingly
//It also catches and handles pauses send in from the keyboard
void idle()
{
	static int i=0;
	if(i++==10000)
	{
		if(openTweener1.isRunning())
		{
			openTweener1.advance();
			if(!openTweener1.isRunning())
			{
				if(openTweener1.isForward())
					openTweener2.start(Tweener<GLfloat>::FORWARD,STEPFRAMES);
				else
				{
					if(pauseSet)
						paused = true;
					else openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES/2);
				}
			}
		}
		else if(openTweener2.isRunning())
		{
			openTweener2.advance();
			if(!openTweener2.isRunning())
			{
				if(openTweener2.isForward())
					openTweener2.start(Tweener<GLfloat>::BACKWARD,STEPFRAMES);
				else
					openTweener1.start(Tweener<GLfloat>::BACKWARD,STEPFRAMES/2);
			}
		}
		else if(paused&&!pauseSet)
		{
			openTweener1.start(Tweener<GLfloat>::FORWARD,STEPFRAMES/2);
			paused = false;
		}
		glutPostRedisplay();
		i=0;
	}
};

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 033:   //ESC key
			exit( EXIT_SUCCESS );
			break;
		case 's':
			pauseSet = true;
			break;
		case 'g':
			pauseSet = false;
			break;
	}
}

//reshape callback function for glut
//calls setAutoVP to avoid distortion of the aspect ratio when the window changes.
void reshape(int width, int height)
{
	currentWidth = width;
	currentHeight = height;
	setAutoVP();
}

//Compares the aspect ratios of the animation and the current window and sets a viewport accordingly to maintain the former.
//Postcondition: A viewport has been set that maintains the aspect ratio of the animation
void setAutoVP()
{
	static const double frameRatio = (double)WIN_HEIGHT/(double)WIN_WIDTH;
	double winRatio = (double)currentHeight/(double)currentWidth;
	if(frameRatio<winRatio)
	{
		glViewport(0,(double(currentHeight-(frameRatio*currentWidth)))/((double)(2)),currentWidth,frameRatio*currentWidth);
	}
	else
	{
		glViewport((double)(currentWidth-((double)(currentHeight)/frameRatio))/2,0,(double)(currentHeight)/frameRatio,currentHeight);
	}
}
