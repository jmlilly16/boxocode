#include "Tweener.h"

template<class T>
Tweener<T>::Tweener(vector<T*> targets, vector<pair<double,double>> endpoints)
	:targets(targets), linearSet(endpoints), duration(0), frameNum(0), running(false), direction(FORWARD)
{
	numTargets = min(targets.size(),endpoints.size());
}

template<class T>
Tweener<T>::~Tweener()
{}

template<class T>
void Tweener<T>::start(Direction dir, int durationFrames)
{
	direction = dir;
	duration = durationFrames;
	running = true;
	switch(direction)
	{
		case FORWARD: setTargets(0.0); frameNum = 0;break;
		case BACKWARD: setTargets(1.0); frameNum = duration;break;
		default: break;
	}
}
template<class T>
bool Tweener<T>::stop()
{
	if(!running) return false;
	running = false;
	return true;
}
template<class T>
bool Tweener<T>::advance()
{
	if(!running) return false;
	switch(direction)
	{
		case FORWARD:
			frameNum++;
			break;
		case BACKWARD:
			frameNum--;
			break;
		default: break;
	}
	double frac = (double)frameNum/(double)duration;
	setTargets(frac);
	if(direction==FORWARD&&frameNum==duration || direction==BACKWARD&&frameNum==0) running = false;
	return true;
}
template<class T>
void Tweener<T>::setTargets(double t)
{
	vector<double> vals = linearSet.calc(t);
	for(int i=0;i<numTargets;i++)
		*(targets[i]) = vals[i];
}
