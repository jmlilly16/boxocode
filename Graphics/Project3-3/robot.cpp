//FILE: robot.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  3/19/15
//  This program draws a robot with a trombone
//  
//
//  Keyboard:
//		<your keys here>
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
#include <stack>

#include "Colors.h"

using namespace std;

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 600;
const int MAX_POINTS = 256;

struct point
{
	vec4 pos;
	vec4 color;
	point(vec4 p, vec4 c)
		:pos(p),color(c)
	{}
	point(){}
} points[MAX_POINTS];

GLuint viewMatLoc,projMatLoc;
mat4 viewMat,projMat;
stack<mat4> matStack;
int pointCount=0;
vec4 currentColor;
GLdouble zoom=2.0,twist=0.0,elevation=0.0,azimuth=0.0;
bool maxPointError=false;
bool filled = true;

void init();		
void display();		
void keyboard( unsigned char key, int x, int y );
//void mouse(int button, int state, int x, int y);
//void idle();
void reshape(int width, int height);
void drawCube();
void setViewMat();
mat4 polarView(GLdouble distance, GLdouble twist, GLdouble elevation, GLdouble azimuth);
mat4 pushMatrix(const mat4&);

struct Robot
{
	struct Arm
	{
		GLdouble shoulderRotX,shoulderRotY,shoulderRotZ;
		GLdouble elbowAngle;
		GLdouble handAngle;
	};
	struct Leg
	{
		GLdouble hipRotX,hipRotY,hipRotZ;
		GLdouble kneeAngle;
		GLdouble footAngle;
	};
	struct Torso
	{
		void drawChest()
		{
			pushMatrix(Scale(0.15,0.22,0.10));
			currentColor = GRAY;
			drawCube();
			matStack.pop();
		}
		void drawShoulders()
		{
			pushMatrix(Scale(0.25,0.14,0.12));
			currentColor = LIGHTGRAY;
			drawCube();
			matStack.pop();
		}
		void draw()
		{
			//pushMatrix(Translate(0.0,1.0,0.0));
			drawChest();
			pushMatrix(Translate(0.0,1.4,0.0));
			drawShoulders();
			matStack.pop();
			matStack.pop();
		}
	} torso;
	Robot()
	{
		torso.draw();
		/*currentColor = GRAY;
		pushMatrix(Translate(0.0,1.0,0.0));
		pushMatrix(Scale(0.75,0.75,0.75));
		pushMatrix(Scale(0.15,0.22,0.10));
		drawCube();
		pushMatrix(Translate(0.0,0.22,0.0));
		pushMatrix(Scale(2.0,0.75,1.1));
		currentColor = LIGHTGRAY;
		drawCube();
		matStack.pop();
		matStack.pop();
		matStack.pop();
		matStack.pop();
		matStack.pop();
		pushMatrix(Scale(0.01,0.01,0.01));
		drawCube();
		matStack.pop();*/
	}
};

int main(int argc, char** argv)
{
	
	//Set up the windowing context:

	//Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB | GLUT_DEPTH);

	//Window Size in pixels
	glutInitWindowSize(800,800);

	//Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 

	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
	//glutCreateWindow("First OpenGL Program!");


	//Pass our display function to the context
	glutDisplayFunc(display);

	//Pass our keyboard function to the context
	glutKeyboardFunc(keyboard);

	glutReshapeFunc(reshape);

	//Call our initialization function
	init();  //called only once, at the beginning

//Entger the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
	//cout << points[0].pos.x << " " << points[0].pos.y << " " << points[0].pos.z << " " << points[0].pos.w << " " << endl;
  	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
	setViewMat();
	glUniformMatrix4fv(viewMatLoc, 1, GL_TRUE, viewMat);
	glUniformMatrix4fv(projMatLoc, 1, GL_TRUE, projMat);
	glEnable(GL_DEPTH_TEST);
  	glDrawArrays(GL_TRIANGLES, 0, pointCount);
	glFlush();
	//cout << "." << endl;
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers( 1, &buffer );
	glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );

	// Load shaders and use the resulting shader program
	GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
	glUseProgram(program);

	// Initialize the vertex position and color attributes from the vertex shader
	GLuint ploc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(ploc);
	glVertexAttribPointer(ploc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(0));
	GLuint cloc = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(cloc);
	glVertexAttribPointer(cloc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(points[0].pos)));

	viewMatLoc = glGetUniformLocation(program,"viewMat");
	projMatLoc = glGetUniformLocation(program,"projMat");

	setViewMat();
	projMat = Ortho(-1.0,1.0,-1.0,1.0,-10.0,10.0);

	/* set clear color to white */
	glClearColor(0.75, 0.75, 1.0, 0.0);
	matStack.push(mat4());
	Robot robot;
}

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 'q':
			twist+=1.0;
			break;
		case 'w':
			elevation+=1.0;
			break;
		case 'e':
			twist-=1.0;
			break;
		case 'a':
			azimuth-=1.0;
			break;
		case 's':
			zoom=1.0;
			azimuth=0.0;
			twist=0.0;
			elevation=0.0;
			break;
		case 'd':
			azimuth+=1.0;
			break;
		case 'z':
			zoom-=0.1;
			break;
		case 'x':
			elevation-=1.0;
			break;
		case 'c':
			zoom+=0.1;
			break;
		case 'r':
			if(filled)
				glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			filled=!filled;
			break;
		case 033:   //ESC key
			exit(EXIT_SUCCESS);
			break;
	}
	glutPostRedisplay();
}

void drawCube()
{
	if(pointCount+36>MAX_POINTS)
	{
		if(!maxPointError)
			cout << "Error: Maximum points reached" << endl;
		return;
	}
	//cout << currentColor.x << " " << currentColor.y << " " << currentColor.z << " " << currentColor.w << endl;
	mat4 currentModel = matStack.top();
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,1.0,1.0),currentColor);//Front
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,1.0,1.0),currentColor);//Top
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,1.0,1.0),currentColor);//Right
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,1.0,1.0),currentColor);//Bottom
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,-1.0,1.0),currentColor);//Left
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,1.0,-1.0,1.0),currentColor);//Back
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(1.0,-1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,1.0,-1.0,1.0),currentColor);
	points[pointCount++] = point(currentModel * vec4(-1.0,-1.0,-1.0,1.0),currentColor);
	glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
	glutPostRedisplay();
}

void reshape(int width, int height)
{
	int x = min(width,height);
	glViewport(0,0,x,x);
}

void setViewMat()
{
	viewMat = polarView(zoom,twist,elevation,azimuth);
}

mat4 polarView(GLdouble distance, GLdouble twist, GLdouble elevation, GLdouble azimuth)
{
	return Translate(0.0,0.0,-distance) * RotateZ(twist) * RotateX(elevation) * RotateY(-azimuth);
}

mat4 pushMatrix(const mat4& m)
{
	matStack.push(m*matStack.top());
	return matStack.top();
}
