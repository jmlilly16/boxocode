attribute vec4 vColor, vPosition;
varying vec4 color;
uniform mat4 viewMat,projMat;

void main()
{
	color = vColor;
	gl_Position = projMat * viewMat * vPosition;
}
