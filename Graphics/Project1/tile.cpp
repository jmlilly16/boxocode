//FILE: tile.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  2/11/15
//	This program draws a tesselating arrow pattern a number of times specified in the command line (eg tile 6)
//	The pattern's colors are set in such a way that the colors fade from the upper left corner to the lower right corner 
//	The "UP" arrows fade from white to black, and the "DOWN" arrows fade from black to white.
//	A few alternative colors are defined, which can be switched around in the drawTessArrow function.
//	
//  Keyboard:
//		ESC: exit
//		'+': Increase tessellation count
//		'-': Decrease tessellation count
//
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
#include <cmath>
using namespace std;

//Stores the position and location of a point using 2 4-dimensional vectors
struct point
{
	vec4 pos;
	vec4 color;
	point(vec4 v = vec4(0.0)) : pos(v), color(v) {}
	point(vec4 pos, vec4 color) : pos(pos), color(color) {}
};

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;			//Size of the window
const int WIN_HEIGHT = 800;
const vec4 WHITE = vec4(1.0,1.0,1.0,1.0);	//Some preset colors
const vec4 BLACK = vec4(0.0,0.0,0.0,1.0);
const vec4 DARKRED = vec4(0.5,0.0,0.0,1.0);
const vec4 LIGHTBLUE = vec4(0.5,0.5,1.0,1.0);
const vec4 DARKBLUE = vec4(0.0,0.0,0.5,1.0);
const int POINTCOUNT = 15;
int tessCount = 0;

//The point locations don't change, so I store them here.
const vec4 coords[] =
{
	vec4(-1.0,1.0,0.0,1.0),
	vec4(-1.0,-1.0,0.0,1.0),
	vec4(1.0,1.0,0.0,1.0),
	vec4(-1.0,-1.0,0.0,1.0),
	vec4(1.0,1.0,0.0,1.0),
	vec4(1.0,-1.0,0.0,1.0),
	vec4(-0.5,1.0,0.0,1.0),
	vec4(-0.5,0.0,0.0,1.0),
	vec4(0.5,1.0,0.0,1.0),
	vec4(-0.5,0.0,0.0,1.0),
	vec4(0.5,1.0,0.0,1.0),
	vec4(0.5,0.0,0.0,1.0),
	vec4(-1.0,0.0,0.0,1.0),
	vec4(1.0,0.0,0.0,1.0),
	vec4(0.0,-1.0,0.0,1.0)
};
//The actual point objects need to be recalculated each time, so they go in a separate array
point points[POINTCOUNT];

void init();		
void display();		
void keyboard(unsigned char key, int x, int y);

/*Sets a viewport to draw in - constricts device coordinates to a specified portion of the screen
Preconditions:
<left>, <right>, <bottom>, and <top> are the respective edges of the desired viewport, given in fractions between 0.0 and 1.0.
Postconditions:
glViewport has been called with the proper values to set a viewport of the specified dimensions
*/
void setVP(float left, float right, float bottom, float top);

/*Calculates and draws the points in the coords array. A color value is calculated for each point, using the following functions, to create a gradient effect
Preconditions:
The current viewport has been set to a valid region of the screen and the data buffer has been set up appropriately
Postcondition:
The tesselation pattern has been drawn in the current viewport, with colors that match the gradient
*/
void drawTessArrow();

//Performs a simple calculation to convert a coordinate pair of pixels into normalized device coordinates
vec2 pix2dev(int x, int y);

//"Mixes" two 4D vectors by averaging each value, weighted by the value <frac>
//ex: frac=0.0 would cause result.x to equal v1.x
//ex: frac=0.5 would cause result.x to equal 0.5v1.x+0.5v2.x
vec4 mixVecs(vec4 v1, vec4 v2, GLfloat frac);

//Just a wrapper for calling pix2dev and converting the result to a vec4
vec4 pix2dev4(int x, int y);

//Calculates the "color fraction" of a position.
//The gradient runs along the line y=-x, with the value at the upper left corner equal to 0 and at the lower right equal to 1.
//The lower left and upper right have values of 0.5, as does the point 0,0.
//In practice, the value is calulated by averaging the fraction "down" of the pos's y value an the fraction 'right' of pos's x value.
//Precondition: vec is between (-1.0,-1.0,-1.0,-1.0) and (1.0,1.0,1.0,1.0)
GLfloat getColorFrac(vec4 pos);

GLfloat getColorFrac(vec4 pos)
{
	//cout << "Calculating color frac for "<<pos.x<<","<<pos.y<<","<<pos.z<<","<<pos.w<<endl;
	GLfloat result = (((pos.x+1.0)/2.0)+(1.0-((pos.y+1.0)/2.0)))/2.0;
	//cout << "Color frac: "<<result<<endl;
	return result;
}

vec4 pix2dev4(int x, int y)
{
	vec2 dev = pix2dev(x,y);
	return vec4(dev.x,dev.y,0.0,1.0);
}

vec4 mixVecs(vec4 v1, vec4 v2, GLfloat frac)
{
	vec4 result = vec4(	(1-frac)*v1.x+frac*v2.x,
				(1-frac)*v1.y+frac*v2.y,
				(1-frac)*v1.z+frac*v2.z,
				(1-frac)*v1.w+frac*v2.w);
	return result;
}

void setVP(float left, float right, float bottom, float top)
{
	int LLx = left*WIN_WIDTH;
	int LLy = bottom*WIN_HEIGHT;
	int URx = right*WIN_WIDTH;
	int URy = top*WIN_HEIGHT;
	int w = URx-LLx;
	int h = URy-LLy;
	//cout << "Viewport: "<<LLx<<", "<<LLy<<", "<<w<<", "<<h<<"."<<endl;
	glViewport(LLx,LLy,w,h);
}

vec2 pix2dev(int x, int y)
{
	vec2 result;
	result.x = (((float)(2.0*x))/(WIN_WIDTH))-1.0;
	result.y = ((2.0*((float)y))/(WIN_HEIGHT))-1.0;
	return result;
}

void drawTessArrow()
{
	//Get the current viewport setting - it's needed to calculate the colors for this particular arrow
	GLint VP[4];
	glGetIntegerv(GL_VIEWPORT,VP);
	//cout << "Viewport: "<<VP[0]<<" "<<VP[1]<<" "<<VP[2]<<" "<<VP[3]<<endl;
	
	//Calculate the coordinates of the upper-left and lower-right corners
	vec4 UL = pix2dev4(VP[0],VP[1]+VP[3]);
	vec4 LR = pix2dev4(VP[0]+VP[2],VP[1]);
	//cout <<"UL:"<<UL.x<<" "<<UL.y<<" "<<endl;
	//cout <<"LR:"<<LR.x<<" "<<LR.y<<" "<<endl; 

	//Calculate the colors of the upper and lower corners, for each set of arrows
	//Up Arrow
	vec4 ULcol1 = mixVecs(WHITE,BLACK,getColorFrac(UL));
	vec4 LRcol1 = mixVecs(WHITE,BLACK,getColorFrac(LR));
	//Down Arrow
	vec4 ULcol2 = mixVecs(BLACK,WHITE,getColorFrac(UL));
	vec4 LRcol2 = mixVecs(BLACK,WHITE,getColorFrac(LR));

	/*cout << "ULCOL1: "<<ULcol1.x<<","<<ULcol1.y<<","<<ULcol1.z<<","<<ULcol1.w<<endl;
	cout << "LRCOL1: "<<LRcol1.x<<","<<LRcol1.y<<","<<LRcol1.z<<","<<LRcol1.w<<endl;
	cout << "ULCOL2: "<<ULcol2.x<<","<<ULcol2.y<<","<<ULcol2.z<<","<<ULcol2.w<<endl;
	cout << "LRCOL2: "<<LRcol2.x<<","<<LRcol2.y<<","<<LRcol2.z<<","<<LRcol2.w<<endl;*/
	
	/*vec4 ULcol1 = WHITE;
	vec4 LRcol1 = BLACK;
	vec4 ULcol2 = BLACK;
	vec4 LRcol2 = WHITE;*/
	
	//For each entry in the coords array, calculate the color for that location; store it in the points array.
	//U-L background
	points[0] = point(coords[0],mixVecs(ULcol1,LRcol1,getColorFrac(coords[0])));
	points[1] = point(coords[1],mixVecs(ULcol1,LRcol1,getColorFrac(coords[1])));
	points[2] = point(coords[2],mixVecs(ULcol1,LRcol1,getColorFrac(coords[2])));
	//L-R background
	points[3] = points[1];
	points[4] = points[2];
	points[5] = point(coords[5],mixVecs(ULcol1,LRcol1,getColorFrac(coords[5])));
	//U-L Stock
	points[6] = point(coords[6],mixVecs(ULcol2,LRcol2,getColorFrac(coords[6])));
	points[7] = point(coords[7],mixVecs(ULcol2,LRcol2,getColorFrac(coords[7])));
	points[8] = point(coords[8],mixVecs(ULcol2,LRcol2,getColorFrac(coords[8])));
	//L-R Stock
	points[9] = points[7];
	points[10] = points[8];
	points[11] = point(coords[11],mixVecs(ULcol2,LRcol2,getColorFrac(coords[11])));
	//HEAD
	points[12] = point(coords[12],mixVecs(ULcol2,LRcol2,getColorFrac(coords[12])));
	points[13] = point(coords[13],mixVecs(ULcol2,LRcol2,getColorFrac(coords[13])));
	points[14] = point(coords[14],mixVecs(ULcol2,LRcol2,getColorFrac(coords[14])));
	
	//Put everything in the data buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
}

int main(int argc, char** argv)
{
	if(argc<2)
	{
		cout << "Syntax: "<<argv[0]<<" <starting tessellation number>"<<endl;
		return 1;
	}
	tessCount = atoi(argv[1]);
	//cout << "Start Tesscount = " << tessCount << ", calculated from "<<argv[1]<<endl;
	//Set up the windowing context:

	//Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	
	//Window Size in pixels
	glutInitWindowSize(WIN_WIDTH,WIN_HEIGHT);
	
	//Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 
	
	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
	//glutCreateWindow("First OpenGL Program!");
	
	//Pass our display function to the context
	glutDisplayFunc(display);
	
	//Pass our keyboard function to the context
	glutKeyboardFunc( keyboard );
	
	//Call our initialization function
	init();  //called only once, at the beginning
	
	//Enter the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT);
	float VPSize = 1.0/tessCount;
	//cout << "tessCount="<<tessCount<<endl;
	//cout << "VPSize="<<VPSize<<endl;
	if(tessCount>50)
		glEnable(GL_LINE_SMOOTH);
	for(int i=0;i<tessCount;i++)
	for(int j=0;j<tessCount;j++)
	{	
		setVP(0.0+i*VPSize,0.0+(i+1)*VPSize,0.0+j*VPSize,0.0+(j+1)*VPSize);
		drawTessArrow();
  		glDrawArrays(GL_TRIANGLES, 0, POINTCOUNT);
	}
	glFlush(); 
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
	
	// Load shaders and use the resulting shader program
	//Use Gouraud shaders for gradient effect
	GLuint program = InitShader( "vshader_gouraud.glsl", "fshader_gouraud.glsl" );
	glUseProgram(program);
	
	// Initialize the vertex position attribute from the vertex shader
	GLuint pos_loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(pos_loc);
	glVertexAttribPointer(pos_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(0));
	
	GLuint color_loc = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(color_loc);
	glVertexAttribPointer(color_loc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(points[0].pos)));
  
  /* set clear color to white */
	glClearColor (1.0, 1.0, 1.0, 0.0);  
}

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 033:   //ESC key
			exit( EXIT_SUCCESS );
			break;
		case '+':
			tessCount++;
			glutPostRedisplay();
			break;
		case '-':
			if(tessCount>1) tessCount--;
			glutPostRedisplay();
			break;
	}
}

