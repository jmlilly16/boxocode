#ifndef COLORS
#define COLORS

#include "Angel.h"

const vec4 RED(1.0,0.0,0.0,1.0);
const vec4 GREEN(0.0,1.0,0.0,1.0);
const vec4 BLUE(1.0,0.0,0.0,1.0);
const vec4 WHITE(1.0,1.0,1.0,1.0);
const vec4 BLACK(0.0,0.0,0.0,1.0);
const vec4 CLEAR(0.0,0.0,0.0,0.0);
const vec4 DARKGRAY(0.25,0.25,0.25,1.0);
const vec4 BROWN(0.25,0.125,0.0,1.0);

#endif
