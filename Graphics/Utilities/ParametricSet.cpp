//FILE: ParametricSet.cpp
//AUTHOR: Mason Lilly
//DATE LAST MODIFIED: 2/20/15
//This file implements the ParametricSet and LinearSet classes outlined in ParametricSet.h


#include "ParametricSet.h"

void ParametricSet::addEquation(ParametricEqn* eqn)
{
	equations.push_back(eqn);
}
vector<double> ParametricSet::calc(double t)
{
	vector<double> result;
	for(ParametricEqn* eqn: equations)
		result.push_back(eqn->fn(t));
	return result;
}

/*This constructor accepts a set of start-end values for linear functions.
It then defines the LinearEqn subclass of ParamtricEqn, which implements a linear function between two endpoints
Finally, it fills its equation set with one instance of LinearEqn for each pair of values given.
*/
LinearSet::LinearSet(vector<pair<double,double>> list)
{
	class LinearEqn : public ParametricEqn
	{
		private:
		double start,diff;
		public:
		LinearEqn(double start, double end)
			:start(start)
		{diff = end-start;}
		double fn(double t)
		{
			return start+t*diff;
		}
	};
	/*for(int i=0;i<count;i++)
		addEquation(new LinearEqn(list[i].first,list[i].second));*/
	for(auto& i: list)
		addEquation(new LinearEqn(i.first,i.second));
}
LinearSet::~LinearSet()
{
	while(equations.size()>0)
	{
		delete equations.back();
		equations.pop_back();
	}
}
