//FILE: ParametricSet.h
//Author: Mason Lilly
//DATE LAST MODIFIED: 2/20/15
//This file describes the ParametricSet class, its helper class ParametricEqn, and its subclass LinearSet

#ifndef PARAMSET
#define PARAMSET

#include <vector>
#include <utility>

using namespace std;

/*
A ParametricEqn is just an object to hold a function, one representing a parametric equation - double-in, double-out.
Since the function is pure-virtual, this object must be subclassed to implement the function before it can be used.
Its intended use is to be grouped in a ParametricSet object for parametric equation operations
It could also be used as a poor-man's lambda function for doubles
*/
class ParametricEqn
{
	public:
	//Virtual function that must be defined in a subclass
	virtual double fn(double t)=0;
};

/*
A ParametricSet is an object containing a collection of ParametricEqns
Once constructed, the object can be given new equations to hold via its addEquation function.
At any time, it can be passed a value t to be passed to every function it currently holds.
*/
class ParametricSet
{
	protected:
	vector<ParametricEqn*> equations;
	public:
	ParametricSet(){}
	~ParametricSet(){}
	/*
	Function to add a ParametricEqn to the object's list. Since ParametricEqns must be subclassed, they have to be stored as pointers.
	Precondition: eqn represents an instance of a subclass of ParametricEqn
	Postcondition: the equation has been added to the set and will be evaluated the next time calc() is called.
	*/
	void addEquation(ParametricEqn* eqn);
	/*
	Evaluates t against all stored equations
	Postcondition: A vector containing the results, in order, of successive calls to each ParametricEqn in the set
	*/
	vector<double> calc(double t);
};

/*
LinearSet is a subclass of ParametricSet, intended to deal specifically with linear equations
*/
class LinearSet : public ParametricSet
{
	public:
	//This constructor takes in a set of start-endpoint pairs and fills the equation set with linear equations between each pair of values.
	LinearSet(vector<pair<double,double>>);
	~LinearSet();
};

#endif
