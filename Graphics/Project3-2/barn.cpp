//TODO: Fill this out
//FILE: barn.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  3/8/15
//  This program displays a barn from different angles.
//  
//
//  Keyboard:
//		ESC: quit
//		f: show the front of the barn
//		b: show the back of the barn
//		t: show the top
//		u: show the bottom
//		l: show the left side
//		r: show the right side
//		c: show the upper-right-front corner
//		d: show the lower-left-back corner
//		w: toggle between wireframe and polygon mode
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
using namespace std;

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 600;
const int NUM_POINTS = 48;

const vec4 frontVantage(0.0,0.0,1.0,1.0);
const vec4 backVantage(0.0,0.0,-1.0,1.0);
const vec4 leftVantage(-1.0,0.0,0.0,1.0);
const vec4 rightVantage(1.0,0.0,0.0,1.0);
const vec4 topVantage(0.0,1.0,0.0,1.0);
const vec4 bottomVantage(0.0,-1.0,0.0,1.0);
const vec4 origin(0.0,0.0,0.0,1.0);
const vec4 UP(0.0,1.0,0.0,0.0);
const vec4 BACK(0.0,0.0,-1.0,0.0);

bool filled = true;
GLuint viewMatLoc,projMatLoc;
mat4 viewMat,projMat;

struct point
{
	vec4 pos;
	vec4 color;
	point(){};
	point(vec4 p, vec4 c)
		:pos(p),color(c)
	{}
} verts[NUM_POINTS] =
{
	//Front Wall
	point(vec4(-0.5,-0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(-0.5,0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(0.5,-0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	//Roof Front
	point(vec4(-0.5,0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(0.0,0.75,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,1.0,0.0,1.0)),
	//Left Wall
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	//Back Wall
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	//Roof Back
	point(vec4(-0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.0,0.75,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	//Right Wall
	point(vec4(0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,-0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	point(vec4(0.5,-0.5,-1.0,1.0),vec4(1.0,0.0,0.0,1.0)),
	//Bottom
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	point(vec4(0.5,-0.5,-1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	point(vec4(0.5,-0.5,1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	point(vec4(0.5,-0.5,1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	point(vec4(-0.5,-0.5,-1.0,1.0),vec4(0.0,1.0,0.0,1.0)),
	//Roof Left
	point(vec4(-0.5,0.5,-1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,-1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(-0.5,0.5,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(-0.5,0.5,-1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	//Roof Right
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,-1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.0,0.75,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.5,0.5,1.0,1.0),vec4(1.0,1.0,1.0,1.0)),
	point(vec4(0.5,0.5,-1.0,1.0),vec4(1.0,1.0,1.0,1.0))
};

//This is sloppy - I had to use an individual toggle for each shape because I couldn't figure out in a hurry how to get around the clipping issues I was having.
bool shapesVisible[9] =
{
	true,	//Front
	true,	//RoofFront
	false,	//Left
	false,	//Back
	false,	//RoofBack
	false,	//Right
	false,	//Bottom
	false,	//RoofLeft
	false	//RoofRight
};


void init();		
void display();		
void keyboard(unsigned char key, int x, int y);
void drawShape(int shapeNum, int index, int len);
//void mouse(int button, int state, int x, int y);
//void idle();

int main(int argc, char** argv) {
	
//Set up the windowing context:

  //Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);

  //Window Size in pixels
	glutInitWindowSize(800,600);

  //Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 

	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
  //glutCreateWindow("First OpenGL Program!");

//Pass our display function to the context
	glutDisplayFunc(display);

//Pass our keyboard function to the context
	glutKeyboardFunc(keyboard);

//Call our initialization function
	init();  //called only once, at the beginning

//Entger the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT);

	glUniformMatrix4fv(viewMatLoc, 1, GL_TRUE, viewMat);
	glUniformMatrix4fv(projMatLoc, 1, GL_TRUE, projMat);

	drawShape(0,0,6);
	drawShape(1,6,3);
	drawShape(2,9,6);
	drawShape(3,15,6);
	drawShape(4,21,3);
	drawShape(5,24,6);
	drawShape(6,30,6);
	drawShape(7,36,6);
	drawShape(8,42,6);

	glFlush(); 
}

void drawShape(int shapeNum, int index, int len)
{
	if(filled)
	{
		if(shapesVisible[shapeNum])
			glDrawArrays(GL_TRIANGLES,index,len);
	}
	else
	{
		glLineWidth(3);
		glDrawArrays(GL_LINE_LOOP,index,len);
	}
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(verts), verts, GL_STATIC_DRAW);
  
	// Load shaders and use the resulting shader program
	GLuint program = InitShader("vshader_gouraud.glsl", "fshader_gouraud.glsl");
	glUseProgram(program);
  
	// Initialize the vertex position attribute from the vertex shader
	GLuint vLoc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(vLoc);
	glVertexAttribPointer(vLoc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(0));
	
	GLuint cLoc = glGetAttribLocation(program, "vColor");
	glEnableVertexAttribArray(cLoc);
	glVertexAttribPointer(cLoc, 4, GL_FLOAT, GL_FALSE, sizeof(point), BUFFER_OFFSET(sizeof(verts[0].pos)));

	viewMatLoc = glGetUniformLocation(program, "viewMat");
	projMatLoc = glGetUniformLocation(program, "projMat");

	viewMat = LookAt(vec4(0.0,0.0,1.0,1.0),vec4(0.0,0.0,0.0,1.0),vec4(0.0,1.0,0.0,0.0));
	projMat = Ortho(-1.0,1.0,-1.0,1.0,-10.0,10.0);
  
  /* set clear color to white */
	glClearColor (0.75, 0.75, 0.75, 1.0);
}

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 033:   //ESC key
			exit( EXIT_SUCCESS );
			break;
		case 'w':
			filled = !filled;
			break;
		case 'f':
			viewMat = LookAt(frontVantage,origin,UP);
			shapesVisible[0] = true;
			shapesVisible[1] = true;
			shapesVisible[2] = false;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = false;
			shapesVisible[6] = false;
			shapesVisible[7] = false;
			shapesVisible[8] = false;
			break;
		case 'b':
			viewMat = LookAt(backVantage,origin,UP);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = false;
			shapesVisible[3] = true;
			shapesVisible[4] = true;
			shapesVisible[5] = false;
			shapesVisible[6] = false;
			shapesVisible[7] = false;
			shapesVisible[8] = false;
			break;
		case 't':
			viewMat = LookAt(topVantage,origin,BACK);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = false;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = false;
			shapesVisible[6] = false;
			shapesVisible[7] = true;
			shapesVisible[8] = true;
			break;
		case 'u':
			viewMat = LookAt(bottomVantage,origin,BACK);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = false;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = false;
			shapesVisible[6] = true;
			shapesVisible[7] = false;
			shapesVisible[8] = false;
			break;
		case 'l':
			viewMat = LookAt(leftVantage,origin,UP);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = true;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = false;
			shapesVisible[6] = false;
			shapesVisible[7] = true;
			shapesVisible[8] = false;
			break;
		case 'r':
			viewMat = LookAt(rightVantage,origin,UP);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = false;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = true;
			shapesVisible[6] = false;
			shapesVisible[7] = false;
			shapesVisible[8] = true;
			break;
		case 'c':
			viewMat = LookAt(vec4(1.0,1.0,1.0,1.0),origin,UP);
			shapesVisible[0] = true;
			shapesVisible[1] = true;
			shapesVisible[2] = false;
			shapesVisible[3] = false;
			shapesVisible[4] = false;
			shapesVisible[5] = true;
			shapesVisible[6] = false;
			shapesVisible[7] = true;
			shapesVisible[8] = true;
			break;
		case 'd':
			viewMat = LookAt(vec4(-1.0,-1.0,-1.0,1.0),origin,UP);
			shapesVisible[0] = false;
			shapesVisible[1] = false;
			shapesVisible[2] = true;
			shapesVisible[3] = true;
			shapesVisible[4] = true;
			shapesVisible[5] = false;
			shapesVisible[6] = true;
			shapesVisible[7] = false;
			shapesVisible[8] = false;
			break;
	}
	glutPostRedisplay();
}

