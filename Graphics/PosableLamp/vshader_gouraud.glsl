attribute vec4 vPosition;
uniform vec4 vColor;
uniform mat4 modelView,projection;
varying vec4 color;

void main()
{
	color = vColor;
	gl_Position = projection * modelView * vPosition;
}
