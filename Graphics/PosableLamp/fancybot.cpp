//TODO: Fill this out
//FILE: fancybot.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  4/13/15
//  This program draws the pixar lamp!
//  
//
//  Keyboard:
//		<your keys here>
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)
#include "Colors.h"

#include <iostream>
#include <stack>
#include <vector>
#include <map>
using namespace std;

class MatStack
{
	private:
	stack<mat4> data;
	public:
	MatStack()
	{
		data.push(mat4());
	}
	mat4 prePush(mat4 m)
	{
		data.push(m * data.top());
		return data.top();
	}
	mat4 postPush(mat4 m)
	{
		data.push(data.top() * m);
		return data.top();
	}
	mat4 pushMat(mat4 m)
	{
		data.push(m);
		return data.top();
	}
	mat4 popMat()
	{
		if(data.size()==1)
		{
			return mat4();
		}
		mat4 m = data.top();
		data.pop();
		return m;
	}
	mat4 topMat()
	{
		return data.top();
	}
} matStack;

struct point
{
	vec4 pos;
	vec4 color;
	point(vec4 p, vec4 c)
		:pos(p),color(c)
	{}
};

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 800;
const int MAX_CYLINDER_POINTS = 10000;
const int MAX_SPHERE_POINTS = 10000;
const double defaultElev = 0.0;
const double defaultAzimuth = 0.0;
const double defaultZoom = 3.0;
const double defaultTwist = 0.0;

//const vec4 ORIGIN(0,0,0,1);
//const vec4 EYE_START(0,0,1,1);

vec4 box[36] = 
{
	//front
	vec4(-1,-1,1,1),
	vec4(-1,1,1,1),
	vec4(1,1,1,1),
	vec4(1,1,1,1),
	vec4(1,-1,1,1),
	vec4(-1,-1,1,1),
	//top
	vec4(-1,1,1,1),
	vec4(-1,1,-1,1),
	vec4(1,1,-1,1),
	vec4(1,1,-1,1),
	vec4(1,1,1,1),
	vec4(-1,1,1,1),
	//right
	vec4(1,-1,1,1),
	vec4(1,1,1,1),
	vec4(1,1,-1,1),
	vec4(1,1,-1,1),
	vec4(1,-1,-1,1),
	vec4(1,-1,1,1),
	//bottom
	vec4(-1,-1,-1,1),
	vec4(-1,-1,1,1),
	vec4(1,-1,1,1),
	vec4(1,-1,1,1),
	vec4(1,-1,-1,1),
	vec4(-1,-1,-1,1),
	//left
	vec4(-1,-1,-1,1),
	vec4(-1,1,-1,1),
	vec4(-1,1,1,1),
	vec4(-1,1,1,1),
	vec4(-1,-1,1,1),
	vec4(-1,-1,-1,1),
	//back
	vec4(1,-1,-1,1),
	vec4(1,1,-1,1),
	vec4(-1,1,-1,1),
	vec4(-1,1,-1,1),
	vec4(-1,-1,-1,1),
	vec4(1,-1,-1,1)
};

int curCylCount;	//make sure this gets initialized
vec4 cylinder[MAX_CYLINDER_POINTS];

int curSphereCount;
vec4 sphere[MAX_SPHERE_POINTS];

vec4 lineSeg[2] = 
{
	vec4(0,0,0,1),
	vec4(0,1,0,1)
};

vec4 currentColor(0,0,0,1);
mat4 modelMat;
mat4 viewMat;
mat4 projMat;

double zoom = defaultZoom;
double elevation = defaultElev;
double azimuth = defaultAzimuth;
double twist = defaultTwist;
double near = 1.0;
double far = 20.0;
bool filled = true;

//Uniforms
GLuint colorLoc;
GLuint modelViewLoc;
GLuint projLoc;

void init();		
void display();		
void keyboard(unsigned char key, int x, int y);
//void mouse(int button, int state, int x, int y);
//void idle();
void renderCylinder(double,double,int);
void renderSphere(int);
void drawBox();
void drawLineSeg();
void drawScene();
void setViewMat();
mat4 polarView(GLdouble,GLdouble,GLdouble,GLdouble);
void updateModelViewMat();
void updateProjMat();
void setColor(vec4);
void pushTranslate(double x, double y, double z);
enum Axis {X,Y,Z};
void pushRotate(Axis,double);
void pushScale(double x, double y, double z);
void popMat();

class Prism
{
	private:
	vec4 color;
	public:
	Prism(vec4 c)
		:color(c)
	{}
	void draw()
	{
		setColor(color);
		glBufferData(GL_ARRAY_BUFFER,36*sizeof(vec4),box,GL_STATIC_DRAW);
		glDrawArrays(GL_TRIANGLES,0,36);
	};
};

class Cylinder
{
	private:
	const static int RES = 4;
	vec4 color;
	double bottom,top;
	public:
	Cylinder(vec4 c, double b, double t)
		: color(c), bottom(b), top(t)
	{}
	void draw()
	{
		setColor(color);
		renderCylinder(bottom,top,RES);
		glBufferData(GL_ARRAY_BUFFER,curCylCount*sizeof(vec4),cylinder,GL_STATIC_DRAW);
		glDrawArrays(GL_TRIANGLES,0,curCylCount);
	}
};

class Sphere
{
	private:
	const static int RES = 4;
	vec4 color;
	public:
	Sphere(vec4 c)
		: color(c)
	{}
	void draw()
	{
		setColor(color);
		glBufferData(GL_ARRAY_BUFFER,curSphereCount*sizeof(vec4),sphere,GL_STATIC_DRAW);
		glDrawArrays(GL_TRIANGLES,0,curSphereCount);
	}
};

struct Lamp
{
	struct Base
	{
		struct Foot : public Cylinder
		{
			Foot() : Cylinder(LIGHTGRAY,1.0,1.0)
			{}
			void draw();
		} foot;
		struct Hinge : public Cylinder
		{
			Hinge() : Cylinder(LIGHTGRAY,1.0,1.0)
			{}
			void draw();
		} hinge;
		void draw();
	} base;
	struct Arm
	{
		struct Sect
		{
			const static double BEAM_WIDTH = 0.04;
			struct Beam : public Cylinder
			{
				Beam() : Cylinder(GRAY,1.0,1.0)
				{}
				void draw();
			} beam1,beam2;
			void draw();
		} sect1, sect2;
		struct Hinge : public Cylinder
		{
			Hinge() : Cylinder(LIGHTGRAY,1.0,1.0)
			{}
			void draw();
		} hinge;
		double angle;
		Arm() : angle(0.0)
		{}
		void draw();
	} arm;
	struct Head
	{
		struct Shade : public Cylinder
		{
			Shade() : Cylinder(LIGHTGRAY,0.4,1.0)
			{}
			void draw();
		} shade;
		struct ShadeBase : public Cylinder
		{
			ShadeBase() : Cylinder(LIGHTGRAY,0.75,1.0)
			{}
			void draw();
		} shadebase;
		struct Hinge : public Cylinder
		{
			Hinge() : Cylinder(LIGHTGRAY,1.0,1.0)
			{}
			void draw();
		} hinge;
		struct Bulb : public Sphere
		{
			Bulb() : Sphere(WHITE)
			{}
		} bulb;
		double rot,lift;
		Head(double h,double l) : rot(h),lift(l)
		{}
		Head() : rot(0.0), lift(0.0)
		{}		
		void draw();
	} head;
	void draw();
	void stand();
} lamp,lamp2;

void Lamp::draw()
{
	pushScale(0.6,0.6,0.6);
	base.draw();
	popMat();
	double height = cos(DegreesToRadians*arm.angle);
	pushTranslate(0,height,-0.12);
	arm.draw();
	pushTranslate(0,height,0);
	pushScale(0.45,0.45,0.45);
	head.draw();
	popMat();
	popMat();
	popMat();
/*	pushTranslate(0.0,-1.0,0.0);
	pushScale(0.6,0.6,0.6);
	base.draw();
	popMat();
	popMat();
	pushTranslate(0.0,0.0,-0.12);
	arm.draw();
	popMat();
	pushTranslate(0.0,1.0,-0.12);
	pushScale(0.45,0.45,0.45);
	head.draw();
	popMat();
	popMat();*/
}

void Lamp::Head::draw()
{
	hinge.draw();
	pushRotate(Y,rot);
	pushRotate(X,-lift);
	pushTranslate(0,0.5,0.0);
	pushTranslate(0,0,0.25);
	shade.draw();
	popMat();
	shadebase.draw();
	popMat();
	pushTranslate(0.0,0.5,1);
	pushScale(0.3,0.3,0.3);
	bulb.draw();
	popMat();
	popMat();
	popMat();
	popMat();
}

void Lamp::Head::Hinge::draw()
{
	pushRotate(Y,90);
	pushScale(0.5,0.5,0.1);
	Cylinder::draw();
	popMat();
	popMat();
}

void Lamp::Head::ShadeBase::draw()
{
	pushScale(0.75,0.75,0.75);
	Cylinder::draw();
	popMat();
}

void Lamp::Head::Shade::draw()
{
	pushScale(1,1,0.75);
	Cylinder::draw();
	popMat();
}

void Lamp::Arm::draw()
{
	pushTranslate(0,0,-sin(DegreesToRadians*angle));
	hinge.draw();
	pushScale(0.5,0.5,0.5);
	pushRotate(X,-angle);
	pushTranslate(0,-1,0);
	sect1.draw();
	popMat();
	popMat();
	pushRotate(X,angle);
	pushTranslate(0,1,0);
	sect2.draw();
	popMat();
	popMat();
	popMat();
	popMat();
	/*double top = 1.0 - 2*sin(angle);
	pushScale(0.5,0.5,0.5);
	pushRotate(X,-angle);
	pushTranslate(0.0,-1.0,0.0);
	sect1.draw();
	popMat();
	popMat();
	hinge.draw();
	pushTranslate(0.0,1.0,0.0);
	pushRotate(X,angle);
	sect2.draw();
	popMat();
	popMat();
	popMat();*/
}

void Lamp::Arm::Hinge::draw()
{
	pushRotate(Y,90);
	pushScale(0.2,0.2,0.1);
	pushScale(0.6,0.6,0.6);
	Cylinder::draw();
	popMat();
	popMat();
	popMat();
}

void Lamp::Arm::Sect::draw()
{
	pushTranslate(0.0,0.0,3*BEAM_WIDTH);
	beam1.draw();
	popMat();
	pushTranslate(-0.0,0.0,-3*BEAM_WIDTH);
	beam2.draw();
	popMat();
}

void Lamp::Arm::Sect::Beam::draw()
{
	pushScale(BEAM_WIDTH,1.0,BEAM_WIDTH);
	pushRotate(X,-90);
	Cylinder::draw();
	popMat();
	popMat();
}

void Lamp::Base::draw()
{
	foot.draw();
	pushTranslate(0.0,0.1,-0.2);
	hinge.draw();
	popMat();
}

void Lamp::Base::Hinge::draw()
{
	pushScale(0.3,0.2,0.3);
	pushRotate(Y,90);
	Cylinder::draw();
	popMat();
	popMat();
}

void Lamp::Base::Foot::draw()
{
	pushScale(1.0,0.1,1.0);
	pushRotate(X,-90);
	Cylinder::draw();
	popMat();
	popMat();
}

void Lamp::stand()
{
	head.rot = 0.0;
	head.lift = 0.0;
	arm.angle = 0;
}

int main(int argc, char** argv)
{
	//Set up the windowing context:
	//Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGBA);
	
	//Window Size in pixels
	glutInitWindowSize(WIN_WIDTH,WIN_HEIGHT);
	
	//Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 
	
	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
	//glutCreateWindow("First OpenGL Program!");
	
	//Pass our display function to the context
	glutDisplayFunc(display);
	
	//Pass our keyboard function to the context
	glutKeyboardFunc( keyboard );
	
	//Call our initialization function
	init();  //called only once, at the beginning
	
	//Enter the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	updateModelViewMat();
	updateProjMat();
  	drawScene();
	glFlush(); 
}

void init() 
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glEnable(GL_DEPTH_TEST);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	// Load shaders and use the resulting shader program
	GLuint program = InitShader("vshader_gouraud.glsl", "fshader_gouraud.glsl");
	glUseProgram(program);
	
	// Initialize the vertex position attribute from the vertex shader
	GLuint loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

	renderCylinder(1.0,1.0,4);
	renderSphere(4);

	colorLoc = glGetUniformLocation(program,"vColor");
	modelViewLoc = glGetUniformLocation(program,"modelView");
	projLoc = glGetUniformLocation(program,"projection");

	modelMat = matStack.topMat();
	setViewMat();
	//projMat = Ortho(-1.0,1.0,-1.0,1.0,-10.0,10.0);

	setColor(BLACK);
	updateModelViewMat();
	updateProjMat();
	
	/* set clear color to white */
	glClearColor (0.0,0.0,0.0,1.0);
	glEnable(GL_DEPTH_TEST);
}

void keyboard( unsigned char key, int x, int y )
{
	switch (key)
	{
		case 'u':
			lamp.arm.angle += 5;
			break;
		case 'o':
			lamp.arm.angle -= 5;
			break;
		case 'j':
			lamp.head.rot += 5;
			break;
		case 'l':
			lamp.head.rot -= 5;
			break;
		case 'i':
			lamp.head.lift += 5;
			break;
		case ',':
			lamp.head.lift -= 5;
			break;
		case 'k':
			lamp.stand();
			break;
		case '+':
			near+=0.1;
			break;
		case '=':
			near-=0.1;
			break;
		case '_':
			far+=0.1;
			break;
		case '-':
			far-=0.1;
			break;
		case 'q':
			twist+=1.0;
			break;
		case 'w':
			elevation+=1.0;
			break;
		case 'e':
			twist-=1.0;
			break;
		case 'a':
			azimuth-=1.0;
			break;
		case 's':
			zoom=defaultZoom;
			azimuth=defaultAzimuth;
			twist=defaultTwist;
			elevation=defaultElev;
			break;
		case 'd':
			azimuth+=1.0;
			break;
		case 'z':
			zoom-=0.1;
			break;
		case 'x':
			elevation-=1.0;
			break;
		case 'c':
			zoom+=0.1;
			break;
		case 'r':
			if(filled)
				glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
			else
				glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
			filled=!filled;
			break;
		case 033:   //ESC key
			exit(EXIT_SUCCESS);
			break;
	}
	glutPostRedisplay();
}

vec4 unit(const vec4 &p)
{
  	vec4 c;
  	double d=0.0;
  	for(int i=0; i<3; i++) 
		d+=p[i]*p[i];
  	d=sqrt(d);
  	if(d > 0.0) 
		for(int i=0; i<3; i++) 
			c[i] = p[i]/d;
  	c[3] = 1.0;
  	return c;
}

void divideLine(vector<vec4>& circle, vec4 a, vec4 b, int resolution)
{
	//cout << "Dividing line between " << a << " and " << b << endl;
	if(resolution>0)
	{
		vec4 mid = unit(a+b);
		divideLine(circle,a,mid,resolution-1);
		divideLine(circle,mid,b,resolution-1);
	}
	else
	{
		//cout << "pushing: " << a << endl;
		circle.push_back(a);
	}
	//cout << endl;
}

void renderCylinder(double bottom, double top, int resolution)
{
	vector<vec4> baseShape;
	baseShape.push_back(vec4(0,1,0,1));
	baseShape.push_back(unit(RotateZ(120)*vec4(0,1,0,1)));
	baseShape.push_back(unit(RotateZ(240)*vec4(0,1,0,1)));
	static map<pair<double,double>,vector<vec4> > memoCircles;
	pair<double,double> index(bottom,top);
	vector<vec4> circle;
	if(memoCircles.count(index)==1)
		circle = memoCircles[index];
	else
	{
		//cout << "Generating new circle" << endl;
		for(int i=0;i<baseShape.size();i++)
		{
			//cout << "Main divide call: " << baseShape[i] << " and " << baseShape[(i+1)%baseShape.size()] << endl;
			divideLine(circle,baseShape[i],baseShape[(i+1)%baseShape.size()],resolution);
		}
		memoCircles[index] = circle;
	}
	vec4 base[circle.size()],cap[circle.size()];
	for(int i=0;i<circle.size();i++)
	{
		//cout << "Start:" << circle[i] << endl;
		base[i] = Scale(bottom,bottom,bottom) * circle[i] + vec4(0,0,-1,0);
		cap[i] = Scale(top,top,top) * circle[i] + vec4(0,0,1,0);
		//cout << "Base: " << base[i] << endl;
		//cout << "Top: " << cap[i] << endl;
	}
		
	int resultCount = 0;
	for(int i=0;i<circle.size();i++)
	{
		if(resultCount+6 > MAX_CYLINDER_POINTS)
		{
			cout << "ERROR: Max cylinder points reached (consider increasing the cap?)" << endl;
			exit(1);
		}
		cylinder[resultCount++] = base[i];
		cylinder[resultCount++] = cap[i];
		cylinder[resultCount++] = base[(i+1)%circle.size()];
		cylinder[resultCount++] = cap[i];
		cylinder[resultCount++] = base[(i+1)%circle.size()];
		cylinder[resultCount++] = cap[(i+1)%circle.size()];
	}
	for(int i=0;i<circle.size();i++)
	{
		cylinder[resultCount++] = vec4(0,0,1,1);
		cylinder[resultCount++] = cap[i];
		cylinder[resultCount++] = cap[(i+1)%circle.size()];
		cylinder[resultCount++] = vec4(0,0,-1,1);
		cylinder[resultCount++] = base[i];
		cylinder[resultCount++] = base[(i+1)%circle.size()];
	}
	curCylCount = resultCount;
}

void divide_triangle(vector<vec4>& data, vec4 a, vec4 b, vec4 c, int n)
{
 	vec4 v1, v2, v3;
 	if(n>0)
 	{
   		v1 = unit(a + b);
   		v2 = unit(a + c);
   		v3 = unit(b + c);   
   		divide_triangle(data,a ,v2, v1, n-1);
   		divide_triangle(data,c ,v3, v2, n-1);
   		divide_triangle(data,b ,v1, v3, n-1);
   		divide_triangle(data,v1 ,v2, v3, n-1);
 	}
 	else 
	{
		data.push_back(a);
		data.push_back(b);
		data.push_back(c);
	}
}

void renderSphere(int n)
{
	const static vec4 tetrahedron[4] = 
	{
		vec4(0,1,0,1),
		unit(RotateX(120) * vec4(0,1,0,1)),
		unit(RotateY(120) * RotateX(120) * vec4(0,1,0,1)),
		unit(RotateY(240) * RotateX(120) * vec4(0,1,0,1))
	};
	vector<vec4> data;
	for(int i=0;i<4;i++)
	{
		divide_triangle(data,tetrahedron[i],tetrahedron[(i+1)%4],tetrahedron[(i+2)%4],n);
	}
	curSphereCount = data.size();
	for(int i=0;i<data.size();i++)
		sphere[i] = data[i];
}
	

void drawBox()
{
	glBufferData(GL_ARRAY_BUFFER,36*sizeof(vec4),box,GL_STATIC_DRAW);
	glDrawArrays(GL_TRIANGLES,0,36);
}

void drawCylinder()
{
	//cout << curCylCount << endl;
	glBufferData(GL_ARRAY_BUFFER,curCylCount*sizeof(vec4),cylinder,GL_STATIC_DRAW);
	glDrawArrays(GL_TRIANGLES,0,curCylCount);
}

void drawLineSeg()
{
	glBufferData(GL_ARRAY_BUFFER,2*sizeof(vec4),lineSeg,GL_STATIC_DRAW);
	glDrawArrays(GL_LINES,0,2);
}

void drawScene()
{
	//pushScale(0.5,0.5,0.5);
	//drawBox();
	//popMat();
	pushTranslate(0,-1,0);
	lamp.draw();
	popMat();
}

void setViewMat()
{
	viewMat = polarView(zoom,twist,elevation,azimuth);
}

mat4 polarView(GLdouble distance, GLdouble twist, GLdouble elevation, GLdouble azimuth)
{
	return Translate(0.0,0.0,-distance) * RotateZ(twist) * RotateX(elevation) * RotateY(-azimuth);
}

void updateModelViewMat()
{
	setViewMat();
	glUniformMatrix4fv(modelViewLoc,1,GL_TRUE,viewMat*matStack.topMat());
}

void updateProjMat()
{
	projMat = Frustum(-1,1,-1,1,near,far);
	glUniformMatrix4fv(projLoc,1,GL_TRUE,projMat);
}

void setColor(vec4 color)
{
	glUniform4fv(colorLoc,1,color);
}

void pushTranslate(double x, double y, double z)
{
	matStack.postPush(Translate(x,y,z));
	updateModelViewMat();
}
void pushRotate(Axis axis, double degrees)
{
	mat4 m;
	switch(axis)
	{
		case X:
			m = RotateX(degrees);
			break;
		case Y:
			m = RotateY(degrees);
			break;
		case Z:
			m = RotateZ(degrees);
			break;
		default: break;
	}
	matStack.postPush(m);
	updateModelViewMat();
}
void pushScale(double x, double y, double z)
{
	matStack.postPush(Scale(x,y,z));
	updateModelViewMat();
}
void popMat()
{
	matStack.popMat();
	updateModelViewMat();
}
