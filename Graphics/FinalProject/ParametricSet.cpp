#include "ParametricSet.h"
#include <cmath>

LinearEquation::LinearEquation(double slope, double intercept) : slope(slope), intercept(intercept)
{}
double LinearEquation::operator()(double t) const
{
	return slope * t + intercept;
}

PolynomialEquation::PolynomialEquation(vector<double> coeffs,double xoff) : coeffs(coeffs),xoff(xoff)
{}
double PolynomialEquation::operator()(double t) const
{
	t += xoff;
	double result = 0.0;
	for(int i=0;i<coeffs.size();i++)
		result += coeffs[i] * pow(t,i);
	return result;
}

ParametricSet::ParametricSet()
{}
ParametricSet::~ParametricSet()
{
	for(int i=0;i<equations.size();i++)
	{
		delete equations[i];
	}
}
void ParametricSet::addEquation(Equation* eq)
{
	equations.push_back(eq);
}
vector<double> ParametricSet::calc(double t) const
{
	vector<double> result;
	for(int i=0;i<equations.size();i++)
		result.push_back((*equations[i])(t));
	return result;
}
