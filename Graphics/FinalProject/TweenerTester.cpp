#include "Tweener.h"

#include <iostream>

int main()
{
	ParametricSet eqns;
	eqns.addEquation(new LinearEquation(2.0,1.0));
	eqns.addEquation(new LinearEquation(0.5,6));
	vector<double> quadraticCoeffs;
	quadraticCoeffs.push_back(2);
	quadraticCoeffs.push_back(0.3);
	quadraticCoeffs.push_back(1);
	eqns.addEquation(new PolynomialEquation(quadraticCoeffs));
	vector<double> answers = eqns.calc(0.2);
	for(int i=0;i<answers.size();i++)
		cout << answers[i] << " " << flush;
	cout << endl;
	cout << endl;
	double someNums[3] = 
	{0.0,0.0,0.0};
	vector<double*> targets;
	for(int i=0;i<3;i++)
		targets.push_back(&(someNums[i]));
	Tweener<double> tweener(targets,eqns);
	tweener.start(100);
	tweener.advance();
	for(int i=0;i<3;i++)
		cout << someNums[i] << " " << flush;
	cout << endl;
	while(tweener.advance())
	{
		for(int i=0;i<3;i++)
			cout << someNums[i] << " " << flush;
		cout << endl;
	}
}
