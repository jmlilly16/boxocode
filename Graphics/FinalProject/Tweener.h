#include "ParametricSet.h"

#include <iostream>
using namespace std;

template<class T>
class Tweener
{
	public:
	enum Direction {FORWARD,BACKWARD};
	private:
	ParametricSet* set;
	const vector<T*>* targets;
	int duration, currentFrame;
	Direction direction;
	bool running;
	public:
	Tweener();
	Tweener(const vector<T*>* t, ParametricSet* s);
	void start(int duration);
	void start(int duration, Direction direction);
	void stop();
	bool advance();
	const inline bool isRunning() const;
	const inline Direction getDirection() const;
};

template <class T>
Tweener<T>::Tweener()
{}
template <class T>
Tweener<T>::Tweener(const vector<T*>* targets, ParametricSet* set) : targets(targets), set(set), duration(0), direction(BACKWARD), currentFrame(0), running(false)
{}
template <class T>
void Tweener<T>::start(int d)
{
	running = true;
	duration = d;
	switch(direction)
	{
		case FORWARD:
			direction = BACKWARD;
			currentFrame = duration;
			return;
		case BACKWARD:
			direction = FORWARD;
			currentFrame = 0;
			return;
	}
}
template <class T>
void Tweener<T>::start(int d, Direction D)
{
	running = true;
	direction = D;
	duration = d;
	switch(direction)
	{
		case FORWARD:
			currentFrame = 0;
			return;
		case BACKWARD:
			currentFrame = duration;
			return;
	}
}
template <class T>
void Tweener<T>::stop()
{
	running = false;
}

template <class T>
bool Tweener<T>::advance()
{
	if(!running) return false;
	//cout << "Advancing frame from " << currentFrame << endl;
	switch(direction)
	{
		case FORWARD:
			currentFrame++;
			break;
		case BACKWARD:
			currentFrame--;
			break;
	}
	double t = (double)currentFrame / (double)duration;
	vector<double> result = set->calc(t);
	int i=0;
	while(i<targets->size()&&i<result.size())
	{
		(*((*targets)[i])) = (T)result[i];
		i++;
	}
	switch(direction)
	{
		case FORWARD:
			if(currentFrame==duration)
				stop();
			break;
		case BACKWARD:
			if(currentFrame==0)
				stop();
			break;
	}
	return true;
}

template <class T>
const inline bool Tweener<T>::isRunning() const
{
	return running;
}

template <class T>
const inline typename Tweener<T>::Direction Tweener<T>::getDirection() const
{
	return direction;
}
