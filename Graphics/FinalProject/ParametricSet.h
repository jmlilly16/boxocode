#include <vector>
using namespace std;

class Equation
{
	public:
	virtual double operator()(double) const = 0;
};

class LinearEquation : public Equation
{
	private:
	double slope,intercept;
	public:
	LinearEquation(double s, double i);
	double operator()(double t) const;
};

class PolynomialEquation : public Equation
{
	vector<double> coeffs;
	double xoff;
	public:
	PolynomialEquation(vector<double> coeffs,double xoff);
	double operator()(double t) const;
};

class ParametricSet
{
	private:
	vector<Equation*> equations;
	public:
	ParametricSet();
	~ParametricSet();
	
	void addEquation(Equation*);
	
	vector<double> calc(double t) const;
};
