//FILE: transform_2.cpp
//NAME:  T. Garrett
//DATE LAST MODIFIED:  02/13/2015
//  This program draws a black triangle on a blue-green background.
//	Sends the Transformation Matrix to the Vertex Shader.
//
//  Keyboard:
//		left and right arrows:  translate the triangle in the x-direction
//		up and down arrows:  translate the triangle in the y-direction
//////////////////////////////////////////////////////////////////////////////////////


//	Always include the following lines in every program (before main):
 #ifdef __APPLE__       // For use with OS X
 #include <GLUT/glut.h>
 #else		       // Other (Linux)
 #include <GL/glut.h>         
 #endif
// glut.h includes gl.h and glu.h

#include <iostream>
#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)
using namespace std;

//GLOBAL VARIABLES:
typedef Angel::vec4  point4;

point4 points[3] = {
	 vec4(-0.5, -0.5, 0.0, 1.0), vec4(0.0, 0.5, 0.0, 1.0), vec4(0.5, -0.5, 0.0, 1.0)
	};

GLdouble tx(0.0), ty(0.0);
GLuint Transformation_loc;

void init();		//called once;  contains any initialization we want to perform
void display();		//called every time the window is redrawn
void keyboard( unsigned char key, int x, int y );
void specialKeyboard (int key, int x, int y);

int main(int argc, char** argv) {
	
//Set up the windowing context:

  //Initialize Window, display mode (singly buffered window, RGB mode).
  glutInit(&argc,argv); 
  glutInitDisplayMode (GLUT_DOUBLE | GLUT_RGB);

  //Window Size in pixels
  glutInitWindowSize(800,600);

  //Window Position (upper left corner of the screen).
  glutInitWindowPosition(0,0); 

  glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
  //glutCreateWindow("First OpenGL Program!");

  glutKeyboardFunc( keyboard );
  glutSpecialFunc( specialKeyboard );

//Pass our display function to the context
  glutDisplayFunc(display);

//Call our initialization function
  init();  //called only once, at the beginning

//Entger the GLUT event processing loop.
  glutMainLoop();

}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	// clear window 
  	glClear(GL_COLOR_BUFFER_BIT); 

  	mat4 M; 
  	M = Translate(tx, ty, 0.0);
	//cout<<M<<endl;
	
	glUniformMatrix4fv( Transformation_loc, 1, GL_TRUE, M );

  	glDrawArrays(GL_TRIANGLES, 0, 3);

  	//flush the buffer (draw it on the screen)
  	glutSwapBuffers();

}


void init() 
{

  // Create a vertex array object
  GLuint vao[1];
  #ifdef __APPLE__       // For use with OS X
    glGenVertexArraysAPPLE(1, vao );
    glBindVertexArrayAPPLE(vao[0] );
  #else		       // Other (Linux)
    glGenVertexArrays(1, vao );
    glBindVertexArray(vao[0] );       
  #endif
  
  // Create and initialize a buffer object
  GLuint buffer;
  glGenBuffers( 1, &buffer );
  glBindBuffer( GL_ARRAY_BUFFER, buffer );
  glBufferData( GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW );
  
  // Load shaders and use the resulting shader program
  GLuint program = InitShader( "vshader_2.glsl", "fshader.glsl" );
  glUseProgram( program );
  
	// Initialize the vertex position attribute from the vertex shader
  GLuint loc = glGetAttribLocation( program, "vPosition" );
  glEnableVertexAttribArray( loc );
  glVertexAttribPointer( loc, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
 
  Transformation_loc = glGetUniformLocation( program, "Transform" );
  
  /* set clear color to green */
	glClearColor (0.3, 1.0, 1.0, 0.0);
  
}

void keyboard( unsigned char key, int x, int y )
{
    switch( key ) {
	case 033: // Escape Key
	case 'q': case 'Q':
	    exit( EXIT_SUCCESS );
	    break;
	}
}

void specialKeyboard (int key, int x, int y){
	switch( key ) {
	case GLUT_KEY_LEFT:
		tx = tx-0.05;
		glutPostRedisplay();
		break; 	
	case GLUT_KEY_RIGHT:
		tx = tx+0.05;
		glutPostRedisplay();
		break; 
	case GLUT_KEY_UP:
		ty = ty+0.05;
		glutPostRedisplay();
		break; 
	case GLUT_KEY_DOWN:
		ty = ty-0.05;
		glutPostRedisplay();
		break; 
	}
}

