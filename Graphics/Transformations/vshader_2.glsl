attribute vec4 vPosition;
uniform mat4 Transform;

void main() 
{
  gl_Position = Transform*vPosition;
} 
