/********************************************************************
* FILE: 	morph.cpp
* NAME: 	
* DATE LAST MODIFIED:  02/02/2015
*
*  Illustrates using the vertex shader to perform basic morphing
*   - Passes 3 arrays to the shader, any two can be used for morphing
*   - Uses Idle Function
*   - sets up elapsed time parameter for use by vertex shader
*
*  Makes use of 
*	 - "subdata" to pack info about all the shapes into the same buffer.
*    - glutIdleFunc
*
* Assumes shaders vmorph.glsl and fmorph.glsl are in same directory
********************************************************************/
#include "Angel.h"
using namespace Angel;

//GLOBALS:
GLuint          time_location;
GLuint          vertices_A_location;
GLuint          vertices_B_location;
GLuint		   	vertices_C_location;
GLuint		   	program;

vec2 vertices_A[3] = {vec2(-1.0, -1.0), vec2(0.0,1.0), vec2(1.0, -1.0)};
vec2 vertices_B[3] = {vec2(1.0, -1.0), vec2(0.0,-1.0), vec2(1.0, 1.0)};
vec2 vertices_C[3] = {vec2(0.0, 0.0), vec2(0.0, 0.0), vec2(0.0, 0.0)};

//FUNCTIONS:
void init();
void display();
void keyboard(unsigned char key, int x, int y);
void idle();


int main(int argc, char** argv)
{

    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE );  //Note double buffering
    glutInitWindowSize(512, 512);
    glutCreateWindow("Simple Shader morphing");
    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutIdleFunc(idle);
    init();
    glutMainLoop();
    return 0;
}
////////////////////////////////////////////////////////////////////////////////////
void init()
{
	GLuint vao[1];
    #ifdef __APPLE__       // For use with OS X
      glGenVertexArraysAPPLE(1, vao );
      glBindVertexArrayAPPLE(vao[0] );
    #else		       // Other (Linux)
      glGenVertexArrays(1, vao );
      glBindVertexArray(vao[0] );       
    #endif

    // Create and initialize a buffer object
    GLuint buffer;
    glGenBuffers( 1, &buffer );
    glBindBuffer( GL_ARRAY_BUFFER, buffer );
	glBufferData( GL_ARRAY_BUFFER, sizeof(vertices_A) + sizeof(vertices_B) + sizeof(vertices_C) , NULL, GL_STATIC_DRAW );

    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices_A), vertices_A);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices_A),  sizeof(vertices_B), vertices_B);
	glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices_A)+ sizeof(vertices_B), sizeof(vertices_C),  vertices_C);

    // Load shaders and use the resulting shader program
    GLuint program = InitShader( "vmorph.glsl", "fmorph.glsl" );
    glUseProgram( program );

    // Initialize the vertex position attribute from the vertex shader
    GLuint vertices_A_location = glGetAttribLocation( program, "vertices1" );
    glEnableVertexAttribArray( vertices_A_location );
    glVertexAttribPointer( vertices_A_location, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );

    GLuint vertices_B_location = glGetAttribLocation( program, "vertices2" );
    glEnableVertexAttribArray( vertices_B_location );
    glVertexAttribPointer( vertices_B_location, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices_A)));

	GLuint vertices_C_location = glGetAttribLocation( program, "vertices3" );
    glEnableVertexAttribArray( vertices_C_location );
    glVertexAttribPointer( vertices_C_location, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices_A)+sizeof(vertices_B)));
	
	time_location = glGetUniformLocation(program, "time");
	
    glClearColor( 1.0, 1.0, 1.0, 1.0 ); /* white background */
  	glLineWidth(3.0);
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// send elapsed time to shaders 
    std::cout<<time_location<<"\n";  //testing purposes
    glUniform1f(time_location, glutGet(GLUT_ELAPSED_TIME));

    glDrawArrays(GL_LINE_LOOP, 0, 3);

    glutSwapBuffers();
}


void keyboard(unsigned char key, int x, int y)
{
    switch (key) {
    case 27:
        exit(0);
        break;
    default:
        break;
    }
}

void idle()
{
   glUniform1f(time_location, glutGet(GLUT_ELAPSED_TIME));
   glutPostRedisplay();
}

