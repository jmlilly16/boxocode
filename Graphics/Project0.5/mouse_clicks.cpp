/********************************************************************
 FILE: mouse_clicks.cpp
 NAME: Mason Lilly
 DATE LAST MODIFIED: 02/03/2015
 DESCRIPTION:
	This program allows the user to click around a canvas marking points.
	One point makes a dot. Two points make a line. Three points make a triangle.
	After three, the cycle repeats.

 KEYS:  ESC to exit program
	'f': Toggles wireframe/filled
		
 Assumes all header files and shader files are in the same directory
********************************************************************/

#ifdef __APPLE__       // For use with OS X
 #include <GLUT/glut.h>
 #else		       // Other (Linux)
 #include <GL/glut.h>         
 #endif
#include "Angel.h"

using namespace std;

//GLOBAL VARIABLES:
bool filled = true;
const int MAXPOINTS = 256;
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 800;
const int POINT_SIZE = 3;
int pointCount = 0;
vec2 points[MAXPOINTS];

//FUNCTIONS:
void init();
void display();
void keyboard(unsigned char key, int x, int y);
void mouse(int button, int state, int x, int y);

//////////////////////////////////////////////////////////////////////

int main(int argc, char **argv)
{
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_RGBA);
    glutInitWindowSize(WIN_WIDTH,WIN_HEIGHT);

	//num_points = atoi(argv[1]); 		//convert the string argument to integer
    glutCreateWindow(argv[0]);	//use the string version for window title

    init();

    glutDisplayFunc(display);
    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);

    glutMainLoop();
    return 0;
}

////////////////////////////////////////////////////////////////////////

void init()
{
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers(1, &buffer);			//generate 1 buffer object name and store in array <buffer>
	glBindBuffer(GL_ARRAY_BUFFER, buffer);  //specifies TARGET and name
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
			  //( TARGET, size, pointer to data that will be copied, usage)
	// Load shaders and use the resulting shader program
	GLuint program = InitShader("vshader_gasket1.glsl", "fshader_gasket1.glsl");
	glUseProgram(program);

	// Initialize the vertex position attribute from the vertex shaders
	GLuint loc = glGetAttribLocation(program, "vPosition"); //returns location of attribute variable
	glEnableVertexAttribArray(loc);	//enables attribute specified at <loc>
						//( index, size, type, normalized?, stride, pointer)
	glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glClearColor( 1.0, 1.0, 1.0, 1.0 ); // white background

}
/////////////////////////////////////////////////////////////////////////////////////

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);     // clear the window
	
	glPointSize(POINT_SIZE);
	
	glDrawArrays(GL_POINTS, 0, pointCount);    // draw the points
	if(pointCount>=3)
	{
		if(filled)
		{
			glDrawArrays(GL_TRIANGLES,0,3*(pointCount/3));
		}
		else
		{
			for(int i=0;i<3*(pointCount/3);i+=3)
				glDrawArrays(GL_LINE_LOOP,i,3);
		}
	}
	if(pointCount%3==2)
	{
		glDrawArrays(GL_LINES,3*(pointCount/3),2);
	}
	glFlush();
}

/////////////////////////////////////////////////////////////////////////////////////

void keyboard( unsigned char key, int x, int y )
{
	switch ( key )
	{
		case 033:
			exit( EXIT_SUCCESS );
			break;
		case 'f':
			filled = !filled;
			cout << "Wireframe Toggled" << endl;
			glutPostRedisplay();
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	if(!(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)) return;
	points[pointCount].x = (GLfloat)(((double)(x*2))/WIN_WIDTH - 1.0);
	points[pointCount].y = (GLfloat)(((double)((WIN_HEIGHT-y)*2))/WIN_HEIGHT - 1.0);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);

	cout << "Mouse clicked at: ("<<x<<","<<y<<"), aka ("<<points[pointCount].x<<","<<points[pointCount].y<<")"<<endl;
	pointCount++;
	if(pointCount==MAXPOINTS)
	{
		cout << "Maximum Points Reached" << endl;
		glutMouseFunc(NULL);
	}
	glutPostRedisplay();
}
