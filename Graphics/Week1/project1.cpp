//FILE:  project1.cpp
//NAME:  T. Garrett
//DATE LAST MODIFIED:  01/04/2015
//Dummy program for demonstration only

#include "matrices.h"  //provides printMessage() 
#include <iostream>
using namespace std;

int main()
{
  printMessage();
  cout<<"Hello, world."<<endl;
  return 0;
}
 
