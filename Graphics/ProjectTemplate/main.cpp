//TODO: Fill this out
//FILE: name.cpp
//NAME:  Mason Lilly
//DATE LAST MODIFIED:  iunno
//  This program <blahblahblah>
//  
//
//  Keyboard:
//		<your keys here>
//  Assumes all header files and shader files are in the directory
//
//////////////////////////////////////////////////////////////////////////////////////

//	Always include the following lines in every program (before main):
#ifdef __APPLE__       // For use with OS X
#include <GLUT/glut.h>
#else		       // Other (Linux)
#include <GL/glut.h>         
#endif
// glut.h includes gl.h and glu.h


#include "Angel.h"  //Provides InitShader (function) 
//Angel.h includes vec.h, which provides vec2 (datatype)

#include <iostream>
using namespace std;

//GLOBAL VARIABLES:
const int WIN_WIDTH = 800;
const int WIN_HEIGHT = 600;

void init();		
void display();		
void keyboard(unsigned char key, int x, int y);
//void mouse(int button, int state, int x, int y);
//void idle();

int main(int argc, char** argv)
{
	//Set up the windowing context:
	//Initialize Window, display mode (singly buffered window, RGB mode).
	glutInit(&argc,argv); 
	glutInitDisplayMode (GLUT_SINGLE | GLUT_RGB);
	
	//Window Size in pixels
	glutInitWindowSize(800,600);
	
	//Window Position (upper left corner of the screen).
	glutInitWindowPosition(0,0); 
	
	glutCreateWindow(argv[0]);  // Window title is name of program (argv[0]) 
	//glutCreateWindow("First OpenGL Program!");
	
	//Pass our display function to the context
	glutDisplayFunc(display);
	
	//Pass our keyboard function to the context
	glutKeyboardFunc( keyboard );
	
	//Call our initialization function
	init();  //called only once, at the beginning
	
	//Enter the GLUT event processing loop.
	glutMainLoop();
}
/////////////////////////////////////////////////////////////////////////////////

void display() 
{
  	glClear(GL_COLOR_BUFFER_BIT);
  	glDrawArrays(GL_POINTS, 0, 6);
	glFlush(); 
}

void init() 
{
	vec4 points[] = {};
	// Create and initialize a buffer object
	GLuint buffer;
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(points), points, GL_STATIC_DRAW);
  
	// Load shaders and use the resulting shader program
	GLuint program = InitShader("vshader_gouraud.glsl", "fshader_gouraud.glsl");
	glUseProgram(program);
  
	// Initialize the vertex position attribute from the vertex shader
	GLuint loc = glGetAttribLocation(program, "vPosition");
	glEnableVertexAttribArray(loc);
	glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
  
  /* set clear color to white */
	glClearColor (1.0, 1.0, 1.0, 0.0);  
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case 033:   //ESC key
			exit(EXIT_SUCCESS);
			break;
	}
}

