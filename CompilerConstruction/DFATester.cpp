#include "DFA.h"
#include <iostream>
using namespace std;

int main()
{
	DFA<int,char> dfa
	{//main initializer list
		{//state_spec initializer list
			{//state_spec initializer
				0,
				false,
				{//conn_spec list
					{//conn_spec initializer
						'a',
						1
					},
					{
						'b',
						2
					},
					{
						'c',
						3
					},
					{
						'm',
						5
					}
				}
			},
			{
				1,
				false,
				{
					{
						'z',
						4
					}
				}
			},
			{
				2,
				false,
				{
					{
						'y',
						4
					}
				}
			},
			{
				3,
				false,
				{
					{
						'x',
						4
					}
				}
			},
			{
				4,
				true
			},
			{
				5,
				false,
				{
					{
						' ',
						4
					}
				}
			}
		},
		0,
		-1
	};
	dfa.print();
	dfa.start();
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	dfa.transition('b');
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	dfa.transition('a');
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	dfa.start();
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	dfa.transition('b');
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	dfa.transition('y');
	cout << dfa.getState() << " " << dfa.isAccept() << endl;
	return 0;
}
			
