#include "SymbolTable.h"
#include <iostream>

using namespace std;

int main()
{
	SymbolTable table;
	string testStrings[9];
	testStrings[0]="A";
	testStrings[1]="B";
	testStrings[2]="Test";
	testStrings[3]="A";
	testStrings[4]="C";
	testStrings[5]="B";
	testStrings[6]="D";
	testStrings[7]="E";
	testStrings[8]="D";
	for(string s: testStrings)
		cout << table.lookup(s) << ", ";
	cout << endl;
	return 0;
}
