#include <map>
#include <iostream>

using namespace std;

int main()
{
	map<int,char> numMap;
	
	numMap.insert(0,'4');
	numMap.insert(1,'3');
	numMap.insert(2,'8');
	//numMap[0] = 4;
	//numMap[1] = '3';
	//numMap[2] = '8';

	cout << numMap[0] << " " << numMap[1] << " " << numMap[2] << endl;
}
