//file:jmlilly16.cpp
//Compilation command: g++ -o <exec_name> jmlilly16.cpp

/*Mason Lilly
Compiler Construction, Winter 2015
Dr. Moorman
*/

/*Synopsis: <exec_name> <filename>
Where <exec_name> is the executable name created from compilation and <filename> is the name of the file to be processed.
Reads in "<filename>" and creates a new file called "<filename>_squished", with the same file extension, which is identical to "<filename>" but with all spaces and tabs removed.
This is the "super" version of the file, which removes comments (designated with C*), capitalizes everything, and leaves quotes alone.
*/

//Here is a test comment to make sure they are properly removed: C* You should not see this text.
//But you should see this text!
	
#include <iostream>
#include <fstream>

using namespace std;

//This function takes in file stream objects for an input file and an output file
//The function reads the input file, character by character, and processes each one according to these rules:
//1. If the character is part of a comment, it is discarded.
//2. A character is considered part of a comment if it follows a 'C' followed immediately by a '*' and precedes the next newline.
//3. If a character is part of a quote, it is written to the output file and no other processing is done to it.
//4. A character is considered part of a quote if it lies between two matched sets of double quotes.
//5. A character can not be in both a quote and a comment. When rule 1 is active, rule 4 will be ignored; rule 2 is ignored with rule 3 is active.
//6. Alphabetic characters are capitalized unless otherwise specified.
//7. If a character is a newline ad the LAST character was a newline, it is discarded.
//8. If nothing else, if the character is neither a space nor a tab, it is written to the output file.
//
//Preconditions: ifFine and outFile represent valid file stream objects ready to be read/written
//Postcondition: outFile contains the contents of inFile, modified by the rules above
void squishFile(fstream& inFile, fstream& outFile)
{
	char letter;
	bool inQuote = false, halfComment = false, inComment = false, lineStart = true; //flags set by various special characters
	while(inFile.get(letter))
	{
		//cout << letter;
		//If the comment flag is on, discard (do not write) characters until the newline symbol is reached.
		if(inComment)
		{
			if(letter=='\n')
			{
				inComment = false;
			}
			else continue;
		}
		//If this flag is on, the last thing we saw was a capital C. Check to see if this is the start of a comment.
		if(halfComment)
		{
			halfComment = false;
			if(letter == '*')
			{
				inComment = true;
				continue;
			}
			//If it's not, it's just a normal 'C' - put it in the output file where it belongs.
			else
			{
				outFile << "C";
				cout << "C";
			}
		}
		//Special processing for non-quoted characters
		if(!inQuote)
		{
			if(letter==' '||letter=='\t') continue; //Skip whitespace
			//If a line contains no non-whitespace characters before the next newline, don't bother printing it.
			if(lineStart)
			{
				lineStart = false;
				if(letter=='\n')
				{
					lineStart = true;
					continue;
				}
			}
			//Capitalize letters
			if(letter>='a'&&letter<='z')
			{
				letter -= 32;
			}
			//Check for the potential start of a comment
			if(letter=='C')
			{
				halfComment = true;
				continue;
			}
		}
		//If the character has made it this far without being skipped, write it to the output file
		outFile << letter;
		cout << letter;
		//Check for quote starts/ends.
		if(letter=='"')
		{
			inQuote=!inQuote;
		}
		//Check for the end of a line
		if(letter=='\n')
		{
			lineStart = true;
		}
	}
}

int main(int argc, char* argv[])
{
	//Unanticipated User-error Problem Solver (UUPS)
	if(argc<1)
	{
		cout << "Bad argument count. See comments in jmlilly16.cpp for syntax." << endl;
		return 1;
	}
	
	//Get the source filename from the command line and create a file object to read it
	string filename(argv[1]);
	fstream inFile(filename.data(),ios_base::in);
	
	//Don't try to work on an imaginary file
	if(!inFile.is_open())
	{
		cout << "File could not be opened." << endl;
		return 1;
	}
	
	//Form the new file name by isolating the stem, appending "_squished", and then re-adding the old suffix, if there is one.
	string newFilename = "";
	int i=0;
	//isolate the stem
	while(i<filename.size()&&filename[i]!='.')
	{
		newFilename+=filename[i++];
	}
	//add the specifier
	newFilename+="_squished";
	//re-add the old suffix
	if(i<filename.size())
	{
		while(i<filename.size())
		{
			newFilename += filename[i++];
		}
	}
	//Use the new filename to create an output file object.
	fstream outFile(newFilename.data(),ios_base::out);
	
	//Send the file to the file-processing function.
	squishFile(inFile,outFile);
	
	//Tidy up and exit cleanly.
	inFile.close();
	outFile.close();
	return 0;
}
