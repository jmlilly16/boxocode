//file:jmlilly16.cpp
//Compilationcommand:g++-o<exec_name>jmlilly16.cpp

/*MasonLilly
CompilerConstruction,Winter2015
Dr.Moorman
*/

/*Synopsis:<exec_name><filename>
Where<exec_name>istheexecutablenamecreatedfromcompilationand<filename>isthenameofthefiletobeprocessed.
Readsin"<filename>"andcreatesanewfilecalled"<filename>_squished",withthesamefileextension,whichisidenticalto"<filename>"butwithallspacesandtabsremoved.
*/

#include<iostream>
#include<fstream>

usingnamespacestd;

intmain(intargc,char*argv[])
{
//UnanticipatedUser-errorProblemSolver(UUPS)
if(argc<1)
{
cout<<"Badargumentcount.Seecommentsinjmlilly16.cppforsyntax."<<endl;
return1;
}

//Getthesourcefilenamefromthecommandlineandcreateafileobjecttoreadit
stringfilename(argv[1]);
fstreaminFile(filename.data(),ios_base::in);

//Don'ttrytoworkonanimaginaryfile
if(!inFile.is_open())
{
cout<<"Filecouldnotbeopened."<<endl;
return1;
}

//Formthenewfilenamebyisolatingthestem,appending"_squished",andthenre-addingtheoldsuffix,ifthereisone.
stringnewFilename="";
inti=0;
cout<<1<<endl;
while(i<filename.size()&&filename[i]!='.')
{
newFilename+=filename[i++];
}
cout<<2<<endl;
if(i<filename.size())
{
newFilename+="_squished";
while(i<filename.size())
{
newFilename+=filename[i++];
}
}
cout<<3<<endl;
//Usethenewfilenametocreateanoutputfileobject.
fstreamoutFile(newFilename.data(),ios_base::out);

//Geteachcharacteroftheinputfile.Aslongasit'sneitheraspacenoratab,outputittothenewfile.
charletter;
while(inFile.get(letter))
{
//cout<<letter;
if(letter!=''&&letter!='\t')
{
outFile<<letter;
cout<<letter;
}
}

//Tidyupandexitcleanly.
inFile.close();
outFile.close();
return0;
}
