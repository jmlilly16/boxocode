//file:jmlilly16.cpp
//Compilation command: g++ -o <exec_name> jmlilly16.cpp

/*Mason Lilly
Compiler Construction, Winter 2015
Dr. Moorman
*/

/*Synopsis: <exec_name> <filename>
Where <exec_name> is the executable name created from compilation and <filename> is the name of the file to be processed.
Reads in "<filename>" and creates a new file called "<filename>_squished", with the same file extension, which is identical to "<filename>" but with all spaces and tabs removed.
*/
	
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[])
{
	//Unanticipated User-error Problem Solver (UUPS)
	if(argc<1)
	{
		cout << "Bad argument count. See comments in jmlilly16.cpp for syntax." << endl;
		return 1;
	}
	
	//Get the source filename from the command line and create a file object to read it
	string filename(argv[1]);
	fstream inFile(filename.data(),ios_base::in);
	
	//Don't try to work on an imaginary file
	if(!inFile.is_open())
	{
		cout << "File could not be opened." << endl;
		return 1;
	}
	
	//Form the new file name by isolating the stem, appending "_squished", and then re-adding the old suffix, if there is one.
	string newFilename = "";
	int i=0;
	cout << 1 << endl;
	while(i<filename.size()&&filename[i]!='.')
	{
		newFilename+=filename[i++];
	}
	cout << 2 << endl;
	if(i<filename.size())
	{
		newFilename+="_squished";
		while(i<filename.size())
		{
			newFilename += filename[i++];
		}
	}
	cout << 3 << endl;
	//Use the new filename to create an output file object.
	fstream outFile(newFilename.data(),ios_base::out);
	
	//Get each character of the input file. As long as it's neither a space nor a tab, output it to the new file.
	char letter;
	while(inFile.get(letter))
	{
		//cout << letter;
		if(letter!=' '&&letter!='\t')
		{
			outFile << letter;
			cout << letter;
		}
	}
	
	//Tidy up and exit cleanly.
	inFile.close();
	outFile.close();
	return 0;
}
