//file: Esubp.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the esubp function, used by the TRANSY executor

#ifndef ESUBP
#define ESUBP

#include "Executor.h"

//This function reads a line of TRANSY code to perform a SUBP operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void esubp(vector<int> line);

#endif
