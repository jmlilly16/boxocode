//File: Tokenizer.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions defined in Tokenizer.h

#include <sstream>
#include <vector>
#include <string>
#include "Compiler.h"

#include "Tokenizer.h"

//The tokenizers declared in Tokenizer.h are defined below.
//To construct them, we first need to define their DFAs.
//Here is the definition of the first one.

//Summary of mainDFA's structure:
//The DFA starts at 0.
//From there, several states can be reached that are the one-length punctuation symbol tokens.
//A - or a digit will send the DFA into the number-acceptor area.
//A letter will send the DFA into the var-acceptor area.
//A $ will send the DFA into the literal-acceptor area.
//A " will send the DFA into the quote-acceptor area.
//Finally, a # will send the DFA into the keyword-accepting area, which handles specific character sequences matching the TRANSY keywords.
//In the number secion are the INT and FLOAT acceptors.
//From the INT acceptor, a letter will send the DFA into the LABEL acceptor.

//How to read this constructor:
//This is a nested-bracket constructor.
//The outermost set of brackets encloses all the members of the objects
//The next level in is the set of state specifiers
//Each state specifier (separated by commas) consists of a state id, whether it accepts, an optional default transition, and an optional list of transition specifiers.
//A transition specifier consists of a trigger symbol, a destination state, and an optional generalizer symbol (ALPHA, NUM, etc)
//The last two elements in the largest set of brackets are the start state and error state id of the DFA.

StringDFA<int> mainDFA
{
	{
		{0,false,
		{
			{'[',1},
			{']',2},
			{'(',3},
			{')',4},
			{'=',5},
			{'>',6},
			{'<',8},
			{'!',10},
			{',',12},
			{'-',13},
			{' ',15,NUM},
			{'0',14},
			{'$',18},
			{' ',20,ALPHA},
			{'"',61},
			{'#',66}
		}
		},
		{66,false,
		{
			{'D',21},
			{'R',24},
			{'W',27},
			{'S',31},
			{'E',34},
			{'C',36},
			{'N',40},
			{'G',42},
			{'L',45},
			{'I',49},
			{'A',51},			
			{'T',63}
		}
		},
		{1,true
		},
		{2,true
		},
		{3,true
		},
		{4,true
		},
		{5,true
		},
		{6,true,
		{
			{'=',7}
		}
		},
		{7,true
		},
		{8,true,
		{
			{'=',9}
		}
		},
		{9,true
		},
		{10,false,
		{
			{'=',11}
		}
		},
		{11,true
		},
		{12,true
		},
		{13,false,
		{
			{' ',15,NUM},
			{'0',14}
		}
		},
		{14,true,
		{
			{'.',16}
		}
		},
		{15,true,
		{
			{'.',16},
			{' ',15,NUM},
			{' ',60,ALPHA}
		}
		},
		{16,false,
		{
			{' ',17,NUM}
		}
		},
		{17,true,
		{
			{' ',17,NUM}
		}
		},
		{18,false,
		{
			{' ',19,ALPHA}
		}
		},
		{19,true,
		{
			{' ',19,ALPHANUM}
		}
		},
		{20,true,
		{
			{' ',20,ALPHANUM}
		}
		},
		{21,true,
		{
			{' ',20,ALPHANUM},
			{'I',22}
		}
		},
		{22,true,
		{
			{'M',23}
		}
		},
		{23,true
		},
		{24,true,
		{
			{' ',20,ALPHANUM},
			{'E',25}
		}
		},
		{25,true,
		{
			{' ',20,ALPHANUM},
			{'A',26}
		}
		},
		{26,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{27,true,
		{
			{' ',20,ALPHANUM},
			{'R',28}
		}
		},
		{28,true,
		{
			{' ',20,ALPHANUM},
			{'I',29}
		}
		},
		{29,true,
		{
			{' ',20,ALPHANUM},
			{'T',30}
		}
		},
		{30,true,
		{
			{' ',20,ALPHANUM},
			{'E',23}
		}
		},
		{31,true,
		{
			{' ',20,ALPHANUM},
			{'T',32},
			{'U',52}
		}
		},
		{32,true,
		{
			{' ',20,ALPHANUM},
			{'O',33}
		}
		},
		{33,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{34,true,
		{
			{' ',20,ALPHANUM},
			{'N',35}
		}
		},
		{35,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{36,true,
		{
			{' ',20,ALPHANUM},
			{'L',59},
			{'D',37}
		}
		},
		{37,true,
		{
			{' ',20,ALPHANUM},
			{'U',38}
		}
		},
		{38,true,
		{
			{' ',20,ALPHANUM},
			{'M',39}
		}
		},
		{39,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{40,true,
		{
			{' ',20,ALPHANUM},
			{'O',41}
		}
		},
		{41,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{42,true,
		{
			{' ',20,ALPHANUM},
			{'O',43}
		}
		},
		{43,true,
		{
			{' ',20,ALPHANUM},
			{'T',44}
		}
		},
		{44,true,
		{
			{' ',20,ALPHANUM},
			{'O',23}
		}
		},
		{45,true,
		{
			{' ',20,ALPHANUM},
			{'I',46},
			{'R',24},
			{'W',27},
			{'O',53}
		}
		},
		{46,true,
		{
			{' ',20,ALPHANUM},
			{'S',47}
		}
		},
		{47,true,
		{
			{' ',20,ALPHANUM},
			{'T',48}
		}
		},
		{48,true,
		{
			{' ',20,ALPHANUM},
			{'O',23}
		}
		},
		{49,true,
		{
			{' ',20,ALPHANUM},
			{'F',50}
		}
		},
		{50,true,
		{
			{'A',23}
		}
		},
		{51,true,
		{
			{' ',20,ALPHANUM},
			{'R',24},
			{'W',27}
		}
		},
		{52,true,
		{
			{' ',20,ALPHANUM},
			{'B',33}
		}
		},
		{53,true,
		{
			{' ',20,ALPHANUM},
			{'O',54}
		}
		},
		{54,true,
		{
			{' ',20,ALPHANUM},
			{'P',55}
		}
		},
		{55,true,
		{
			{'-',56}
		}
		},
		{56,true,
		{
			{' ',20,ALPHANUM},
			{'E',57}
		}
		},
		{57,true,
		{
			{' ',20,ALPHANUM},
			{'N',58}
		}
		},
		{58,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{59,true,
		{
			{' ',20,ALPHANUM},
			{'S',23}
		}
		},
		{60,true,
		{
			{' ',60,ALPHANUM}
		}
		},
		{61,false,61,
		{
			{'"',62}
		}
		},
		{62,true
		},
		{63,true,
		{
			{' ',20,ALPHANUM},
			{'H',64}
		}
		},
		{64,true,
		{
			{' ',20,ALPHANUM},
			{'E',65}
		}
		},
		{65,true,
		{
			{' ',20,ALPHANUM},
			{'N',23}
		}
		}
	},
	0,
	-1
};

//Here we actually construct the tokenizer. This is another bracket initializer, taking in a reference to the DFA just constructed and information to construct a map between ints and lexical categories. This map specifies which accepting states indicate acceptance of which categories.

Tokenizer tokenizer
{
	&mainDFA,
	{
		{1,LBRACK},
		{2,RBRACK},
		{3,LPAREN},
		{4,RPAREN},
		{5,EQ},
		{6,GTHAN},
		{7,GTHANEQ},
		{8,LTHAN},
		{9,LTHANEQ},
		{11,NOTEQ},
		{12,COMMA},
		{17,FLOAT},
		{14,INT},
		{15,INT},
		{19,LITERAL},
		{20,VAR},
		{23,KEYWORD},
		{50,KEYWORD},
		{55,KEYWORD},
		{21,VAR},
		{22,VAR},
		{24,VAR},
		{25,VAR},
		{26,VAR},
		{51,VAR},
		{27,VAR},
		{28,VAR},
		{29,VAR},
		{30,VAR},
		{31,VAR},
		{52,VAR},
		{32,VAR},
		{33,VAR},
		{34,VAR},
		{35,VAR},
		{59,VAR},
		{36,VAR},
		{37,VAR},
		{38,VAR},
		{39,VAR},
		{40,VAR},
		{41,VAR},
		{42,VAR},
		{43,VAR},
		{44,VAR},
		{45,VAR},
		{46,VAR},
		{47,VAR},
		{48,VAR},
		{49,VAR},
		{53,VAR},
		{54,VAR},
		{56,VAR},
		{57,VAR},
		{58,VAR},
		{60,LABEL},
		{62,QUOTE},
		{63,VAR},
		{64,VAR},
		{65,VAR}
	}
};

//Here is the DFA used by the assignment DFA.
//Its start state goes to several single-state operator branches, as well as a number branch and a var branch.

StringDFA<int> assignDFA
{
	{
		{0,false,
		{
			{'(',1},
			{')',2},
			{'[',3},
			{']',4},
			{'+',5},
			{'-',6},
			{'*',7},
			{'/',8},
			{'^',9},
			{'=',10},
			{' ',12,NUM},
			{'0',11},
			{' ',15,ALPHA},
			{'%',16},
			{'<',17},
			{'>',19},
			{'!',21},
			{'&',24},
			{'|',25},
			{'_',26}
		}
		},
		{1,true
		},
		{2,true
		},
		{3,true
		},
		{4,true
		},
		{5,true
		},
		{6,true
		},
		{7,true
		},
		{8,true
		},
		{9,true
		},
		{10,true,
		{
			{'=',23}
		}
		},
		{11,true,
		{
			{'.',13}
		}
		},
		{12,true,
		{
			{' ',12,NUM},
			{'.',13}
		}
		},
		{13,false,
		{
			{' ',14,NUM}
		}
		},
		{14,true,
		{
			{' ',14,NUM}
		}
		},
		{15,true,
		{
			{' ',15,ALPHANUM}
		}
		},
		{16,true
		},
		{17,true,
		{
			{'=',18}
		}
		},
		{18,true
		},
		{19,true,
		{
			{'=',20}
		}
		},
		{20,true
		},
		{21,true,
		{
			{'=',22}
		}
		},
		{22,true
		},
		{23,true
		},
		{24,true
		},
		{25,true
		},
		{26,true
		}
	},
	0,
	-1
};

//Construction of the assignment tokenizer

Tokenizer assignTokenizer
{
	&assignDFA,
	{
		{1,LPAREN},
		{2,RPAREN},
		{3,LBRACK},
		{4,RBRACK},
		{5,PLUS},
		{6,MINUS},
		{7,MULTIPLY},
		{8,DIVIDE},
		{9,POWER},
		{10,EQ},
		{11,INT},
		{12,INT},
		{14,FLOAT},
		{15,VAR},
		{16,MODULO},
		{17,LTHAN},
		{18,LTHANEQ},
		{19,GTHAN},
		{20,GTHANEQ},
		{21,NOT},
		{22,NOTEQ},
		{23,EQUALS},
		{24,AND},
		{25,OR},
		{26,FLOOR}
	}
};

vector<Token> Tokenizer::tokenizeLine(string line)
{
	vector<Token> tokens;
	char c=' ';
	stringstream feed(line);
	tokenDFA->start();
	int lastState=tokenDFA->getState();
	string symbol = "";
	bool lastTime=true;
	bool badToken=false;
	
	//Transition the DFA on each character in the string.
	//As soon as you enter the error state, check the state just before it.
	//If it was an accepting state, you've found a valid token!
	//Restart the DFA and retransition of the character that send you into error.
	//Look up the lexical category of the last accepting state on the acceptance map, and make a token object with that category and all the character's you've seen since the beginning or the last time you made a token.
	//Collect all the tokens you make and return them at the end.
	//If you find a bad token (moved to the error state from a nonaccepting state), report this as an error at the end.
	while((c = feed.get()) && (feed.good()||lastTime))
	{
		if(!feed.good()) lastTime = false;
		//cout << "DFA Transitioning from " << tokenDFA->getState() << " on " << c << " to " << flush;
		tokenDFA->transition(c);
		//cout << tokenDFA->getState() << endl;
		if(tokenDFA->isError() || !feed.good())
		{
			if(tokenDFA->isAccept(lastState))
			{
				//cout << "Found Token: " << Token(idCats.at(lastState),symbol).toString() << endl;
				tokens.push_back(Token(idCats.at(lastState),symbol));
				symbol="";
			}
			else
			{
				badToken = true;
				//cout << "Error: Bad Token: " << Token(BADTOKEN,symbol).toString() << endl;
				tokens.push_back(Token(BADTOKEN,symbol));
				symbol="";
			}
			//Back up and start again from the new symbol
			tokenDFA->start();
			tokenDFA->transition(c);
		}
		symbol+=c;
		lastState = tokenDFA->getState();
	}
	return tokens;
}
