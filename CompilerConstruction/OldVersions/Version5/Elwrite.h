//file: Elwrite.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the Elwrite function, used by the TRANSY executor

#ifndef ELWRITE
#define ELWRITE

#include "Executor.h"

//This function reads a line of TRANSY code to perform an LWRITE operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void elwrite(vector<int> line);

#endif
