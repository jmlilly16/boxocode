//file:Preprocess.cpp

/*Mason Lilly
Compiler Construction, Winter 2015
Dr. Moorman
*/

//This file implements the functions described in Preprocess.h.

#include <iostream>
#include <fstream>
#include <sstream>

#include "Preprocess.h"
#include "Compiler.h"

using namespace std;

//Gets the two-character string that, when found on a line, triggers an in-line comment (which should be removed during preprocessing).
//The string that gets returned depends on whether the compiler's alternateComment mode has been set.
//Returns: Either the standard comment string "C*" if not in alternateComment mode, or the alternate string "//" otherwise.
string getCommentString()
{
	if(alternateComment)
	{
		return "//";
	}
	else
	{
		return "C*";
	}
}

//This function takes in the file name of a .transy file and attempts to open it.
//An output file is created that has the same stem as the input file, with the extention .noblanks
//The function reads the input file, character by character, and processes each one according to these rules:
//1. If the character is part of a comment, it is discarded.
//2. A character is considered part of a comment if it follows the character sequence returned by getCommentString() and preceeds the next newline.
//3. If a character is part of a quote, it is written to the output file and no other processing is done to it.
//4. A character is considered part of a quote if it lies between two matched double quotes.
//5. A character can not be in both a quote and a comment. When rule 1 is active, rule 4 will be ignored; rule 2 is ignored with rule 3 is active.
//6. Alphabetic characters are capitalized.
//7. If a ':' is encountered, all the characters read since the last newline are flagged as a line label. An entry is made in the line label table and those characters (including the ':') are discarded.
//7. If a character is a newline and the LAST character was a newline, it is discarded.
//8. If nothing else, if the character is neither a space nor a tab, it is written to the output file.
//9. A record matching output line numbers to input line numbers is kept, in the form of a map. This gets returned.

PreprocessResult preprocess(string filename)
{
	//Setup
	PreprocessResult result;	//The package describing the results of preprocessing
	result.success = false;
	result.error=false;
	string extension = "";
	//If there's an extension, write it down.
	if(filename.find(".")!=filename.npos) extension = filename.substr(filename.find("."));
	//If there WASN'T an extension, set it to TRANSYEXT and add that to the filename.
	if(extension.size()==0)
	{
		extension=TRANSYEXT;
		filename+=extension;
	}
	//Reject file names that aren't meant for the compiler
	else if(extension!=TRANSYEXT)
	{
		cout << "File must be a "<<TRANSYEXT<<" file" << endl;
		return result;
	}
	result.outputName = getFileStem(filename)+NOBLANKSEXT;
	
	cout << "Using file " << filename << endl;
	ifstream inFile(filename.data());
	if(!inFile.is_open())
	{
		cout << "Could not open file " << filename << endl;
		return result;
	}
	ofstream outFile(result.outputName.data());
	if(!outFile.is_open())
	{
		cout << "Could not open output file" << endl;
		return result;
	}
	
	//PROCESSING//
	char letter;
	bool inQuote = false, halfComment = false, inComment = false, lineStart = true; //flags set by various special characters
	int sourceLine=0,outLine=0; //These ints keep track of where in the input/output files the preprocessor is.
	string buffer="";	//The buffer stores everything between the last newline and the current character
				//It is flushed to the output stream and emptied when the next newline is encountered.
	//cout << "Starting buffer: Contents: " << buffer << endl;
	while(inFile.get(letter))
	{
		//cout << letter << endl;
		if(letter=='\n')
			sourceLine++;
		//If the comment flag is on, discard (do not write) characters until the newline symbol is reached.
		if(inComment)
		{
			if(letter=='\n')
			{
				inComment = false;
			}
			else continue;
		}
		//If this flag is on, the last thing we saw was the first character of the comment string. Check to see if this is the start of a comment.
		if(halfComment)
		{
			halfComment = false;
			//If the current character is the second character of the comment string, this is the start of a comment!
			if(letter == getCommentString()[1])
			{
				//cout << "Found a comment!" << endl;
				inComment = true;
				continue;
			}
			//If it's not, it's just a normal character - put it in the output file where it belongs.
			else
			{
				buffer+=getCommentString()[0];
				//cout << "Buffer contents: " << buffer << endl;
			}
		}
		//Special processing for non-quoted characters
		if(!inQuote)
		{
			if(letter==' '||letter=='\t') continue; //Skip whitespace
			
			//Capitalize letters
			if(letter>='a'&&letter<='z')
			{
				letter -= 32;
			}
			//Check for the potential start of a comment
			if(letter==getCommentString()[0])
			{
				halfComment = true;
				//cout << "Found a half comment..." << endl;
				continue;
			}
			//Check for line labels
			if(letter==':')
			{
				//cout << "Found a line label. Processing..." << endl;
				//cout << "\""<<buffer<<"\"" << endl;
				bool badLabel = false;
				char* c=&buffer[0];
				while(*c!=0)
				{
					//Line labels can only contain certain characters
					if(!((*c>='0'&&*c<='9')||(*c>='A'&&*c<='Z')||(*c>='a'&&*c<='z')))
					{
						reportError("Ill-formed line label on line "+to_string(sourceLine)+": "+buffer);
						result.error=true;
						badLabel = true;
						break;
					}
					c++;
				}
				if(!badLabel)
				{
					result.lineTable.lookup(buffer,outLine+1);
				}
				//Throw out whatever characters made up the line label - we won't need them in compilation
				buffer="";
				//cout << "Buffer flushed: " << buffer << endl;
				continue;
			}
			//If a line contains no non-whitespace characters before the next newline, don't bother printing it.
			//Also, if you've printed a character since the last newLine, mark that you're no longer at the start of a line.
			if(lineStart)
			{
				lineStart = false;
				if(letter=='\n')
				{
					lineStart = true;
					continue;
				}
			}
		}
		//If the character has made it this far without being skipped, write it to the output file
		buffer+=letter;
		//cout << "Buffer contents: " << buffer << endl;
		//cout << letter;
		//Check for quote starts/ends.
		if(letter=='"')
		{
			inQuote=!inQuote;
		}
		//Check for the end of a line
		if(letter=='\n')
		{
			lineStart = true;
			outLine++;
			result.lineIndex[outLine] = sourceLine;
			//cout << "Flushing buffer" << endl;
			outFile << buffer;
			buffer = "";
			//cout << "Buffer contents: " << buffer << endl;
		}
	}
	
	//Cleanup
	inFile.close();
	outFile.close();
	result.success = true;
	return result;
}

//This function does a reduced version of preprocessing. In retrospect, it probably would have been more elegant to have one preprocessing function with several processing options that can be turned on and off. But hey, it works.
//This function skips capitalizing letters and the removal of blank lines, whitespace, and comments.
//The only processing it does do is mark and throw out line labels.
//Otherwise, the noblanks file created is identical to the one passed in.

PreprocessResult preprocess_min(string filename)
{
	//Setup
	PreprocessResult result;
	result.success = false;
	result.error = false;
	string extension = "";
	//If there's an extension, write it down.
	if(filename.find(".")!=filename.npos) extension = filename.substr(filename.find("."));
	//If there WASN'T an extension, set it to NOBLANKSEXT and add that to the filename.
	if(extension.size()==0)
	{
		extension = NOBLANKSEXT;
		filename+=extension;
	}
	bool alreadyNoblanks = extension==NOBLANKSEXT;
	//Reject files not meant for the compiler
	if(!(extension==TRANSYEXT||alreadyNoblanks))
	{
		cout << "File must be a "<<TRANSYEXT<<" or "<<NOBLANKSEXT<<" file" << endl;
		return result;
	}
	//The NOBLANKS file to return is the one you have already if it's a noblanks file. Otherwise, make the filename for one.
	if(alreadyNoblanks)
		result.outputName = filename;
	else
		result.outputName = getFileStem(filename)+NOBLANKSEXT;
	
	cout << "Using file " << filename << endl;
	ifstream inFile(filename.data());

	if(!inFile.is_open())
	{
		cout << "Could not open file " << filename << endl;
		return result;
	}

	string outFilename = result.outputName+(alreadyNoblanks?"foo":"");
	ofstream outFile(outFilename.data());
	if(!alreadyNoblanks&&!outFile.is_open())
	{
		cout << "Could not open output file" << endl;
		return result;
	}
	char letter;
	int sourceLine=0;
	string buffer="";
	while(inFile.get(letter))
	{
		//cout << letter << flush;
		if(letter==':')
		{
			//cout << "Found a line label. Processing..." << endl;
			//cout << "\""<<buffer<<"\"" << endl;
			bool badLabel = false;
			char* c=&buffer[0];
			while(*c!=0)
			{
				if(!((*c>='0'&&*c<='9')||(*c>='A'&&*c<='Z')||(*c>='a'&&*c<='z')))
				{
					reportError("Ill-formed line label on line "+to_string(sourceLine)+": "+buffer);
					result.error=true;
					badLabel = true;
					break;
				}
				c++;
			}
			if(!badLabel)
			{
				result.lineTable.lookup(buffer,sourceLine);
			}
			buffer="";
			//cout << "Buffer flushed: " << buffer << endl;
			continue;
		}
		if(letter=='\n')
		{
			buffer += letter;
			sourceLine++;
			result.lineIndex[sourceLine] = sourceLine;
			cout << buffer;
			outFile << buffer;
			buffer = "";
		}
		else
			buffer+=letter;
	}
	inFile.close();
	if(!alreadyNoblanks)
		outFile.close();
	else
	{
		outFile.close();
		ifstream swapIn(outFilename.data());
		ofstream swapOut(filename.data());
		while(swapIn.good())
			swapOut.put(swapIn.get());
		swapIn.close();
		swapOut.close();
		remove(outFilename.data());
	}
	result.success = true;
	return result;
}
