//file: Tgoto.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tgoto function, described in Tgoto.h

#include "Tgoto.h"
#include <sstream>
#include <iostream>

string tgoto(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=2)
	{
		reportError("Argument number mismatch for GOTO");
		return result;
	}
	result+=" ";
	result += verifyLexCat(tokens[1],{VAR,KEYWORD,INT,LABEL})?	//As noted in Tokenizer.h, Labels can also look like keywords, vars, and ints.
		lookupLabel(tokens[1].text) : BADLINE;
	return result;
}
