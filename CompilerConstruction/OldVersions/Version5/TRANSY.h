//file:TRANSY.h
//Author:Mason Lilly
//Last Modified: 4/8/15

//This file defines basic functions and constants used by both the compiler and the executor of the TRANSY language

#ifndef TRANSY
#define TRANSY

#include <string>
#include <iostream>

using namespace std;

//Declarations of file extensions
const extern string DEFAULTFILENAME;
const extern string OBJEXT;
const extern string COREEXT;
const extern string LITERALEXT;

//How many decimal places the streams in the program should use for the program's floating-point numbers
const extern int FLOAT_PRECISION;

//Readline gets a line of text from an istream, terminated by \n, and returns it as a string (without the \n).
//I wrote it because C++'s string handling methods are awkward and confusing.
//Parameters:
//	The istream you want to read from.
//Returns:
//	A string, representing as many characters as could be read from the stream before a \n was found.
string readLine(istream&);

//This function isolates the file stem of a given file name. The stem is defines as all the characters preceding the first '.' of a file name, or the entire file name if no '.' exists.
//Parameters: <filename>: The filename whose stem you want isolated
//Return: A string containing the stem of <filename>
//Preconditions: none
//Postconditions: A string containing the stem of <filename> has been returned
string getFileStem(string filename);

#endif
