//file:Elisto.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Elisto.h

#include "Elisto.h"

#include <iostream>

void elisto(vector<int> line)
{
	Info("Executing Listo");
	Info("Object Code:");
	for(auto i : coderef)
	{
		for(auto j : i)
			cout << j << " ";
		cout << endl;
	}
}
