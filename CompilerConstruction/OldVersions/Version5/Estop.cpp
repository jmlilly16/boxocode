//file:Estop.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Estop.h

#include "Estop.h"

void estop(vector<int> line)
{
	Info("Executing Stop");
	stopReached = true;
}
