//file: Eifa.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eifa function, used by the TRANSY executor

#ifndef EIFA
#define EIFA

#include "Executor.h"

//This function reads a line of TRANSY code to perform an IFA operation, as specified in the user manual. 
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eifa(vector<int> line);

#endif
