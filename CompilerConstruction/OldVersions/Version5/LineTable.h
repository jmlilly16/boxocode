//File: LineTable.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the LineTable class. It is a cousin of the symbol table, except its lookup functions differently

#ifndef LINETABLE
#define LINETABLE

#include <map>
#include <string>

#include "Table.h"

class LineTable : public Table
{
	private:
	int lastPos;
	public:
	LineTable();
	~LineTable();
	
	//This function takes in the text of a line label and, optionally, the desired position of that label in the table.
	//If a position is given and the label is not in the table, it is placed at that position. This position is returned.
	//If a position is given and the label IS in the table, the label is not moved - its old position is returned.
	//If no position is given and the label is in the table, the label's position is returned.
	//If no position is given and the label is not in the table, an error is reported.
	//Reported Errors:
	//	Nonexistant line labels
	int lookup(string entry, int pos=-1);
};

#endif
