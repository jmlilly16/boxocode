//file:Enop.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Enop.h

#include "Enop.h"

void enop(vector<int> line)
{
	Info("Executing Nop");
}
