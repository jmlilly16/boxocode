//file: Tnop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tnop function, described in Tnop.h

#include "Tnop.h"
#include <sstream>
#include <iostream>

string tnop(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to NOP");
	return result;
}
