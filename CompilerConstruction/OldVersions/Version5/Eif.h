//file: Eif.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eif function, used by the TRANSY executor

#ifndef EIF
#define EIF

#include "Executor.h"

//This function reads a line of TRANSY code to perform a IF operation, as specified in the user manual. 
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eif(vector<int> line);

#endif
