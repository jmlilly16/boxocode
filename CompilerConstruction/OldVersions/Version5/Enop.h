//file: Enop.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the enop function, used by the TRANSY executor

#ifndef ENOP
#define ENOP

#include "Executor.h"

//This function reads a line of TRANSY code to perform a NOP operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void enop(vector<int> line);

#endif
