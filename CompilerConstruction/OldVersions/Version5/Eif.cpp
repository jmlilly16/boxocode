//file:Eif.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Eif.h

#include "Eif.h"

typedef bool (*CompFunc)(double,double);

bool EQ(double a,double b){return a==b;}
bool L(double a,double b){return a<b;}
bool G(double a,double b){return a>b;}
bool NEQ(double a,double b){return a!=b;}
bool LEQ(double a,double b){return a<=b;}
bool GEQ(double a,double b){return a>=b;}

vector<CompFunc> comps{EQ,L,G,NEQ,LEQ,GEQ};

void eif(vector<int> line)
{
	if(line.size()<5)
		Error("If invoked with too few arguments");
	Info("Executing If");
	if(bigDebug)
	{
		Info("Using test "+to_string(line[2]),true);
		Info("Using LHS = variable at "+to_string(line[1]),true);
		Info("...with the value "+to_string(coreRetrieve(line[1])),true);
		Info("Using RHS = variable at "+to_string(line[3]),true);
		Info("...with the value "+to_string(coreRetrieve(line[3])),true);
	}
	if(comps[line[2]](coreRetrieve(line[1]),coreRetrieve(line[3])))
	{
		Info("Test passed, setting Program Counter to "+to_string(line[4]),true);
		programCounter = line[4];
	}
	else
		Info("Test failed",true);
}
