//file:Eifa.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Eifa.h

#include "Eifa.h"

void eifa(vector<int> line)
{
	if(line.size()<5)
		Error("Ifa invoked with too few arguments");
	Info("Executing Ifa");
	Info("Using val = variable at "+to_string(line[1]),true);
	double val = coreRetrieve(line[1]);
	Info("...with the value "+to_string(val),true);
	if(val<0)
	{
		Info("val is negative, setting Program Counter to "+to_string(line[2]),true);
		programCounter = line[2];
	}
	else if(val==0)
	{
		Info("val is 0, setting Program Counter to "+to_string(line[3]),true);
		programCounter = line[3];
	}
	else if(val>0)
	{
		Info("val is positive, setting Program Counter to "+to_string(line[4]),true);
		programCounter = line[4];
	}
}
