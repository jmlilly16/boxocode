//file: Tcdump.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tread.h

#include "Tcdump.h"
#include <sstream>
#include <iostream>
#include <string>
#include "Core.h"
using namespace std;

//This function takes in a line of TRANSY code identified as a CDUMP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial C D U M P characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tcdump(vector<Token> tokens)
{
	int start,end;
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()<4)
	{
		reportError("Too few arguments given to CDUMP");
		return result;
	}
	
	result+=" ";	//Check for INTness, and make sure it's positive.
	result += 	verifyLexCat(tokens[1],INT)?
			(
				((start = atoi(tokens[1].text.data()))>=0)?
				tokens[1].text : 
				(reportError("CDUMP Start location must be positive"),BADLINE)
			) : BADLINE;
	
	verifyLexCat(tokens[2],COMMA);
	
	result+=" ";	//Check for INTness, and make sure it's less than the core's size and greater than the previous value.
	result += 	verifyLexCat(tokens[3],INT)?
			(
				(atoi(tokens[3].text.data())<CORESIZE)?
				(
					(atoi(tokens[3].text.data())>start)?
					tokens[3].text
					:(reportError("CDUMP End must be greater than Start"),BADLINE)
				)
				:(reportError("CDUMP End location must not be greater than "+to_string(CORESIZE)),BADLINE)
			) : BADLINE;
	
	if(tokens.size()>4)
		reportError("Too many arguments given to CDUMP");
	return result;
}
