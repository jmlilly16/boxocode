//file: Tloop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

#include "Tloop.h"

string tloop(vector<Token> tokens)
{
	loops.push(currentLinenum_);
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=8)
	{
		reportError("Wrong number of arguments for LOOP");
		return result;
	}
	
	result+=" ";
	result += verifyLexCat(tokens[1],{VAR,KEYWORD})?
		lookupSymbol(tokens[1].text) : BADLINE;
	
	verifyLexCat(tokens[2],EQ);
	
	result+=" ";
	result += verifyLexCat(tokens[3],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[3].text) : BADLINE;
	
	verifyLexCat(tokens[4],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[5],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[5].text) : BADLINE;

	verifyLexCat(tokens[6],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[7],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[7].text) : BADLINE;
	return result;
}
