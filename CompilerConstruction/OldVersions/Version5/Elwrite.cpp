//file:Elwrite.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Elwrite.h

#include "Elwrite.h"

#include <iostream>

void elwrite(vector<int> line)
{
	Info("Executing Lwrite");
	if(bigDebug)
		for(auto i = ++line.begin();i!=line.end();i++)
		{
			Info("Writing literal from location "+to_string(*i),true);
			cout << literalRetrieve(*i) << endl;
		}
	else
		for(auto i = ++line.begin();i!=line.end();i++)
			cout << literalRetrieve(*i) << endl;
};
