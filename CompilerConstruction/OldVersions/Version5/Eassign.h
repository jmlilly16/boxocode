//file: Eassign.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eassign function, used by the TRANSY executor

#ifndef EASSIGN
#define EASSIGN

#include "Executor.h"

//This function reads a line of TRANSY code to perform an ASSIGNMENT operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eassign(vector<int> line);

#endif
