//file: Tlisto.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tlisto function, described in Tlisto.h

#include "Tlisto.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a LISTO command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial L I S T O characters, another character will cause an error.
string tlisto(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to LISTO");
	return result;
}
