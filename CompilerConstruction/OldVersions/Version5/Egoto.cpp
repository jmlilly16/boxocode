//file:Egoto.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Egoto.h

#include "Egoto.h"

void egoto(vector<int> line)
{
	if(line.size()<2)
		Error("Goto invoked with too few arguments");
	Info("Executing Goto");
	Info("Setting Program Counter to "+to_string(line[1]),true);
	programCounter = line[1];
}
