//file:Eassign.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file implements the function declared in Eassign.h

#include "Eassign.h"

#include <cmath>

#include "assoc.h"

//The AssignStack is a one-instance class that maintains a stack containing both L-values and R-values.
//A given value can be pushed as either of the two freely.
//If an L-Value is popped as an R-Value, it will be dereferenced.
//If an R-Value is popped as an L-Value, an error is thrown.
class AssignStack
{
	private:
	//Stores a value along with whether it is an L-Value or an R-Value.
	struct AssignVal
	{
		core_val val;
		bool isLVal;
		AssignVal(double v, bool i)
			:val(v),isLVal(i)
		{}
	};
	stack<AssignVal> data;
	public:
	//Pushes a value onto the stack, with the information that it is an L-Value.
	void pushl(double val)
	{
		//cout << "Pushing " << val << " as Lvalue " << endl;
		Info("Pushing "+to_string(val)+" as L-Value",true);
		data.push(AssignVal(val,true));
	}
	//Pushes a value onto the stack, with the information that it is an R-Value.
	void pushr(double val)
	{
		//cout << "Pushing " << val << " as Rvalue " << endl;
		Info("Pushing "+to_string(val)+" as R-Value",true);
		data.push(AssignVal(val,false));
	}
	//Attempts to pop a value, treating it as an L-Value.
	//If the stack is empty, an error is thrown.
	//If the top of the stack is an L-Value, it is returned.
	//If the top of the stack is an R-Value, an error is thrown.
	double popl()
	{
		if(data.empty())
			Error("Attempted operation with no operands");
		AssignVal a = data.top();
		//cout << "Popped " << a.val << " to use as LValue" << endl;
		data.pop();
		if(!a.isLVal)
			Error("Attempted to use RValue as LValue");
		Info("Popped "+to_string(a.val)+" as L-Value",true);
		return a.val;
	}
	//Attempts to pop a value, treating it as an R-Value.
	//If the stack is empty, an error is thrown.
	//If the top of the stack is an L-Value, the value in Core at that address is returned instead.
	//If the top of the stack is an R-Value, it is returned.
	double popr()
	{
		if(data.empty())
			Error("Attempted operation with no operands");
		AssignVal a = data.top();
		//cout << "Popped " << a.val << " to use as RValue" << endl;	
		data.pop();
		if(a.isLVal)
		{
			//cout << "It was an LValue. Converting to RValue: " << coreRetrieve(int(a.val)) << endl;
			if(bigDebug)
				Info("Popped the L-Value "+to_string(a.val),true);
				Info("...and converted it to the R-Value "+to_string(coreRetrieve(int(a.val))),true);
			return coreRetrieve(int(a.val));
		}
		else
		{
			Info("Popped "+to_string(a.val)+" as R-Value",true);
			return a.val;
		}
	}
} assignStack;

//Operation function pointer
typedef void (*AssignFunc)();

void assign()
{
	//cout << "Performing an Assign operation" << endl;
	double value = assignStack.popr();
	double addr = assignStack.popl();
	if(bigDebug)
	{
		Info("Performing an Assign operation.",true);
		Info("Storing the value "+to_string(value)+" at the address "+to_string(addr),true);
	}
	coreStore(int(addr),value);
}
void dereference()
{
	//cout << "Performing a Dereference operation" << endl;
	double offset = assignStack.popr();
	double addr = assignStack.popl();
	if(bigDebug)
	{
		Info("Performing a Dereference operation",true);
		Info("Adding the offset "+to_string(offset)+" to the base address "+to_string(addr)+": result is "+to_string(addr+offset),true);
	}
	//cout << "Resulting address is " << (int(addr)+int(offset)) << endl;
	assignStack.pushl(int(addr)+int(offset));
}
void power()
{
	///cout << "Performing a Power operation" << endl;
	double exponent = assignStack.popr();
	double base = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Power operation",true);
		Info("Raising the base "+to_string(base)+" to the power of "+to_string(exponent)+": result is "+to_string(pow(base,exponent)),true);
	}
	assignStack.pushr(pow(base,exponent));
}
void multiply()
{
	//cout << "Performing a Multiply operation" << endl;
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Multiply operation",true);
		Info("Multiplying "+to_string(a)+" by "+to_string(b)+": result is "+to_string(a*b),true);
	}
	assignStack.pushr(a*b);
}
void divide()
{
	//cout << "Performing a Divide operation" << endl;
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(b==0.0)
		Error("Attempted to divide by 0");
	if(bigDebug)
	{
		Info("Performing a Divide operation",true);
		Info("Dividing "+to_string(a)+" by "+to_string(b)+": result is "+to_string(a/b),true);
	}
	assignStack.pushr(a/b);
}
void add()
{
	//cout << "Performing an Add operation" << endl;
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing an Add operation",true);
		Info("Adding "+to_string(a)+" from "+to_string(b)+": result is "+to_string(a+b),true);
	}
	assignStack.pushr(a+b);
}
void subtract()
{
	//cout << "Performing a Subtract operation" << endl;
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Subtract operation",true);
		Info("Subtracting "+to_string(a)+" from "+to_string(b)+": result is "+to_string(a-b),true);
	}
	assignStack.pushr(a-b);
}

//Takes the floating-modulo of its two operands. Highest multiple of the base that is less than the dividend is subtracted from the dividend.
void modulo()
{
	//cout << "Performing a Modulo operation" << endl;
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Modulo operation",true);
		Info("Using operands a="+to_string(a)+", b="+to_string(b),true);
		Info("b goes into a evenly "+to_string(int(a/b))+" times",true);
		Info("a-b*"+to_string(floor(a/b))+"= "+to_string(a-floor(a/b)*b),true);
	}
	assignStack.pushr(a-floor(a/b)*b);
}

void lessthan()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Less-Than operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a<b)?1:0),true);
	}
	assignStack.pushr((a<b)?1:0);
}

void greaterthan()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Less-Than operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a>b)?1:0),true);
	}
	assignStack.pushr((a>b)?1:0);
}

void lessthanoreq()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Less-Than-Or-Equal-To operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a<=b)?1:0),true);
	}
	assignStack.pushr((a<=b)?1:0);
}

void greaterthanoreq()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Greater-Than-Or-Equal-To operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a>=b)?1:0),true);
	}
	assignStack.pushr((a>=b)?1:0);
}

void equals()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing an Equal-To operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a==b)?1:0),true);
	}
	assignStack.pushr((a==b)?1:0);
}

void notequals()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Not-Equal-To operation",true);
		Info("Comparing "+to_string(a)+" from "+to_string(b)+": result is "+to_string((a!=b)?1:0),true);
	}
	assignStack.pushr((a!=b)?1:0);
}

void Not()
{
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a NOT operation",true);
		Info("Notting "+to_string(a)+": result is "+to_string((a==0)?1:0),true);
	}
	assignStack.pushr((a==0)?1:0);
}

void And()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing an AND operation",true);
		Info("Anding "+to_string(a)+" with "+to_string(b)+": result is "+to_string(((a!=0)&&(b!=0))?1:0),true);
	}
	assignStack.pushr(((a!=0)&&(b!=0))?1:0);
}

void Or()
{
	double b = assignStack.popr();
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing an OR operation",true);
		Info("Oring "+to_string(a)+" with "+to_string(b)+": result is "+to_string(((a!=0)||(b!=0))?1:0),true);
	}
	assignStack.pushr(((a!=0)||(b!=0))?1:0);
}

void Floor()
{
	double a = assignStack.popr();
	if(bigDebug)
	{
		Info("Performing a Floor operation",true);
		Info("Flooring "+to_string(a)+": result is "+to_string(floor(a)),true);
	}
	assignStack.pushr(floor(a));
}

//Association mapping the opCodes for each operation used in the object code to the function that performs it.
assoc<int,AssignFunc> assignFuncs	//Note this won't update with the array in Keywords - you'll have to do that manually if one changes.
{
	{-1,assign},
	{-2,dereference},
	{-3,power},
	{-4,multiply},
	{-5,divide},
	{-6,add},
	{-7,subtract},
	{-8,modulo},
	{-9,lessthan},
	{-10,lessthanoreq},
	{-11,greaterthan},
	{-12,greaterthanoreq},
	{-13,equals},
	{-14,notequals},
	{-15,Not},
	{-16,And},
	{-17,Or},
	{-18,Floor}
};

void eassign(vector<int> line)
{
	Info("Executing Assign");
	for(auto i = ++line.begin();i!=line.end();i++)
	{
		if(*i<0)	//Is it an operator?
			assignFuncs[*i]();
		else
			assignStack.pushl(*i); //Every value comes in as an L-Value.
	}
}
