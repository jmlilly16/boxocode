//File: LiteralCore.cpp
//Author: Mason Lilly
//Last Modified: 4/8/15

//This file defines the LiteralCore object, a class for storing the strings handled by the TRANSY compiler and executor
//The LiteralCore uses a map object to map addresses to literals.
//It supports get and set operations, and can save/load itself to/from a filename or stream.

#ifndef LITCORE
#define LITCORE

#include <map>
#include <string>

using namespace std;

typedef int core_addr;

class LiteralCore
{
	private:
	map<core_addr,string> data;
	public:
	LiteralCore();
	~LiteralCore();
	
	string get(core_addr); //Returns the string stored at the given address. If a previously unreferenced address is given, a blank string is returned.
	void set(core_addr,string); //Sets the data at the given address to the given string, overwriting that address's previous contents.
	bool isInit(core_addr); //Returns true if there is a string associated with the given address.

	void load(istream&); //Loads data into the object's memory from the given istream, which can be a file. Assumes the stream is valid to be read.
	bool load(string); //Attempts to construct an ifstream from the given filename. If this succeeds, passes it to the above function. Returns false if the given file could not be opened.

	void save(ostream&); //Writes the contents of the object's memory to the given ostream, which can be a file. Assumes the given ostream is valid for writing.
	bool save(string); //Attempts to construct an ofstream from the given filename. If this succeeds, passes it to the above function. Returns false if the given filename could not be opened.
};

#endif
