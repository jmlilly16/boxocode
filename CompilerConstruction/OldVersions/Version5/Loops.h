//file:Loops.h
//Author: Mason Lilly
//Last Modified: 4/8/15

//This file is used by the TRANSY compiler.
//It defines the LoopDef struct, used during runtime for storing information about a given invokation of the Loop command.
//It also declares loopStack, a globally accessible stack of LoopDefs, used to track any loops the current execution is in.

#ifndef LOOPS
#define LOOPS

#include "Executor.h"

struct LoopDef
{
	core_addr variable; //The counter variable
	core_addr limit; //The limit - if variable is less than this, the loop will continue.
	core_addr increment; //The increment - how much variable changes at the end of each iteration
	int returnLine; //The line number to return to if the continue-test fails.
	LoopDef(core_addr v, core_addr l, core_addr i, int r) //Basic constructor
		:variable(v),limit(l),increment(i),returnLine(r)
	{}
};

extern stack<LoopDef> loopStack;

#endif
