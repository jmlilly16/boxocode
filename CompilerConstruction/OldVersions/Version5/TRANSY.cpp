//file:TRANSY.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the constants and functions declared in TRANSY.h

#include "TRANSY.h"

//Filename constants
const string DEFAULTFILENAME = "test";
const string OBJEXT = ".obj";
const string COREEXT = ".core";
const string LITERALEXT = ".literal";

//Decimal places to use in streams
const int FLOAT_PRECISION = 10;

//Helper function to get a line from a string, because c++'s string manipulation is annoying
string readLine(istream& stream)
{
	string result = "";
	char c=0;
	while((c = stream.get()) && stream.good()) //Get a character - if the stream is still good, you got a character and not an eof-error.
	{
		if(c=='\n')
			return result;
		result+=c;
	}
	return result;
}
//Helper function to isolate the stem of a file
string getFileStem(string filename)
{
	string stem = "";
	int i=0;
	while(i<filename.size()&&filename[i]!='.')
	{
		stem+=filename[i++];
	}
	return stem;
}
