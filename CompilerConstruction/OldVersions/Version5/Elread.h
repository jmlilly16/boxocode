//file: Elread.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the elread function, used by the TRANSY executor

#ifndef ELREAD
#define ELREAD

#include "Executor.h"

//This function reads a line of TRANSY code to perform an LREAD operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void elread(vector<int> line);

#endif
