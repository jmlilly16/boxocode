//File: Core.h
//Author: Mason Lilly
//Last Modified: 4/8/15

//This file describes the Core object.
//Core represents the block of core memory used by the TRANSY compiler and executor, represented as an array of doubles.
//Core initializes all its values to the UNINITVAL constant, to represent locations that have not yet been assigned a value.
//The object can retrieve and store values in any of its addresses, as well as report whether an address has been initialized.
//Finally, the array contains a "hidden" last element which represents whether the core represents a valid compilation.
//This value can be set and checked via accessor methods.
//The core can be saved to and loaded from given file-names or iostreams.

#ifndef CORE
#define CORE
#include "TRANSY.h"

#include <string>

using namespace std;

typedef int core_addr;
typedef double core_val;

const static int CORESIZE = 1000; //Represents how many data elements are in the Core. Actual size will be one larger, to store the validation entry.
const extern core_val UNINITVAL; //Value stored in uninitialized entries. This should be something that is not likely to show up during normal operation.

class Core
{
	private:
	core_val data[CORESIZE+1];
	public:
	Core();
	~Core();
	
	core_val get(core_addr) const; //Returns the value at core_addr in the array, or UNINITVAL if out of bounds.
	void set(core_addr,core_val); //Sets the value at core_addr to core_val. Does nothing if core_addr is out of bounds.
	
	bool isInit(core_addr) const; //Returns true if the value at core_addr is anything but UNINITVAL.
	
	//Called to set the final entry from 0 to 1, to indicate the file is valid for use.
	void setValid();
	bool isValid() const; //Returns true if the final entry is 1 (i.e., the core is valid)

	//Saves the contents of the object to the given ostream, which can be a file. Assumes the ostream is valid to be written to.
	void save(ostream&);
	//Opens the file at the given filename and creates an ofstream, which is passed to the above function. Returns false if the given filename could not be opened.
	bool save(string);
	//Loads data into the object from the given istream, which can be a file. Assumes the given stream is valid for reading.
	void load(istream&);
	//Opens the file at the given filename and creates an ifstream, which is passed to the above function. Returns false if the file could not be opened.
	bool load(string);
};

#endif
