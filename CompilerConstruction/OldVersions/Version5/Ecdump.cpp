//file:Ecdump.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Ecdump.h

#include "Ecdump.h"

#include <iostream>

typedef ostream& (*manip)(ostream&);

void ecdump(vector<int> line)
{
	if(line.size()<3)
		Error("Cdump invoked with too few arguments");
	Info("Executing Cdump");
	Info("Using start = "+to_string(line[1]),true);
	Info("Using end = "+to_string(line[2]),true);
	int start=line[1],end=line[2];
	char* s;
	Info("Core between "+to_string(start)+" and "+to_string(end));
	if(!compactWrites)
		for(int i=start;i<end;i++)
			cout << core_.get(i) << endl;
	else
	{
		int count = 0;
		for(int i=start;i<end;i++)
			cout << core_.get(i) <<" "<< ((++count<LINE_ITEMS)?(manip)flush:((count=0),(manip)endl));
		if(count>0) cout << endl;
	}
}
