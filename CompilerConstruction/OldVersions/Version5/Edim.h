//file: Edim.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the Edim function, used by the TRANSY executor

#ifndef EDIM
#define EDIM

#include "Executor.h"

//The Edim function does nothing - its header and definition are provided for possible future expansion.	
void edim(vector<int> line);

#endif
