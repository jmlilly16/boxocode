//file:Eread.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Eread.h

#include "Eread.h"

#include <iostream>
#include <cmath>

void eread(vector<int> line)
{
	Info("Executing Read");
	if(bigDebug)
		for(auto i = ++line.begin();i!=line.end();i++)
		{
			Info("Reading number into location "+to_string(*i),true);
			double input = inputDouble();
			Info("Read "+to_string(input),true);
			coreStore(*i,input);
		}
	else
		for(auto i = ++line.begin();i!=line.end();i++)
			coreStore(*i,inputDouble());
}
