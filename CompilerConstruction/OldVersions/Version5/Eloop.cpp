//file:Eloop.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Eloop.h

#include "Eloop.h"

#include "Loops.h"

void eloop(vector<int> line)
{
	if(line.size()<5)
		Error("Loop invoked with too few arguments");
	Info("Executing Loop");
	loopStack.push(LoopDef(line[1],line[3],line[4],programCounter));
	if(bigDebug) Info("Pushed a loop with counter address "+to_string(loopStack.top().variable)+", limit "+to_string(coreRetrieve(loopStack.top().limit))+", increment "+to_string(coreRetrieve(loopStack.top().increment))+", and returnLine "+to_string(loopStack.top().returnLine),true);
	coreStore(line[1],coreRetrieve(line[2])-coreRetrieve(loopStack.top().increment));	//initialize
	if(bigDebug) Info("Initialized variable at "+to_string(loopStack.top().variable)+" to "+to_string(coreRetrieve(loopStack.top().variable))),true;
	
	//Advance the program counter to the corresponding loop-end statement, which will do the first test.
	int nestCounter=0;
	bool done=false;
	programCounter--;	//Back up one just in case the next loop-end is the very next line (PC is incremented immediately)
	while(!done and ++programCounter<coderef.size())
		switch(coderef[programCounter][0])
		{
			case 14: ++nestCounter; break;
			case 15: if(nestCounter--==0) done=true;
			default: break;
		}
	if(!done)
		Error("LOOP without LOOP-END");	//This is checked for during compilation, but this is here to avoid segmentation faults.
	Info("Advancing program counter to next loop-end at "+to_string(programCounter),true);
}
