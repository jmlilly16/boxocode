//file:Eawrite.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Eawrite.h

#include "Eawrite.h"

#include <iostream>

typedef ostream& (*manip)(ostream&);

void eawrite(vector<int> line)
{
	if(line.size()<4)
		Error("Awrite invoked with too few arguments");
	Info("Executing Awrite");
	Info("Using base address of "+to_string(line[1]),true);
	int baseAddr = line[1];
	Info("Using start = variable at "+to_string(line[2]),true);
	int start = coreRetrieve(line[2]);
	Info("...with the value "+to_string(start),true);
	Info("Using end = variable at "+to_string(line[3]),true);
	int end = coreRetrieve(line[3]);
	Info("...with the value "+to_string(end),true);
	if(!compactWrites)
		for(int i=start;i<end;i++)
			cout << coreRetrieve(baseAddr+i) << endl;
	else
	{
		int count = 0;
		for(int i=start;i<end;i++)
			cout << coreRetrieve(baseAddr+i) <<" "<< ((++count<LINE_ITEMS)?(manip)flush:((count=0),(manip)endl));
		if(count>0) cout << endl;
	}
}
