//file: Tifa.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

#include "Tifa.h"

string tifa(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=9)
	{
		reportError("Wrong number of arguments for IFA");
		return result;
	}
	verifyLexCat(tokens[1],LPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[2],{VAR,KEYWORD})?
		lookupSymbol(tokens[2].text) : BADLINE;
	
	verifyLexCat(tokens[3],RPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[4],{VAR,KEYWORD,INT,LABEL})?
		lookupLabel(tokens[4].text) : BADLINE;
	
	verifyLexCat(tokens[5],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[6],{VAR,KEYWORD,INT,LABEL})?
		lookupLabel(tokens[6].text) : BADLINE;
	
	verifyLexCat(tokens[7],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[8],{VAR,KEYWORD,INT,LABEL})?
		lookupLabel(tokens[8].text) : BADLINE;
	
	return result;
}
