//file:Executor.h
//Author:Mason Lilly
//Last Modified:4/8/15

//This file declares the constants, globals variables, and functions used by the TRANSY executor.

#ifndef EXECUTOR
#define EXECUTOR

#include "TRANSY.h"

#include <stack>
#include <vector>

#include "Core.h"
#include "LiteralCore.h"

using namespace std;

//Constants needed by certain executor functions. Included here for the sake of centralization.
const extern int LINE_ITEMS;
const extern int CLS_LINES;

//Execution mode flags.
extern bool defaultZeroes; //If this is set, when referencing the Core, accessing an uninitialized value will not throw an error - it is treated as 0.0 instead.
extern bool compactWrites; //If this is set, the awrite, write, and cdump functions will write LINE_ITEMS items before proceeding to the next line.
extern bool noWarn; //If this is set, warnings will be suppressed - currently the only warning occurs if no literal file was found.
extern bool debug; //If this is set, basic debug output will be given. Namely, each command's name will be declared as it is executed.
extern bool bigDebug; //If this and the above flag are set, exhaustive output will be given. All values used by commands will be stated.

//Control variables
extern bool stopReached; //Once this flag is set, execution will stop and go into shutdown after it finishes its current command.
extern int programCounter; //At any given point, this variable represents the next line of object code the program will execute. It can be set to different values to transfer control.

const extern vector<vector<int>>& coderef; //This gives files that include this header a constant reference to the object code. Used by listo.
const extern Core& core_; //Objects should almost always access core memory via the core functions below. However, if a function, e.g. cdump, needs direct access (e.g. to bypass the "uninitialized value" error-checking), this const-reference can be used to look at Core's contents.

core_val coreRetrieve(core_addr); //Returns the value stored in Core at the address given. If Core reports that that value is uninitialized, and defaultZeroes is not set, an error is thrown.
void coreStore(core_addr,core_val); //Stores the given value in Core at the given address.

string literalRetrieve(core_addr); //Returns the literal stored in the LiteralCore at the address given. If that address has not been initialized, an empty string is returned.
void literalStore(core_addr,string); //Stores the given string in the LiteralCore at the given address.

//Output functions.

//Called by any part of the program that encounters an error.
//Outputs the given error message, and then exits the program to prevent undesired operation.
void Error(string message);

//Relays a warning - an error message that does not necessitate closing the program. Currently the only warning is if a literal file is not found.
void Warn(string message);

//Outputs debugging info.
//Debugging info occurs on two levels: Normal and "Big".
//Normal debugging is activated by setting the 'debug' flag.
//In Normal debugging, all e-functions output their names as they execute.
//Big debugging is activated by setting BOTH the 'debug' and 'bigDebug' flags.
//In Big debugging, functions output everything they can.
//If this function is called and debug is not set, it exits.
//If it is called with onlyOnBig set to true and bigDebug is not set, it exits.
//Otherwise, the given message is outputted to the screen.
void Info(string message,bool onlyOnBig=false);

//Custom input function to obtain a double from standard input.
//If a user inputs 1234.g65, cin >> double will extract 1234.0, but leave the g65 characters on the stream.
//Any subsequent calls to cin >> double will return 0, because the first character left on the stream is a g.
//This function extracts a double to return, then uses getline to clear any remaining characters from cin.
double inputDouble();

#endif
