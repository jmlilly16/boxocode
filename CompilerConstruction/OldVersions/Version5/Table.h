//File: Table.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file describes the Table class, which is a base class for the SymbolTable and LineTable classes.

#ifndef TABLE
#define TABLE

#include <map>
#include <string>

using namespace std;

//A Table is really just a special case of a std::map
class Table : public map<string,int>
{
	public:
	Table();
	~Table();
	//This function is used to check if a symbol exists in the table without trying to place it if it doesn't.
	//Parameters: <entry>: The string to be looked up
	//Returns: True if the entry exists in the table, false if not
	bool contains(string entry) const;
};

#endif
