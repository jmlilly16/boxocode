//file:Elread.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Elread.h

#include "Elread.h"

#include <iostream>

void elread(vector<int> line)
{
	Info("Executing Lread");
	string input;
	if(bigDebug)
		for(auto i = ++line.begin();i!=line.end();i++)
		{
			Info("Reading literal into location "+to_string(*i),true);
			getline(cin,input);
			Info("Read "+input);
			literalStore(*i,input);
		}
	else
		for(auto i = ++line.begin();i!=line.end();i++)
			literalStore(*i,(getline(cin,input),input));
}
