//file:Esubp.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Esubp.h

#include "Esubp.h"

#include <cmath>

typedef double (*MathFunc)(double);

vector<MathFunc> mathFuncs{sin,cos,exp,abs,log,sqrt};

void esubp(vector<int> line)
{
	if(line.size()<4)
		Error("Subp invoked with too few arguments");
	Info("Executing Subp");
	if(bigDebug)
	{
		Info("Using function "+to_string(line[1]),true);
		Info("Using argument = variable at "+to_string(line[1]),true);
		Info("...with the value"+to_string(coreRetrieve(line[2])),true);
		Info("Result: "+to_string(mathFuncs[line[1]](coreRetrieve(line[2]))),true);
		Info("Storing result in location "+to_string(line[3]),true);
	}
	coreStore(line[3],mathFuncs[line[1]](coreRetrieve(line[2])));
}
