//file:Ecls.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file defines the function declared in Ecls.h

#include "Ecls.h"

#include <iostream>

void ecls(vector<int> line)
{
	Info("Executing Cls");
	for(int i=0;i<CLS_LINES;i++)
		cout << endl;
}
