//file:Executor.cpp
//Author:Mason Lilly
//Last Modified: 4/8/15

//This is the main file for the TRANSY executor.
//It defines everything declared in Executor.h, the main executor function, and some helper functions.

//The executor command line accepts a filename, optionally preceded by any of a number of command-line flags, detailed below.
//From the filename given, it loads a .obj and .core file with the same stem, and attempts to load a .literal file.
//It proceeds to execute each line of object code in order by calling a function specific to each command in the TRANSY language and passing in the line in question.

//FEATURES:
//As the executor runs, three kinds of messages may appear.
//Errors: These are thrown any time the program encounters a non-recoverable error, due to a problem that could not be caught at compile time.
//	Typically is something that would cause the system to crash if execution was allowed to continue. The program always exits after throwing an error.
//Warnings: These are occurrances that could pose problems, but are not worth crashing the program over. (e.g. not finding a literal file)
//Info: Debugging information that is typically not displayed unless one of the debug modes is enabled.
//BONUS FEATURES! Fun stuff!
//These operators have been added to assignment:
//	Comparisson operators: Each evaluates to 1.0 if the two operands pass the given test. Tests work the same as in C++.
// 		==
//		!=
//		<
//		>
//		<=
//		>=
//	Logical operators:
//		& : Evaluates to 1 if both operands are nonzero - 0 otherwise.
//		| : Evaluates to 1 if either operand is nonzero - 0 otherwise.
//		! : Evaluates to 1 if the operand immediately following is zero.
//	Floor operator:
//		_ : Evaluates to the integer part of the operand immediately following
//	Modulo operator:
//		% : Evaluates to the difference between the first operand and the highest multiple of the second operand that is less than the first

//Command Line Flags/Execution Modes
//-z: Default Zeroes. Normally, when an uninitialized memory location is accessed, an error is thrown. Setting this flag will override this behavior, instead causing uninitialized memory addresses to return the number 0.0.
//-c: Compact Writes. Normally, the cdump, awrite, and write commands output one value on a line. Setting this will instead cause these functions to output a number of items per line equal to the LINE_ITEMS constant, defined below.
//-w: No-Warn. Suppresses warning messages.
//-d: Normal Debug Mode. Causes each execution function to output its name as it is executed.
//-D: Big Debug Mode. Causes each execution function to output as much data as it can about its execution.
//-u: Dump on Error. On a successful execution, the final state of the core file is written back to the file it was read from. On a failed execution, the core file is left alone. Since it might be useful for debugging to know the state of core when an error occurs, setting this flag will cause the Error function to create a .dump file, containing the contents of both the Core and Literal Core.

#include "Executor.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include "Earead.h"
#include "Eassign.h"
#include "Eawrite.h"
#include "Ecdump.h"
#include "Ecls.h"
#include "Edim.h"
#include "Egoto.h"
#include "Eifa.h"
#include "Eif.h"
#include "Elisto.h"
#include "Eloop.h"
#include "Eloopend.h"
#include "Elread.h"
#include "Elwrite.h"
#include "Enop.h"
#include "Eread.h"
#include "Estop.h"
#include "Esubp.h"
#include "Ewrite.h"

//*Global variables listed in header*//
const int LINE_ITEMS = 8; //NOTE: Don't make this less than 1.
const int CLS_LINES = 50; //NOTE: Don't make this one less than 0.

const string DUMPEXT = ".dump"; //File extension for the dump files.

//Mode flags
bool defaultZeroes = false;
bool compactWrites = false;
bool noWarn = false;
bool debug = false;
bool bigDebug = false;
bool dumpOnError = false;

bool stopReached = false;

string filestem; //The stem of all files used in this execution
vector<vector<int>> code; //Used to store the read-in object code
const vector<vector<int>>& coderef = code; //const-reference so functions can see it
Core core; //The core memory
const Core& core_ = core;
LiteralCore literal; //The literal memory

int programCounter; //Represents what line number will be executed next.

//Function pointer for the e-functions
typedef void (*ExecFunc)(vector<int>);

map<int,ExecFunc> execFuncs
{
	{0,edim},
	{1,eread},
	{2,ewrite},
	{3,estop},
	{5,ecdump},
	{6,elisto},
	{7,enop},
	{8,egoto},
	{10,eifa},
	{11,earead},
	{12,eawrite},
	{13,esubp},
	{14,eloop},
	{15,eloopend},
	{16,elread},
	{17,elwrite},
	{18,eif},
	{19,ecls},
	{20,eassign}
};

//Do file handling to open and read the object file, core file, and literal file.
//Error if object or core doesn't exist or if core is bad, Warn if literal doesn't exist
void loadTransyFiles(string stem);

//Save all data back to the files you got it from.
void saveTransyFiles(string stem);

int main(int argc, char** argv)
{
	//***Setup, Command-line parsing***//
	char c;
	int i;
	string cmdLineFilename;
	//Set default file name if no parameters were given
	if(argc==1)
		cmdLineFilename = DEFAULTFILENAME;
	else
	{
		while(--argc>0&&(*++argv)[0]=='-')
			while(c=*++argv[0])
				switch(c)
				{	//The flags are kept in Compiler.h
					case 'c':
						cout << "Using compact writes" << endl;
						compactWrites = true;
						break;
					case 'd':
						cout << "Showing debug output" << endl;
						debug = true;
						break;
					case 'D':
						cout << "Showing thorough debug output" << endl;
						bigDebug = true;
						debug = true;
						break;
					case 'u':
						cout << "Will dump core on error" << endl;
						dumpOnError = true;
						break;
					case 'w':
						cout << "Suppressing warnings" << endl;
						noWarn = true;
						break;
					case 'z':
						cout << "Uninitialized values default to 0" << endl;
						defaultZeroes = true;
						break;
					default:
						cout << "Warning: Unrecognized command-line flag: " << c << endl;
				}
		cmdLineFilename = *argv;
		//Also set default file name if options were given but no filename
		if(cmdLineFilename == "") cmdLineFilename = DEFAULTFILENAME;
	}
	filestem = getFileStem(cmdLineFilename);
	loadTransyFiles(filestem);
	cin.precision(FLOAT_PRECISION);
	cout.precision(FLOAT_PRECISION);
	
	//***Execution***//
	programCounter = 0;
	vector<int> line;
	while(!stopReached)
	{
		line = code[programCounter++];
		execFuncs[line[0]](line);
	}

	//***Cleanup***//
	saveTransyFiles(filestem);
}

void loadTransyFiles(string stem)
{
	string objFilename = stem+OBJEXT, coreFilename = stem+COREEXT, literalFilename = stem+LITERALEXT;
	ifstream objFile(objFilename), coreFile(coreFilename), literalFile(literalFilename);
	
	if(!objFile.is_open())
		Error("Could not open object file "+objFilename);
	if(!coreFile.is_open())
		Error("Could not open core file "+coreFilename);
	
	string line;
	int n;
	vector<int> codeLine;
	codeLine.push_back(0);
	code.push_back(codeLine);
	while((line = readLine(objFile))!="" && objFile.good())
	{
		stringstream splitter(line);
		codeLine = vector<int>();
		//while((splitter >> n) && splitter.good())
		while(splitter.good())
		{
			splitter >> n;
			codeLine.push_back(n);
		}
		code.push_back(codeLine);
	}
	/*for(auto i: code)
	{
		for(auto j: i)
			//cout << j << " " << flush;
		//cout << endl;
	}*/
	
	core.load(coreFile);
	if(!core.isValid())
		Error("Core is invalid");
	
	if(!literalFile.is_open())
		Warn("Could not open literal file "+literalFilename);
	else
		literal.load(literalFile);

	objFile.close();
	coreFile.close();
	literalFile.close();
}

void saveTransyFiles(string stem)
{
	string coreFilename = stem+COREEXT, literalFilename = stem+LITERALEXT;
	core.save(coreFilename);
	literal.save(literalFilename);
}

core_val coreRetrieve(core_addr addr)
{
	if(core.isInit(addr))
		return core.get(addr);
	else if(defaultZeroes)
		return 0.0;
	else
		Error("Tried to read uninitialized value");
}
void coreStore(core_addr addr, core_val value)
{
	core.set(addr,value);
}

string literalRetrieve(core_addr addr)
{
	return literal.get(addr);
}
void literalStore(core_addr addr, string value)
{
	literal.set(addr,value);
}

void Error(string message)
{
	cout << "ERROR: " << message << endl;
	if(dumpOnError)
	{
		cout << "(dumping core)" << endl;
		string dumpFilename = filestem+DUMPEXT;
		ofstream dump(dumpFilename);
		if(!dump.is_open()) cout << "Couldn't open dump file" << endl;
		dump << "//*****************************//" << endl;
		dump << "//**********CORE DUMP**********//" << endl;
		dump << "//*****************************//" << endl;
		core.save(dump);
		dump << endl << endl << endl;
		dump << "//****************************//" << endl;
		dump << "//**********LITERALS**********//" << endl;
		dump << "//****************************//" << endl;
		literal.save(dump);
		dump.close();
	}
	exit(1);
}
void Warn(string message)
{
	if(!noWarn)
		cout << "WARNING: " << message << endl;
}
void Info(string message,bool onlyOnBig)
{
	if(debug&&(!onlyOnBig||onlyOnBig&&bigDebug))
		cout << "INFO: " << message << endl;
}
void Info(string message)
{
	Info(message,false);
}

double inputDouble()
{
	double result = 0.0;
	string input = "";
	while(input=="")
		getline(cin,input);
	return atof(input.data());
	/*char c;
	bool dotReached = false;
	int decimalplaces = 0;
	while((c = cin.get()) && cin.good())
	{
		if((c<'0'||c>'9')&&c!='.'&&c!='-')
		{
			if(c=='\n') return result;
			else break;
		}
		if(c=='.')
		{
			dotReached = true;
			continue;
		}
		if(!dotReached)
		{
			result = result*10 + int(c-'0');
		}
		else
		{
			result = result + int(c-'0')*pow(10.0,--decimalplaces);
		}
	}
	string remainder;
	getline(cin,remainder);*/
	return result;
}
