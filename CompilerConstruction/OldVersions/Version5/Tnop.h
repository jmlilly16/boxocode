//file: Tnop.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

//This file defines the tnop function, used for processing lines of TRANSY code identified as NOP commands.

#ifndef TNOP
#define TNOP

#include "Compiler.h"
#include "Tokenizer.h"
#include "Keywords.h"
#include <string>

using namespace std;

//This function takes in a tokenized line of TRANSY code and interprets it as a NOP command
//Parameters:
//	A vector representing the tokenized line.
//Returns: A string representation of the object code produced by the given line.
//Preconditions: The compiler has a properly preprocessed .obj file
//Postconditions: A line of object code has been returned and any errors have been reported 
string tnop(vector<Token>);

#endif
