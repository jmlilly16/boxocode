//file: Estop.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the estop function, used by the TRANSY executor

#ifndef ESTOP
#define ESTOP

#include "Executor.h"

//This function reads a line of TRANSY code to perform a CDUMP operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void estop(vector<int> line);

#endif
