//file: Elisto.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the elisto function, used by the TRANSY executor

#ifndef ELISTO
#define ELISTO

#include "Executor.h"

//This function reads a line of TRANSY code to perform a LISTO operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void elisto(vector<int> line);

#endif
