//File: assoc.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the assoc class template
//An assoc makes an association between objects of two types.
//It can be thought of as a two-way std::map, in which for each pair each member can be accessed by referencing the other.
//For example: assoc<char,int> alphabet {{'a',1},{'b',2},{'c',3}}
//This creates an assoc called alphabet in which alphabet['a']==1 and alphabet[1]=='a' are both true.
//The [] operator is the primary method of placing and accessing elements.
//The container is built so that it can be used in three ways with range-based for loops:
//for(auto& i : someAssoc) will iterate over the underlying pair<TL,TR> objects
//for(auto& i : someAssoc.left) will iterate over the TL values, and
//for(auto& i : someAssoc.right) will iterate over the TR values.
//Both keys are unique; No two values of type TL or TR can be the same in the container

#ifndef ASSOC
#define ASSOC

#include <initializer_list>
#include <iterator>
#include <vector>
using namespace std;

//TL and TR are the two types you wish to associate - colloquially called the "left" and "right" types.
//The behavior of the class is undefined if the two types are the same.
template <class TL,class TR>
class assoc
{
	private:
	//entries holds the list of associations and is iterated over by various functions.
	vector<pair<TL,TR>> entries;
	public:
	//leftIterator defines an iterator that specifically traverses the "left" elements.
	class leftIterator : public iterator<forward_iterator_tag,pair<TL,TR>>
	{
		//Internally, the iterator is a pointer to a pair (one "association")
		pair<TL,TR>* p;
		public:
		//Various constructors
		leftIterator(pair<TL,TR>* x):p(x){}
		leftIterator(const leftIterator& other):p(other.p){}	//Useful tidbit: Objects of the same class can access each other's private members!
		leftIterator& operator++() {++p;return *this;}
		//Comparing and dereferencing the iterator does so with respect to the "left" value of the pair
		bool operator==(const leftIterator& other){return p->first==other.p->first;}
		bool operator!=(const leftIterator& other){return p->first!=other.p->first;}
		TL& operator*(){return p->first;}
	};
	//Same as leftIterator, but for the "right" elements
	class rightIterator : public iterator<forward_iterator_tag,pair<TL,TR>>
	{
		pair<TL,TR>* p;
		public:
		rightIterator(pair<TL,TR>* x):p(x){}
		rightIterator(const rightIterator& other):p(other.p){}
		rightIterator& operator++() {++p;return *this;}
		bool operator==(const rightIterator& other){return p->second==other.p->second;}
		bool operator!=(const rightIterator& other){return p->second!=other.p->second;}
		TR& operator*(){return p->second;}
	};
	/*These two structs are strange.
	 *They exist to let you perform a nice bit of syntactic sugar with the for-each loop, as mentioned above.
	 *Since the 'left' struct defines its own begin and end functions, it can be passed to the for loop instead of the entire object.
	 *This lets you define specific behavior for traversing the left elements.
	*/
	struct left
	{
		public:
		//Structs (rergettably) aren't considered members of the instances, they're members of the class.
		//Thus, to access the instance-tied variable 'entries', 'left' needs to be instanciated and given a reference to its host assoc.
		assoc* host; 
		left(assoc* host):host(host){}
		leftIterator begin(){return leftIterator(&(*(host->entries.begin())));}
		leftIterator end(){return leftIterator(&(*(host->entries.end())));}
	} left; //Both the member and the type are called 'left'. You aren't supposed to define copies of the struct.
	//See above
	struct right
	{
		public:
		assoc* host;
		right(assoc* host):host(host){}
		rightIterator begin(){return rightIterator(&(*(host->entries.begin())));}
		rightIterator end(){return rightIterator(&(*(host->entries.end())));}
	} right;
	assoc():left(0),right(0){left.host=this;right.host=this;}
	/*Assoc can be initialized with an initializer list: assoc<char,string> foo {{'a',"A"},{'b',"B"}}*/
	assoc(initializer_list<pair<TL,TR>>);
	/*
	The operator[] overloads can be used to access either end of the association by passing in either a TL or a TR.
	If the key that gets passed exists in one of the association's entries, its counterpart is returned.
	If it does not exist, the counterpart for the first entry in the association is returned (it would throw an exception, but I don't know how to do those :(. You should check yourself with contains() if you're not sure, instead)
	*/
	const TL& operator[](const TR&) const;
	const TR& operator[](const TL&) const;
	/*
	Creates an association between the first and second operands.
	If the first operand is already in the container, its counterpart is replaced with the second operand.
	If the second operand is already in the container, the function fails and returns false. (Returns true otherwise)
	If neither exists, a new entry is created.
	*/
	bool set(const TL&, const TR&);
	bool set(const TR&, const TL&);
	//Returns true of the operand already exists in the association
	bool contains(const TL&) const;
	bool contains(const TR&) const;
	//Used to make the object traversible by a for-each loop
	auto begin()->decltype(entries.begin()) {return entries.begin();}
	auto end()->decltype(entries.end()) {return entries.end();}
	//Overloads for the begin and end functions in the left and right structs
	leftIterator beginLeft(){return leftIterator(&(*(entries.begin())));}
	leftIterator endLeft(){return leftIterator(&(*(entries.end())));}
	rightIterator beginRight(){return rightIterator(&(*(entries.begin())));}
	rightIterator endRight(){return rightIterator(&(*(entries.begin())));}
};

template <class TL, class TR>
assoc<TL,TR>::assoc(initializer_list<pair<TL,TR>> list):left(this),right(this)
{
	for(auto& i: list) entries.push_back(i);
}

template <class TL, class TR>
const TL& assoc<TL,TR>::operator[](const TR& key) const
{
	for(auto& i: entries)
		if(i.second==key)
			return i.first;
	return entries.front().first;
}

template <class TL, class TR>
const TR& assoc<TL,TR>::operator[](const TL& key) const
{
	for(auto& i: entries)
		if(i.first==key)
			return i.second;
	return entries.front().second;
}

template <class TL, class TR>
bool assoc<TL,TR>::set(const TL& l, const TR& r)
{
	if(contains(r))
		return false;
	for(auto& i: entries)
		if(i.first==l)
		{
			i.second=r;
			return true;
		}
	entries.push_back(pair<TL,TR>(l,r));
	return true;
}

template <class TL, class TR>
bool assoc<TL,TR>::set(const TR& r, const TL& l)
{
	if(contains(l))
		return false;
	for(auto& i: entries)
		if(i.second==r)
		{
			i.first=l;
			return true;
		}
	entries.push_back(pair<TL,TR>(l,r));
	return true;
}

template <class TL, class TR>
bool assoc<TL,TR>::contains(const TL& key) const
{
	for(auto& i: entries)
		if(i.first==key)
			return true;
	return false;
}

template <class TL, class TR>
bool assoc<TL,TR>::contains(const TR& key) const
{
	for(auto& i: entries)
		if(i.second==key)
			return true;
	return false;
}

#endif
