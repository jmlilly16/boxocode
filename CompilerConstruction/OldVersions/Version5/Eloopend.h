//file: Eloopend.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eloopend function, used by the TRANSY executor

#ifndef ELEND
#define ELEND

#include "Executor.h"

//This function reads a line of TRANSY code to perform a LOOPEND operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
//Postcondition:
void eloopend(vector<int> line);

#endif
