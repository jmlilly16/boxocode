//file:Earead.cpp
//Author:Mason Lilly
//Last Modified:4/8/15

//This file implements the function declared in Earead.h

#include "Earead.h"

#include <iostream>

void earead(vector<int> line)
{
	if(line.size()<4)
		Error("Aread invoked with too few arguments");
	Info("Executing Aread");
	Info("Using base address of "+to_string(line[1]),true);
	int baseAddr = line[1];
	Info("Using start = variable at address "+to_string(line[2]),true);
	int start = coreRetrieve(line[2]);
	Info("...with the value "+to_string(start),true);
	Info("Using end = variable at address "+to_string(line[3]),true);
	int end = coreRetrieve(line[3]);
	Info("...with the value "+to_string(end),true);
	for(int i=start;i<end;i++)
		coreStore(baseAddr+i,inputDouble());
}
