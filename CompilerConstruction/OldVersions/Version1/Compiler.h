//file: Compiler.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file is the header for the main code file for the TRANSY compiler
//It includes prototypes for several helper functions implemented in Compiler.cpp

//TODO: Change reportError so it can simply ask a global which line and line number are being processed 

#ifndef COMPILER
#define COMPILER

#include <string>

using namespace std;

//The compiler maintains a symbol table object, which matches the names of variables found in the program with their core memory addresses.
//This function references a given symbol against the compiler's symbol table
//Parameters: <symbol>: The symbol to be looked up
//Return: The index of <symbol> in the symbol table.
//Preconditions: <symbol> is a valid, non-empty string
//Postconditions: An index representing <symbol>'s entry in the symbol table has been returned.
int lookupSymbol(string symbol);

//This is a method that can be called from anywhere within the program to announce that an error has been encountered.
//When it is called, the error will be announced to the user on standard output and an "error" flag will be set in the compiler.
//At the end of compilation, if the error flag has been set, the .obj file will be deleted.
//Because this command needs to be able to report the line and location of the error, this information should be passed into any function that might report an error.
//Parameters:
//	<errorMsg>: A string describing the error
//	<line>: The line from the .noblanks file in which the error was encountered.
//	<lineNum>: The line number in the .noblanks file on which the error was encountered. When reporting the error, this will be converted to the matching line number in the .transy file via a call to getSourceLineNum.
//Return: none
//Preconditions: none
//Postconditions: The error has been printed to standard output, and the compiler's error flag has been set.
void reportError(string errorMsg,string line,int lineNum);

//This is a method that cal be called by any of the command-parsing functions to simultaneously check the validity of and obtain the core memory address of an isolated symbol
//Symbols must start with a letter and thereafter contain only alphanumeric characters or the underscore.
//If a symbol does not meet these conditions or has a length of 0, an error is reported and an empty string is returned.
//Parameters:
//	<symbol>: The symbol to be parsed
//	<line>: The .noblanks line currently being processed (for error reporting, see reportError)
//	<lineNum> : The number of the .noblanks line being processed (for error reporting, see reportError)
//Return: The string representation of the core memory address of the symbol given, or a blank string if the symbol is invalid
//	NOTE: If an error was reported, it is moot that a blank string is returned, since the .obj file will be deleted (see reportError)
//Preconditions: none
//Postconditions: If the symbol was valid, its core memory location has been returned; Otherwise an error has been reported.
string parseSymbol(string symbol,string line,int lineNum);

//The compiler keeps an internal list of which .noblanks line numbers match up to which .transy line numbers. This funcion lets you reference that list.
//Parameters: <noBlankLineNum>: The line number of a line in the .noblanks file
//Return: The line number of the matching line in the .transy file
//Preconditions: A .transy file has been opened, preprocessed successfully, and the compiler currently has a working .obj file
//Postconditions: The matching line number has been returned
int getSourceLineNum(int noBlankLineNum);

#endif
