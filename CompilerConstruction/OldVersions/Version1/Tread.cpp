//file: Tread.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tread.h

#include "Tread.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a READ command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial R E A D characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tread(string line, int lineNum)
{
	string result = "",symbol="";
	char c = ' ';
	stringstream feed(line);

	result = "1";
	for(int i=0;i<4;i++) feed.get();
	while((c = feed.get()) && feed.good())
	{
		if(c!=',')
			symbol+=c;
		else
		{
			result+=" "+parseSymbol(symbol,line,lineNum);
			symbol="";
		}
	}
	result+=" "+parseSymbol(symbol,line,lineNum);
	return result;
}
