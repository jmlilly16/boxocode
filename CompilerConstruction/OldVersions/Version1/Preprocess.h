//file: Preprocess.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This header defines the preprocess function, which performs a number of helpful transformations on a text file to make it readable by the TRANSY compiler.
//It also defines the getFileStem function.

#ifndef PREPROCESS
#define PREPROCESS

#include <string>
#include <map>

using namespace std;

//This function isolates the file stem of a given file name. The stem is defines as all the characters preceding the first '.' of a file name, or the entire file name if no '.' exists.
//It is used by both the compiler and the preprocessor in their file operations.
//Parameters: <filename>: The filename whose stem you want isolated
//Return: A string containing the stem of <filename>
//Preconditions: none
//Postconditions: A string containing the stem of <filename> has been returned
string getFileStem(string filename);

//This struct exists only to be returned by the preprocess function.
//It contains all the results of preprocessing: Whether it worked, the name of the .obj file created, and a map representing the matching of .obj lines to .transy lines.
struct PreprocessResult
{
	bool success;
	string outputName;
	map<int,int> lineIndex;//NoBlanks line number,Source line number
};

//Because requiring "struct foo" syntax is silly.
typedef struct PreprocessResult PreprocessResult;

//This function preprocesses a .transy file to make it easily digestible by the TRANSY compiler.
//It performs the following transformations:
//-Blank lines, whitespace not in quotes, and comments (defined with C*) are removed
//-Alphabetic characters not inquotes are capitalized
//The transformed .transy file is written to a .obj file with the same stem.
//Additionally, the function builds a map mapping the line numbers of the created .noblanks file to the original .transy file
//Parameters: <filename>: Filename of the .transy file to be preprocessed (must actually end in .transy)
//Preconditions: none
//Returns/Postconditions: A struct containing result information.
//	If the preprocessing succeeded, the struct's success flag will be true and the outputName and lineIndex fields will be defined as above.
//	If an error occured, the error will be reported, the success field will be false and the values of the other two fields are undefined.
//
PreprocessResult preprocess(string filename);

#endif
