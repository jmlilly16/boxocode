//file: Compiler.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This is the main file for the TRANSY Compiler.
//It processes a .transy file, checks it for errors, and, if all goes well, produces a .obj file that can be run by the interpreter.
//While processing, the compiler creates an intermediary file called a .noblanks file, which is typically deleted at the end of the program if no errors occurred.
//The user can specify the -n command-line option to force retention of this file.

//Currently, the compiler only supports READ, WRITE, and STOP commands, plus the END statement.
//Variable names must begin with a letter and thereafter contain only alphanumeric characters and the underscore.
//Line labels are allowed but not implemented (they are simply removed by the parser).

#include <iostream>
#include <fstream>
#include <string>

#include "Compiler.h"

#include "Preprocess.h"
#include "SymbolTable.h"
#include "Tread.h"
#include "Twrite.h"
#include "Tstop.h"

//BADLINE isn't terribly important; it's from an earlier implementation, but I left it in so tildes will show up in the .obj file if something really screws up.
const static string BADLINE = "~";
//This flag will get set to true by the reportError function. If it is set when the compiler finishes, the .obj file will be deleted.
//This is so a user doesn't accidentally try to run something there was errors in, and keeps me from having to figure out what goes in the .obj file when errors occur.
bool error = false;
//This holds the line index mapping returned by the preprocessor.
map<int,int> sourceLineIndex;
//The symbol table for variable names
SymbolTable table;

//Helper function to get a line from a string, because c++'s string manipulation is annoying
string readLine(ifstream& stream)
{
	string result = "";
	char c=0;
	while((c = stream.get()) && stream.good())
	{
		if(c=='\n')
			return result;
		result+=c;
	}
	return result;
}

int main(int argc, char** argv)
{
	//Setup, Command-line parsing
	bool keepNB = false;
	char c;
	int i;
	while(--argc>0&&(*++argv)[0]=='-')
		while(c=*++argv[0])
			switch(c)
			{
				case 'n':
					keepNB = true;
					cout << "Noblanks file will be preserved." << endl;
					break;
				default:
					cout << "Warning: Unrecognized command-line flag: " << c << endl;
			}
	cout << *argv << endl;

	//Preprocessing
	//Pass the given file name to the preprocessor, make sure everything checks out.
	PreprocessResult p = preprocess(*argv);
	if(!p.success)
	{
		cout << "Preprocessing failed." << endl;
		return 1;
	}
	string noblankName = p.outputName;
	string objectFileName = getFileStem(noblankName)+".obj";
	sourceLineIndex = p.lineIndex;

	/*for(auto i = p.lineIndex.begin();i!=p.lineIndex.end();i++)
	{
		cout << i->first << "->" << i->second << endl;
	}*/
	
	//Main processing loop
	//Set up the in and out files, then parse each line and turn it into object code.
	ifstream noblankFile(noblankName.data());
	ofstream outFile(objectFileName.data());
	string inLine,outLine;
	bool end=false;
	int lineNum = 1;
	
	//Take the file one line at a time.
	//Remove line labels.
	//Check the beginning of each line for matches against all the command names.
	//When one is found, pass it to the appropriate encoder function.
	//Reports an error if no commands match.
	while((inLine = readLine(noblankFile))!=""&&!end)
	{
		cout << "Parsing Line " << lineNum << ": " << inLine << endl;
		if(inLine.find(":")!=inLine.npos)
			inLine = inLine.substr(inLine.find(":")+1);
		outLine = BADLINE;
		if(inLine.substr(0,4) == "READ")
			outLine = tread(inLine,lineNum);
		else if(inLine.substr(0,5) == "WRITE")
			outLine = twrite(inLine,lineNum);
		else if(inLine.substr(0,4) == "STOP")
			outLine = tstop(inLine,lineNum);
		else if(inLine.substr(0,3) == "END")
			end = true;
		else reportError("Unrecognized command",inLine,lineNum);
		if(!error&&!end)
		{
			outFile << outLine << endl;
		}
		lineNum++;
	}
	if(!end) reportError("Program must end with an END statement",inLine,lineNum);
	
	if(!keepNB)
	{
		remove(noblankName.data());
	}
	if(error)
	{
		remove(objectFileName.data());
	}
					
	return 0;
}

//References the compiler's symbol table for a given symbol
int lookupSymbol(string symbol)
{
	return table.lookup(symbol);
}

//Reports an error on standard output, consisting of a description and the line and line number on which the error was found.
//Also sets the error flag to tell the compiler to delete the .obj file.
void reportError(string errorMsg,string line,int lineNum)
{
	cout << "ERROR: " << errorMsg << "\n\tLine " << getSourceLineNum(lineNum) << ":" << line << endl;
	error = true;
}

//Checks the validity of a symbol according to the variable rules describe at the top of this file, then passes it to lookupSymbol.
string parseSymbol(string symbol, string line, int lineNum)
{
	//cout << "Parsing Symbol: " << symbol << endl;
	if(symbol.size()==0)
	{
		reportError("Encountered 0-length symbol",line,lineNum);
		return "";
	}
	if(symbol[0]<65||symbol[0]>90)
	{
		reportError("Symbol "+symbol+" must start with a letter",line,lineNum);
		return "";
	}
	int i=1;
	while(i<symbol.length())
	{
		if(!((symbol[i]>='A'&&symbol[i]<='Z')||(symbol[i]>='0'&&symbol[i]<='9')||symbol[i]=='_'))
		{
			reportError("Symbol "+symbol+" contains a "+symbol[i]+"; must only contain [a...Z|0...9|_]",line,lineNum);
			return "";
		}
		i++;
	}
	return to_string(lookupSymbol(symbol));
}

//References the compiler's object-to-source line number mappings (for error reporting)
int getSourceLineNum(int noblankLineNum)
{
	return sourceLineIndex[noblankLineNum];
}
