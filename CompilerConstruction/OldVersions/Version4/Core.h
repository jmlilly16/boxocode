//File: Core.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file describes the Core object.
//Core represents the block of core memory used by the TRANSY compiler and executor, represented as an array of floats.
//Eventually, this object will be able to read itself in from a file as well.
//For now, it just receives values from the compiler and saves itself to a file.

#ifndef CORE
#define CORE

#include <string>

using namespace std;

static const int CORESIZE = 1000;
static const float UNINITVAL = 0.123456789;

class Core
{
	private:
	float data[CORESIZE+1];
	public:
	Core();
	~Core();
	
	//Operator overloads to access the elements in the array.
	float& operator[](int index);
	const float& operator[](int index) const;
	
	//Called to set the final entry from 0 to 1, to indicate the file is valid for use.
	void setValid();
	//When called with a filename, saves the contents of data to that file
	void save(string);
};

#endif
