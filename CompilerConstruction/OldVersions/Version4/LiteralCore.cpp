//File: LiteralCore.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions defined in LiteralCore.h

#include "LiteralCore.h"

#include <fstream>

LiteralCore::LiteralCore(){}
LiteralCore::~LiteralCore(){}

string& LiteralCore::operator[](int index)
{
	return data[index];
}

const string& LiteralCore::operator[](int index) const
{
	return data.at(index);
}

void LiteralCore::save(string filename)
{
	ofstream file(filename);
	for(auto i: data)
		file << i.second << endl;	//Write each string stored on a line
	file.close();
}
