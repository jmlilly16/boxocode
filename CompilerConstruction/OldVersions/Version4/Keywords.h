//File: Keywords.
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the different keywords in TRANSY, and associates those keywords with their textual representations and object codes.
//Also defines the object codes for the operators used by the IF function

#ifndef KEYWORDS
#define KEYWORDS

#include <map>
#include <string>

#include "assoc.h"

enum Keyword {DIM,READ,WRITE,STOP,END,CDUMP,NOP,GOTO,LISTO,IFA,IF,THEN,AREAD,AWRITE,LREAD,LWRITE,SUBP,LOOP,LOOPEND,CLS,ASSIGN};

static const assoc<Keyword,string> kwNames = 
{
	{DIM,"DIM"},
	{READ,"READ"},
	{WRITE,"WRITE"},
	{STOP,"STOP"},
	{END,"END"},
	{CDUMP,"CDUMP"},
	{NOP,"NOP"},
	{GOTO,"GOTO"},
	{LISTO,"LISTO"},
	{IF,"IF"},
	{THEN,"THEN"},
	{IFA,"IFA"},
	{AREAD,"AREAD"},
	{AWRITE,"AWRITE"},
	{LREAD,"LREAD"},
	{LWRITE,"LWRITE"},
	{SUBP,"SUBP"},
	{LOOP,"LOOP"},
	{LOOPEND,"LOOP-END"},
	{CLS,"CLS"},
	{ASSIGN,"ASSIGN"}
};

static const assoc<Keyword,string> opCodes = 
{
	{DIM,"0"},
	{READ,"1"},
	{WRITE,"2"},
	{STOP,"3"},
	{END,"4"},
	{CDUMP,"5"},
	{NOP,"7"},
	{GOTO,"8"},
	{LISTO,"6"},
	{IF,"18"},
	{IFA,"10"},
	{AREAD,"11"},
	{AWRITE,"12"},
	{LREAD,"16"},
	{LWRITE,"17"},
	{SUBP,"13"},
	{LOOP,"14"},
	{LOOPEND,"15"},
	{CLS,"19"},
	{ASSIGN,"20"}
};

static const map<string,string> compCodes =
{
	{"=","0"},
	{"<","1"},
	{">","2"},
	{"!=","3"},
	{"<=","4"},
	{">=","5"}
};

#endif
