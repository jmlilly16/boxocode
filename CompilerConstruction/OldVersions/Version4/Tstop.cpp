//file: Tstop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tstop function, described in Tstop.h

#include "Tstop.h"
#include <sstream>
#include <iostream>

string tstop(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to STOP");
	return result;
}
