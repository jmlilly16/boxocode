//File: StringDFA.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the StringDFA class, a subclass of the special case of the DFA object where the alphabet type is characters.
//This class enhances the all-at-once constructor so that types of values can be specified in a connection specification, such as NUM and ALPHANUM. This makes building an object of this type easier.

#ifndef STRINGDFA
#define STRINGDFA

#include "DFA.h"

//Enum to represent the four types of connections that can be given:
//SINGLE - just the character specified. All other types override the given character.
//NUM - the digits 0-9
//ALPHANUM - the digits 0-9, and the letters a-z and A-Z
//ALPHA - the letters a-z and A-Z
enum ConnType {SINGLE,NUM,ALPHANUM,ALPHA};

//Defines the class.
//StringDFA is itself a template - its idType is still variable.
//The class that it extends is the case of DFA with the given idType and char as the alphabet type.
template <class idType>
class StringDFA : public DFA<idType,char>
{
	public:
	//This subclass inherits from the superclass's state_spec struct.
	//The reason for redeclaring the struct here is to redefine the conn_spec struct, which must change to hold the new ConnType field.
	struct sdfa_state_spec : public DFA<idType,char>::state_spec
	{
		//redefinition of the conn_spec struct, inherited from DFA<idType,char>::state_spec.
		//This struct also inherits from its counterpart in the superclass, acquiring the trigger value and target id.
		//It also adds a field of type ConnType, to let the user specify whether this is to be a connection on a single character or a group of connections of a certain class.
		struct conn_spec : public DFA<idType,char>::state_spec::conn_spec
		{
			ConnType type;
			//Redeclarations of the constructors
			conn_spec(){}
			//Note that type defaults to SINGLE.
			conn_spec(char c, idType i, ConnType t = SINGLE)
				:DFA<idType,char>::state_spec::conn_spec(c,i),type(t)
			{}
		};
		//Redeclaration so this version will use the new conn_spec rather than the one it would inherit from its base class
		vector<conn_spec> conns;
		//Redeclarations of the constructors
		sdfa_state_spec(){}
		sdfa_state_spec(idType i, bool a, vector<conn_spec> c = vector<conn_spec>())
			:DFA<idType,char>::state_spec(i,a),conns(c)
		{}
		sdfa_state_spec(idType i, bool a, idType d, vector<conn_spec> c = vector<conn_spec>())
			:DFA<idType,char>::state_spec(i,a,d),conns(c)
		{}
	};
	StringDFA(){}
	StringDFA(vector<sdfa_state_spec>, idType, idType);
	
	vector<typename DFA<idType,char>::state_spec> expandSpecs(vector<sdfa_state_spec>);
};

/*
Modified version of the all-at-once constructor.
It does very little work itself.
It accepts a vector of the new sdfa_state_spec structs, then passes it into the expandSpecs function, which turns it into a vector of the old state_spec structs. It then passes it to the base class's constructor, which constructs the object as usual.
*/
template <class idType>
StringDFA<idType>::StringDFA(vector<typename StringDFA<idType>::sdfa_state_spec> list, idType startID, idType errorID)
	:DFA<idType,char>(expandSpecs(list),startID,errorID)
{}

/*
This function is the key part of the construction process.
It takes in a vector of the new sdfa_state_spec structs, analyzes them, and returns an array of regular state_spec objects which can be passed to a DFA<idType,char> constructor.
See the inline comments for specifi explanation of the process.
In general, for each state that has a connection with a ConnType of anything other than SINGLE, that connection translates into a group of connections as defined up by the ConnType enum.

Parameters:
	<list>: a vector of the sdfa_state_spec objects
Returns:
	a vector of state_spec objects representing the properly-expanded sdfa_state_specs.
*/
template <class idType>
vector<typename DFA<idType,char>::state_spec> StringDFA<idType>::expandSpecs(vector<typename StringDFA<idType>::sdfa_state_spec> list)
{
	vector<typename DFA<idType,char>::state_spec> new_specs;
	//Loop through the list given
	for(typename StringDFA<idType>::sdfa_state_spec i: list)
	{
		//For each sdfa_state_spec given, make a state_spec to be its equivalent.
		typename DFA<idType,char>::state_spec s(i.id,i.accept);
		if(i.useDefTrans)
		{
			s.useDefTrans=true;
			s.def = i.def;
		}
		//Loop through the sdfa_state_spec's connection list
		for(typename StringDFA<idType>::sdfa_state_spec::conn_spec j: i.conns)
		{
			//For each one, if it's a SINGLE connnection, add an equivalent connection to the new state_spec's connection list
			//If it's anything else, add the appropriate group of connections to the new state_spec's connection list.
			switch(j.type)
			{
				case SINGLE:
					s.conns.push_back({j.trigger,j.nextState});
					break;
				case NUM:
				case ALPHANUM:
					for(char c = '0';c<='9';c++)
						s.conns.push_back({c,j.nextState});
					if(j.type==NUM) break;
				case ALPHA:
					for(char c = 'a';c<='z';c++)
						s.conns.push_back({c,j.nextState});
					for(char c = 'A';c<='Z';c++)
						s.conns.push_back({c,j.nextState});
			}
		}
		new_specs.push_back(s);
	}
	return new_specs;
}

#endif
