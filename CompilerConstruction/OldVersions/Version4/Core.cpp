//File: Core.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions described in the file Core.h

#include "Core.h"

#include <fstream>

Core::Core()
{
	//Set everything to 'uninitialized'
	for(int i=0;i<CORESIZE;i++)
		data[i]=UNINITVAL;
	data[CORESIZE] = 0;
}

Core::~Core()
{}

float& Core::operator[](int index)
{
	return data[index];
}

const float& Core::operator[](int index) const
{
	return data[index];
}

void Core::setValid()
{
	data[CORESIZE] = 1;	//Mark the last entry to signify the core goes with a successful compilation
}

void Core::save(string filename)
{
	ofstream file(filename);
	for(int i=0;i<=CORESIZE;i++)
		file << data[i] << endl;	//Output each number sequentially on a line.
	file.close();
}
