//file: Tawrite.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the taread function, described in Tawrite.h

#include "Tawrite.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a AWRITE command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial A W R I T E characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tawrite(vector<Token> tokens)
{
	string result = "";
	result += opCodes[kwNames[tokens[0].text]];
	if(tokens.size()<6)
		reportError("Too few arguments given to AWRITE");
	
	result+=" ";
	result += verifyLexCat(tokens[1],{VAR,KEYWORD})?
		lookupSymbol(tokens[1].text) : BADLINE;
	
	verifyLexCat(tokens[2],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[3],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[3].text) : BADLINE;
	
	verifyLexCat(tokens[4],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[5],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[5].text) : BADLINE;
	
	if(tokens.size()>6)
		reportError("Too many arguments given to AWRITE");
	return result;
}
