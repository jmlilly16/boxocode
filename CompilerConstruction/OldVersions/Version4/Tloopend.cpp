//file: Tloopend.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tloopend function, described in Tloopend.h

#include "Tloopend.h"
#include <sstream>
#include <iostream>

string tloopend(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to LOOPEND");
	return result;
}
