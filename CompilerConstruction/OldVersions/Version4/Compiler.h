//file: Compiler.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

//This file is the header for the main code file for the TRANSY compiler
//It includes prototypes for several helper functions implemented in Compiler.cpp and used by files all over the program.
//It also contains links to the compiler-wide mode flags, plus several other helpful constants.

#ifndef COMPILER
#define COMPILER

#include "Tokenizer.h"

#include <string>

using namespace std;

//**CONSTANTS**//

//Default file stem if no filename is given
const string DEFAULTFILENAME = "test";
//Extension for TRANSY files
const string TRANSYEXT = ".transy";
//Extension for NOBLANKS files
const string NOBLANKSEXT = ".noblanks";
//Extension for OBJECT files
const string OBJEXT = ".obj";
//Extension for CORE files
const string COREEXT = ".core";
//Extension for LITERAL files
const string LITERALEXT = ".literal";

//BADLINE is used by all the parser functions as a placeholder output for a value that couldn't be determined due to an error
const static string BADLINE = "~";

//**GLOBAL VARIABLES**//

//MODE FLAGS//
//Defined in Compiler.cpp.
//These are set early in compilation as the command line arguments are parsed.
//Each corresponds to some special mode for compilation. 

extern bool alternateComment;	//Setting this causes the preprocessor to use "//" to denote comments
				//This is useful if the default value of "C*" is causing problems.
extern bool interactive;	//Runs the compiler in an "interactive mode". Only for users that know what they're doing.
				//In this mode, as the compiler runs, if it encounters an error on a given line,
				//it offer to let the user to retype the line and attempt to recompile it.
				//This has certain limitations:
				//	-Lines must me re-entered WITHNOSPACESANDINALLCAPS, since they will not be re-passed through the preprocessor
				//	-Line labels cannot be changed in this way. By the time the code reaches this step of compilation, labels have been stripped away and logged in the labelTable.
				//	-This will not change the program's source code. It is mainly for working out syntactic errors.
				//	-Since the user is allowed to skip retyping an error line, retyping a line will not clear the compilation's error flag. Thus, this option must be used with -o in order to produce executable code.
				//	-Lines cannot be added or removed from the program in this way
				//	(A line can be effectively removed by replacing it with NOP)
extern bool keepNB;		//Causes the compiler to preserve the noblanks file after compilation
extern bool keepObj;		//Causes the compiler to preserve the object file even if compilation fails
extern bool skipPrep;		//Skips the comment-and-space removal and letter-capitalization steps of preprocessing
extern bool tokenLines;		//Causes lines to be output by standard output and by error messages as tokens instead of plain text
extern bool quiet;		//Normally the compiler outputs each line before it is parsed. This suppresses that.

//**FUNCTION PROTOTYPES AND DESCRIPTIONS**//

//The compiler maintains a symbol table object, which matches the names of variables found in the program with their core memory addresses.
//This function references a given symbol against the compiler's symbol table.
//Parameters: 	<symbol>: The symbol to be looked up
//		<size>: Defaults to 1. This can be used when looking up a symbol that is known to be an array, so that if this is its first time being referenced sufficient size will be reserved in the table.
//Return: The string of the index of <symbol> in the symbol table.
//Preconditions:
//	<symbol> is a valid, non-empty string; <size> is positive
//Postconditions:
//	An index representing <symbol>'s entry in the symbol table has been returned and <symbol>'s size is accurately reflected in the table.
//	If the symbol represents a number (does not begin with a letter), its value has been placed in the proper place in the core object
string lookupSymbol(string symbol,int size=1);

//Referenes the line label table, either by the preprocessor to build it or by the compiler to determine line numbers.
//Parameters:
//	<label>: The text of the label to be looked up
//	<pos>:	Defaults to -1 ("do not place"). If this value is given, the label will attempt to place the line label at the given position. If -1 is given, the function is lookup-only and will report an error if the label does not exist.
//Precondition:
//	<pos> >= -1
//Postcondition:
//	An index representing the line label's corresponding object line number has been returned.
//	If the <pos> variable is given, the label has been placed at that position if it did not already exist.
string lookupLabel(string label,int pos=-1);

//References the literal table - similar to the symbol table but holds addresses in the literal core.
//Parameters:
//	<literal>: The literal to be looked up, either a $LITERAL or a "QUOTE"
//Returns:
//	The index of the given literal in the literal table
//Postcondition:
//	The index has been returned
string lookupLiteral(string literal);

//This function can be used to simply check if a symbol EXISTS in the symbol table, without creating an entry for it or looking up its location.
//Parameters:	<symbol>: The symbol to be checked
//Return: True if <symbol> exists in the table, false otherwise
//Precondition: <symbol is a valid, non-empty string
//Postcondition: True has been returned if <symbol> exists in the table, false otherwise.
bool symbolExists(string symbol);

//This function can be used to simply check if a label EXISTS in the label table, without creating an entry for it or looking up its location.
//Parameters:	<label>: The label to be checked
//Return: True if <label> exists in the table, false otherwise
//Precondition: <label> is a valid, non-empty string
//Postcondition: True has been returned if <label> exists in the table, false otherwise.
bool labelExists(string label);

//This function can be used to simply check if a literal EXISTS in the literal table, without creating an entry for it or looking up its location.
//Parameters:	<literal>: The literal to be checked
//Return: True if <literal> exists in the table, false otherwise
//Precondition: <literal> is a valid, non-empty string
//Postcondition: True has been returned if <literal> exists in the table, false otherwise.
bool literalExists(string literal);

//This is a function that can be called from anywhere within the program to announce that an error has been encountered.
//When it is called, the error will be announced to the user on standard output and an "error" flag will be set in the compiler.
//This function uses the currentLine and currentLinenum variables to report the location of the error; be sure to keep these up-to-date.
//At the end of compilation, if the error flag has been set, the .obj file will be deleted.
//If the tokenLines flag has been set, this function will tokenize the currentLine before printing it.
//Parameters:
//	<errorMsg>: A string describing the error
//Return: none
//Preconditions: currentLine and currentLinenum are properly set to the current line and line number being processed.
//Postconditions: The error has been printed to standard output, and the compiler's error flag has been set.
void reportError(string errorMsg);

//This is an overload of reportError to compensate for an infinite loop that resulted when the tokenizer tried to report an error on a line that was already being tokenized for error-reporting purposes.
//This version never calls the tokenizer, since it receives a tokenized line in its parameter list
//void reportError(string errorMsg,vector<Token> tokenizedLine);

//The compiler keeps an internal list of which .noblanks line numbers match up to which .transy line numbers. This funcion lets you reference that list.
//Parameters: <noBlankLineNum>: The line number of a line in the .noblanks file
//Return: The line number of the matching line in the .transy file
//Preconditions: A .transy file has been opened, preprocessed successfully, and the compiler currently has a working .obj file
//Postconditions: The matching line number has been returned
int getSourceLineNum(int noBlankLineNum);

#endif
