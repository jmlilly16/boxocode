//file: Tassign.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 2/26/15

//This file implements the tassign function. It's a doozy.

#include "Tassign.h"

#include <stack>
#include <set>

//We'll be using null pointers for the transducer
typedef void (*NullFunc)();

//Stores the current token being processed
Token* input;

bool wantState;	//Do we want an identifier (true) or an operator (false)?

stack<Token> s1,s2;

//TABLE FUNCTIONS//
//A series of short functions to be called from the transducer table.
//Each does different kinds of stack manipulation.
//They are documented with their definitions below.

void S0();
void S1();
void S2();
void U1();
void U2();
void U3();
void U4();
void N();
void F1();
void F2();
void E();
void R();

//OpType is the type used by the transducer table for its indices.
//Most of its members are copies of lexical categories, with the addition of Funky (for negation), Null (for empty stacks), and Eol (for empty input)
enum OpType {Id,Eq,Plus,Minus,Mult,Div,Modulo,Lparen,Rparen,Lbrack,Rbrack,Power,Funky,Null,Eol};

//This set aids in converting lexical categories to OpTypes
//Make sure that every key in the lex2Op map is in the allowedLexCats set 
set<LexCat> allowedLexCats
{
	{KEYWORD,VAR,INT,FLOAT,EQ,PLUS,MINUS,MULTIPLY,DIVIDE,MODULO,LPAREN,RPAREN,LBRACK,RBRACK,POWER}
};
map<LexCat,OpType> lex2Op
{
	{KEYWORD,Id},
	{INT,Id},
	{FLOAT,Id},
	{VAR,Id},
	{EQ,Eq},
	{PLUS,Plus},
	{MINUS,Minus},
	{MULTIPLY,Mult},
	{DIVIDE,Div},
	{MODULO,Modulo},
	{LPAREN,Lparen},
	{RPAREN,Rparen},
	{LBRACK,Lbrack},
	{RBRACK,Rbrack},
	{POWER,Power},
	{COMMA,Funky}
};
//The opCodes for the different operators - unique to assignment statements.
map<LexCat,string> assignOps
{
	{EQ,"-1"},
	{LBRACK,"-2"},
	{POWER,"-3"},
	{MULTIPLY,"-4"},
	{DIVIDE,"-5"},
	{PLUS,"-6"},
	{MINUS,"-7"},
	{MODULO,"-8"}
};

//Function to get the OpType of whatever element is on top of the stack.
//If the stack is empty, Null is returned.
//If not, the OpType corresponsing to the LexCat of the top element is returned.
//If the LexCat does not correspond to an OpType, an error is thrown.
OpType getStackTopType();
//Function to get the OpType of whatever element is in the input pointer
//If the input pointer is null, Eol is returned.
//If not, the OpType corresponsing to the LexCat of the input element is returned.
//If the LexCat does not correspond to an OpType, an error is thrown.
OpType getInputType();

//TRANSDUCER TABLES//
//Following are two tables that make up the transducer.
//The outermost set of keys (the rows) correspond to the types of element that can be on top of the operator stack.
//The keys in the rows correspond to the possible types of input that can be received.

//The two tables are used depending on whether an operator or an identifier should generally follow in the syntax.
//(As a general rule, the two should alternate)
//The wantState variable determines which table to use, and is set by the transducer functions (S1() et al).

//Rparen is a special case stack top type. Because an Rparen will never show up naturally in the op stack (it and its Lparen counterpart are typically discarded), I use it as a safeguard against using dereferencing something that should not be dereferenced (an integer, a just-closed set of brackets or parentheses - all things that typically should be followed by an operator, just not THAT operator). 

map<OpType,map<OpType,NullFunc>> wantIDTable
{
	{Null,
		{{Id,S0},{Eq,E},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,E},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Eq,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Plus,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Minus,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Mult,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Div,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Modulo,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Lparen,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Rparen,
		{{Id,E},{Eq,E},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Lbrack,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Rbrack,
		{{Id,E},{Eq,E},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,E},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Power,
		{{Id,S1},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Funky,
		{{Id,F2},{Eq,E},{Plus,N},{Minus,F1},{Mult,E},{Div,E},{Modulo,E},{Lparen,S2},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	}
};

map<OpType,map<OpType,NullFunc>> wantOpTable
{
	{Null,
		{{Id,E},{Eq,S2},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,E},{Rparen,E},{Lbrack,S2},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Eq,
		{{Id,E},{Eq,E},{Plus,S2},{Minus,S2},{Mult,S2},{Div,S2},{Modulo,S2},{Lparen,E},{Rparen,E},{Lbrack,S2},{Rbrack,E},{Power,S2},{Eol,U3}}
	},
	{Plus,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,S2},{Div,S2},{Modulo,S2},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Minus,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,S2},{Div,S2},{Modulo,S2},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Mult,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,U1},{Div,U1},{Modulo,U1},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Div,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,U1},{Div,U1},{Modulo,U1},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Modulo,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,U1},{Div,U1},{Modulo,U1},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Lparen,
		{{Id,E},{Eq,E},{Plus,S2},{Minus,S2},{Mult,S2},{Div,S2},{Modulo,S2},{Lparen,E},{Rparen,U2},{Lbrack,S2},{Rbrack,E},{Power,S2},{Eol,E}}
	},
	{Rparen,
		{{Id,R},{Eq,R},{Plus,R},{Minus,R},{Mult,R},{Div,R},{Modulo,R},{Lparen,R},{Rparen,R},{Lbrack,E},{Rbrack,R},{Power,R},{Eol,R}}
	},
	{Lbrack,
		{{Id,E},{Eq,E},{Plus,S2},{Minus,S2},{Mult,S2},{Div,S2},{Modulo,S2},{Lparen,E},{Rparen,E},{Lbrack,S2},{Rbrack,U4},{Power,S2},{Eol,E}}
	},
	{Rbrack,
		{{Id,E},{Eq,E},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,E},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	},
	{Power,
		{{Id,E},{Eq,E},{Plus,U1},{Minus,U1},{Mult,U1},{Div,U1},{Modulo,U1},{Lparen,E},{Rparen,U2},{Lbrack,U2},{Rbrack,U4},{Power,S2},{Eol,U3}}
	},
	{Funky,
		{{Id,E},{Eq,E},{Plus,E},{Minus,E},{Mult,E},{Div,E},{Modulo,E},{Lparen,E},{Rparen,E},{Lbrack,E},{Rbrack,E},{Power,E},{Eol,E}}
	}
};

//The transducer itself is a map mapping bools to the two tables above.
map<bool,map<OpType,map<OpType,NullFunc>>> transducer
{
	{true,wantIDTable},{false,wantOpTable}
};

//Used specifically for the case when the transducer is looking for its first operand (before the =).
//Checks to make sure the identifier received was not a constant (you can't assign values to those)
//Then, calls S1 to put it on the identifier stack.
void S0()
{
	switch(input->category)
	{
		case INT:
		case FLOAT:
			reportError("Left-Hand Side of assignment must be a variable");
		default:
			break;
	}
	S1();//Complain, but do it anyway
}

//Pushes an identifier onto the identifier stack.
//If the identifier it pushes is a number, it pushes an RParen so the number doesn't get dereferenced.
//After this function, the transducer wants an operator.
void S1()
{
	s1.push(*input);
	switch(input->category)
	{
		case INT:
		case FLOAT:
			s2.push(Token(RPAREN,")"));
		default:
			break;
	}
	wantState = false;
}

//Pushes an operator into the operator stack.
//After this functino, the transducer wants an identifier.
void S2()
{
	s2.push(*input);
	wantState = true;
}

//Closes off a preceding operation of equal or higher precedence:
//Moves the top operator from the operator stack to the id stack, then calls S2.
void U1()
{
	s1.push(s2.top());
	s2.pop();
	S2();
}

//Resolves a set of parentheses.
//Moves symbols from the op stack to the id stack until an Lparen is found.
//If an Lbrack is found, that's an error because it means something like this happened: a+(b[6)]
//Both parentheses are thrown out.
//Following this function, the transducer wants an operator.
//After resolving the parentheses, it calls F2 to try to resolve negatives.
//Finally, whether a negative was resolved by F2 or not, it pushes an Rparen onto the op stack, since (2+2)[a] can't happen.
void U2()
{
	while(!s2.empty() && s2.top().category!=LPAREN)
	{
		if(s2.top().category == LBRACK)
			E();

		s1.push(s2.top());
		s2.pop();
	}
	if(!s2.empty())
		s2.pop();
	else 
		E();
		
	wantState = false;
	F2();
	s2.push(Token(RPAREN,")"));
}

//Resolves everything it can, to handle end-of-line
//Pops the entirety of the op stack to the id stack.
//If an Lparen or an Rparen are found, that's an error since it means you didn't close them.
void U3()
{
	while(s2.size()>0)
	{
		if(s2.top().category == LPAREN || s2.top().category == LBRACK)
			E();
		s1.push(s2.top());
		s2.pop();
	}
}

//Resolves a set of brackets.
//Moves symbols from the op stack to the id stack until an Lbrack is found.
//If an Lparen is found, that's an error because it means something like this happened: b[8*(6+7])
//The right bracket is thrown out, and the left bracket is pushed onto the id stack.
//Following this function, the transducer wants an operator.
//Pushes an Rparen onto the op stack since a[5][6] is wrong.
void U4()
{
	while(!s2.empty() && s2.top().category!=LBRACK)
	{
		if(s2.top().category == LPAREN)
			E();
		s1.push(s2.top());
		s2.pop();
	}
	if(!s2.empty())
	{
		s1.push(s2.top());
		s2.pop();
	}
	s2.push(Token(RPAREN,")"));
	wantState = false;
}

//Do nothing. Used to handle the "plussative" operator: a=+b. That symbol just gets thrown out.
void N()
{
}

//Used for handling the negative operator. When a MINUS is found when an identifier was expected, the minus is a negation operator.
//That operator gets pushed onto the op stack like any other operator.
//It is represented as a comma and referred to in the table as "Funky"
void F1()
{
	s2.push(Token(COMMA,","));
}

//Used for resolving the negative operator. When the Funky symbol is on top of the stack and an identifier is found, that identifier is made part of a -1*x operation, where x is the identifier.
//EXCEPTION: If the identifier is a *number*, a "-" is squeezed into the text of that number's token.
//Multiple -'s can be chained together: a=----b. Pairs of these simply cancel each other out, so multiple -1*x operations won't show up in the object code.
//While it's true that 0-x would be computationally faster, using multiplication was the only way to make negation work with parenthetical operations: a=-(b/c)
void F2()
{
	bool extraOp = false;
	while(!s2.empty()&&getStackTopType()==Funky)
	{
		s2.pop();
		extraOp=!extraOp;
	}
	if(extraOp)
	{
		if(input->category==INT || input->category==FLOAT)
		{
			input->text = "-"+input->text;
			S1();
		}
		else
		{
			S1();
			s1.push(Token(INT,"-1"));
			s1.push(Token(MULTIPLY,"*"));
			s2.push(Token(RPAREN,")"));
		}
	}
}

//When an Rparen tops the operator stack, it means: throw an error if you see a [, otherwise behave as what's under you.
//This second case is accomplished here by popping the Rparen off the op stack and recalling the transducer with the same input.
void R()
{
	s2.pop();
	transducer[wantState][getStackTopType()][getInputType()]();
}

//Generic error reporting function.
//In a perfect world, specific error messages would be reported.
void E()
{
	reportError("Error parsing assignment statement (Check your syntax)");
}

OpType getStackTopType()
{
	if(s2.size()==0)
		return Null;
	return lex2Op[s2.top().category];
}

OpType getInputType()
{
	if(input==0)
		return Eol;
	if(!allowedLexCats.count(input->category)>0)
	{
		reportError("Unrecognized token type in assignment statement: "+input->toString());
		return Null;
	};
	return lex2Op[input->category];
}

string tassign(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	
	input = 0;
	wantState = true;
	//Call the transducer with each successive token in the input string.
	for(auto i=(++tokens.begin());i!=tokens.end();i++)
	{
		input = &(*i);
		//cout << "Transducer received " << input->toString() << endl;
		transducer[wantState][getStackTopType()][getInputType()]();
	}
	//Call it one more time with Eol.
	input = 0;
	transducer[wantState][getStackTopType()][getInputType()]();
	
	//Reverse the stack
	stack<Token> reverse;
	while(!s1.empty())
	{
		reverse.push(s1.top());
		s1.pop();
	}
	string c;
	//Build the result string.
	//Convert operators to their opcodes.
	//Look up symbols in the symbol table.
	//Ignore any Rparens that may have slipped in.
	while(!reverse.empty())
	{
		c="";
		switch(reverse.top().category)
		{
			case KEYWORD:
			case VAR:
			case INT:
			case FLOAT:
				c = lookupSymbol(reverse.top().text);
				break;
			case EQ:
			case LBRACK:
			case POWER:
			case MULTIPLY:
			case DIVIDE:
			case MODULO:
			case PLUS:
			case MINUS:
				c = assignOps[reverse.top().category];
			default:
				break;
		}
		reverse.pop();
		if(c!="")
			result += " " + c;
	}
	return result;
}
