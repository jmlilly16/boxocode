//File: Tassign.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

//This file defines the tassign function, used for parsing TRANSY assignment statements.
//This is the FUN one!

#ifndef TASSIGN
#define TASSIGN

#include "Compiler.h"
#include "Tokenizer.h"
#include "Keywords.h"
#include <string>

//Parameters:
//	vector<Token>: a tokenized line of TRANSY code.
//Returns:
//	A string containing the line of object code produced by the assignment statement, ready to be written to an object file.
//Postconditions:
//	Any errors encountered during parsing have been reported.
string tassign(vector<Token>);

#endif
