//file:Preprocess.cpp

/*Mason Lilly
Compiler Construction, Winter 2015
Dr. Moorman
*/

//This file implements the functions described in Preprocess.h.
//Its main purpose is to "chew" a .transy file to make it easier for the compiler to digest.

#include <iostream>
#include <fstream>

#include "Preprocess.h"
#include "Compiler.h"

using namespace std;

//This function takes in the file name of a .transy file and attempts to open it.
//An output file is created that has the same stem as the input file, with the extention .noblanks
//The function reads the input file, character by character, and processes each one according to these rules:
//1. If the character is part of a comment, it is discarded.
//2. A character is considered part of a comment if it follows a 'C' followed immediately by a '*' and precedes the next newline.
//3. If a character is part of a quote, it is written to the output file and no other processing is done to it.
//4. A character is considered part of a quote if it lies between two matched sets of double quotes.
//5. A character can not be in both a quote and a comment. When rule 1 is active, rule 4 will be ignored; rule 2 is ignored with rule 3 is active.
//6. Alphabetic characters are capitalized unless otherwise specified.
//7. If a character is a newline ad the LAST character was a newline, it is discarded.
//8. If nothing else, if the character is neither a space nor a tab, it is written to the output file.
//9. A record matching output line numbers to input line numbers is kept, in the form of a map. This gets returned.

PreprocessResult preprocess(string filename)
{
	//Setup
	PreprocessResult result;
	result.success = false;
	string extension = "";
	//If there's an extension, write it down.
	if(filename.find(".")!=filename.npos) extension = filename.substr(filename.find("."));
	//If there WASN'T an extension, set it to TRANSYEXT and add that to the filename.
	if(extension.size()==0)
	{
		extension=TRANSYEXT;
		filename+=extension;
	}
	//Reject file names that aren't meant for the compiler
	else if(extension!=TRANSYEXT)
	{
		cout << "File must be a "<<TRANSYEXT<<" file" << endl;
		return result;
	}
	result.outputName = getFileStem(filename)+NOBLANKSEXT;
	
	cout << "Using file " << filename << endl;
	ifstream inFile(filename.data());
	if(!inFile.is_open())
	{
		cout << "Could not open file " << filename << endl;
		return result;
	}
	ofstream outFile(result.outputName.data());
	if(!outFile.is_open())
	{
		cout << "Could not open output file" << endl;
		return result;
	}
	
	//Processing
	char letter;
	bool inQuote = false, halfComment = false, inComment = false, lineStart = true; //flags set by various special characters
	int sourceLine=0,outLine=0; //These ints keep track of where in the input/output files the preprocessor is.
	while(inFile.get(letter))
	{
		//cout << letter;
		if(letter=='\n')
			sourceLine++;
		//If the comment flag is on, discard (do not write) characters until the newline symbol is reached.
		if(inComment)
		{
			if(letter=='\n')
			{
				inComment = false;
			}
			else continue;
		}
		//If this flag is on, the last thing we saw was a capital C. Check to see if this is the start of a comment.
		if(halfComment)
		{
			halfComment = false;
			if(letter == '*')
			{
				inComment = true;
				continue;
			}
			//If it's not, it's just a normal 'C' - put it in the output file where it belongs.
			else
			{
				outFile << "C";
				cout << "C";
			}
		}
		//Special processing for non-quoted characters
		if(!inQuote)
		{
			if(letter==' '||letter=='\t') continue; //Skip whitespace
			
			//Capitalize letters
			if(letter>='a'&&letter<='z')
			{
				letter -= 32;
			}
			//Check for the potential start of a comment
			if(letter=='C')
			{
				halfComment = true;
				continue;
			}
			//If a line contains no non-whitespace characters before the next newline, don't bother printing it.
			//Also, if you've printed a character since the last newLine, mark that you're no longer at the start of a line.
			if(lineStart)
			{
				lineStart = false;
				if(letter=='\n')
				{
					lineStart = true;
					continue;
				}
			}
		}
		//If the character has made it this far without being skipped, write it to the output file
		outFile << letter;
		//cout << letter;
		//Check for quote starts/ends.
		if(letter=='"')
		{
			inQuote=!inQuote;
		}
		//Check for the end of a line
		if(letter=='\n')
		{
			lineStart = true;
			outLine++;
			result.lineIndex[outLine] = sourceLine;
		}
	}
	
	//Cleanup
	inFile.close();
	outFile.close();
	result.success = true;
	return result;
}

PreprocessResult preprocess_min(string filename)
{
	//Setup
	PreprocessResult result;
	result.success = false;
	string extension = "";
	//If there's an extension, write it down.
	if(filename.find(".")!=filename.npos) extension = filename.substr(filename.find("."));
	//If there WASN'T an extension, set it to NOBLANKSEXT and add that to the filename.
	if(extension.size()==0)
	{
		extension = NOBLANKSEXT;
		filename+=extension;
	}
	bool alreadyNoblanks = extension==NOBLANKSEXT;
	//Reject files not meant for the compiler
	if(!(extension==TRANSYEXT||alreadyNoblanks))
	{
		cout << "File must be a "<<TRANSYEXT<<" or "<<NOBLANKSEXT<<" file" << endl;
		return result;
	}
	//The NOBLANKS file to return is the one you have already if it's a noblanks file. Otherwise, make the filename for one.
	if(alreadyNoblanks)
		result.outputName = filename;
	else
		result.outputName = getFileStem(filename)+NOBLANKSEXT;
	
	cout << "Using file " << filename << endl;
	ifstream inFile(filename.data());

	if(!inFile.is_open())
	{
		cout << "Could not open file " << filename << endl;
		return result;
	}

	string outFilename = result.outputName+(alreadyNoblanks?"foo":"");
	ofstream outFile(outFilename.data());
	if(!alreadyNoblanks&&!outFile.is_open())
	{
		cout << "Could not open output file" << endl;
		return result;
	}
	char letter;
	int sourceLine=0;
	while(inFile.get(letter))
	{
		if(letter=='\n')
		{
			sourceLine++;
			result.lineIndex[sourceLine] = sourceLine;
		}
		if(!alreadyNoblanks)
			outFile << letter;
	}
	inFile.close();
	if(!alreadyNoblanks)
		outFile.close();
	else
		remove(outFilename.data());
	result.success = true;
	return result;
}
//Helper function to isolate the stem of a file
string getFileStem(string filename)
{
	string stem = "";
	int i=0;
	while(i<filename.size()&&filename[i]!='.')
	{
		stem+=filename[i++];
	}
	return stem;
}
