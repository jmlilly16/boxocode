//file: Tawrite.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the taread function, described in Tawrite.h

#include "Tawrite.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a AWRITE command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial A W R I T E characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tawrite(string line, int lineNum)
{
	string result = "",symbol="";
	char c = ' ';
	bool foundName = false;
	int argCount = 0;
	stringstream feed(line);

	result = "11";
	for(int i=0;i<6;i++) feed.get();
	while((c = feed.get()) && feed.good())
	{
		if(c!=',')
			symbol+=c;
		else
		{
			if(!foundName)
			{
				result+=" "+to_string(lookupSymbol(parseSymbol(symbol,line,lineNum)));
				foundName=true;
			}
			else
			{
				result+=" "+to_string(lookupSymbol(parseNumber(symbol,line,lineNum)));
			}
			symbol="";
			if(++argCount>2)
			{
				reportError("Too many arguments given to AWRITE",line,lineNum);
				return result;
			}
		}
	}
	result+=" "+to_string(lookupSymbol(parseNumber(symbol,line,lineNum)));
	return result;
}
