//file: SymbolTable.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the SymbolTable object described in SymbolTable.h.

#include "SymbolTable.h"

SymbolTable::SymbolTable()
	:nextLocation(0)
{}
SymbolTable::~SymbolTable()
{}
//This function scans through the underlying vector looking for the given string (symbol).
//If it finds it, it returns the index at which it was found.
//If it doesn't, the string is added to the end of the vector and its index is returned.
int SymbolTable::lookup(string entry,int size)
{
	if(data.count(entry)==1)
		return data[entry];
	else
	{
		data[entry] = nextLocation;
		nextLocation+=size;
		return data[entry];
	}
}

bool SymbolTable::contains(const string entry) const
{
	return data.count(entry)==1;
}
