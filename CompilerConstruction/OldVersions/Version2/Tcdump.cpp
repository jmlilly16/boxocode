//file: Tcdump.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tread.h

#include "Tcdump.h"
#include <sstream>
#include <iostream>
#include <string>
using namespace std;

//This function takes in a line of TRANSY code identified as a CDUMP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial C D U M P characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tcdump(string line, int lineNum)
{
	string result = "",symbol="";
	bool foundComma = false;
	char c = ' ';
	stringstream feed(line);

	result = "5";
	for(int i=0;i<5;i++) feed.get();
	while((c = feed.get()) && feed.good())
	{
		if(c==',')
		{
			if(foundComma)
			{
				reportError("Too many arguments given to CDUMP",line,lineNum);
				return result;
			}
			else
			{
				result+=" "+to_string(lookupSymbol(parseNumber(symbol,line,lineNum)));
				symbol="";
				foundComma=true;
			}
		}
		else
			symbol+=c;
	}
	result+=" "+to_string(lookupSymbol(parseNumber(symbol,line,lineNum)));
	return result;
}
