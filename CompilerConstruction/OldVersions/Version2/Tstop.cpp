//file: Tstop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tstop function, described in Tstop.h

#include "Tstop.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a STOP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial S T O P characters, another character will cause an error.
string tstop(string line, int lineNum)
{
	string result = "";
	char c = ' ';
	stringstream feed(line);
	result = "3";
	
	for(int i=0;i<4;i++) feed.get();
	if(feed.get()&&feed.good())
		reportError("STOP given too many parameters",line,lineNum);
	return result;
}
