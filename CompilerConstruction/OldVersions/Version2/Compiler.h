//file: Compiler.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file is the header for the main code file for the TRANSY compiler
//It includes prototypes for several helper functions implemented in Compiler.cpp

//TODO: Change reportError so it can simply ask a global which line and line number are being processed 

#ifndef COMPILER
#define COMPILER

#include <string>

using namespace std;

//Default file stem if no filename is given
const string DEFAULTFILENAME = "test";
//Extension for TRANSY files
const string TRANSYEXT = ".transy";
//Extension for NOBLANKS files
const string NOBLANKSEXT = ".noblanks";
//Extension for OBJECT files
const string OBJEXT = ".obj";

//The compiler maintains a symbol table object, which matches the names of variables found in the program with their core memory addresses.
//This function references a given symbol against the compiler's symbol table.
//Parameters: 	<symbol>: The symbol to be looked up
//		<size>: Defaults to 1. This can be used when looking up a symbol that is known to be an array, so that if this is its first time being referenced sufficient size will be reserved in the table.
//Return: The index of <symbol> in the symbol table.
//Preconditions: <symbol> is a valid, non-empty string; <size> is positive
//Postconditions: An index representing <symbol>'s entry in the symbol table has been returned and <symbol>'s size is accurately reflected in the table.
int lookupSymbol(string symbol,int size=1);

//This function can be used to simply check if a symbol EXISTS in the table, without creating an entry for it or looking up its location.
//Parameters:	<symbol>: The symbol to be checked
//Return: True if <symbol> exists in the table, false otherwise
//Precondition: <symbol is a valid, non-empty string
//Postcondition: True has been returned if <symbol> exists in the table, false otherwise.
bool symbolExists(string symbol);

//This is a function that can be called from anywhere within the program to announce that an error has been encountered.
//When it is called, the error will be announced to the user on standard output and an "error" flag will be set in the compiler.
//At the end of compilation, if the error flag has been set, the .obj file will be deleted.
//Because this command needs to be able to report the line and location of the error, this information should be passed into any function that might report an error.
//Parameters:
//	<errorMsg>: A string describing the error
//	<line>: The line from the .noblanks file in which the error was encountered.
//	<lineNum>: The line number in the .noblanks file on which the error was encountered. When reporting the error, this will be converted to the matching line number in the .transy file via a call to getSourceLineNum.
//Return: none
//Preconditions: none
//Postconditions: The error has been printed to standard output, and the compiler's error flag has been set.
void reportError(string errorMsg,string line,int lineNum);

//This is a function that can be called by any of the command-parsing functions to check the validity of an isolated symbol
//Symbols must start with a letter and thereafter contain only alphanumeric characters or the underscore.
//If a symbol does not meet these conditions or has a length of 0, an error is reported.
//Parameters:
//	<symbol>: The symbol to be parsed
//	<line>: The .noblanks line currently being processed (for error reporting, see reportError)
//	<lineNum> : The number of the .noblanks line being processed (for error reporting, see reportError)
//Return: <symbol>
//Preconditions: none
//Postconditions: If the symbol was invalid, the proper error has been reported.
string parseSymbol(string symbol,string line,int lineNum);

//This function behaves as parseSymbol, except that it requires that all characters in <symbol> be numeric (Negative numbers are currently not handled since there are not needed by any of the currently implemented functions),
string parseNumber(string symbol,string line,int lineNum);

//The compiler keeps an internal list of which .noblanks line numbers match up to which .transy line numbers. This funcion lets you reference that list.
//Parameters: <noBlankLineNum>: The line number of a line in the .noblanks file
//Return: The line number of the matching line in the .transy file
//Preconditions: A .transy file has been opened, preprocessed successfully, and the compiler currently has a working .obj file
//Postconditions: The matching line number has been returned
int getSourceLineNum(int noBlankLineNum);

#endif
