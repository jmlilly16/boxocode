//file: Tdim.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tdim.h

#include "Tdim.h"
#include <sstream>
#include <iostream>
#include <stdlib.h>

//This function takes in a line of TRANSY code identified as a DIM command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial D I M characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tdim(string line, int lineNum)
{
	string result="",symbol="",size="";
	char c = ' ';
	int state = 1;
	stringstream feed(line);

	result = "0";
	for(int i=0;i<3;i++) feed.get();
	while((c = feed.get()) && feed.good())
	{
		if(state==1)
		{
			if(c=='[')
				state=2;
			else
				symbol+=c;
		}
		else if(state==2)
		{
			if(c==']')
			{
				symbol = parseSymbol(symbol,line,lineNum);
				if(symbolExists(symbol))
					reportError("Symbol "+symbol+" already exists",line,lineNum);
				else
					result+=" "+to_string(lookupSymbol(parseSymbol(symbol,line,lineNum),atoi(parseNumber(size,line,lineNum).data())));
				state=3;
				symbol="";
				size="";
			}
			else
				size+=c;
		}
		else if(state==3)
		{
			if(c!=',')
			{
				reportError("Expected ',' after ']'",line,lineNum);
				return result;
			}
			else
			{
				state=1;
			}
		}
	}
	if(state==1)
		reportError("Array name given with no size (format: name[size])",line,lineNum);
	else if(state==2)
		reportError("Unclosed size brackets (format: name[size])",line,lineNum);
	return result;
}
