//file: SymbolTable.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This header defines the SymbolTable class.

#ifndef SYMTABLE
#define SYMTABLE

#include <map>
#include <string>

using namespace std;

//The SymbolTable class stores strings given to it and matches them with integers.
//The first string given will be matched with 0, the second with 1, and so on.
//When the table is referenced for a given string, the integer that that string matches will be returned.
class SymbolTable
{
	private:
	map<string,int> data;
	int nextLocation;
	public:
	SymbolTable();
	~SymbolTable();
	//This function is used to reference the symbol table.
	//Parameters: <entry>: The string to be referenced
	//Returns: The corresponding integer of the referenced string
	//Preconditions: entry is a valid string
	//Postconditions: If entry already existed in the table, the index of that string in the underlying vector will be returned.
	//		Otherwise, the string will be added to the end of the underlying vector and its index will be returned.
	int lookup(string entry,int size=1);
	//This function is used to check if a symbol exists in the symbol table without trying to place it if it doesn't.
	//Parameters: <entry>: The string to be looked up
	//Returns: True if the entry exists in the table, false if not
	bool contains(const string entry) const;
};

#endif
