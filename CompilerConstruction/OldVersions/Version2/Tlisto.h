//file: Tlisto.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file defines the tstop function, used for processing lines of TRANSY code identified as STOP commands.

#ifndef TLISTO
#define TLISTO

#include "Compiler.h"
#include <string>

using namespace std;

//This function takes in a line of TRANSY code and interprets it as a LISTO command
//Parameters:
//	<line>: the line of TRANSY code to be interpreted
//	<lineNum>: the index of <line> in the working .obj file (for error reporting)
//Returns: A string representation of the object code produced by the given line.
//Preconditions: The compiler has a properly preprocessed .obj file
//Postconditions: A valid line of object code has been returned, or an error has been reported
string tlisto(string line, int lineNum);

#endif
