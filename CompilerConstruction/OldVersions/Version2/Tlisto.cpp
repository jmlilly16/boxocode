//file: Tlisto.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tlisto function, described in Tlisto.h

#include "Tlisto.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a LISTO command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial L I S T O characters, another character will cause an error.
string tlisto(string line, int lineNum)
{
	string result = "";
	char c = ' ';
	stringstream feed(line);
	result = "6";
	
	for(int i=0;i<5;i++) feed.get();
	if(feed.get()&&feed.good())
		reportError("LISTO given too many parameters",line,lineNum);
	return result;
}
