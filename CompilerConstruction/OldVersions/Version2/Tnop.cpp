//file: Tnop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tnop function, described in Tnop.h

#include "Tnop.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a NOP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial N O P characters, another character will cause an error.
string tnop(string line, int lineNum)
{
	string result = "";
	char c = ' ';
	stringstream feed(line);
	result = "7";
	
	for(int i=0;i<3;i++) feed.get();
	if(feed.get()&&feed.good())
		reportError("NOP given too many parameters",line,lineNum);
	return result;
}
