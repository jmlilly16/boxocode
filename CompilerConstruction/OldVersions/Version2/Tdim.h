//file: Tdim.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file defines the tread function, used for processing lines of TRANSY code identified as DIM commands.

#ifndef TDIM
#define TDIM

#include <string>
#include "Compiler.h"

using namespace std;

//This function takes in a line of TRANSY code and interprets it as a DIM command
//Parameters:
//	<line>: the line of TRANSY code to be interpreted
//	<lineNum>: the index of <line> in the working .obj file (for error reporting)
//Returns: A string representation of the object code produced by the given line.
//Preconditions: The compiler has a properly preprocessed .obj file and <line> is a DIM command
//Postconditions: A valid line of object code has been returned, or an error has been reported
string tdim(string line, int lineNum);

#endif
