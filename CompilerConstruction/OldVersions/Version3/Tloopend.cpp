//file: Tloopend.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tloopend function, described in Tloopend.h

#include "Tloopend.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a STOP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial S T O P characters, another character will cause an error.
string tloopend(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to LOOPEND");
	return result;
}
