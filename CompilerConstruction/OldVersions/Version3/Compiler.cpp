//file: Compiler.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last modified: 3/8/15

//This is the main file for the TRANSY Compiler.
//It processes a .transy file, checks it for errors, and, if all goes well, produces a .obj file that can be run by the interpreter.
//While processing, the compiler creates an intermediary file called a .noblanks file, which is typically deleted at the end of the program if no errors occurred.
//The compiler also produces a .core file to store the record of the program's starting core memory state.
//It also make a .literal file, similar to .core except that it has no size constraints and hold string literals.

//Syntax:
//compiler [options] [filename]
//
//If no filename is given, the filename will default to "test"
//If a filename with no stem is given, it will be interpreted as <filename>.transy
//OPTIONS:
//-n : Keep the noblanks file at the end of compilation
//-o : Keep the object file regardless of if there was an error
//-p : Suppress normal preprocessing. The preprocessor removes whitespace, comments, and blank lines - this will turn those features off.
//	Make sure you know what you're doing.
//	Note that in either mode the preprocessor will collect line label names and remove them from the file as it is processed. Feeding in a .noblanks file with line labels will cause it to lose those labels.
//	If this option is specified, an extentionless filename will default to <filename>.noblanks, and both .transy and .noblanks extensions are allowed.
//-q : Quiet. During normal operation, the compiler outputs each line's text as it parses it, labelled with its line number in the .noblanks file. This flag will suppress that, e.g. to only show errors.
//-t : whenever a line is displayed, display it as tokens rather than plaintext

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "Compiler.h"

#include "Core.h"
#include "LiteralCore.h"
#include "Keywords.h"
#include "Preprocess.h"
#include "SymbolTable.h"
#include "Tokenizer.h"
#include "Tdim.h"
#include "Tread.h"
#include "Twrite.h"
#include "Tstop.h"
#include "Tcdump.h"
#include "Tlisto.h"
#include "Tnop.h"
#include "Tgoto.h"
#include "Tif.h"
#include "Tifa.h"
#include "Taread.h"
#include "Tawrite.h"
#include "Tlread.h"
#include "Tlwrite.h"
#include "Tsubp.h"
#include "Tloop.h"
#include "Tloopend.h"
#include "Tcls.h"

//Function pointer definition to make the nifty lambda table below work.
typedef string (*ParseFunc)(vector<Token>);

//Set by the End function, and used by the main function to stop compilation.
bool endReached=false;

//Function to call whenver an END command is encountered.
//Merely sets endReached to true, which will prompt the compiler to stop.
//Precondition:
//	Should only be called when END has been found in a file
//Postcondition:
//	endReached has been set to true
string End(vector<Token> tokens)
{
	endReached = true;
	return "";
}

//Lambda table to connect Keyword categories to their parser functions.
//Used in the main parsing loop.
map<Keyword,ParseFunc> parseFuncs =
{
	{DIM,tdim},
	{READ,tread},
	{WRITE,twrite},
	{STOP,tstop},
	{CDUMP,tcdump},
	{NOP,tnop},
	{GOTO,tgoto},
	{LISTO,tlisto},
	{IF,tif},
	{IFA,tifa},
	{AREAD,taread},
	{AWRITE,tawrite},
	{LREAD,tlread},
	{LWRITE,tlwrite},
	{SUBP,tsubp},
	{LOOP,tloop},
	{LOOPEND,tloopend},
	{CLS,tcls},
	{END,End}
};

//This flag will get set to true by the reportError function. If it is set when the compiler finishes, the .obj file will be deleted.
bool error = false;

//The preprocessor returns a mapping of source line numbers to noblanks line numbers, for error reporting purposes.
//This variable holds that mapping so it can be used anywhere in the file.
map<int,int> sourceLineIndex;
//The Symbol table maps variables and some numbers to the addresses where those values will be held in core memory.
//The Literal table does the same thing but with string literals.
SymbolTable table,litTable;

//The Line Label Table is similar to the symbol table, except it holds the source line numbers that correspond to the various line labels found by the preprocessor.
LineTable labTable;

//Review the commends in the SymbolTable and LineTable files for the technical differences between the two.

//This is the compiler's representation of core memory. The core object is the same that will be used by the executor, but for the compiler's purpose it simply gets numbers stuffed into it and saves itself to a file.
Core core;

//The core, but for literals.
LiteralCore litCore;

//These globals represent the text and source line number of the line currently being processed.
//They are used by the reportError function and exist outside that function so the main loop can change their values.
string currentLine;
int currentLinenum;

//Some helper functions

//Readline gets a line of text from an ifstream, terminated by \n, and returns it as a string (without the \n).
//I wrote it because C++'s string handling methods are awkward and confusing.
//Parameters:
//	The ifstream you want to read from.
//Returns:
//	A string, representing as many characters as could be read from the stream before a \n was found.
string readLine(ifstream&);
//Causes the core object to save itself with a given filename
//Parameters:
//	The filename to be used
void saveCoreFile(string);
//Causes the literal core object to save itself with a given filename
//Parameters:
//	The filename to be used
void saveLiteralFile(string);

int main(int argc, char** argv)
{
	//***Setup, Command-line parsing***//
	char c;
	int i;
	string cmdLineFilename;
	//Set default file name if no parameters were given
	if(argc==1)
		cmdLineFilename = DEFAULTFILENAME;
	else
	{
		while(--argc>0&&(*++argv)[0]=='-')
			while(c=*++argv[0])
				switch(c)
				{	//The flags are kept in Compiler.h
					case 'n':
						keepNB = true;
						cout << "Noblanks file will be preserved." << endl;
						break;
					case 'o':
						keepObj = true;
						cout << "Object file will be preserved." << endl;
						break;
					case 'p':
						skipPrep = true;
						cout << "Skipping preprocessing." << endl;
						break;
					case 't':
						tokenLines = true;
						cout << "Showing tokenized lines." << endl;
						break;
					case 'q':
						quiet = true;
						cout << "Suppressing as-read output" << endl;
						break;
					default:
						cout << "Warning: Unrecognized command-line flag: " << c << endl;
				}
		cmdLineFilename = *argv;
		//Also set default file name if options were given but no filename
		if(cmdLineFilename == "") cmdLineFilename = DEFAULTFILENAME;
	}

	//***Preprocessing***//
	//Pass the given file name to the proper preprocessor function, and make sure everything checks out
	PreprocessResult p = (skipPrep?preprocess_min(cmdLineFilename):preprocess(cmdLineFilename));
	if(!p.success)
	{
		cout << "Preprocessing failed." << endl;
		return 1;
	}
	if(p.error)
	{
		cout << "Error during preprocessing" << endl;
		return 1;
	}
	//Make the filenames to be used by the rest of the program
	string noblankName = p.outputName;
	string stem = getFileStem(noblankName);
	string objectFileName = stem+OBJEXT;
	string coreFileName = stem+COREEXT;
	string literalFileName = stem+LITERALEXT;
	sourceLineIndex = p.lineIndex;
	labTable = p.lineTable;
	
	//***Main parsing loop***//
	//Set up the in and out files and other variables
	ifstream noblankFile(noblankName.data());
	ofstream outFile(objectFileName.data());
	string inLine,outLine;
	vector<Token> tokens;
	currentLinenum = 1;
	
	//Takes in one line of the .noblanks file at a time
	//Each line is tokenized and echoed
	//The lambda table is then used to send the tokenized line to the proper parse function.
	//Whatever that function gives back is output to the .obj file.
	//Errors Reported:
	//	Lines that don't begin with a recognized keyword
	//	Encountering the end of the file without reaching an END statement
	while((inLine = readLine(noblankFile))!=""&&!endReached)
	{
		//Holdover from an earlier version - shouldn't do anything, but I didn't have time to test taking it out.
		if(inLine.find(":")!=inLine.npos)
			inLine = inLine.substr(inLine.find(":")+1);
		outLine = BADLINE;
		currentLine = inLine;
		tokens = tokenizeLine(inLine);
		if(!quiet)
		{
			if(tokenLines)
			{
				cout << "Parsing Line ";
				for(auto i: tokens)
					cout << i.toString() << " ";
				cout << endl;
			}
			else
				cout << "Parsing Line " << currentLinenum << ": " << inLine << endl;
		}
		if(tokens[0].category!=KEYWORD)
			reportError("Line must begin with a keyword");
		else
			outLine = parseFuncs[kwNames[tokens[0].text]](tokens);
		if(!endReached)
		{
			outFile << outLine << endl;
		}
		currentLinenum++;
	}
	if(!endReached) reportError("Program must end with an END statement");
	
	
	//***Cleanup***//
	//Close the streams and remove the unnecessary files
	noblankFile.close();
	outFile.close();
	if(!keepNB)
	{
		remove(noblankName.data());
	}
	if(error&&!keepObj)
	{
		remove(objectFileName.data());
	}
	if(!error)
		core.setValid();
	core.save(coreFileName);
	litCore.save(literalFileName);
	return 0;
}

//Helper function to get a line from a string, because c++'s string manipulation is annoying
string readLine(ifstream& stream)
{
	string result = "";
	char c=0;
	while((c = stream.get()) && stream.good())
	{
		if(c=='\n')
			return result;
		result+=c;
	}
	return result;
}

//References the compiler's symbol table for a given symbol
//Puts the symbol straight into core memory if it's a number instead of a variable name.
string lookupSymbol(string symbol,int size)
{
	char c=symbol[0];
	int index = table.lookup(symbol,size);
	if((c>='0'&&c<='9')||c=='-')
		core[index] = atoi(symbol.data());
	return to_string(index);
}

//References the compiler's line label table for a given line label
string lookupLabel(string label,int pos)
{
	return to_string(labTable.lookup(label,pos));
}

//References the compiler's literal table for a given literal
//Puts the literal into the literal core if it's a quoted string instead of a literal name.
string lookupLiteral(string literal)
{
	int index = litTable.lookup(literal);
	if(literal[0]=='"')
	{
		string contents = literal;
		contents.erase(0,1);
		contents.erase(contents.size()-1,1);
		litCore[index] = contents;
	}
	else
	{
		litCore[index] = "";
	}
	return to_string(index);
}

//Checks the symbol table for the existence of a symbol
bool symbolExists(string symbol)
{
	return table.contains(symbol);
}

//Checks the label table for the existence of a line label
bool labelExists(string label)
{
	return labTable.contains(label);
}

//Checks the literal table for the existence of a literal
bool literalExists(string literal)
{
	return litTable.contains(literal);
}

//Reports an error on standard output, consisting of a description and the line and line number on which the error was found.
//Also sets the error flag to tell the compiler to delete the .obj file.
void reportError(string errorMsg)
{
	cout << "ERROR: " << errorMsg << "\n\tLine " << getSourceLineNum(currentLinenum) << ":";
	if(!tokenLines)
		cout << currentLine << endl;
	else
	{
		for(auto i: tokenizeLine(currentLine,false))
			cout << i.toString();
		cout << endl;
	}
	error = true;
}

//Overload of reportError to report a tokenized line.
void reportError(string errorMsg, vector<Token> tokens)
{
	cout << "ERROR: " << errorMsg << "\n\tLine " << getSourceLineNum(currentLinenum) << ":";
	for(auto i: tokens)
	{
		cout << i.toString();
	}
	cout << endl;
	error = true;
}

//Checks that a given token is of a specific category
bool verifyLexCat(Token token, LexCat cat)
{
	if(token.category!=cat)
	{
		reportError("Expected a "+catNames.at(cat)+" but got "+token.toString());
		return false;
	}
	return true;
}

//Checks that a given token is of one of a group of categories
bool verifyLexCat(Token token, vector<LexCat> cats)
{
	bool match = false;
	for(auto i: cats)
		if(token.category==i)
			match = true;
	if(!match)
	{
		string msg = "Expected one of ";
		for(auto i: cats)
			msg += catNames.at(i)+", ";
		msg+="but got "+token.toString();
		reportError(msg);
	}
	return match;
}

//References the compiler's object-to-source line number mappings (for error reporting)
int getSourceLineNum(int noblankLineNum)
{
	return sourceLineIndex[noblankLineNum];
}
