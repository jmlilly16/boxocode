//file: Tcls.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tcls function, described in Tcls.h

#include "Tcls.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a CLS command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial C L S characters, another character will cause an error.
string tcls(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to CLS");
	return result;
}
