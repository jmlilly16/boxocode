//file: Tnop.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tnop function, described in Tnop.h

#include "Tnop.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a NOP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial N O P characters, another character will cause an error.
string tnop(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()>1)
		reportError("Too many arguments given to NOP");
	return result;
}
