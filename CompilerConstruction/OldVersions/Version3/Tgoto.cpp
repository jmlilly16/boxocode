//file: Tgoto.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tgoto function, described in Tgoto.h

#include "Tgoto.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a READ command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial R E A D characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tgoto(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=2)
		reportError("Argument number mismatch for GOTO");
	result+=" ";
	result += verifyLexCat(tokens[1],{VAR,INT,LABEL})?
		lookupLabel(tokens[1].text) : BADLINE;
	return result;
}
