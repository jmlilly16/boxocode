#include "Tif.h"

string tif(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=7)
		reportError("Wrong number of arguments for IF");
	
	verifyLexCat(tokens[1],LPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[2],{VAR,INT})?
		lookupSymbol(tokens[2].text) : BADLINE;
	
	result+=" ";
	result += verifyLexCat(tokens[3],{EQ,GTHAN,LTHAN,NOTEQ,GTHANEQ,LTHANEQ})?
		compCodes.at(tokens[3].text) : BADLINE;
	
	result+=" ";
	result += verifyLexCat(tokens[4],{VAR,INT})?
		lookupSymbol(tokens[4].text) : BADLINE;

	verifyLexCat(tokens[5],RPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[6],{VAR,INT,LABEL})?
		lookupLabel(tokens[6].text) : BADLINE;
	
	return result;
}
