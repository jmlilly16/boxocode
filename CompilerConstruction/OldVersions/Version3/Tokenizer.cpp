//File: Tokenizer.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions defined in Tokenizer.h

#include <sstream>
#include <vector>
#include <string>
#include "Compiler.h"

#include "Tokenizer.h"

vector<Token> tokenizeLine(string line, bool checkBadTokens)
{
	vector<Token> tokens;
	char c=' ';
	stringstream feed(line);
	tokenDFA.start();
	int lastState=tokenDFA.getState();
	string symbol = "";
	bool lastTime=true;
	bool badToken=false;
	
	//Transition the DFA on each character in the string.
	//As soon as you enter the error state, check the state just before it.
	//If it was an accepting state, you've found a valid token!
	//Restart the DFA and retransition of the character that send you into error.
	//Look up the lexical category of the last accepting state on the acceptance map, and make a token object with that category and all the character's you've seen since the beginning or the last time you made a token.
	//Collect all the tokens you make and return them at the end.
	//If you find a bad token (moved to the error state from a nonaccepting state), report this as an error at the end.
	while((c = feed.get()) && (feed.good()||lastTime))
	{
		if(!feed.good()) lastTime = false;
		//cout << "DFA Transitioning from " << tokenDFA.getState() << " on " << c << " to ";
		tokenDFA.transition(c);
		//cout << tokenDFA.getState() << endl;
		if(tokenDFA.isError() || !feed.good())
		{
			if(tokenDFA.isAccept(lastState))
			{
				//cout << "Found Token: " << Token(idCats.at(lastState),symbol).toString() << endl;
				tokens.push_back(Token(idCats.at(lastState),symbol));
				symbol="";
			}
			else
			{
				//reportError("Bad Token: "+Token(BADTOKEN,symbol).toString());
				badToken = true;
				//cout << "Error: Bad Token: " << Token(BADTOKEN,symbol).toString() << endl;
				tokens.push_back(Token(BADTOKEN,symbol));
				symbol="";
			}
			//Back up and start again from the new symbol
			tokenDFA.start();
			tokenDFA.transition(c);
		}
		symbol+=c;
		lastState = tokenDFA.getState();
	}
	if(badToken&&checkBadTokens)
	{
		if(tokenLines)
			reportError("Bad Token(s)");
		else
			reportError("Bad Tokens(s)",tokens);
	}
	return tokens;
}
