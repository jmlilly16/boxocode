#include "Tsubp.h"

string tsubp(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=7)
		reportError("Wrong number of arguments for IF");
	
	if(verifyLexCat(tokens[1],VAR))
	{
		string text = tokens[1].text;
		if(text=="SIN")
			result+=" 0";
		else if(text=="COS")
			result+=" 1";
		else if(text=="EXP")
			result+=" 2";
		else if(text=="ABS")
			result+=" 3";
		else if(text=="ALG")
			result+=" 4";
		else if(text=="SQR")
			result+=" 5";
		else
			reportError("Unknown function given to SUBP: "+text);
	}
	
	verifyLexCat(tokens[2],LPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[3],{VAR,INT,FLOAT})?
		lookupSymbol(tokens[3].text) : BADLINE;
	
	verifyLexCat(tokens[4],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[5],VAR)?
		lookupSymbol(tokens[5].text) : BADLINE;
	
	verifyLexCat(tokens[6],RPAREN);
	return result;
}
