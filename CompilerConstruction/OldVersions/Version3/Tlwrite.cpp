//file: Tlwrite.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tlwrite function, described in Tlwrite.h

#include "Tlread.h"
#include <sstream>
#include <iostream>

//This function takes in a line of TRANSY code identified as a LREAD command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial R E A D characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tlwrite(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	bool state = false; //Are we looking for a comma? If not, a var
	for(auto i = ++(tokens.begin());i!=tokens.end();i++)
	{
		if(!state)
		{
			result+=" ";
			result += verifyLexCat(*i,{LITERAL,QUOTE})?
				lookupLiteral(i->text) : BADLINE;
			state=true;
		}
		else
		{
			verifyLexCat(*i,COMMA);
			state=false;
		}
	}
	if(!state)
		reportError("End of line reached unexpectedly");
	return result;
}
