//File: LiteralCore.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the LiteralCore object, a class for storing the strings handled by the TRANSY compiler and executor
//Currently, all it does is accept data and write itself to a file.

#ifndef LITCORE
#define LITCORE

#include <map>
#include <string>

using namespace std;

class LiteralCore
{
	private:
	map<int,string> data;
	public:
	LiteralCore();
	~LiteralCore();
	
	//Operator overloads the access elements of the literal core
	string& operator[](int);
	const string& operator[](int) const;
	//When called with a filename, causes the object to save its contents at that filename.
	void save(string);
};

#endif
