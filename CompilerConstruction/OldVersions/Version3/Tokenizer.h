//File: Tokenizer.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the Tokenizer, used by the TRANSY compiler to scan lines of given text and split it into tokens: units of text labelled with their lexical categories.
//It uses a DFA to parse the given string.
//It also defins Lexical Categories and their textual represendations

#ifndef TOKENIZER
#define TOKENIZER

#include <map>
#include <initializer_list>
#include <string>
#include "StringDFA.h"

//All the lexical categories currently recognized by TRANSY
enum LexCat {BADTOKEN,KEYWORD,VAR,LITERAL,LABEL,QUOTE,FLOAT,INT,COMMA,LBRACK,RBRACK,LPAREN,RPAREN,EQ,LTHAN,GTHAN,LTHANEQ,GTHANEQ,NOTEQ};

//The textual representations of the lexical categories
const map<LexCat,string> catNames = 
{
	{BADTOKEN,"BADTOKEN"},
	{KEYWORD,"KEYWORD"},
	{VAR,"VAR"},
	{LITERAL,"LITERAL"},
	{FLOAT,"FLOAT"},
	{INT,"INT"},
	{COMMA,"COMMA"},
	{LBRACK,"LBRACK"},
	{RBRACK,"RBRACK"},
	{LPAREN,"LPAREN"},
	{RPAREN,"RPAREN"},
	{EQ,"EQ"},
	{LTHAN,"LTHAN"},
	{GTHAN,"GTHAN"},
	{LTHANEQ,"LTHANEQ"},
	{GTHANEQ,"GTHANEQ"},
	{NOTEQ,"NOTEQ"},
	{LABEL,"LABEL"},
	{QUOTE,"QUOTE"}
};

//Struct to represent a single token of TRANSY code - a unit of text that fits into a single lexical category.
struct Token
{
	LexCat category;
	string text;
	Token(LexCat cat, string s)
		:category(cat),text(s)
	{}
	string toString()
	{
		string result = "";
		result += "<";
		result += catNames.at(category);
		result += ",";
		result += text;
		result += ">";
		return result;
	}
};

//This map determines which accepting states in the DFA match up with which lexical categories.
const map<int,LexCat> idCats =
{
	{1,LBRACK},
	{2,RBRACK},
	{3,LPAREN},
	{4,RPAREN},
	{5,EQ},
	{6,GTHAN},
	{7,GTHANEQ},
	{8,LTHAN},
	{9,LTHANEQ},
	{11,NOTEQ},
	{12,COMMA},
	{17,FLOAT},
	{14,INT},
	{15,INT},
	{19,LITERAL},
	{20,VAR},
	{23,KEYWORD},
	{50,KEYWORD},
	{55,KEYWORD},
	{21,VAR},
	{22,VAR},
	{24,VAR},
	{25,VAR},
	{26,VAR},
	{51,VAR},
	{27,VAR},
	{28,VAR},
	{29,VAR},
	{30,VAR},
	{31,VAR},
	{52,VAR},
	{32,VAR},
	{33,VAR},
	{34,VAR},
	{35,VAR},
	{59,VAR},
	{36,VAR},
	{37,VAR},
	{38,VAR},
	{39,VAR},
	{40,VAR},
	{41,VAR},
	{42,VAR},
	{43,VAR},
	{44,VAR},
	{45,VAR},
	{46,VAR},
	{47,VAR},
	{48,VAR},
	{49,VAR},
	{53,VAR},
	{54,VAR},
	{56,VAR},
	{57,VAR},
	{58,VAR},
	{60,LABEL},
	{62,QUOTE}
};

//The definition of the DFA used by the tokenizer.
//The outermost set of brackets is the entire initializer list.
//The first set of nested brackets is the list of all the state definitions for the states in the DFA
//Within each state is its identifier number, whether it accepts, an optional default transition, and a list of transitions it can make.
//Within each transition is the character that triggers it, the number of the target state, and an optional category that labels that transition as accepting all numbers, alphabetic characters, both, or just the character specified (the last is the default).
//Following the list of states are the id numbers of the start and error states.	
static StringDFA<int> tokenDFA
{
	{
		{0,false,
		{
			{'[',1},
			{']',2},
			{'(',3},
			{')',4},
			{'=',5},
			{'>',6},
			{'<',8},
			{'!',10},
			{',',12},
			{'-',13},
			{' ',15,NUM},
			{'0',14},
			{'$',18},
			{' ',20,ALPHA},
			{'D',21},
			{'R',24},
			{'W',27},
			{'S',31},
			{'E',34},
			{'C',36},
			{'N',40},
			{'G',42},
			{'L',45},
			{'I',49},
			{'A',51},
			{'"',61}
		}
		},
		{1,true
		},
		{2,true
		},
		{3,true
		},
		{4,true
		},
		{5,true
		},
		{6,true,
		{
			{'=',7}
		}
		},
		{7,true
		},
		{8,true,
		{
			{'=',9}
		}
		},
		{9,true
		},
		{10,false,
		{
			{'=',11}
		}
		},
		{11,true
		},
		{12,true
		},
		{13,false,
		{
			{' ',15,NUM},
			{'0',14}
		}
		},
		{14,true,
		{
			{'.',16}
		}
		},
		{15,true,
		{
			{'.',16},
			{' ',15,NUM},
			{' ',60,ALPHA}
		}
		},
		{16,false,
		{
			{' ',17,NUM}
		}
		},
		{17,true,
		{
			{' ',17,NUM}
		}
		},
		{18,false,
		{
			{' ',19,ALPHA}
		}
		},
		{19,true,
		{
			{' ',19,ALPHANUM}
		}
		},
		{20,true,
		{
			{' ',20,ALPHANUM}
		}
		},
		{21,true,
		{
			{' ',20,ALPHANUM},
			{'I',22}
		}
		},
		{22,true,
		{
			{'M',23}
		}
		},
		{23,true
		},
		{24,true,
		{
			{' ',20,ALPHANUM},
			{'E',25}
		}
		},
		{25,true,
		{
			{' ',20,ALPHANUM},
			{'A',26}
		}
		},
		{26,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{27,true,
		{
			{' ',20,ALPHANUM},
			{'R',28}
		}
		},
		{28,true,
		{
			{' ',20,ALPHANUM},
			{'I',29}
		}
		},
		{29,true,
		{
			{' ',20,ALPHANUM},
			{'T',30}
		}
		},
		{30,true,
		{
			{' ',20,ALPHANUM},
			{'E',23}
		}
		},
		{31,true,
		{
			{' ',20,ALPHANUM},
			{'T',32},
			{'U',52}
		}
		},
		{32,true,
		{
			{' ',20,ALPHANUM},
			{'O',33}
		}
		},
		{33,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{34,true,
		{
			{' ',20,ALPHANUM},
			{'N',35}
		}
		},
		{35,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{36,true,
		{
			{' ',20,ALPHANUM},
			{'L',59},
			{'D',37}
		}
		},
		{37,true,
		{
			{' ',20,ALPHANUM},
			{'U',38}
		}
		},
		{38,true,
		{
			{' ',20,ALPHANUM},
			{'M',39}
		}
		},
		{39,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{40,true,
		{
			{' ',20,ALPHANUM},
			{'O',41}
		}
		},
		{41,true,
		{
			{' ',20,ALPHANUM},
			{'P',23}
		}
		},
		{42,true,
		{
			{' ',20,ALPHANUM},
			{'O',43}
		}
		},
		{43,true,
		{
			{' ',20,ALPHANUM},
			{'T',44}
		}
		},
		{44,true,
		{
			{' ',20,ALPHANUM},
			{'O',23}
		}
		},
		{45,true,
		{
			{' ',20,ALPHANUM},
			{'I',46},
			{'R',24},
			{'W',27},
			{'O',53}
		}
		},
		{46,true,
		{
			{' ',20,ALPHANUM},
			{'S',47}
		}
		},
		{47,true,
		{
			{' ',20,ALPHANUM},
			{'T',48}
		}
		},
		{48,true,
		{
			{' ',20,ALPHANUM},
			{'O',23}
		}
		},
		{49,true,
		{
			{' ',20,ALPHANUM},
			{'F',50}
		}
		},
		{50,true,
		{
			{'A',23}
		}
		},
		{51,true,
		{
			{' ',20,ALPHANUM},
			{'R',24},
			{'W',27}
		}
		},
		{52,true,
		{
			{' ',20,ALPHANUM},
			{'B',33}
		}
		},
		{53,true,
		{
			{' ',20,ALPHANUM},
			{'O',54}
		}
		},
		{54,true,
		{
			{' ',20,ALPHANUM},
			{'P',55}
		}
		},
		{55,true,
		{
			{'-',56}
		}
		},
		{56,true,
		{
			{' ',20,ALPHANUM},
			{'E',57}
		}
		},
		{57,true,
		{
			{' ',20,ALPHANUM},
			{'N',58}
		}
		},
		{58,true,
		{
			{' ',20,ALPHANUM},
			{'D',23}
		}
		},
		{59,true,
		{
			{' ',20,ALPHANUM},
			{'S',23}
		}
		},
		{60,true,
		{
			{' ',60,ALPHANUM}
		}
		},
		{61,false,61,
		{
			{'"',62}
		}
		},
		{62,true
		}
	},
	0,
	-1
};

//The main tokenization function.
//Accepts a string line of code and uses the DFA to turn it into a vector of tokens.
//By default, it will report an error if a token not fitting any lexical category is found.
//This can be suppressed by passing in "false" to the second parameter.
vector<Token> tokenizeLine(string line,bool checkBadTokens = true);

//This function will verify that a given token fits in a given lexical category.
//If it doesn't, an error will be reported.
//Returns true if the token matches the category.
bool verifyLexCat(Token token, LexCat cat);

//This overload of the above function performs the same operation on a group of categories - true will be returned if the token matches any of the categories given 
bool verifyLexCat(Token token, vector<LexCat> cats);

#endif
