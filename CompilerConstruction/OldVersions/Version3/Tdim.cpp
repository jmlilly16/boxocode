//file: Tdim.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tdim.h

#include "Tdim.h"
#include <sstream>
#include <iostream>
#include <stdlib.h>

//This function takes in a line of TRANSY code identified as a DIM command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial D I M characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tdim(vector<Token> tokens)
{
	string result = "",symbol = "";
	int size = 0;
	result += opCodes[kwNames[tokens[0].text]];
	
	int commas = 0;
	bool format = true;
	while(true)
	{
		if(tokens.size()<5*(commas+1))
		{
			reportError("Array declaration is incomplete");
			return result; //No point in continuing now...
		}
		format = false;
		if(verifyLexCat(tokens[(5*commas)+1],VAR))
		{
			format = true;
			if(symbolExists(tokens[(5*commas)+1].text))
				reportError("Symbol "+tokens[(5*commas)+1].text+" already exists");
			else
			{
				symbol = tokens[(5*commas)+1].text;
		
				format &= verifyLexCat(tokens[(5*commas)+2],LBRACK);
		
				size = (format &= verifyLexCat(tokens[(5*commas)+3],INT))?
					atoi(tokens[(5*commas)+3].text.data()) : 0;
		
				format &= verifyLexCat(tokens[(5*commas)+4],RBRACK);
		
				result+=" ";
				result += format?
					lookupSymbol(symbol,size) : BADLINE;
				
				result+=" ";
				result += format?
					to_string(size) : BADLINE;
			}
		}
		if(tokens.size()==5*(commas+1)) break;
		verifyLexCat(tokens[(5*commas)+5],COMMA);
		commas++;
	}
	return result;
}
