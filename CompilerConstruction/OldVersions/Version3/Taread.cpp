//file: Taread.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the taread function, described in Taread.h

#include "Taread.h"
#include <sstream>
#include <iostream>

//This function parses a tokenized line of TRANSY code as an AREAD command.
string taread(vector<Token> tokens)
{
	string result = "";
	result += opCodes[kwNames[tokens[0].text]];
	if(tokens.size()<6)
		reportError("Too few arguments given to AREAD");

	result += verifyLexCat(tokens[1],VAR)?
		" "+lookupSymbol(tokens[1].text) : " "+BADLINE;
	
	verifyLexCat(tokens[2],COMMA);
	
	result += verifyLexCat(tokens[3],{VAR,INT})?
		" "+lookupSymbol(tokens[3].text) : " "+BADLINE;
	
	verifyLexCat(tokens[4],COMMA);
	
	result += verifyLexCat(tokens[5],{VAR,INT})?
		" "+lookupSymbol(tokens[5].text) : " "+BADLINE;
	if(tokens.size()>6)
		reportError("Too many arguments given to AREAD");
	return result;
}
