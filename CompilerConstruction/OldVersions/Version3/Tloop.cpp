#include "Tloop.h"

string tloop(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()!=8)
		reportError("Wrong number of arguments for IF");
	
	result+=" ";
	result += verifyLexCat(tokens[1],VAR)?
		lookupSymbol(tokens[1].text) : BADLINE;
	
	verifyLexCat(tokens[2],EQ);
	
	result+=" ";
	result += verifyLexCat(tokens[3],{VAR,INT})?
		lookupSymbol(tokens[3].text) : BADLINE;
	
	verifyLexCat(tokens[4],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[5],{VAR,INT})?
		lookupSymbol(tokens[5].text) : BADLINE;

	verifyLexCat(tokens[6],COMMA);
	
	result+=" ";
	result += verifyLexCat(tokens[7],{VAR,INT})?
		lookupSymbol(tokens[7].text) : BADLINE;
	return result;
}
