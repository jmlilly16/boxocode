//File: LiteralCore.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions defined in LiteralCore.h

#include "LiteralCore.h"

#include <fstream>
#include <iostream>

#include "TRANSY.h"	//For readLine

LiteralCore::LiteralCore(){}
LiteralCore::~LiteralCore(){}

string LiteralCore::get(core_addr addr)
{
	return data[addr];
}
void LiteralCore::set(core_addr addr, string value)
{
	data[addr] = value;
}
bool LiteralCore::isInit(core_addr addr)
{
	return data.count(addr)==1;
}

void LiteralCore::load(istream& stream)
{
	string line;
	int i=0;
	while(line = readLine(stream), stream.good())
		data[i++] = line;
}
bool LiteralCore::load(string filename)
{
	ifstream file(filename);
	if(!file.is_open())
	{
		cout << "Could not open file" << endl;
		return false;
	}
	load(file);
	return true;
}
	
void LiteralCore::save(ostream& stream)
{
	for(auto i: data)
		stream << i.second << endl;	//Write each string stored on a line
}
void LiteralCore::save(string filename)
{
	ofstream file(filename);
	save(file);
	file.close();
}
