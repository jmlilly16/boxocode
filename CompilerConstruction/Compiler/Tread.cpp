//file: Tread.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tread.h

#include "Tread.h"
#include <sstream>
#include <iostream>

string tread(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	bool state = false; //Are we looking for a comma? If not, a var
	for(auto i = ++(tokens.begin());i!=tokens.end();i++)
	{
		if(!state)
		{
			result+=" ";
			result += verifyLexCat(*i,{VAR,KEYWORD})?
				lookupSymbol(i->text) : BADLINE;
			state=true;
		}
		else
		{
			verifyLexCat(*i,COMMA);
			state=false;
		}
	}
	if(!state)
		reportError("End of line reached unexpectedly");
	return result;
}
