//file: Compiler.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last modified: 3/8/15

//This is the main file for the TRANSY Compiler.
//It processes a .transy file, checks it for errors, and, if all goes well, produces a .obj file that can be run by the interpreter.
//While processing, the compiler creates an intermediary file called a .noblanks file, which is typically deleted at the end of the program if no errors occurred.
//The compiler also produces a .core file to store the record of the program's starting core memory state.
//It also make a .literal file, similar to .core except that it has no size constraints and hold string literals.

//Syntax:
//compiler [options] [filename]
//
//If no filename is given, the filename will default to "test"
//If a filename with no stem is given, it will be interpreted as <filename>.transy
//OPTIONS:
//-c : Alternate Comment mode. Normally, the preprocessor uses "C*" as its specifier for comments.
//	If that causes problems, the user use this option to use "//" for comments instead.
//-i : Interactive mode (prototype - try it out!)
//	Runs the code in an interactive mode, which allows the user to correct syntax errors as they are found.
//	In this mode, when a line fails to parse correctly, the user will be given the option to retype the line.
//	If they do, the line will be reparsed, as many times as the user likes until a line parses correctly.
//	This feature is currently limited:
//		It will not change the source code
//		No preprocessing will be done on the line, meaning:
//			THELINEMUSTBETYPEDLIKETHIS
//			NoLabels:Can be changed or added
//		It will not clear the error flag for the compilation run, meaning the core will still be unexecutable
//			(But that's okay because there's no executor yet!)
//		Because the error flag is not cleared, this mode must be used with -o for the object file to be preserved.
//		(Eventually I will set it up so the error flag CAN be cleared)
//		Sometimes entering a correct line will still produce errors, and entering it identically a second time will be accepted. I haven't had a chance to fully investigate this problem yet.
//-n : Keep the noblanks file at the end of compilation
//-o : Keep the object file regardless of if there was an error
//-p : Suppress normal preprocessing. The preprocessor removes whitespace, comments, and blank lines - this will turn those features off.
//	Make sure you know what you're doing.
//	Note that in either mode the preprocessor will collect line label names and remove them from the file as it is processed. Feeding in a .noblanks file with line labels will cause it to lose those labels.
//	If this option is specified, an extentionless filename will default to <filename>.noblanks, and both .transy and .noblanks extensions are allowed.
//-q : Quiet. During normal operation, the compiler outputs each line's text as it parses it, labelled with its line number in the .noblanks file. This flag will suppress that, e.g. to only show errors.
//-t : whenever a line is displayed, display it as tokens rather than plaintext

//Extra Features!!!
//(aka stuff I want to make sure you see)
//-The command line arguments c,i,q, and t (see above)
//-Modulo division! The % symbol can be used in assignment statements
//Very nice tokenization!
//Handles this assignment statement: a=--+-++-+-+--+++-+++--+-+--b
//	(Disclaimer: It doesn't handle this one: a=--+-++-+-+--+++-+++--+-+---b (note extra minus))
//Completely error free!*

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>

#include "Compiler.h"

#include "Core.h"
#include "LiteralCore.h"
#include "Keywords.h"
#include "Preprocess.h"
#include "SymbolTable.h"
#include "Tokenizer.h"
#include "Tdim.h"
#include "Tread.h"
#include "Twrite.h"
#include "Tstop.h"
#include "Tcdump.h"
#include "Tlisto.h"
#include "Tnop.h"
#include "Tgoto.h"
#include "Tif.h"
#include "Tifa.h"
#include "Taread.h"
#include "Tawrite.h"
#include "Tlread.h"
#include "Tlwrite.h"
#include "Tsubp.h"
#include "Tloop.h"
#include "Tloopend.h"
#include "Tcls.h"
#include "Tassign.h"

const string BADLINE = "~";
//Extension for TRANSY files
const string TRANSYEXT = ".transy";
//Extension for NOBLANKS files
const string NOBLANKSEXT = ".noblanks";

//Function pointer definition to make the nifty lambda table below work.
typedef string (*ParseFunc)(vector<Token>);

//Set by the End function, and used by the main function to stop compilation.
bool endReached=false;

//Definitions of the global mode flags
bool alternateComment = false;
bool interactive = false;
bool keepNB = false;
bool keepObj = false;
bool skipPrep = false;
bool tokenLines = false;
bool quiet = false;

stack<int> loops;

//Function to call whenver an END command is encountered.
//Merely sets endReached to true, which will prompt the compiler to stop.
//Precondition:
//	Should only be called when END has been found in a file
//Postcondition:
//	endReached has been set to true
string End(vector<Token> tokens)
{
	endReached = true;
	return "";
}

//Lambda table to connect Keyword categories to their parser functions.
//Used in the main parsing loop.
map<Keyword,ParseFunc> parseFuncs =
{
	{DIM,tdim},
	{READ,tread},
	{WRITE,twrite},
	{STOP,tstop},
	{CDUMP,tcdump},
	{NOP,tnop},
	{GOTO,tgoto},
	{LISTO,tlisto},
	{IF,tif},
	{IFA,tifa},
	{AREAD,taread},
	{AWRITE,tawrite},
	{LREAD,tlread},
	{LWRITE,tlwrite},
	{SUBP,tsubp},
	{LOOP,tloop},
	{LOOPEND,tloopend},
	{CLS,tcls},
	{ASSIGN,tassign},
	{END,End}
};

//This flag will get set to true by the reportError function. If it is set when the compiler finishes, the .obj file will be deleted.
int errorLines = 0;
//Also gets set in reportError. This one is used by the interactive mode (see Compiler.h) to tell if the current line has an error.
//As processing of a line begins, this flag is cleared, while error will remain set so that the compilation will still fail.
bool localError = false;

//The preprocessor returns a mapping of source line numbers to noblanks line numbers, for error reporting purposes.
//This variable holds that mapping so it can be used anywhere in the file.
map<int,int> sourceLineIndex;
//The Symbol table maps variables and some numbers to the addresses where those values will be held in core memory.
//The Literal table does the same thing but with string literals.
SymbolTable table,litTable;

//The Line Label Table is similar to the symbol table, except it holds the source line numbers that correspond to the various line labels found by the preprocessor.
LineTable labTable;

//Review the commends in the SymbolTable and LineTable files for the technical differences between the two.

//This is the compiler's representation of core memory. The core object is the same that will be used by the executor, but for the compiler's purpose it simply gets numbers stuffed into it and saves itself to a file.
Core core;

//The core, but for literals.
LiteralCore litCore;

//These globals represent the text and source line number of the line currently being processed.
//They are used by the reportError function and exist outside that function so the main loop can change their values.
string currentLine;
int currentLinenum;

const string& currentLine_ = currentLine;
const int& currentLinenum_ = currentLinenum;

//Some helper functions
//Causes the core object to save itself with a given filename
//Parameters:
//	The filename to be used
void saveCoreFile(string);
//Causes the literal core object to save itself with a given filename
//Parameters:
//	The filename to be used
void saveLiteralFile(string);

//Checks the given line for certain flags (is it an assignment?) and picks the corresponding tokenizer.
//Sends the line to that tokenizer and returns the result.
vector<Token> getTokens(string line);

//Checks a string of tokens for BADTOKEN tokens and reports errors if it finds any.
void checkTokens(vector<Token>);

int main(int argc, char** argv)
{
	//***Setup, Command-line parsing***//
	char c;
	int i;
	string cmdLineFilename;
	//Set default file name if no parameters were given
	if(argc==1)
		cmdLineFilename = DEFAULTFILENAME;
	else
	{
		while(--argc>0&&(*++argv)[0]=='-')
			while(c=*++argv[0])
				switch(c)
				{	//The flags are kept in Compiler.h
					case 'c':
						alternateComment = true;
						cout << "Compiling using '//' for comments" << endl;
						break;
					case 'i':
						interactive = true;
						cout << "Compiling interactively" << endl;
						break;
					case 'n':
						keepNB = true;
						cout << "Noblanks file will be preserved." << endl;
						break;
					case 'o':
						keepObj = true;
						cout << "Object file will be preserved." << endl;
						break;
					case 'p':
						skipPrep = true;
						cout << "Skipping preprocessing." << endl;
						break;
					case 't':
						tokenLines = true;
						cout << "Showing tokenized lines." << endl;
						break;
					case 'q':
						quiet = true;
						cout << "Suppressing as-read output" << endl;
						break;
					default:
						cout << "Warning: Unrecognized command-line flag: " << c << endl;
				}
		cmdLineFilename = *argv;
		//Also set default file name if options were given but no filename
		if(cmdLineFilename == "") cmdLineFilename = DEFAULTFILENAME;
	}

	//***Preprocessing***//
	//Pass the given file name to the proper preprocessor function, and make sure everything checks out
	PreprocessResult p = (skipPrep?preprocess_min(cmdLineFilename):preprocess(cmdLineFilename));
	if(!p.success)
	{
		cout << "Preprocessing failed." << endl;
		return 1;
	}
	if(p.error)
	{
		cout << "Error during preprocessing" << endl;
		return 1;
	}
	//Make the filenames to be used by the rest of the program
	string noblankName = p.outputName;
	string stem = getFileStem(noblankName);
	string objectFileName = stem+OBJEXT;
	string coreFileName = stem+COREEXT;
	string literalFileName = stem+LITERALEXT;
	sourceLineIndex = p.lineIndex;
	labTable = p.lineTable;
	
	//***Main parsing loop***//
	//Set up the in and out files and other variables
	ifstream noblankFile(noblankName.data());
	ofstream outFile(objectFileName.data());
	string inLine,outLine;
	vector<Token> tokens;
	currentLinenum = 1;
	
	//Takes in one line of the .noblanks file at a time
	//Each line is tokenized and echoed
	//The lambda table is then used to send the tokenized line to the proper parse function.
	//Whatever that function gives back is output to the .obj file.
	//Errors Reported:
	//	Lines that don't begin with a recognized keyword
	//	Encountering the end of the file without reaching an END statement
	while((inLine = readLine(noblankFile))!=""&&!endReached)
	{
		mainloop:	//I know, I know, gotos bad. It was easier than rewriting the control flow of this whole section.
		localError = false;
		//Holdover from an earlier version - shouldn't do anything, but I didn't have time to test taking it out.
		//if(inLine.find(":")!=inLine.npos)
		//	inLine = inLine.substr(inLine.find(":")+1);
		outLine = BADLINE;
		currentLine = inLine;

		//Check syntax hints to differentiate between a loop and an assignment, to determine the right tokenizer to use.
		tokens = getTokens(inLine);
		
		//Output the line to standard output so the user can follow along, unless they told you not to.
		if(!quiet)
		{
			if(tokenLines)
			{
				cout << "Parsing Line ";
				for(auto i: tokens)
					cout << i.toString() << " ";
				cout << endl;
			}
			else
				cout << "Parsing Line: " << inLine << endl;
		}
		//Check for bad tokens
		checkTokens(tokens);
		//Check for a badly-formed line. Otherwise, find the right parse function and parse away!
		if(tokens[0].category!=KEYWORD)
			reportError("Line must begin with a keyword");
		else
			outLine = parseFuncs[kwNames[tokens[0].text]](tokens);
		//Offer to correct a line?
		if(localError&&interactive)
		{
			char input;
			bool goodinput = false;
			while(!goodinput)
			{
				cout << "Retype line? (y/n): " << flush;
				cin >> input;
				cin.ignore();
				goodinput = true;
				switch(input)
				{
					case 'y':
						errorLines--;
						getline(cin,inLine);
						goto mainloop;
					case 'n': break;
					default: goodinput = false;
				}
			}
		}
		//Send the parsed line to the object file
		if(!endReached)
			outFile << outLine << endl;
		else
			outFile << tstop({Token(KEYWORD,"STOP")}) << endl;	//Put a stop at the end of the program.
		currentLinenum++;
	}
	if(!endReached) reportError("Program must end with an END statement");
	if(!loops.empty()) reportError("Unclosed loop on line "+loops.top());
	
	//***Cleanup***//
	//Close the streams and remove the unnecessary files
	noblankFile.close();
	outFile.close();
	if(!keepNB)
	{
		remove(noblankName.data());
	}
	if(errorLines>0&&!keepObj)
	{
		remove(objectFileName.data());
	}
	if(errorLines==0)
	{
		core.setValid();
		cout << "Compilation Successful!" << endl;
	}
	core.save(coreFileName);
	litCore.save(literalFileName);
	return 0;
}

//References the compiler's symbol table for a given symbol
//Puts the symbol straight into core memory if it's a number instead of a variable name.
string lookupSymbol(string symbol,int size)
{
	char c=symbol[0];
	int index = table.lookup(symbol,size);
	if((c>='0'&&c<='9')||c=='-')
		core.set(index,atof(symbol.data()));
	return to_string(index);
}

//References the compiler's line label table for a given line label
string lookupLabel(string label,int pos)
{
	return to_string(labTable.lookup(label,pos));
}

//References the compiler's literal table for a given literal
//Puts the literal into the literal core if it's a quoted string instead of a literal name.
string lookupLiteral(string literal)
{
	int index = litTable.lookup(literal);
	if(literal[0]=='"')
	{
		string contents = literal;
		contents.erase(0,1);
		contents.erase(contents.size()-1,1);
		litCore.set(index,contents);
	}
	else
	{
		litCore.set(index,"");
	}
	return to_string(index);
}

//Checks the symbol table for the existence of a symbol
bool symbolExists(string symbol)
{
	return table.contains(symbol);
}

//Checks the label table for the existence of a line label
bool labelExists(string label)
{
	return labTable.contains(label);
}

//Checks the literal table for the existence of a literal
bool literalExists(string literal)
{
	return litTable.contains(literal);
}

//Reports an error on standard output, consisting of a description and the line and line number on which the error was found.
//Also sets the error flag to tell the compiler to delete the .obj file.
void reportError(string errorMsg)
{
	cout << "ERROR: " << errorMsg << "\n\tLine " << getSourceLineNum(currentLinenum) << ":";
	if(!tokenLines)
		cout << currentLine << endl;
	else
	{
		for(auto i: getTokens(currentLine))
			cout << i.toString();
		cout << endl;
	}
	if(!localError)
	{
		errorLines++;
		localError = true;
	}
}

//Overload of reportError to report a tokenized line.
/*void reportError(string errorMsg, vector<Token> tokens)
{
	cout << "ERROR: " << errorMsg << "\n\tLine " << getSourceLineNum(currentLinenum) << ":";
	for(auto i: tokens)
	{
		cout << i.toString();
	}
	cout << endl;
	error = true;
}*/

//Checks that a given token is of a specific category
bool verifyLexCat(Token token, LexCat cat)
{
	if(token.category!=cat)
	{
		reportError("Expected a "+catNames.at(cat)+" but got "+token.toString());
		return false;
	}
	return true;
}

//Checks that a given token is of one of a group of categories
bool verifyLexCat(Token token, vector<LexCat> cats)
{
	bool match = false;
	for(auto i: cats)
		if(token.category==i)
			match = true;
	if(!match)
	{
		string msg = "Expected one of ";
		for(auto i: cats)
			msg += catNames.at(i)+", ";
		msg+="but got "+token.toString();
		reportError(msg);
	}
	return match;
}

//References the compiler's object-to-source line number mappings (for error reporting)
int getSourceLineNum(int noblankLineNum)
{
	return sourceLineIndex[noblankLineNum];
}

vector<Token> getTokens(string line)
{
	vector<Token> tokens;
	if(line.find("=")!=line.npos && line.find(",")==line.npos && line.find("\"")==line.npos && line.find("THEN")==line.npos)
	{
		tokens = assignTokenizer.tokenizeLine(line);
		tokens.insert(tokens.begin(),Token(KEYWORD,"ASSIGN"));
		return tokens;
	}
	else
	{
		line = KEYCHAR + line;
		tokens = tokenizer.tokenizeLine(line);
		tokens[0].text = tokens[0].text.substr(1);
		return tokens;
	}
}

void checkTokens(vector<Token> tokens)
{
	for(auto i: tokens)
		if(i.category == BADTOKEN)
			reportError("Bad token: "+i.toString());
}
