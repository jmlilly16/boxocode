//File: LineTable.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//Thihs file implements the functions defined in LineTable.h

#include "LineTable.h"

#include "Compiler.h"

LineTable::LineTable()
	:lastPos(-1)
{}
LineTable::~LineTable()
{}

int LineTable::lookup(string entry, int pos)
{
	if(!contains(entry))
	{
		if(pos==-1)
		{
			reportError("Line label "+entry+" does not exist");
			return -1;
		}
		else
		{
			operator[](entry) = pos;
			return pos;
		}
	}
	else
	{
		return operator[](entry);
	}
}
