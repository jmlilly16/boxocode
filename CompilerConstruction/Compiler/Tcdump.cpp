//file: Tcdump.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the tread function, described in Tread.h

#include "Tcdump.h"
#include <sstream>
#include <iostream>
#include <string>
using namespace std;

//This function takes in a line of TRANSY code identified as a CDUMP command and encodes it into object code.
//The line is placed into a stringstream and read character-by-character.
//After the initial C D U M P characters, each non-comma character is stuffed into a string.
//When a comma is encountered, that string is considered terminated and is passed to the parseSymbol function.
//The result of this call is appended to the resulting object code.
string tcdump(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()<4)
		reportError("Too few arguments given to CDUMP");
	
	result+=" ";	//Check for INTness, and make sure it's positive.
	result += 	verifyLexCat(tokens[1],{VAR,KEYWORD,INT})?
			(
				(atoi(tokens[1].text.data())>=0)?
				lookupSymbol(tokens[1].text) : 
				(reportError("Arguments to CDUMP must be positive"),BADLINE)
			) : BADLINE;
	
	verifyLexCat(tokens[2],COMMA);
	
	result+=" ";	//Check for INTness, and make sure it's positive.
	result += 	verifyLexCat(tokens[3],{VAR,KEYWORD,INT})?
			(
				(atoi(tokens[3].text.data())>=0)?
				lookupSymbol(tokens[3].text) : 
				(reportError("Arguments to CDUMP must be positive"),BADLINE)
			) : BADLINE;
	
	if(tokens.size()>4)
		reportError("Too many arguments given to CDUMP");
	return result;
}
