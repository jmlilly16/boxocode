//file: .h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the  function, used by the TRANSY executor

#ifndef E
#define E

#include "Executor.h"

//This function 
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
//Postcondition:
//	
void e (vector<int> line);

#endif
