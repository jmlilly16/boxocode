#include "Eassign.h"

#include <cmath>

#include "assoc.h"

class AssignStack
{
	private:
	struct AssignVal
	{
		core_val val;
		bool isLVal;
		AssignVal(double v, bool i)
			:val(v),isLVal(i)
		{}
	};
	stack<AssignVal> data;
	public:
	void pushl(double val)
	{
		//cout << "Pushing " << val << " as Lvalue " << endl;
		data.push(AssignVal(val,true));
	}
	void pushr(double val)
	{
		//cout << "Pushing " << val << " as Rvalue " << endl;
		data.push(AssignVal(val,false));
	}
	double popl()
	{
		if(data.empty())
			Error("Attempted operation with no operands");
		AssignVal a = data.top();
		//cout << "Popped " << a.val << " to use as LValue" << endl;
		data.pop();
		if(!a.isLVal)
			Error("Attempted to use RValue as LValue");
		return a.val;
	}
	double popr()
	{
		if(data.empty())
			Error("Attempted operation with no operands");
		AssignVal a = data.top();
		//cout << "Popped " << a.val << " to use as RValue" << endl;	
		data.pop();
		if(a.isLVal)
		{
			//cout << "It was an LValue. Converting to RValue: " << coreRetrieve(int(a.val)) << endl;
			return coreRetrieve(int(a.val));
		}
		else
			return a.val;
	}
} assignStack;

typedef void (*AssignFunc)();

void assign()
{
	Info("Performing an Assign operation");
	double value = assignStack.popr();
	double addr = assignStack.popl();
	coreStore(int(addr),value);
}
void dereference()
{
	Info("Performing a Dereference operation");
	double offset = assignStack.popr();
	double addr = assignStack.popl();	
	//cout << "Resulting address is " << (int(addr)+int(offset)) << endl;
	assignStack.pushl(int(addr)+int(offset));
}
void power()
{
	Info("Performing a Power operation");
	double exponent = assignStack.popr();
	double base = assignStack.popr();
	assignStack.pushr(pow(base,exponent));
}
void multiply()
{
	Info("Performing a Multiply operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr(a*b);
}
void divide()
{
	Info("Performing a Divide operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr(a/b);
}
void add()
{
	Info("Performing an Add operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr(a+b);
}
void subtract()
{
	Info("Performing a Subtract operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr(a-b);
}
void modulo()
{
	Info("Performing a Modulo operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr(a-floor(a/b)*b);
}
void lessthan()
{
	Info("Performing a Less-Than operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a<b)?1:0);
}
void lessthanoreq()
{
	Info("Performing a Less-Than-Or-Equal operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a<=b)?1:0);
}
void greaterthan()
{
	Info("Performing a Greater-Than operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a>b)?1:0);
}
void greaterthanoreq()
{
	Info("Performing an Assign operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a>=b)?1:0);
}
void equals()
{
	Info("Performing an Equals operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a==b)?1:0);
}
void notequals()
{
	Info("Performing a Not-Equals operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a!=b)?1:0);
}
void Not()
{
	Info("Performing a Not operation");
	double a = assignStack.popr();
	assignStack.pushr((!a)?1:0);
}
void And()
{
	Info("Performing an And operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a and b)?1:0);
}
void Or()
{
	Info("Performing an Or operation");
	double b = assignStack.popr();
	double a = assignStack.popr();
	assignStack.pushr((a or b)?1:0);
}
void Floor()
{
	Info("Performing a Floor operation");
	double a = assignStack.popr();
	assignStack.pushr(int(a));
}

assoc<int,AssignFunc> assignFuncs	//Note this won't update with the array in Keywords - you'll have to do that manually if one changes.
{
	{-1,assign},
	{-2,dereference},
	{-3,power},
	{-4,multiply},
	{-5,divide},
	{-6,add},
	{-7,subtract},
	{-8,modulo},
	{-9,lessthan},
	{-10,lessthanoreq},
	{-11,greaterthan},
	{-12,greaterthanoreq},
	{-13,equals},
	{-14,notequals},
	{-15,Not},
	{-16,And},
	{-17,Or},
	{-18,Floor}
};

void eassign(vector<int> line)
{
	Info("Executing Assign");
	for(auto i = ++line.begin();i!=line.end();i++)
	{
		if(*i<0)	//Is it an operator?
			assignFuncs[*i]();
		else
			assignStack.pushl(*i);
	}
}
