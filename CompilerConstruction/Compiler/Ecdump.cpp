#include "Ecdump.h"

#include <iostream>

typedef ostream& (*manip)(ostream&);

void ecdump(vector<int> line)
{
	Info("Executing Cdump");
	int start=coreRetrieve(line[1]),end=coreRetrieve(line[2]);
	char* s;
	Info("Core between "+to_string(start)+" and "+to_string(end));
	if(!compactWrites)
		for(int i=start;i<end;i++)
			cout << core_.get(i) << endl;
	else
	{
		int count = 0;
		for(int i=start;i<end;i++)
			cout << core_.get(i) <<" "<< ((++count<LINE_ITEMS)?(manip)flush:((count=0),(manip)endl));
		if(count>0) cout << endl;
			
	}
}
