//File: Core.h
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file describes the Core object.
//Core represents the block of core memory used by the TRANSY compiler and executor, represented as an array of floats.
//Eventually, this object will be able to read itself in from a file as well.
//For now, it just receives values from the compiler and saves itself to a file.

#ifndef CORE
#define CORE
#include "TRANSY.h"

#include <string>

using namespace std;

typedef int core_addr;
typedef double core_val;

const static int CORESIZE = 1000;
const extern core_val UNINITVAL;

class Core
{
	private:
	core_val data[CORESIZE+1];
	public:
	Core();
	~Core();
	
	core_val get(core_addr) const;
	void set(core_addr,core_val);
	
	bool isInit(core_addr) const;
	
	//Called to set the final entry from 0 to 1, to indicate the file is valid for use.
	void setValid();
	bool isValid() const;
	//When called with a filename, saves the contents of data to that file
	void save(ostream&);
	void save(string);
	void load(istream&);
	bool load(string);
};

#endif
