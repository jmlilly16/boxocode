#include "Eif.h"

typedef bool (*CompFunc)(double,double);

bool EQ(double a,double b){return a==b;}
bool L(double a,double b){return a<b;}
bool G(double a,double b){return a>b;}
bool NEQ(double a,double b){return a!=b;}
bool LEQ(double a,double b){return a<=b;}
bool GEQ(double a,double b){return a>=b;}

vector<CompFunc> comps{EQ,L,G,NEQ,LEQ,GEQ};

void eif(vector<int> line)
{
	Info("Executing If");
	if(comps[line[2]](coreRetrieve(line[1]),coreRetrieve(line[3])))
		programCounter = line[4];
}
