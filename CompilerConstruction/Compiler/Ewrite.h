//file: Ewrite.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the ewrite function, used by the TRANSY executor

#ifndef EWRITE
#define EWRITE

#include "Executor.h"

//This function reads a line of TRANSY code to perform a WRITE operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void ewrite(vector<int> line);

#endif
