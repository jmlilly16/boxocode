//file: Egoto.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the Egoto function, used by the TRANSY executor

#ifndef EGOTO
#define EGOTO

#include "Executor.h"

//This function reads a line of TRANSY code to perform a GOTO operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void egoto(vector<int> line);

#endif
