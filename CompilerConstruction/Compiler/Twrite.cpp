//file: Twrite.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction

//This file implements the twrite function, described in Twrite.h

#include "Twrite.h"
#include <sstream>
#include <iostream>

string twrite(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	bool state = false; //Are we looking for a comma? If not, a var
	for(auto i = ++(tokens.begin());i!=tokens.end();i++)
	{
		if(!state)
		{
			result+=" ";
			result += verifyLexCat(*i,{VAR,KEYWORD})?
				lookupSymbol(i->text) : BADLINE;
			state = true;
		}
		else
		{
			verifyLexCat(*i,COMMA);
			state=false;
		}
	}
	if(!state)
		reportError("End of line reached unexpectedly");
	return result;
}
