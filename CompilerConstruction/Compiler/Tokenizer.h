//File: Tokenizer.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

//This file defines the Tokenizer object.
//A Tokenizer scans a given line of text and breaks it apart as best it can into Tokens - pieces of code labeled with lexical categories.
//It uses a StringDFA object to perform its scanning operations.
//In earlier versions of the compiler, the grammar of transy languages could be represented as a regular expression and thus was fully processable by a DFA.
//However, more recent additions to the compiler specs have made the grammar more complex. The DFA approach still functions, but several crutches had to be set up to make it work right, notably relating to the loop, if, and assignment operations.
//These DFA workarounds are described throughout the documentation, wherever they occur.

//This file also defines the dictionary of lexical categories used in TRANSY, as well as how those categories are represented as strings.

//Furthermore, it contains links to two globally accessible Tokenizer objects, described below.
//Finally, it contains the headers for two error-checking functions relating to lexical categories.

#ifndef TRANSYTOKENIZER
#define TRANSYTOKENIZER

#include <map>
#include <initializer_list>
#include <string>
#include "StringDFA.h"

//DFA workaround. The main tokenizer needed to be able to recognize, say the snippet "dimori[20]", in different contexts as either
//	the keyword DIM, the variable ORI, a left bracket, the number 20, and a right bracket (as in a DIM command) or as
//	the variable dimori, a left bracked, the number 20, and a right bracket (as perhaps in an assignment statement).
//The DFA can't tell context, but we know that keyword almost always occur at the beginning of lines.
//So, the DFA is setup so that a '#' at the beginning of a line causes it to look for a keyword, and in all other cases a var.
//The only exception is the THEN keyword found in an IF statement. This has its own workaround described in Tif.cpp.
const char KEYCHAR = '#';

//All the lexical categories recognized by TRANSY
enum LexCat {BADTOKEN,KEYWORD,VAR,LITERAL,LABEL,QUOTE,FLOAT,INT,COMMA,LBRACK,RBRACK,LPAREN,RPAREN,EQ,EQUALS,LTHAN,GTHAN,LTHANEQ,GTHANEQ,NOTEQ,PLUS,MINUS,MULTIPLY,DIVIDE,MODULO,POWER,NOT,OR,AND,FLOOR};

//Here's what they mean:
//BADTOKEN: A default category for any symbol that has no meaning in TRANSY. Examples include &,_,#DI,0., ,"abc,and {.
//KEYWORD: Any one of the TRANSY keywords. Example: DIM, READ
//VAR: A symbol representing a numeric variable. Example: a, jimmyjohn, polly57
//LITERAL: A symbol representing a string variable. Example: $name, $bob01
//LABEL: Labels can take the same forms as KEYWORDs,VARs, and INTs. But they also have forms they can take specific to them. This category is a catchall for those forms. Examples: 12joe, 0budiah
//QUOTE: A quoted string. Examples: "Hello World!", "DUH99t"
//FLOAT: A decimal number. Examples: 12.5, 0.0, -0.98
//INT: An integer number. Examples: 123,0,450,-2
//COMMA: ,
//LBRACK: [
//RBRACK: ]
//LPAREN: (
//RPAREN: )
//EQ: =
//LTHAN: <
//GTHAN: >
//LTHANEQ: <=
//GTHANEQ: >=
//NOTEQ: !=
//PLUS: +
//MINUS: -
//MULTIPLY: *
//DIVIDE: /
//MODULO: %
//POWER: ^

//As with labels, some of these categories overlap. A Keyword can generally be used anywhere Var can, and Int can be used wherever a Float can.

//The textual representations of the lexical categories
const map<LexCat,string> catNames = 
{
	{BADTOKEN,"BADTOKEN"},
	{KEYWORD,"KEYWORD"},
	{VAR,"VAR"},
	{LITERAL,"LITERAL"},
	{FLOAT,"FLOAT"},
	{INT,"INT"},
	{COMMA,"COMMA"},
	{LBRACK,"LBRACK"},
	{RBRACK,"RBRACK"},
	{LPAREN,"LPAREN"},
	{RPAREN,"RPAREN"},
	{EQ,"EQ"},
	{EQUALS,"EQUALS"},
	{LTHAN,"LTHAN"},
	{GTHAN,"GTHAN"},
	{LTHANEQ,"LTHANEQ"},
	{GTHANEQ,"GTHANEQ"},
	{NOTEQ,"NOTEQ"},
	{LABEL,"LABEL"},
	{QUOTE,"QUOTE"},
	{PLUS,"PLUS"},
	{MINUS,"MINUS"},
	{MULTIPLY,"MULTIPLY"},
	{DIVIDE,"DIVIDE"},
	{MODULO,"MODULO"},
	{POWER,"POWER"},
	{NOT,"NOT"},
	{AND,"AND"},
	{OR,"OR"},
	{FLOOR,"FLOOR"}
};

//Struct to represent a single token of TRANSY code - a unit of text that fits into a single lexical category.
struct Token
{
	LexCat category;
	string text;
	Token(LexCat cat, string s)
		:category(cat),text(s)
	{}
	string toString()
	{
		string result = "";
		result += "<";
		result += catNames.at(category);
		result += ",";
		result += text;
		result += ">";
		return result;
	}
};

//The definition of the Tokenizer class
//The Tokenizer consists of a DFA that represents the regular expression used for scanning,
//and a map associating the accepting states in the DFA with the lexical categories they represent.
class Tokenizer
{
	private:
	StringDFA<int>* tokenDFA;
	map<int,LexCat> idCats;
	public:
	Tokenizer(StringDFA<int>* dfa, map<int,LexCat> idCats)
		:tokenDFA(dfa),idCats(idCats)
	{}
	//Converts the given line into TRANSY tokens
	//Parameters:
	//	<line>: The line of TRANSY code to be tokenized.
	//Returns:
	//	A vector of tokens.
	vector<Token> tokenizeLine(string line);
	//Just calls the DFA's print fuction. Mainly for debugging.
	void print()
	{
		tokenDFA->print();
	}
};

//tokenizer is the main Tokenizer used by the compiler. It processes all statements except for assignment statements.
//Likewise, assignTokenizer handles all assignment statements.
//This dichotomy eliminates the problem that the main tokenizer will, in an assignment statement, incorrectly flag a variable, at the beginning of the line and starting with a keyword, as a keyword. 
extern Tokenizer tokenizer;
extern Tokenizer assignTokenizer;

//This function will verify that a given token fits in a given lexical category.
//If it doesn't, an error will be reported.
//Returns true if the token matches the category.
bool verifyLexCat(Token token, LexCat cat);

//This overload of the above function performs the same operation on a group of categories - true will be returned if the token matches any of the categories given 
bool verifyLexCat(Token token, vector<LexCat> cats);

#endif
