#ifndef LOOPS
#define LOOPS

#include "Executor.h"

struct LoopDef
{
	core_addr variable;
	core_addr limit;
	core_addr increment;
	int returnLine;
	LoopDef(core_addr v, core_addr l, core_addr i, int r)
		:variable(v),limit(l),increment(i),returnLine(r)
	{}
};

extern stack<LoopDef> loopStack;

#endif
