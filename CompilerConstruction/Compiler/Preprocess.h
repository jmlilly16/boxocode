//file: Preprocess.h
//Author: Mason Lilly
//Last modified: 3/26/15
//Class: CS3024 - Compiler Construction

//This header defines the preprocess function, which performs a number of helpful transformations on a text file to make it readable by the TRANSY compiler.
//It also defines the getFileStem function and a variant preprocess_min function.

#ifndef PREPROCESS
#define PREPROCESS

#include <string>
#include <map>

#include "LineTable.h"

using namespace std;

//This struct exists only to be returned by the preprocess function.
//It contains all the results of preprocessing: Whether it worked, the name of the .obj file created, a map representing the matching of .obj lines to .transy lines, and a LineTable holding the locations of all the line labels found.
struct PreprocessResult
{
	bool success;
	bool error;
	string outputName;
	map<int,int> lineIndex;//NoBlanks line number,Source line number
	LineTable lineTable;
};

//Because requiring "struct foo" syntax is silly.
typedef struct PreprocessResult PreprocessResult;

//This function preprocesses a .transy file to make it easily digestible by the TRANSY compiler.
//If a filename with no extension is given, a .transy extension is assumed and appended.
//It performs the following transformations:
//-Blank lines, whitespace not in quotes, and comments (defined with C*) are removed
//-Alphabetic characters not inquotes are capitalized
//The transformed .transy file is written to a .obj file with the same stem.
//Additionally, the function builds a map mapping the line numbers of the created .noblanks file to the original .transy file
//Parameters: <filename>: Filename of the .transy file to be preprocessed (must actually end in .transy)
//Preconditions: none
//Returns/Postconditions: A struct containing result information.
//	If the preprocessing succeeded, the struct's success flag will be true and the outputName and lineIndex fields will be defined as above.
//	If an error occured, the error will be reported, the success field will be false and the values of the other two fields are undefined.
//
PreprocessResult preprocess(string filename);

//This function does minimal preprocessing on a .transy or .noblanks file.
//Namely: The line-number index is built as is needed by the compiler - since no transformations are done on the file, the form of this index will be <x1,x1>,<x2,x2>,...,<xn,xn> where n is the number of lines in the given file.
//Files with the extensions .transy or .noblanks are accepted.
//If a file with no extension is given, an extension of .noblanks is assumed and appended.
//The function behaves slightly differently depending if a .transy or a .noblanks file was given, in that:
//If a .transy file is given, its contents are copied to a .noblanks file with the same stem and THAT filename is returned in the result.
//If a .noblanks file is given, it is merely traversed to build the trivial line-number index and build the line label table.
//(Actually, a dummy 'noblanks' file is created, but it is removed as the function returns)
PreprocessResult preprocess_min(string filename);

#endif
