//file: Tif.cpp
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/26/15

#include "Tif.h"

string tif(vector<Token> tokens)
{
	string result = "";
	result+=opCodes[kwNames[tokens[0].text]];
	if(tokens.size()<7)
		reportError("Wrong number of arguments for IF");
	
	verifyLexCat(tokens[1],LPAREN);
	
	result+=" ";
	result += verifyLexCat(tokens[2],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[2].text) : BADLINE;
	
	result+=" ";
	result += verifyLexCat(tokens[3],{EQ,GTHAN,LTHAN,NOTEQ,GTHANEQ,LTHANEQ})?
		compCodes.at(tokens[3].text) : BADLINE;
	
	result+=" ";
	result += verifyLexCat(tokens[4],{VAR,KEYWORD,INT})?
		lookupSymbol(tokens[4].text) : BADLINE;

	verifyLexCat(tokens[5],RPAREN);

	//Because of a DFA workaround in the tokenizer, THEN never gets flagged as a keyword.
	//Instead, it will come in as a VAR with the next "THENFOO"
	//This block of code will frankenstein the "THEN" out of that VAR token and insert a THEN token just before it.
	//The code proceeds as normally.
	if(tokens[6].category == VAR && tokens[6].text.substr(0,4)=="THEN")
	{
		string oldtext = tokens[6].text;
		tokens[6] = Token(KEYWORD,"THEN");
		while(tokens.size()>7) tokens.erase(tokens.end());
		tokens.push_back(Token(LABEL,oldtext.substr(4)));
	}
	//But, you know, just in case THEN DOES show up, we'll want it to still work.
	else if(!(tokens[6].category == KEYWORD && tokens[6].text == "THEN"))
	{
		reportError("Expected THEN after IF");
	}
	result+=" ";
	result += verifyLexCat(tokens[7],{VAR,KEYWORD,INT,LABEL})?
	lookupLabel(tokens[7].text) : BADLINE;
		
	return result;
}
