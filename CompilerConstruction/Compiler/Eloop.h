//file: Eloop.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eloop function, used by the TRANSY executor

#ifndef ELOOOP
#define ELOOOP

#include "Executor.h"

//This function reads a line of TRANSY code to perform a LOOP operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eloop(vector<int> line);

#endif
