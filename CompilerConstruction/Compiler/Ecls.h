//file: Ecls.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This function reads a line of TRANSY code to perform a CLS operation, as specified in the user manual.

#ifndef ECLS
#define ECLS

#include "Executor.h"

//This function 
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
//Postcondition:
//	
void ecls(vector<int> line);

#endif
