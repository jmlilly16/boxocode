//File: Core.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file implements the functions described in the file Core.h

#include "Core.h"

#include <fstream>
#include <iostream>

const core_val UNINITVAL = 0.123456789;
const int CORE_PRECISION = 10;

Core::Core()
{
	//Set everything to 'uninitialized'
	for(int i=0;i<CORESIZE;i++)
		data[i]=UNINITVAL;
	data[CORESIZE] = 0;
}

Core::~Core()
{}

core_val Core::get(core_addr addr) const
{
	return data[addr];
}

void Core::set(core_addr addr, core_val value)
{
	data[addr] = value;
}

bool Core::isInit(core_addr addr) const
{
	return data[addr]!=UNINITVAL;
}

void Core::setValid()
{
	data[CORESIZE] = 1;	//Mark the last entry to signify the core goes with a successful compilation
}

bool Core::isValid() const
{
	return data[CORESIZE] == 1;
}

void Core::save(ostream& stream)
{
	stream.precision(FLOAT_PRECISION);
	for(int i=0;i<=CORESIZE;i++)
		stream << data[i] << endl;	//Output each number sequentially on a line.
}

void Core::save(string filename)
{
	ofstream file(filename);
	save(file);
	file.close();
}

void Core::load(istream& stream)
{
	core_val val;
	for(int i=0;i<=CORESIZE&&stream.good();stream>>data[i++])
		0;
}

bool Core::load(string filename)
{
	ifstream file(filename);
	if(!file.is_open())
	{
		cout << "Core could not open file" << endl;
		return false;
	}
	load(file);
	return true;
}
