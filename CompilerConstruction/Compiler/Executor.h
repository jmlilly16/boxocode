#ifndef EXECUTOR
#define EXECUTOR

#include "TRANSY.h"

#include <stack>
#include <vector>

#include "Core.h"
#include "LiteralCore.h"

using namespace std;

const extern int LINE_ITEMS;
const extern int CLS_LINES;

extern bool defaultZeroes;
extern bool compactWrites;
extern bool noWarn;
extern bool debug;

extern bool stopReached;
extern int programCounter;

const extern vector<vector<int>>& coderef;
const extern Core& core_;

core_val coreRetrieve(core_addr);
void coreStore(core_addr,core_val);

string literalRetrieve(core_addr);
void literalStore(core_addr,string);

void Error(string message);
void Warn(string message);
void Info(string message);
extern char* infoString;

#endif
