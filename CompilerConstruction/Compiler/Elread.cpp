#include "Elread.h"

#include <iostream>

void elread(vector<int> line)
{
	Info("Executing Lread");
	string input;
	for(auto i = line.begin()++;i!=line.end();i++)
		literalStore(*i,(getline(cin,input),input));
}
