//file: Eawrite.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eawrite function, used by the TRANSY executor

#ifndef EAWRITE
#define EAWRITE

#include "Executor.h"

//This function reads a line of TRANSY object code to perform an AWRITE operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eawrite(vector<int> line);

#endif
