#include "Eloop.h"

#include "Loops.h"

void eloop(vector<int> line)
{
	Info("Executing Loop");
	loopStack.push(LoopDef(line[1],line[3],line[4],programCounter));
	Info("Pushed a loop with counter address "+to_string(loopStack.top().variable)+", limit "+to_string(coreRetrieve(loopStack.top().limit))+", increment "+to_string(coreRetrieve(loopStack.top().increment))+", and returnLine "+to_string(loopStack.top().returnLine));
	coreStore(line[1],coreRetrieve(line[2])-coreRetrieve(loopStack.top().increment));	//initialize
	Info("Initialized variable at "+to_string(loopStack.top().variable)+" to "+to_string(coreRetrieve(loopStack.top().variable)));
	
	//Advance the program counter to the corresponding loop-end statement, which will do the first test.
	int nestCounter=0;
	bool done=false;
	programCounter--;	//Back up one just in case the next loop-end is the very next line (PC is incremented immediately)
	while(!done and ++programCounter<coderef.size())
		switch(coderef[programCounter][0])
		{
			case 14: ++nestCounter; break;
			case 15: if(nestCounter--==0) done=true;
			default: break;
		}
	if(!done)
		Error("LOOP without LOOP-END");	//This is checked for during compilation, but this is here to avoid segmentation faults.
	Info("Advancing program counter to "+to_string(programCounter));
}
