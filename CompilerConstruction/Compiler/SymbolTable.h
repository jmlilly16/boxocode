//file: SymbolTable.h
//Author: Mason Lilly
//Class: CS3024 - Compiler Construction
//Last Modified: 3/8/15

//This header defines the SymbolTable class.

#ifndef SYMTABLE
#define SYMTABLE

#include <map>
#include <string>

#include "Table.h"
#include "Core.h"

using namespace std;

//The SymbolTable class stores strings given to it and matches them with integers.
//The first string given will be matched with 0, the second with 1, and so on.
//When the table is referenced for a given string, the integer that that string matches will be returned.
class SymbolTable : public Table
{
	private:
	int nextLocation;
	public:
	SymbolTable();
	~SymbolTable();
	//This function is used to reference the symbol table.
	//Parameters: <entry>: The string to be referenced
	//Returns: The corresponding integer of the referenced string
	//Preconditions: entry is a valid string
	//Postconditions: If entry already existed in the table, the index of that string in the underlying vector will be returned.
	//		Otherwise, the string will be added to the end of the underlying vector and its index will be returned.
	int lookup(string entry,int size=1);
	//Returns true if the table contains a string already
	bool contains(string entry) const;
};

#endif
