#include "TRANSY.h"

const string DEFAULTFILENAME = "test";
const string OBJEXT = ".obj";
const string COREEXT = ".core";
const string LITERALEXT = ".literal";

const int FLOAT_PRECISION = 10;

//Helper function to get a line from a string, because c++'s string manipulation is annoying
string readLine(istream& stream)
{
	string result = "";
	char c=0;
	while((c = stream.get()) && stream.good())
	{
		if(c=='\n')
			return result;
		result+=c;
	}
	return result;
}
//Helper function to isolate the stem of a file
string getFileStem(string filename)
{
	string stem = "";
	int i=0;
	while(i<filename.size()&&filename[i]!='.')
	{
		stem+=filename[i++];
	}
	return stem;
}
