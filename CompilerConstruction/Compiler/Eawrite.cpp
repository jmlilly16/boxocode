#include "Eawrite.h"

#include <iostream>

typedef ostream& (*manip)(ostream&);

void eawrite(vector<int> line)
{
	Info("Executing Awrite");
	int baseAddr = line[1], start = coreRetrieve(line[2]), end = coreRetrieve(line[3]);
	if(!compactWrites)
		for(int i=start;i<end;i++)
			cout << coreRetrieve(baseAddr+i) << endl;
	else
	{
		int count = 0;
		for(int i=start;i<end;i++)
			cout << coreRetrieve(baseAddr+i) <<" "<< ((++count<LINE_ITEMS)?(manip)flush:((count=0),(manip)endl));
		if(count>0) cout << endl;
	}
}
