#include "Eloopend.h"

#include "Loops.h"

void eloopend(vector<int> line)
{
	Info("Executing Loop-End");
	if(loopStack.empty())
		Error("LOOP-END without LOOP");
	LoopDef loop = loopStack.top();
	coreStore(loop.variable,coreRetrieve(loop.variable)+coreRetrieve(loop.increment));
	Info("Counter Variable is now "+to_string(coreRetrieve(loop.variable)));
	if(coreRetrieve(loop.variable)<coreRetrieve(loop.limit))
	{
		programCounter = loop.returnLine;
		Info("Test passed, program counter is now "+to_string(programCounter));
	}
	else
	{
		loopStack.pop();
		Info("Test failed, popping loop and continuing.");
	}
}
