#include "Elwrite.h"

#include <iostream>

void elwrite(vector<int> line)
{
	Info("Executing Lwrite");
	for(auto i = ++line.begin();i!=line.end();i++)
		cout << literalRetrieve(*i) << endl;
};
