//file: Earead.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the earead function, used by the TRANSY executor

#ifndef EAREAD
#define EAREAD

#include "Executor.h"

//This function reads a line of object code to perform an AREAD operation, as defined in the user manual.
//In general, AREAD performs successive READ operations, storing one value in each memory address between the indices in the array specified.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
//Postcondition:
//	X values have been read from input and stored in the core, where X = line[3]-line[2]
void earead(vector<int> line);

#endif
