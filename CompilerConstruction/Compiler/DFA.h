//file: DFA.h
//Author: Mason Lilly
//Last Modified: 2/26/15

//This file defines the template for a class representing a Discrete Finite Automaton, or DFA.
//The DFA is a collection of states, some of which are 'accepting' states, and a series of transitions connecting those states.
//The template parameters in this object are the idType of the states - what type of object you want to use to label the states, most often int but possible strings or something more complicated - and the alphabet type - what type of object you want to use to label the transitions, e.g. char.
//Internally, States are represented by a private struct of the DFA object called State, and transitions are represented as pairs associating a value of alphType with a pointer to a State.

//The DFA can be constructed piecewise via a blank constructor and subsequent calls to addState, addTransition, and setAccept.
//It can also be constructed "all-at-once" via an initializer list.
//To help with the initializer list construction, a nested struct called state_spec is defined.
//The state_spec struct contains the information necessary to construct a State internally: an id, whether the state is accepting or not, and a list of conn_specs.
//conn_spec is a nested struct within state_spec, and contains the information necessary to construct a transition between two states: the trigger for that transition, and the id of the next transition.
//The idea behind these two structs is that a DFA can be constructed entirely as nested initializer lists:
//E.G. DFA<int,char>{{{0,false,{{'a',1},{'b',2}}},{1,false,{{'c',3}}},{2,false,{{'c',3}}},{3,true}},0,-1};
//This is not a particularly useful feature, but I thought it was cool and wanted to try it.

//TODO: Fix: Causes seg faults if used as a member of a class. (A pointer CAN be used)

#ifndef DFAHEADER
#define DFAHEADER

#include <vector>
#include <iostream>
#include "assoc.h"

using namespace std;

template<class idType, class alphType>
class DFA
{
	private:
	struct State;
	
	State* startState;	//Where the DFA starts
	State* errorState;	//Where the DFA goes when a transition fails
	State* currentState;	//Where it is now
	vector<State*> states;	//List of all the states. Holds pointers to them so they can all be cleanly deleted at termination.
	assoc<idType,State*> stateIDs;	//Associates the ids of the states to the states themselves
	
	struct State //Struct to represent the states of the DFA
	{
		typedef pair<alphType,State*> transition;
		bool accept;	//Does the state accept?
		vector<transition> transitions;	//Where can we go from here?
		State* defTrans;
		State()
			:accept(false),transitions(vector<transition>())
		{}
		State(bool a, vector<transition> t = vector<transition>(), State* def = 0)
			:accept(a),transitions(t),defTrans(def)
		{}
	};
	public:
	struct state_spec	//Used by the all-at-once constructor and one overload of the addState function to specify the parameters of a state
	{
		struct conn_spec	//Represents the parameters of a transition between states 
		{
			alphType trigger;	//What symbol will trigger it?
			idType nextState;	//Where do you go?
			conn_spec(){}
			conn_spec(alphType t, idType n):trigger(t),nextState(n){}
		};
		idType id;	//What's this state's name?
		bool accept;	//Is it an accepting state?
		vector<conn_spec> conns;	//What transitions can it have?
		idType def;	//The id of the default transition
		bool useDefTrans;
		state_spec()
			:def(stateIDs[errorState])
		{}
		//This constructor allows the connections list to be left out with a default parameter
		state_spec(idType i, bool a, idType d, vector<conn_spec> c = vector<conn_spec>())
			:id(i),accept(a),conns(c),def(d),useDefTrans(true)
		{}
		state_spec(idType i, bool a, vector<conn_spec> c = vector<conn_spec>())
			:id(i),accept(a),conns(c),useDefTrans(false)
		{}
	};
	typedef typename state_spec::conn_spec conn_spec;

	//Since all the function implementations are in this file on account of it being a template,
	//I've put the function comments down with the individual functions
	DFA(vector<state_spec>, idType startID, idType errorID);
	DFA();
	~DFA();
	void addState(state_spec);
	void addState(idType);
	bool setAccept(idType,bool);
	bool addTransition(idType,alphType,idType);
	bool setDefaultTransition(idType,idType); //TODO
	bool clearTransitions(idType);
	void start();
	void transition(alphType);
	bool setState(idType);
	void setStartState(idType);
	void setErrorState(idType);
	idType getState() const;
	bool isAccept() const;
	bool isAccept(idType) const;
	idType getDefaultTransition(idType) const; //TODO
	bool isError() const;
	void print() const;
	bool containsState(idType) const;
	const vector<pair<alphType,idType>> getTransitions(idType) const;
};

/*
This is the "all-at-once" constructor.
It takes in a list of state_specs, an id for the start-state (which had better be one of the states in the state_spec list) and an id for the error-state (which can be anything, but ideally should be one that wasn't in the state_spec list).
The use of a vector of state_spec structs allows the constructor to be called entirely with nested braces. Which seemed neat.

Postcondition: A DFA has been constructed according to the parameters listed in list, with startID as its start state and errorID naming its error state.
*/
template<class I, class A>
DFA<I,A>::DFA(vector<state_spec> list, I startID, I errorID)
{
	//The preconditions state that startID shouldn't be a state not declared in a state_spec, but just in case,
	//the constructor can declare a dummy case for whatever ID you give it.
	if(!containsState(startID))
		addState(startID);
	startState = stateIDs[startID];
	if(!containsState(errorID))
		addState({errorID,false,errorID});
	errorState = stateIDs[errorID];
	currentState = startState;
	//Loop through the state_specs and create or modify a state based on each one.
	for(typename vector<state_spec>::iterator i = list.begin();i!=list.end();i++)
	{
		addState(*i);
	}
}
//This creates a blank DFA.
//To give it any meaning, states and transitions need to be added with the DFA's modifier methods.
//To prevent segmentation faults, the start,error,and current states are initialized to a blank state created by a default-constructed idType object.
//Note that this means in order to use this constructor, whatever type used for the idType needs to be default-constructible.
template<class I, class A>
DFA<I,A>::DFA()
{
	I id;
	setStartState(id);
	setErrorState(id);
	currentState=startState;
}

//Deletes all the states on the way out
template<class I, class A>
DFA<I,A>::~DFA()
{
	for(State* i: states)
		delete i;
}

/*
Parameters:
	<spec>: a state_spec object detailing the state to be created
Postcondition:
	If the state did not exist already, it has been created.
	The state's accepting field has been set to that of <spec>
	All transitions detailed in <spec>.conns have been created and added to the object; any previous transitions on the state will be overridden.
*/
template<class I, class A>
void DFA<I,A>::addState(state_spec spec)
{
	if(!stateIDs.contains(spec.id))
	{
		states.push_back(new State());
		stateIDs.set(spec.id,states.back());
	}
	stateIDs[spec.id]->accept = spec.accept;
	for(conn_spec j: spec.conns)
	{
		addTransition(spec.id,j.trigger,j.nextState);
	}
	if(spec.useDefTrans)
		setDefaultTransition(spec.id,spec.def);
	else
		setDefaultTransition(spec.id,stateIDs[errorState]);
}
/*
Overload of addState that allows you to simply specify an ID to have it placed into the DFA.
All this function does is construct a state_spec with the ID specified by <id>, an accepting value of false, and a blank conn_spec list, and forward this state_spec to the other version of addState.
*/
template<class I, class A>
void DFA<I,A>::addState(I id)
{
	addState(state_spec(id,false));
}
/*
Modifies whether a state is accepting or not.
Parameters:
	<id>: the id of the state you want to modify
	<accept>: whether this state should be accepting or not
Returns:
	false if the specified ID is not a state currently in the DFA, true otherwise
Postcondition:
	If <id> exists in the DFA, its accepting value has been set to <accept>
*/
template<class I, class A>
bool DFA<I,A>::setAccept(I id, bool accept)
{
	if(!containsState(id))
		return false;
	stateIDs[id]->accept = accept;
	return true;
}
/*
Adds a transition from one state to another.
Parameters:
	<from>: The id of the state from which this transition originates
	<trig>: the symbol that triggers this transition
	<to>: the id of the state to which this transition leads
Returns:
	False if <from> is not a state currently in the DFA, true otherwise
Postcondition:
	Assuming <from> exists in the DFA, a transition has been created between <from> and <to> on the symbol <trig>.
	If <to> did not exist before this function call, it has been created and is a non-accepting state with a blank transition list
	If a transition already existed from <from> on symbol <trig>, this new transition overrides that and will be used in its place.
	Note that it does not delete the old transition.
*/
template<class I, class A>
bool DFA<I,A>::addTransition(I from, A trig, I to)
{
	if(!stateIDs.contains(from))
		return false;
	if(!stateIDs.contains(to))
		addState(to);
	stateIDs[from]->transitions.insert(stateIDs[from]->transitions.begin(),pair<A,State*>(trig,stateIDs[to]));
	return true;
}
template<class I, class A>
bool DFA<I,A>::setDefaultTransition(I from, I to)
{
	if(!stateIDs.contains(from))
		return false;
	if(!stateIDs.contains(to))
		addState(to);
	stateIDs[from]->defTrans = stateIDs[to];
	return true;
}
/*
Clears the transition list of a specified state
Parameters:
	<id>: the id of the state to be modified
Returns:
	false if id doesn't exist in the DFA, true otherwise
Postcondition:
	the transition list of the state specified by <id> has been emptied
*/
template<class I, class A>
bool DFA<I,A>::clearTransitions(I id)
{
	if(!containsState(id))
		return false;
	stateIDs[id]->transitions.clear();
	return true;
}
/*
Sets the DFA to its start state
Postcondition:
	currentState now equals startState
*/
template<class I, class A>
void DFA<I,A>::start()
{
	currentState = startState;
}
/*
Performs a transition on the given symbol
The current state's transition list is traversed forwards until a transition whose trigger matches <input> is found.
The DFA's currentState is then set to that transition's nextState value.
The DFA uses the first transition it finds. Since the transitions are stored in the order of most-recent-first, the most recently added transition on the given symbol will be used.
If no suitable transition is found, the DFA is set to the error state.
Parameters:
	<input>: the symbol on which to transition
Postcondition:
	The DFA's state has been set to the state specified by the first transition matching <input> in the DFA's old state's transition list, or to the error state if no such transition exists.
*/
template<class I, class A>
void DFA<I,A>::transition(A input)
{
	for(auto& i: currentState->transitions)
	{
		if(i.first==input)
		{
			currentState = i.second;
			return;
		}
	}
	currentState = currentState->defTrans;
}
/*
Attempts to manually set the DFA's state to a certain value
Parameters:
	<id>: The id of the state to move to
Returns:
	false if the specified state does not exist, true otherwise
Postcondition:
	Assuming <id> exists in the DFA, the DFA's state has been set to that state.
*/
template<class I, class A>
bool DFA<I,A>::setState(I id)
{
	if(!containsState(id))
		return false;
	currentState = stateIDs[id];
	return true;
}
/*
Attempts to modify the DFA's start state
Mainly useful when the default constructor is used, to set the start state to something other than the default-constructed state provided by that constructor.
Parameter:
	<id>: The id of the state you want to be the start state
Returns:
	false if <id> does not exist in the DFA, true otherwise
Postcondition:
	The DFA's start state has been set to the state specified by <id>
*/
template<class I, class A>
void DFA<I,A>::setStartState(I id)
{
	if(!containsState(id))
		addState(id);
	startState = stateIDs[id];
}
/*
Attempts to modify the DFA's error state
Mainly useful when the default constructor is used, to set the error state to something other than the default-constructed state provided by that constructor.
Parameter:
	<id>: The id of the state you want to be the error state
Returns:
	false if <id> does not exist in the DFA, true otherwise
Postcondition:
	The DFA's error state has been set to the state specified by <id>
*/
template<class I, class A>
void DFA<I,A>::setErrorState(I id)
{
	if(!containsState(id))
		addState(id);
	errorState = stateIDs[id];
}
/*
Gets the current state of the DFA
Returns:
	The current state the DFA
*/
template<class I, class A>
I DFA<I,A>::getState() const
{
	return stateIDs[currentState];
}
/*
Returns:
	True if the current state's accepting status is true, false if it's false.
*/
template<class I, class A>
bool DFA<I,A>::isAccept() const
{
	return currentState->accept;
}
/*
Checks if a specific state is accepting
Parameters:
	<id>: the id of the state to inspect
Returns:
	false if <id> does not exist in the DFA, otherwise returns whether the state specified by <id> is accepting or not.
*/
template<class I, class A>
bool DFA<I,A>::isAccept(I id) const
{
	if(!containsState(id))
		return false;
	return stateIDs[id]->accept;
}
template<class I, class A>
I DFA<I,A>::getDefaultTransition(I id) const
{
	if(!containsState(id))
		return stateIDs[errorState];
	else
		return stateIDs[stateIDs[id]->defTrans];
}
/*
Returns: Whether or not the DFA is in the error state
*/
template<class I, class A>
bool DFA<I,A>::isError() const
{
	return currentState==errorState;
}
/*
Pretty-prints the states and transitions listed in the DFA
*/
template<class I, class A>
void DFA<I,A>::print() const
{
	for(State* i: states)
	{
		cout << stateIDs[i] << ":" << endl;
		cout << "\t" << (i->accept?"TRUE":"FALSE") << endl;
		for(auto& j: i->transitions)
			cout << "\t'"<<j.first<<"': "<<stateIDs[j.second]<<endl;
		cout << "\tdefault: " << stateIDs[i->defTrans] << endl;
	}
}
/*
Returns whether or not the given id exists in the DFA
*/
template<class I, class A>
bool DFA<I,A>::containsState(I id) const
{
	return stateIDs.contains(id);
}
/*
Returns a vector of all the transitions held by a specified state
Parameters:
	<id>: the id of the state to inspect
Returns:
	a copy of the specified state's transition list
Preconditions:
	<id> exists in the DFA
Postconditions:
	a copy of the specified state's transition list has been returned
*/
template<class I, class A>
const vector<pair<A,I>> DFA<I,A>::getTransitions(I id) const
{
	vector<pair<A,I>> result;
	for(auto t: stateIDs[id]->transitions)
		result.push_back(pair<A,I>(t.first,stateIDs[t.second]));
	return result;
}

#endif
