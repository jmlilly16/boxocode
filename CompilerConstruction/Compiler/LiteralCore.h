//File: LiteralCore.cpp
//Author: Mason Lilly
//Last Modified: 3/8/15

//This file defines the LiteralCore object, a class for storing the strings handled by the TRANSY compiler and executor
//Currently, all it does is accept data and write itself to a file.

#ifndef LITCORE
#define LITCORE

#include <map>
#include <string>

using namespace std;

typedef int core_addr;

class LiteralCore
{
	private:
	map<core_addr,string> data;
	public:
	LiteralCore();
	~LiteralCore();
	
	string get(core_addr);
	void set(core_addr,string);
	bool isInit(core_addr);

	void load(istream&);
	bool load(string);
	//When called with a filename, causes the object to save its contents at that filename.
	void save(ostream&);
	void save(string);
};

#endif
