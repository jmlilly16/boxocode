#include "Ewrite.h"

#include <iostream>

typedef ostream& (*manip)(ostream&);

void ewrite(vector<int> line)
{
	Info("Executing Write");
	if(!compactWrites)
		for(auto i = ++line.begin();i!=line.end();i++)
			cout << coreRetrieve(*i) << endl;
	else
	{
		int count = 0;
		for(auto i = ++line.begin()++;i!=line.end();i++)
			cout << coreRetrieve(*i)<<" "<<((++count<LINE_ITEMS)?(manip)flush:((count=0),(manip)endl));
		if(count>0) cout << endl;
	}
}
