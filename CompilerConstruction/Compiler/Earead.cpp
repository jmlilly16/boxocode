#include "Earead.h"

#include <iostream>

void earead(vector<int> line)
{
	Info("Executing Aread");
	int baseAddr = line[1], start = coreRetrieve(line[2]), end = coreRetrieve(line[3]);
	double input;
	for(int i=start;i<end;i++)
		coreStore(baseAddr+i,((cin >> input),input));
}
