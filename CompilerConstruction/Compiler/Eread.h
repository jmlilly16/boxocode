//file: Eread.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the eread function, used by the TRANSY executor

#ifndef EREAD
#define EREAD

#include "Executor.h"

//This function reads a line of TRANSY code to perform a READ operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void eread(vector<int> line);

#endif
