#include "Eread.h"

#include <iostream>

void eread(vector<int> line)
{
	Info("Executing Read");
	double input;
	for(auto i = ++line.begin();i!=line.end();i++)
		coreStore(*i,((cin >> input),input));
}
