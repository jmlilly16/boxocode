#include "Esubp.h"

#include <cmath>

typedef double (*MathFunc)(double);

vector<MathFunc> mathFuncs{sin,cos,exp,abs,log,sqrt};

void esubp(vector<int> line)
{
	Info("Executing Subp");
	coreStore(line[3],mathFuncs[line[1]](coreRetrieve(line[2])));
}
