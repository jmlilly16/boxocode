#include "Executor.h"

#include <iostream>
#include <fstream>
#include <sstream>

#include "Earead.h"
#include "Eassign.h"
#include "Eawrite.h"
#include "Ecdump.h"
#include "Ecls.h"
#include "Edim.h"
#include "Egoto.h"
#include "Eifa.h"
#include "Eif.h"
#include "Elisto.h"
#include "Eloop.h"
#include "Eloopend.h"
#include "Elread.h"
#include "Elwrite.h"
#include "Enop.h"
#include "Eread.h"
#include "Estop.h"
#include "Esubp.h"
#include "Ewrite.h"

//*Global variables listed in header*//
const int LINE_ITEMS = 8;
const int CLS_LINES = 50;

const string DUMPEXT = ".dump";

bool defaultZeroes = false;
bool compactWrites = false;
bool noWarn = false;
bool debug = false;
bool dumpOnError = false;

bool stopReached = false;

string filestem;
vector<vector<int>> code;
const vector<vector<int>>& coderef = code;
Core core;
const Core& core_ = core;
LiteralCore literal;

int programCounter;

typedef void (*ExecFunc)(vector<int>);

map<int,ExecFunc> execFuncs
{
	{0,edim},
	{1,eread},
	{2,ewrite},
	{3,estop},
	{5,ecdump},
	{6,elisto},
	{7,enop},
	{8,egoto},
	{10,eifa},
	{11,earead},
	{12,eawrite},
	{13,esubp},
	{14,eloop},
	{15,eloopend},
	{16,elread},
	{17,elwrite},
	{18,eif},
	{19,ecls},
	{20,eassign}
};

//Do file handling to open and read the object file, core file, and literal file.
//Error if object or core doesn't exist or if core is bad, Warn if literal doesn't exist
void loadTransyFiles(string stem);

void saveTransyFiles(string stem);

int main(int argc, char** argv)
{
	//***Setup, Command-line parsing***//
	char c;
	int i;
	string cmdLineFilename;
	//Set default file name if no parameters were given
	if(argc==1)
		cmdLineFilename = DEFAULTFILENAME;
	else
	{
		while(--argc>0&&(*++argv)[0]=='-')
			while(c=*++argv[0])
				switch(c)
				{	//The flags are kept in Compiler.h
					case 'c':
						cout << "Using compact writes" << endl;
						compactWrites = true;
						break;
					case 'd':
						cout << "Showing debug output" << endl;
						debug = true;
						break;
					case 'u':
						cout << "Will dump core on error" << endl;
						dumpOnError = true;
						break;
					case 'w':
						cout << "Suppressing warnings" << endl;
						noWarn = true;
						break;
					case 'z':
						cout << "Uninitialized values default to 0" << endl;
						defaultZeroes = true;
						break;
					default:
						cout << "Warning: Unrecognized command-line flag: " << c << endl;
				}
		cmdLineFilename = *argv;
		//Also set default file name if options were given but no filename
		if(cmdLineFilename == "") cmdLineFilename = DEFAULTFILENAME;
	}
	filestem = getFileStem(cmdLineFilename);
	loadTransyFiles(filestem);
	cin.precision(FLOAT_PRECISION);
	cout.precision(FLOAT_PRECISION);
	
	//***Execution***//
	programCounter = 0;
	vector<int> line;
	while(!stopReached)
	{
		line = code[programCounter++];
		execFuncs[line[0]](line);
	}

	//***Cleanup***//
	saveTransyFiles(filestem);
}

void loadTransyFiles(string stem)
{
	string objFilename = stem+OBJEXT, coreFilename = stem+COREEXT, literalFilename = stem+LITERALEXT;
	ifstream objFile(objFilename), coreFile(coreFilename), literalFile(literalFilename);
	
	if(!objFile.is_open())
		Error("Could not open object file "+objFilename);
	if(!coreFile.is_open())
		Error("Could not open core file "+coreFilename);
	
	string line;
	int n;
	vector<int> codeLine;
	codeLine.push_back(0);
	code.push_back(codeLine);
	while((line = readLine(objFile))!="" && objFile.good())
	{
		stringstream splitter(line);
		codeLine = vector<int>();
		//while((splitter >> n) && splitter.good())
		while(splitter.good())
		{
			splitter >> n;
			codeLine.push_back(n);
		}
		code.push_back(codeLine);
	}
	/*for(auto i: code)
	{
		for(auto j: i)
			//cout << j << " " << flush;
		//cout << endl;
	}*/
	
	core.load(coreFile);
	if(!core.isValid())
		Error("Core is invalid");
	
	if(!literalFile.is_open())
		Warn("Could not open literal file "+literalFilename);
	else
		literal.load(literalFile);

	objFile.close();
	coreFile.close();
	literalFile.close();
}

void saveTransyFiles(string stem)
{
	string coreFilename = stem+COREEXT, literalFilename = stem+LITERALEXT;
	core.save(coreFilename);
	literal.save(literalFilename);
}

core_val coreRetrieve(core_addr addr)
{
	if(core.isInit(addr))
		return core.get(addr);
	else if(defaultZeroes)
		return 0.0;
	else
		Error("Tried to read uninitialized value");
}
void coreStore(core_addr addr, core_val value)
{
	core.set(addr,value);
}

string literalRetrieve(core_addr addr)
{
	return literal.get(addr);
}
void literalStore(core_addr addr, string value)
{
	literal.set(addr,value);
}

void Error(string message)
{
	cout << "ERROR: " << message << endl;
	if(dumpOnError)
	{
		cout << "(dumping core)" << endl;
		string dumpFilename = filestem+DUMPEXT;
		ofstream dump(dumpFilename);
		if(!dump.is_open()) cout << "Couldn't open dump file" << endl;
		dump << "//*****************************//" << endl;
		dump << "//**********CORE DUMP**********//" << endl;
		dump << "//*****************************//" << endl;
		core.save(dump);
		dump << endl << endl << endl;
		dump << "//****************************//" << endl;
		dump << "//**********LITERALS**********//" << endl;
		dump << "//****************************//" << endl;
		literal.save(dump);
		dump.close();
	}
	exit(1);
}
void Warn(string message)
{
	if(!noWarn)
		cout << "WARNING: " << message << endl;
}
void Info(string message)
{
	if(debug)
		cout << "INFO: " << message << endl;
}

char* infoString;
