//file: Ecdump.h
//Author: Mason Lilly
//Last Modified: 4/5/15

//This file declares the ecdump function, used by the TRANSY executor

#ifndef ECDUMP
#define ECDUMP

#include "Executor.h"

//This function reads a line of TRANSY code to perform a CDUMP operation, as specified in the user manual.
//Parameters:
//	line: A line of TRANSY object code
//Returns:
//	Nothing
//Preconditions:
//	The core object exists and is useable
void ecdump(vector<int> line);

#endif
