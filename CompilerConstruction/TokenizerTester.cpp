#include <iostream>

#include "Tokenizer.h"

using namespace std;

int main()
{
	tokenDFA.print();
	vector<Token> result = tokenizeLine("ABC\"L\"SD$TPQ0.5");
	for(Token t: result)
		cout << "Token: " << t.toString() << endl;
}
