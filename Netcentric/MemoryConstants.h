//file:MemoryConstants

#define MAX_PAGES 16
#define MAX_SEGMENTS 8
#define SEGMENT_SIZE 1024
#define PAGE_SIZE 64
#define NUM_FRAMES 8
#define MAX_PROCESSES 24

#define EMPTY 0

