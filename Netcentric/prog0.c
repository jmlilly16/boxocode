#include <stdio.h>
#include <string.h>
#include "prog0.h"

//I tried compiling using the BUFFER_SIZE constant, but encountered compiler errors that I could only fix by using a literal.
#define BUFFER_SIZE 5

static unsigned char bitBuffer[BUFFER_SIZE];
static unsigned char shift(unsigned char byte, int val)	//Needed to write this because shifting by a negative value doesn't work.
{
	if(val>0) return byte>>val;
	else
	{
		val = -val;
		return byte<<val;
	}
}

int putBB(unsigned char* source, unsigned startInd, unsigned count)
{
	if(startInd+count>8*BUFFER_SIZE) return 0;
	int sourceInd = 0, bitOffset = startInd % 8,destInd = startInd / 8;
	unsigned char mask=-1<<7;
	
	while(count)
	{
		//I see your yikes function and I raise you:
		bitBuffer[destInd] = (~(shift(mask,bitOffset))&bitBuffer[destInd])|(shift(source[sourceInd]&mask,bitOffset));
		mask>>=1;
		if(mask==0)
		{
			mask=-1<<7;
			sourceInd++;
			bitOffset+=8;
		}
		if(shift(mask,bitOffset)==0)
		{
			destInd++;
			bitOffset-=8;
		}
		count--;
	}
	return 1;
}
int getBB(unsigned char* dest, unsigned startInd, unsigned count)
{
	if(startInd+count>8*BUFFER_SIZE) return 0;
	int sourceInd = startInd / 8, destInd = 0, bitOffset = startInd % 8;
	unsigned char mask =-1<<7;
	
	while(count)
	{
		dest[destInd] = (dest[destInd]&(~mask))|(shift(shift(mask,bitOffset)&bitBuffer[sourceInd],-bitOffset));
		mask>>=1;
		if(mask==0)
		{
			mask =-1<<7;
			destInd++;
			bitOffset+=8;
		}
		if(shift(mask,bitOffset)==0)
		{
			sourceInd++;
			bitOffset-=8;
		}
		count--;
	}
	return 1;
}

void dumpBB()
{
	unsigned char byte = 0,mask = 1;
	int i=0,j=0;
	for(i=0;i<BUFFER_SIZE;i++)
	{
		byte = bitBuffer[i];
		mask = 1 << 7;
		for(j=0;j<8;j++)
		{
			if(byte&mask) putchar('1');
			else putchar('0');
			mask >>= 1;
			if((j+1)%4==0) putchar(' ');	
		}
	}
	putchar('\n');
}
static void printByte(unsigned char byte)
{
	int j=0;
	unsigned char mask=0;
	mask = 1 << 7;
	for(j=0;j<8;j++)
	{
		if(byte&mask) putchar('1');
		else putchar('0');
		mask >>= 1;
		if((j+1)%4==0) putchar(' ');
	}
	putchar('\n');
}
