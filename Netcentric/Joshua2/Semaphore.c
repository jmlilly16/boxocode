//file:Semaphore.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file implements the functions defined in Semaphore.h

#include <stdlib.h>
#include <stdio.h>
#include "Scheduler.h"
#define DEFSCHED
#include "JoshuaConstants.h"
#include "PCB.h"
#include "Queue.h"
#include "Semaphore.h"

struct semaphore
{
	int available;
	Queue* waiting;
};

Semaphore* initSemaphore(int i)
{
	Semaphore* sem = malloc(sizeof(Semaphore));
	sem-> available = i;
	sem-> waiting = initQueue();
	return sem;
}

void terminateSemaphore(Semaphore* sem)
{
	terminateQueue(sem-> waiting);
	free(sem);
}

//Decrements the availability of s, and if it becomes negative blocks the current process
void check(Semaphore* s)
{
	if(--(s-> available) < 0)
	{
		Scheduler_currentProcess() -> state = BLOCKED;
		Q_push(s->waiting,Scheduler_currentProcess());
		printf("\tProcess %s has become BLOCKED by a semaphore.\n",Scheduler_currentProcess()->name);
		getchar();
	}
}

//Increments the availability of s and, if possibe, readies the next waiting process
void clear(Semaphore* s)
{
	if(++(s-> available) < 1) //((PCB*)Q_pop(s->waiting))-> state = READY;
	{
		PCB* p = (PCB*)Q_pop(s->waiting);
		p->state = READY;
		printf("Semaphore cleared, %s is now READY\n",p->name);
		getchar();
	}
}
