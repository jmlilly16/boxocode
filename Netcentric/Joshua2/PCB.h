//file:PCB.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This header defines the Process Control Block structure, and includes a prototype for the state2Text debug function

#ifndef PCb
#define PCb
#include "moses_m.h"
#define DEFMEM
#include "JoshuaConstants.h"

//Stores all information associated with a process
typedef struct pcb
{
	REGTYPE registers[5];
	int timeLeft,lastTimeLeft,totalTimeLeft;
	long lastPCounter;
	long PCounter;
	char* name;
	int state;
	int numSegs;
	unsigned char segments[MAX_SEGMENTS];
} PCB;

char* state2Text(PCB*);

#endif
