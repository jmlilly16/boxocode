//file: CycleList.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This data structure manages nodes that point to data and each other in such a way as to create a closed 'loop' of data, i.e. following the data links will visit each node once and return to the starting point.
//The structure consists of one pointer, a "spinner" that points to whichever node happens to be the "top" of the cycle at this moment.
//"Rotating" the cycle consists of pointing the spinner to spinner->next
//When the set is initialized, the spinner points to an empty node which points to itself.
//**No node will ever not point to another node in the list**
//When the first data item is inserted, it is placed in this node.
//For all other insertions, the data is placed in a new node created just before the current top node.

#ifndef CYCLELIST
#define CYCLELIST

typedef struct CLNode			//CLNode holds pointers to the data and the next node in the cycle
{
	void* data;
	struct CLNode* next;
} CLNode;

typedef struct CycleList
{
	//Holds the address of the current "top" node of the cycle
	CLNode* spinner;
} CycleList;

static CLNode* initCLNode();
CycleList* initCycleList();
void endCycleList(CycleList*,int);

void CycleList_insert(CycleList*,void*);
void* CycleList_getData(CycleList*);
void CycleList_rotate(CycleList*);
void* CycleList_remove(CycleList*);

int CycleList_size(CycleList*);
#endif
