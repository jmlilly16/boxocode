//file:JoshuaConstants.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//Programmer's Note:
//I ended up with so many constants used by so many places that I put them all in here.
//No code here, just lots of Preprocessor definitions.
//A file that includes this file should, immediately before the include directive,
//define any of DEFPSWMAP, DEFSCHED, DEFMEM, or DEFCALLTYPES.
//Each one will cause a certain block of constants to be defined for that file.
//This isn't really necessary, but in case the program gets even bigger I thought it'd help to keep things separate.

//#ifndef JOSHUACONSTS
//#define JOSHUACONSTS

//Constants showing what data is in which bits of the PSW
#ifdef DEFPSWMAP
#define USERMODE1 0
#define USERMODE2 1
#define INTERRUPTS 2
#define UNUSED1 3
#define UNUSED2 4
#define TERMINATEFLAG 5
#define EXCEPTIONFLAG 6
#define TIMERFLAG 8
#define SYSCALLFLAG 9
#define SCTYPE1 10
#define SCTYPE2 11
#define SCTYPE3 12
#define SCTYPE4 13
#define VMEMTYPE1 14
#define VMEMTYPE2 15
//NOTE: Bits 16-31: Timer
//NOTE: Bits 64-127 Program Counter
#undef DEFPSWMAP
#endif

#ifdef DEFSCHED
//Process States
#define READY 1		//Will run the next time it reaches the top of the queue
#define RUNNING 2	//Currently in the CPU and running
#define INTERRUPTED 3	//Set any time a process exits the PCB to have an interrupt handled.
#define BLOCKED 4	//Waiting for some outside activity; skipped by the scheduler
#define PASSING 5	//Instructs the scheduler to skip a process once, then set it to READY
#define TERMINATED -1	//Set when a process exits the scheduling queue.
//Time Management
#define TIMESLICE 64
#define RUNTIME 2048
#undef DEFSCHED
#endif

#ifdef DEFMEM
//Memory Limits
#define MAX_PAGES 16
#define MAX_SEGMENTS 8
#define SEGMENT_SIZE 1024
#define PAGE_SIZE 64
#define NUM_FRAMES 8
#define MAX_PROCESSES 24
//Frame State Flags
#define EMPTY 1		//Frame has no meaningful data in it; can be used as needed
#define LOCKED 2	//Frame has been recently filled with data that has not yet been accessed
#define REFERENCED 4	//Frame was read from or written to since it was last considered for replacement
#define MODIFIED 8	//Frame has been modified since it was loaded
//Segment Protection Flags
#define READONLY 0
#define WRITEABLE 1
#define SHARED 2
//Empty Constants
#define UNLOADED -1	//Set for a page that does not exist in a frame
#define FAULTADDR (VMEMADDR)-1	//Used to represent an address that could not be resolved due to a page fault
#define CRASHADDR -2		//Used to represent a read/write operation that failed due to a crashed process
#define NILSEGMENT -1
#define FAULT 0		//Returned by a MM function that resulted in a page fault
//Address BitManipulation
#define SYSSEGMASK 261120	//The system segment table can have much larger indices, so a longer mask is needed
#define SEGMASK 7168
#define DISPMASK 63
#define PAGEMASK 960
#define FRAMEMASK 448
#define SEGNUMSHIFT 10
#define PAGENUMSHIFT 6
//Operation Types - Constants passed to MemoryManager.c::makePhysAddr() to indicate a read or write operation
#define READ 1
#define WRITE 2
#undef DEFMEM
#endif

#ifdef DEFCALLTYPES	//Constants that indicate the meaning of values left in the PSW
#define TERMINATE 5
#define CRASH 6
#define TIMER 8

#define VMEMACCESS 3
#define NEWPROC 5
#define IOREQ 8
#define IOEXEC 9
#define IOCOMPL 10

#define MEMREQ 0
#define MEMFETCH 1
#define MEMWRITE 2
#define MEMCOMPL 3
#undef DEFCALLTYPES
#endif

//#endif
