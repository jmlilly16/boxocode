//file: Scheduler.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file represents the implementation of the Scheduler pseudoobject described in Scheduler.h

#include <stdlib.h>
#include <stdio.h>
#include "Scheduler.h"
#define DEFSCHED
#include "JoshuaConstants.h"

//The queue itself is hidden in this file, to keep it local
static CycleList* Q;

void Scheduler_init()
{
	Q = initCycleList();
}
void Scheduler_terminate()
{
	endCycleList(Q,1);
}

//Takes a newly constructed process and gives it a spot in the queue. Also loads up its time account to a full RUNTIME.
void Scheduler_addProcess(PCB* process)
{
	printf("Scheduler received the process \"%s\". Adding it to the queue.\n",process -> name);
	printf("\tQueue size before insertion: %d.\n",CycleList_size(Q));
	CycleList_insert(Q,process);
	printf("\tQueue size after insertion: %d.\n",CycleList_size(Q));
	process -> totalTimeLeft = RUNTIME;
	printf("\tGave %s %d units of runtime.\n",process -> name,RUNTIME);
	Scheduler_printQueue();
}
//Removes a given process from the queue
void Scheduler_removeProcess(PCB* process)
{
	//Since often you'll be terminating the most recently running process, go ahead and check for that first.
	if(CycleList_getData(Q) == process)
	{
		CycleList_remove(Q);
		Scheduler_printQueue();
		return;
	}
	//Otherwise, start looking through the cycle. 
	PCB* returnProcess = CycleList_getData(Q);
	CycleList_rotate(Q);
	PCB* testProcess = CycleList_getData(Q);
	while(testProcess!=process && testProcess!=returnProcess)
	{
		CycleList_rotate(Q);
		testProcess = CycleList_getData(Q);
	}
	if(testProcess==returnProcess) return; //Didn't find it
	else
	{
		CycleList_remove(Q);		//Found it, remove it.
		//Now put the queue back where it was.
		while(CycleList_getData(Q) != returnProcess) CycleList_rotate(Q);
	}
	Scheduler_printQueue();
}

//Possibly the most important function in Joshua.
//This function analyzes processes in the Scheduling Queue until it finds one fit to run.
//Processes that were INTERRUPTED (usually only the top process) get the CPU back if they had time left.
//	Otherwise, a timer interrupt is assumed and they are marked READY and rotated out.
//Processes marked READY were either rotated out by a timer interrupt or were blocked by a system call and are now ready to execute again.
//	In case of the former, they are given a fresh time slice and loaded.
//	In case of the latter, they will have some time left and are allowed to finish executing that.
//Processes that are BLOCKED are waiting for something and are immediately skipped.
//A process that is PASSING is skipped and set to READY.
//A process with any other state is skipped since that means something went wrong. 
PCB* Scheduler_readyNextProcess()
{
	PCB* currentProcess = (PCB*)CycleList_getData(Q);
	switch(currentProcess -> state)
	{
		/*case TERMINATED://This process just died, remove it.
			printf("Removing and deleting terminated process %s from the queue",currentProcess -> name);
			Scheduler_removeProcess(currentProcess);	//Take it out
			free(currentProcess);				//Delete it
			currentProcess = CycleList_getData(Q);
			
			return Scheduler_readyNextProcess();		//Back to what you were doing*/
		case INTERRUPTED:	//The process was interrupted, see if it should get control back
			if(currentProcess -> timeLeft)
			{	
				printf("\t%s has %d time units left in its slice, returning control\n",currentProcess-> name,currentProcess-> timeLeft);
				return currentProcess;
			}
			//If the process is interrupted AND out of time, it was a timer interrupt.
			//Rotate the queue and check the next one.
			else
			{
				currentProcess -> state = READY;
				printf("\t%s is out of time, moving to next process\n",currentProcess-> name);
				CycleList_rotate(Q);
				return Scheduler_readyNextProcess();
			}			
		case BLOCKED:		//Skip this process, it's not ready to run
			printf("\t%s is BLOCKED, skipping it.\n",currentProcess->name);
			CycleList_rotate(Q);
			return Scheduler_readyNextProcess();
		case PASSING:
			currentProcess -> state = READY;
			CycleList_rotate(Q);
			return Scheduler_readyNextProcess();
		case READY:	//Hand out a new time slice to the next process in line.
			printf("\tNew active process is %s.",currentProcess -> name);
			if(currentProcess -> timeLeft)
			{
				printf(" It has %d units left in its current slice.",currentProcess-> timeLeft);
			}
			else
			{
				printf(" Giving it a fresh time slice.\n");
				//Assume that the totalTimeLeft was positive (timeouts are handled in the timer interrupt handler), so give it a slice.	
				currentProcess -> totalTimeLeft -= TIMESLICE;
				currentProcess -> timeLeft = TIMESLICE;
				printf("\t%s has %d units of runtime remaining",currentProcess -> name,currentProcess -> totalTimeLeft);
			}
			printf("\n");
			currentProcess -> state = RUNNING;
			return currentProcess;
		default://This case shouldn't ever run.
			printf("WARNING: Process with unknown state detected. Skipping it...\n");
			CycleList_rotate(Q);
			return Scheduler_readyNextProcess();
	}
}

//Returns the active process. I plan to replace this with an external variable declaration soon.
PCB* Scheduler_currentProcess()
{
	return CycleList_getData(Q);
}

//Returns the number of running processes
int Scheduler_queueSize()
{
	return CycleList_size(Q);
}

//Debugging function that prints the current state of the queue.
void Scheduler_printQueue()
{
	printf("Current Queue: ");
	int i,j=CycleList_size(Q);
	for(i=0;i<j;i++)
	{
		printf("%s ",((PCB*)CycleList_getData(Q)) -> name);
		CycleList_rotate(Q);
	}
	printf("\n");
}
