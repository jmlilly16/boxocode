//file:Queue.h
//Mason Lilly

//This file defines a standard Queue, to be implemented in C on top of a LinkedList

#ifndef QUEUE
#define QUEUE
#include "LinkedList.h"

typedef struct queue Queue;

Queue* initQueue();
void terminateQueue(Queue*);

void Q_push(Queue*,void*);
void* Q_pop(Queue*);

#endif
