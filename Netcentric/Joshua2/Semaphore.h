//file: Semaphore.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file was added to Joshua for Project I Program 3 - Shared Memory Management
//It defines the Semaphore structure, used by Joshua to control when programs get access to the IO buffer.

#ifndef SEMAPH
#define SEMAPH

typedef struct semaphore Semaphore;

Semaphore* initSemaphore(int i);	//Creates and returns a new Semaphore with the starting availability of i
Semaphore* initSemaphore();		//This overload assumes a binary Semaphore and calls the other function with (1)
void terminateSemaphore(Semaphore*);	//Frees all memory associated with a semaphore

void check(Semaphore*);			//Decrements the availability of s, and if it becomes negative blocks the current process
void clear(Semaphore*);			//Increments the availability of s and, if possibe, readies the next waiting process

#endif
