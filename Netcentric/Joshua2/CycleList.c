//file: CycleList.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This is the implementation of the data structure defined in CycleList.h

#include <stdlib.h>
#include <stdio.h>
#include "CycleList.h"

//Sets up the cycle with an empty dummy node, returns a pointer to the list
CycleList* initCycleList()
{
	CycleList* list = malloc(sizeof(CycleList));
	list -> spinner = initCLNode();
	list -> spinner -> next = list -> spinner;
	return list;
}

//Makes a node and returns it
static CLNode* initCLNode()
{
	CLNode* node = malloc(sizeof(CLNode));
	node -> data = 0;
	node -> next = 0;
	return node;
}

//Puts a new node pointing at data at the "end" of the cycle
void CycleList_insert(CycleList* list, void* data)
{
	//Abort if the list is unitialized
	if(list -> spinner == 0)
	{
		return;
	}
	//If all that's in the list is the dummy node, put the data in it
	if(list -> spinner -> data == 0)
	{
		list -> spinner -> data = data;
	}
	//Otherwise, find the "last" node and insert a new node after it, then point that node at the spinner
	else	
	{
		CLNode* tracer = list -> spinner;
		while(tracer -> next != list -> spinner)
		{
			tracer = tracer -> next;
		}
		tracer -> next = initCLNode();
		tracer = tracer -> next;
		tracer -> data = data;
		tracer -> next = list -> spinner;
	}
}

//Gets the data of the node pointed at by the spinner
void* CycleList_getData(CycleList* list)
{
	if(list -> spinner == 0) return 0;
	return list -> spinner -> data;
}

//Rotates the spinner so that it points at the next element
void CycleList_rotate(CycleList* list)
{
	if(list -> spinner == 0) return;
	list -> spinner = list -> spinner -> next;
}

//Removes the node pointed at by the spinner, then returns a pointer to the data that was in it
void* CycleList_remove(CycleList* list)
{
	if(list -> spinner == 0) return 0;
	void* savedData = list -> spinner -> data;
	//if you're removing the last node in the list, just empty it to make it a dummy node.
	if(list -> spinner -> next == list -> spinner)
	{
		list -> spinner -> data = 0;
	}
	else
	{
		CLNode* tracer = list -> spinner;
		while(tracer -> next != list -> spinner)
			tracer = tracer -> next;
		tracer -> next = list -> spinner -> next;
		free(list -> spinner);
		list -> spinner = tracer -> next;
	}
	return savedData;
}

//Makes a list ready for deletion. deleteData should be 0 if the cycle points to data you stll want to use.
void endCycleList(CycleList* list,int deleteData)
{
	void* data=list -> spinner -> data;
	while(data)
		data = CycleList_remove(list);
	free(list -> spinner);
	free(list);
}

//Gets the length of a CycleList
int CycleList_size(CycleList* list)
{
	CLNode* startNode = list -> spinner;
	int count = 0;
	do
	{
		list -> spinner = list -> spinner -> next;
		count++;
	}while(list -> spinner != startNode);
	if(list -> spinner -> data == 0) return 0;
	return count;
}
