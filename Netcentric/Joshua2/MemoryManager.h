//file:MemoryManager.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file defines and describes the structures and public functions used by Joshua's Memory Manager.

#ifndef MEM
#define MEM
#include "moses_m.h"
#define DEFMEM
#define DEFCALLTYPES
#include "JoshuaConstants.h"
#include "PCB.h"
#include "Semaphore.h"

//Represents one Vmem Frame.
//frameAddr is used to hold the physical address of the frame, set when the manager is initialized
//state is used to keep track of what's been done to the frame lately.
//is a bitmask with four flags: (EMPTY|LOCKED|REFERENCED|MODIFIED). Their meanings are explained in JoshuaConstants.c.
//segNum and pageNum store the segment and page currently held within a frame.
typedef struct frame
{
	VMEMADDR frameAddr;
	unsigned char state;
	unsigned char segNum;
	unsigned char pageNum;
} Frame;

//Represents a page. An array of these is held by each segment.
//Its frame field holds either the number of the frame currently holding the frame
//or UNLOADED to indicate it is not present in memory.
typedef struct page
{
	int frame;
} Page;

//Rerpresents one data segment.
//Holds the disk address, protection level, and length of a segment, plus an array of the pages it owns.
//Also keeps track of how many processes claim it as a segment.
//Also points at a semaphore that keeps shared segments safe from race conditions.
//NOTE on how this structure is used: the MM keeps track of a global table of these, so that each disk address is only loaded once.
//Each process does have its own table, but it is an array of integers representing the indices of the entries in the system table
//that the process claims.
//Almost everywhere in this program that refers to a segment number does so with respect to the system, not a process.
typedef struct segment
{
	VDISKADDR diskAddr;
	unsigned char protectionMask;
	unsigned char ownerCount;
	int length;
	Page pages[MAX_PAGES];
	Semaphore* semaphore;
} Segment;

//Initializes the frame and segment tables and opens the log file for output
void MM_init();
//Terminates the dynamically allocated semaphores associated with each segment
void MM_terminate();

//Called by the Interrupt Handler upon process initialization.
//Parses a process's segment address list and tries to reserve a spot in the system segment table for each one requested.
void MM_claimSegments(REGTYPE* segAddrList, PCB* owner);
//Called by the Interrupt Handler on process termination.
//Causes the MM to release a process's hold on all the segments it owns, clearing segment table entries and unloading pages as necessary
void MM_releaseSegments(PCB* process);

//Checks the semaphore on a given shared segment to protect upcoming operations.
void MM_checkSegment(PCB* process, VDISKADDR addr);
//Clears the semaphore on a given shared segment so other processes can use it.
void MM_clearSegment(PCB* process, VDISKADDR addr);

//Attempts to read data from the logical address specified.
//If the logical address exists in memory, the read operation is performed and 1 is returned.
//If it is not, the MM will attempt to load the necessary page(s), and a 0 is returned to indicate failure.
int MM_read(PCB* process, LOGICADDR address,int bytes, char* dest);
//Same as above, but calls a different moses function and will mark accessed frames as modified.
int MM_write(PCB* process, LOGICADDR address,int bytes, char* source);

#endif
