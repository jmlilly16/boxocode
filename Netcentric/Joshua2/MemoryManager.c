//file:MemoryManager.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file implements the functions described in MemoryManager.h, and includes several helper functions.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "moses_m.h"
#include "Semaphore.h"
#include "InterruptHandlers.h"
#include "MemoryManager.h"

static Frame frames[NUM_FRAMES];
static Segment segmentTable[MAX_PROCESSES*MAX_SEGMENTS];
static FILE* swapLog;
static int faultCount=0;

//**PROTOTYPES**//
//This file uses several helper functions, described with their prototypes below.

/*
This function takes in a segment address and checks all existing segments to see if their addresses match the one requested.
If a match is found, it marks that segment as having one additional owner and returns the *system* index of the found segment.
If not, it fills the next entry in the system segment table with the necessary information and returns the index of this new entry.
Called by:	MM_claimSegments() for each segment address a process requests.
Calls:		None
*/
static int reserveSegment(VDISKADDR,unsigned char,int);

/*
This function takes in a frame number and, if that frame holds data,
saves that data to its proper segment on disk by calling pageW() with
the frame's physical address and the segment address and page number of the page loaded in the frame.
The frame is then marked as EMPTY and the page is marked as UNLOADED.
Called by:	releaseSegment() for each page in an outgoing segment that is still in a frame
		loadPage() before data is loaded into a dirty frame
Calls:		None
*/		
static void savePage(int);

/*
This function uses the Second Chance algorithm to select a frame to receive data.
Each time it is called, it scans through the frames, checking their states.
Any frame that is REFERENCED or LOCKED is passed over.
As each frame is observed, the REFERENCED flag is unset, so that if it is observed a second time, it might be selected.
As soon as the function observes a frame that is either EMPTY or neither LOCKED nor REFERENCED, that frame's number is returned.
The victim number is static, so that each time the function is called it picks up with the frame after the last victim.
Called by:	loadPage() to determine what frame to use
Calls:		pageW() to save the data
*/
static int selectFrame();

/*
This function takes a system segment number and a page number in that segment and attempts to load it into a frame.
It selects the frame using selectFrame(). If that frame is occupied, it first empties it with savePage().
Then it sends that frame's address to pageR() along with the segment address of the segment requested and the page number.
The page is marked as being loaded into the frame, and the frame is marked as LOCKED, so it will not be overwritten before it is accessed.
Called by:	makePhysAddr() when a page is requested that is not in memory
Calls:		selectFrame() to choose a victim frame
		savePage() if that frame is empty
		pageR() to load the data
*/
static void loadPage();

/*
This function decrements a given segment's owner count. If this brings it to 0, it saves all currently loaded pages of that segment, to free memory and to reflect any final changes make by a closing process to its data.
Called by:	releaseSegments() for each segment owned by a process
Calls:		savePage()
*/
static void releaseSegment();

/*
This function converts the logical addresses that processes give to the logical addresses that the system uses.
Essentially all it does is replace the bits for the segment number with the system segment number.
It also checks to make sure the requested segment number corresponds to a real, valid system segment
and will crash the calling process if it doesn't.
Called by:	MM_read()
		MM_write()
Calls:		none
*/
static LOGICADDR makeSysAddr(PCB*,LOGICADDR);

/*
This is possibly the most important function in the Memory Manager
It takes in a process and a logical address with respect to the system (calculated by makePhysAddr()),
then calculates a physical address.
It then checks if that address is loaded in memory.
If it is not, it causes a page fault and calls loadPage().
If it is, it REFERENCES that address. This can be either a READ-reference or a WRITE-reference, specified by the third parameter.
Either reference will cause the function to mark the frame containing the accessed data as REFERENCED, for purposes of frame victim-selection.
However, a WRITE-reference will also mark that frame as MODIFIED.
A WRITE operation will also cause the function to check if the requested segment is write-protected.
If it is, it will crash the calling process
Once all this is completed, the function returns the physical address of the memory referenced.
Called by:	MM_read()
		MM_write()
Calls:		loadPage()
*/
static VMEMADDR makePhysAddr(PCB*,LOGICADDR,int);

void MM_init()
{
	long int i;
	for(i=0;i<NUM_FRAMES;i++)
	{
		frames[i].state = EMPTY;
		frames[i].frameAddr = (VMEMADDR)(i<<6);
	}
	for(i=0;i<MAX_PROCESSES*MAX_SEGMENTS;i++)
	{
		segmentTable[i].length = 0;
		segmentTable[i].semaphore = initSemaphore(1);
	}
	swapLog = fopen("paging.txt","w");
}

void MM_terminate()
{
	int i;
	for(i=0;i<MAX_PROCESSES*MAX_SEGMENTS;i++)
	{
		terminateSemaphore(segmentTable[i].semaphore);
	}
}


static int reserveSegment(VDISKADDR segAddr,unsigned char key,int len)
{
	int i=0;
	//check all the segments already in use
	while(segmentTable[i].length!=0)
	{
		if(segmentTable[i].diskAddr == segAddr)
		{
			//if it's already there, add to its owner count, then return that segment ID.
			segmentTable[i].ownerCount++;
			return i;
		}
		else i++;
	}
	//If you didn't find the requested segment, make a table entry for it.
	segmentTable[i].diskAddr = segAddr;

	//The protection number is a bitmask with 1 for write and 2 for shared. If the given protection number is SHARED, also set the WRITEABLE bit.
	segmentTable[i].protectionMask = key|(key==SHARED?WRITEABLE:0);

	segmentTable[i].ownerCount = 1;
	segmentTable[i].length = len;
	int j;
	for(j=0;j<MAX_PAGES;j++)
	{
		segmentTable[i].pages[j].frame=UNLOADED;
	}	
	//segDump(segAddr);
	return i;
}

void MM_claimSegments(REGTYPE* segAddrList, PCB* owner)
{
	VDISKADDR tempSegAddrs[MAX_SEGMENTS];
	unsigned int tempSegLens[MAX_SEGMENTS];
	unsigned char tempSegKeys[MAX_SEGMENTS];
	char tempSegNum=0;
	unsigned int* intscanner=(unsigned int *)segAddrList;
	
	while((*segAddrList)!=0)
	{
		printf("segPointer: %X\n",(unsigned)((long unsigned)segAddrList));
		tempSegLens[tempSegNum]=*((unsigned int *)(segAddrList++));
		intscanner=(unsigned int *)segAddrList;
		tempSegAddrs[tempSegNum]=(VDISKADDR)((long unsigned int)(*(intscanner++)));
		tempSegKeys[tempSegNum]=(unsigned char)(*(intscanner));
		printf("scanner: %X,%d\n",(unsigned)((long unsigned)intscanner),*intscanner);
		tempSegNum++;
		segAddrList++;
	}
	int i;
	for(i=0;i<tempSegNum;i++)
	{
		printf("Segment %d bytes long at %X, key %d\n",tempSegLens[i],(unsigned int)((long unsigned)tempSegAddrs[i]),tempSegKeys[i]);
	}
	owner -> numSegs = tempSegNum;
	for(i=0;i<tempSegNum;i++)
	{
		owner -> segments[i] = reserveSegment(tempSegAddrs[i],tempSegKeys[i],tempSegLens[i]);
	}
	printf("%s Segment Table\n ",owner->name);
	i=0;
	while(i<MAX_SEGMENTS)
	{
		printf("%d, ",owner->segments[i]);
		i++;
	}
	//getchar();
	/*printf("\nNew Global Segment Table:\n");
	i=0;
	while(i<MAX_PROCESSES*MAX_SEGMENTS)
	{	
		printf("%d:%X, %d\n",i,(unsigned)(long unsigned)segmentTable[i].diskAddr,segmentTable[i].protectionMask);
		i++;
	}
	getchar();*/
}

//Saves the contents of the given frame to disk, and marks the page as UNLOADED and the frame as EMPTY
static void savePage(int frameNum)
{
	printf("\tSaving contents of frame %d\n",frameNum);
	//Ensure this frame isn't empty
	if(frames[frameNum].state&EMPTY) return;
	int oldSeg = frames[frameNum].segNum, oldPage = frames[frameNum].pageNum;
	//The write-to-disk operation
	printf("\nHere is the segment that's about to be saved:\n");
	segPrint(segmentTable[oldSeg].diskAddr);
	pageW(frames[frameNum].frameAddr,segmentTable[oldSeg].diskAddr,oldPage);
	printf("\nHere it is, updated with the saved data:\n");
	segPrint(segmentTable[oldSeg].diskAddr);
	getchar();
	//Tell the outgoing page it's been replaced;
	printf("\tSegment %d, page %d has been UNLOADED\n",oldSeg,oldPage);
	fprintf(swapLog,"Offloaded dirty page %d from system segment %d (x%X) to disk\n",oldPage,oldSeg,(unsigned)(long unsigned)segmentTable[oldSeg].diskAddr);
	//printf("\t(Dumping Vmem)\n");
	//vmemDump();
	segmentTable[oldSeg].pages[oldPage].frame = UNLOADED;
	printf("\tFrame %d is now EMPTY\n",frameNum);
	frames[frameNum].state=EMPTY;
	//Output the segment whose disk data was just updated
	//printf("Dumping updated segment\n");
	//segDump(segmentTable[oldSeg].diskAddr);
	//getchar();
}

//TODO:What if they're all locked?
static int selectFrame()
{
	static int victim = 0;
	printf("Choosing a victim frame\n");
	if(!(victim<NUM_FRAMES)) victim=0;
	//Look for a frame that is either EMPTY, or neither LOCKED nor REFERENCED
	printf("\tFrame %d's state is %d\n",victim,frames[victim].state);
	while(!(frames[victim].state&EMPTY) && frames[victim].state&(LOCKED|REFERENCED))
	{
		printf("\tFrame %d won't work, trying the next one...\n",victim);
		frames[victim].state&=~REFERENCED;
		printf("\tFrame %d's new state is %d\n",victim,frames[victim].state);
		//getchar();
		victim++;
		if(!(victim<NUM_FRAMES)) victim=0;
		printf("\tFrame %d's state is %d\n",victim,frames[victim].state);
	}
	return victim++;
}

//Loads a given page within a given segment into the next available frame.
//The frame to be filled is selected with the selectFrame() function.
//If the selected fram has modified data in it, offloads that data to disk.
static void loadPage(int segNum, int pageNum)
{
	//Ensure this page isn't already in a frame
	if(segmentTable[segNum].pages[pageNum].frame!=UNLOADED) return;
	//Pick a frame using the Second Chance Algorithm
	int targetFrame = selectFrame();
	if(targetFrame==-1)
	{
		printf("No available frames. Page request denied.\n");
		fprintf(swapLog,"No frames were available; request ignored.\n");
		return;
	}
	printf("\tSelected frame %d to hold the page.\n",targetFrame);
	fprintf(swapLog,"Frame %d was selected\n",targetFrame);
	//If this frame is dirty, dump it to disk
	if(frames[targetFrame].state&MODIFIED) savePage(targetFrame);
	//The read-from-disk operation
	pageR(frames[targetFrame].frameAddr,segmentTable[segNum].diskAddr,pageNum);
	//LOCK the frame, since it's new. Also clear all other status flags.
	frames[targetFrame].state=LOCKED;
	frames[targetFrame].segNum = segNum;
	frames[targetFrame].pageNum = pageNum;
	//Mark the page that was loaded as present in this frame.
	segmentTable[segNum].pages[pageNum].frame = targetFrame;
	printf("\tFrame %d has been loaded with segment %d, page %d\n",targetFrame,segNum,pageNum);
	printf("\tFrame %d's new state is %d\n",targetFrame,frames[targetFrame].state);
	//printf("\t(Dumping Vmem)\n");
	//vmemDump();
	//getchar();
}

//This function is called by MM_releaseSegments() for each segment owned by the outgoing process.
//It decrements the given segment's owner count in the system segment table,
//and if this brings it to zero it invokes savePage() to dump it to disk.
static void releaseSegment(int segNum)
{
	if(--(segmentTable[segNum].ownerCount)<1)
	{
		int i;
		for(i=0;i<MAX_PAGES;i++)
		{
			if(segmentTable[segNum].pages[i].frame!=UNLOADED)
			{
				printf("Segment %d, page %d is in frame %d.\n",segNum,i,segmentTable[segNum].pages[i].frame);
				savePage(segmentTable[segNum].pages[i].frame);
			}
		}
	}
}
	
void MM_releaseSegments(PCB* process)
{
	int i=0;
	printf("Process %s owns segments ",process->name);
	while(i<MAX_SEGMENTS)
	{
		printf("%d, ",process->segments[i]);
		i++;
	}
	printf("\n");
	i=0;
	while(i<process->numSegs)
	{
		printf("Releasing %s's hold on system segment %d\n",process->name,process->segments[i]);
		//getchar();
		releaseSegment(process->segments[i]);
		i++;
	}
	//getchar();
}

static LOGICADDR makeSysAddr(PCB* process, LOGICADDR logic)
{
	printf("%s is accessing logical address %X:\n\tSegment %d\n\tPage %d\n\tDisplacement %d\n",process->name,(unsigned)(long unsigned)logic,(int)((long)logic&SEGMASK)>>SEGNUMSHIFT,(int)((long)logic&PAGEMASK)>>PAGENUMSHIFT,(int)(long)logic&DISPMASK);
	int locSegNum = ((long)logic&SEGMASK)>>SEGNUMSHIFT;
	if(!(locSegNum<process->numSegs))
	{
		printf("\t%s tried to access a bad segment number (%d)\n",process->name,locSegNum);
		getchar();
		terminateProcess(process,CRASH);
		return (LOGICADDR)CRASHADDR;
	}
	unsigned sysSegNum = process->segments[locSegNum];
	printf("\tThis corresponds to System Segment %d. Shifted this is %X\n",sysSegNum,sysSegNum<<SEGNUMSHIFT);
	if(segmentTable[sysSegNum].length==0)
	{
		printf("\t%s tried to access a segment that doesn't exist (%d)\n",process->name,sysSegNum);
		getchar();
		terminateProcess(process,CRASH);
		return (LOGICADDR)CRASHADDR;
	}
	LOGICADDR sysAddr = (LOGICADDR)(long)((sysSegNum<<SEGNUMSHIFT)|((long)logic&(PAGEMASK|DISPMASK)));
	printf("...which is System Address %X:\n\tSegment %d\n\tPage %d\n\tDisplacement %d\n",
		(unsigned)(unsigned long)sysAddr,(int)((long)sysAddr&SYSSEGMASK)>>SEGNUMSHIFT,(int)((long)sysAddr&PAGEMASK)>>PAGENUMSHIFT,(int)((long)sysAddr&DISPMASK));
	return sysAddr;
}

static VMEMADDR makePhysAddr(PCB* process, LOGICADDR logic,int oppType)
{
	int segNum = ((long)logic&SYSSEGMASK)>>SEGNUMSHIFT;
	if(oppType==WRITE && !(segmentTable[segNum].protectionMask&WRITEABLE))//TODO: Set up the protection keys
	{
		printf("\t%s tried to write to a read-only segment\n",process->name);
		getchar();
		terminateProcess(process,CRASH);
		return (VMEMADDR)CRASHADDR;
	} 
	int pageNum = ((long int)logic&PAGEMASK)>>PAGENUMSHIFT;
	if(segmentTable[segNum].pages[pageNum].frame == UNLOADED)
	{
		//Page Fault!
		printf("\tFAULT! That page isn't loaded. Attempting to load page...\n");
		fprintf(swapLog,"\n--%d--\n%s requested system segment %d (x%X), page %d.\n",++faultCount,process->name,segNum,(unsigned)(long unsigned)segmentTable[segNum].diskAddr,pageNum);
		loadPage(segNum,pageNum);
		return FAULTADDR;
	}
	//Look up the physical address of the frame
	int frameNum = segmentTable[segNum].pages[pageNum].frame;
	VMEMADDR addr = frames[frameNum].frameAddr;
	printf("\tRequested data is in frame %d with address %X. With displacement:",frameNum,(unsigned)((long unsigned)addr));

	//Add in the displacement from the logical address
	addr = (VMEMADDR)((long)(addr) | ((long)logic)&DISPMASK);
	printf("%X\n",(unsigned)((long unsigned)addr));
	
	printf("\tChanged Frame %d's state from %d to ",frameNum,frames[frameNum].state);
	frames[frameNum].state&=(~LOCKED);
	frames[frameNum].state|=REFERENCED;
	if(oppType==WRITE) frames[frameNum].state |= MODIFIED;
	printf("%d\n",frames[frameNum].state);

	return addr;
}

//Performs a down operation on the semaphore associated with a given process. The semaphore will block the process if necessary.
void MM_checkSegment(PCB* process, VDISKADDR addr)
{
	int i=-1;
	while(segmentTable[++i].diskAddr != addr)
		if(!(segmentTable[i].length>0))
			terminateProcess(process,CRASH);
	check(segmentTable[i].semaphore);
}

//Performs an up operation on the semaphore associated with the process at addr. The semaphore will ready the next process if necessary.
void MM_clearSegment(PCB* process, VDISKADDR addr)
{
	int i=-1;
	while(segmentTable[++i].diskAddr != addr)
		if(!(segmentTable[i].length>0))
			terminateProcess(process,CRASH);
	clear(segmentTable[i].semaphore);
}

//This function takes in the calling process and information about the data requested and performs a read operation if possible.
//First, it uses the helper function makeSysAddr() to convert the supplied address to one the system understands.
//It then ensures that the requested operation will not run off the end of a segment and crashes it if it will.
//Next, it uses the helper function makePhysAddr() to READ-reference
//the physical address represented by the converted address.
//If makePhysAddr() returns with a fault, this function relays that fault by returning a failure.
//In the case of a page fault, the function can assume that makePhysAddr will load the needed page accordingly
//for the next time it is requested.
//Otherwise, this function passes the calculated physical address, length, and destination to vmemR to perform the read operation.
//If the requested operation runs off the end of a page but not the segment,
//it will perform the operation on the first page, then load the next page and finish the operation immediately.
//I.e. if a process doesn't return a fault on the first page of its operation, it gets to do the whole thing for free.
//Not the most equal opportunity, but this way the system avoids locking all available frames trying to load for a big read/write.
int MM_read(PCB* process, LOGICADDR logic, int length, char* dest)
{
	printf("Request to read data:\n");
	logic = (LOGICADDR)((long)logic & (SEGMASK|PAGEMASK|DISPMASK));
	LOGICADDR sysLogic = makeSysAddr(process,logic);
	if(sysLogic==(LOGICADDR)CRASHADDR) return CRASH;
	int segNum = ((long)sysLogic&SYSSEGMASK)>>SEGNUMSHIFT;
	int segDisplacement = (long)sysLogic&(PAGEMASK|DISPMASK);
	printf("\t%s is reading %d bytes from segment %d, starting at displacement %d. Segment %d is %d bytes long.\n",
		process->name,length,segNum,segDisplacement,segNum,segmentTable[segNum].length);
	if(segmentTable[segNum].length<length+segDisplacement)
	{
		printf("\t%s overflowed its segment\n",process->name);
		getchar();
		terminateProcess(process,CRASH);
		return CRASH;
	}
	VMEMADDR source = makePhysAddr(process,sysLogic,READ);
	if(source==FAULTADDR) return FAULT;
	int overflowLen = length - (PAGE_SIZE - ((long)sysLogic&DISPMASK));
	int origLength = length;
	if(overflowLen>0)
	{
		length-=overflowLen;
		printf("\tOperation overflows page by %d bytes, truncating length to %d\n",overflowLen,length);
	}
	printf("\nHere is the Vmem:\n");
	vmemPrint();
	vmemR(dest,source,length);
	char* result = malloc(origLength);
	strncpy(result,dest,length);
	result[origLength]='\0';
	printf("\n\tRead \"%s\" from Vmem\n",result);
	getchar();
	if(overflowLen>0)
	{
		logic+=length;
		dest+=length;
		int pageNum = ((long)logic&PAGEMASK)>>PAGENUMSHIFT;
		if(segmentTable[segNum].pages[pageNum].frame == UNLOADED)
		{
			printf("\t(Read operation crosses page boundary, preloading next page)\n");
			loadPage(segNum,pageNum);
		}
		MM_read(process,logic,overflowLen,dest);
		strncpy(result+length,dest,overflowLen);
		printf("\n\tRead \"%s\" from Vmem\n",result);
		getchar();
	}
	free(result);
	return 1;
}
//This function works exactly like MM_read except it calls makePhysAddr as a WRITE-reference and calls vmemW instead of vmemR.
//After all the additions to it in this version, I'd really like to combine the two
int MM_write(PCB* process, LOGICADDR logic, int length, char* source)
{	
	printf("Request to write data:\n");
	logic = (LOGICADDR)((long)logic & (SEGMASK|PAGEMASK|DISPMASK));
	LOGICADDR sysLogic = makeSysAddr(process,logic);
	if(sysLogic==(LOGICADDR)CRASHADDR) return CRASH;
	int segNum = ((long)sysLogic&SYSSEGMASK)>>SEGNUMSHIFT;
	int segDisplacement = (long)sysLogic&(PAGEMASK|DISPMASK);
	printf("\t%s is writing %d bytes to segment %d, starting at displacement %d. Segment %d is %d bytes long.\n",
		process->name,length,segNum,segDisplacement,segNum,segmentTable[segNum].length);
	if(segmentTable[segNum].length<length+segDisplacement)
	{
		printf("\t%s overflowed its segment\n",process->name);
		getchar();
		terminateProcess(process,CRASH);
		return CRASH;
	}
	VMEMADDR dest = makePhysAddr(process,sysLogic,WRITE);
	if(dest==FAULTADDR) return FAULT;
	int overflowLen = length - (PAGE_SIZE - ((long)sysLogic&DISPMASK));
	int origLength = length;
	if(overflowLen>0) 
	{
		length-=overflowLen;
		printf("\tOperation overflows page by %d bytes, truncating length to %d\n",overflowLen,length);
	}
	printf("\nHere is the Vmem before the write operation:\n");
	vmemPrint();
	vmemW(dest,source,length);
	printf("\nHere it is after the write operation:\n");
	vmemPrint();
	if(overflowLen>0)
	{
		logic+=length;
		source+=length;
		int pageNum = ((long)logic&PAGEMASK)>>PAGENUMSHIFT;
		if(segmentTable[segNum].pages[pageNum].frame == UNLOADED)
		{
			printf("\t(Write operation crosses page boundary, preloading next page)\n");
			loadPage(segNum,pageNum);
		}
		MM_write(process,logic,overflowLen,source);
	}
	char* result = malloc(origLength);
	strncpy(result,source,origLength);
	result[origLength]='\0';
	printf("\n\tWrote \"%s\" to Vmem\n",result);
	free(result);
	getchar();
	return 1;
}
