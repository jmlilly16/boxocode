//file:InterruptHandlers.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This header defines all the functions needed to handle different types of interrupts in Joshua.
//It also defines constants to represent each type of interrupt and system call.
#ifndef INTHNDLR
#define INTHNDLR

#include <stdlib.h>
#include "moses_m.h"
#include "Scheduler.h"
#include "Semaphore.h"
#define DEFCALLTYPES
#include "JoshuaConstants.h"

extern Semaphore* IO;

//Interrupts 5,6, and sometimes 8 - the int determines which "flavor" of termination you want.
void terminateProcess(PCB*,int);

//Interrupt 8 (Timer interrupt)
void checkTimeout(PCB*);

//Handles System Call 5. Constructs a process's PCB, then sends its memory list address to the memory manager, which loads its segments.
PCB* createProcess();

//System calls 8,9, and 10 are short enough to be handled entirely within the handleInterrupt function

//Called with each cycle of he Control Loop
//Determines what type of interrupt has occured, then handles it accordingly
void handleInterrupt();
#endif
