//file: InterruptHandlers.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file is the implementation of the functions defined in InterruptHandlers.h

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "InterruptHandlers.h"
#define DEFSCHED
#define DEFCALLTYPES
#define DEFPSWMAP
#define DEFMEM
#include "JoshuaConstants.h"
#include "moses_m.h"

//Creates a new process from the values in registers Ux and Vx, returns a Process Control Block to represent it
PCB* createProcess()
{
	PCB* newProcess = malloc(sizeof(PCB));
	newProcess -> name = malloc(sizeof(char));
	strcpy(newProcess -> name,Ux);
	int i;
	for(i=0;i<MAX_SEGMENTS;i++)
	{
		newProcess -> segments[i] = NILSEGMENT;
	}
	MM_claimSegments((REGTYPE)(Sx),newProcess);
	newProcess -> PCounter = (long)(Vx);
	newProcess -> totalTimeLeft = 0;
	newProcess -> timeLeft = 0;
	newProcess -> state = READY;
	//printf("Created and returned the new process \"%s\",\n\tLocated at %lX,\n\tWith first instruction at %lX.\n",newProcess -> name,(unsigned long)newProcess,(unsigned long)(newProcess -> PCounter));
	return newProcess;
}

//Called whenever a process terminates voluntarily, crashes, or timeouts.
//Instructs the Scheduler and Memory Manager to remove all data associated with the process, then echoes a termination message
void terminateProcess(PCB* process,int crash)
{
	process -> state = TERMINATED;
	Scheduler_removeProcess(process);
	MM_releaseSegments(process);
	printf("Process %s has ",process -> name);
	switch(crash)
	{
		case TERMINATE:	printf("terminated."); break;
		case CRASH: 	printf("crashed. We can't tell you why."); break;
		case TIMER: 	printf("timed out."); break;
		default: 	printf("ended...somehow."); break;
	}
	printf("\n");
	getchar();
	free(process);
}

//Called whenever a process requests data that isn't loaded into memory.
//Backs up the process's PC and Timer to their previous values, and marks it to be skipped by the scheduler.
void pageFault(PCB* process)
{
	printf("\t%s generated a Page Fault, passing its turn.\n",process->name);
	process -> state = PASSING;
	process -> PCounter = process -> lastPCounter;
	process -> timeLeft = process -> lastTimeLeft;
}

//Called upon timer interrupts
//Since time allocation is the job of the scheduler, this just checks to see if the total time budget of a process has been expended
void checkTimeout(PCB* process)
{
	if(process -> totalTimeLeft <= 0) terminateProcess(process,TIMER);
}

//This function is called every cycle of the Control Loop, determines what type of interrupt happened, and handles it accordingly
void handleInterrupt()
{
	PCB* process = Scheduler_currentProcess();
	printf("Handling CPU interrupt:\n");
	//NOTE: The Interrupt Type constants are not used here because these are physical bits that I can't change, whereas I could mess with the interrupt numbers.
	if(readBit((char*)PSW,TERMINATEFLAG)) {printf("\tTermination Interrupt detected\n");getchar();terminateProcess(process,TERMINATE);}
	else if(readBit((char*)PSW,EXCEPTIONFLAG)) {printf("\tCrash Interrupt detected\n");getchar();terminateProcess(process,CRASH);}
	else if(readBit((char*)PSW,TIMERFLAG)) {printf("\tTimer Interrupt detected\n");checkTimeout(process);}
	else if(readBit((char*)PSW,SYSCALLFLAG))
	{
		printf("\tSystem Call detected, checking type:\n");
		char* typePtr = (char*)PSW;
		unsigned char type = *(typePtr+1);
		type <<= 2;
		type >>= 4;
		printf("\t\tCall Code %d\n",type);
		switch(type)
		{
			case VMEMACCESS:
				printf("\t%s accessing Virtual Memory...\n",process-> name);
				type = *(typePtr+1);
				type <<=6;
				type >>=6;
				switch(type)
				{
					case MEMREQ:	MM_checkSegment(process,(LOGICADDR)(Rx));break;
					case MEMFETCH:	printf("\tAttempting to fetch data from Vmem\n");
							char* tempDest = malloc(1);
							switch(MM_read(process,(LOGICADDR)(Rx),(long int)(Sx),(char*)Tx))
							{
								case FAULT:	pageFault(process);break;
								case CRASH:	return;
								default:	break;
							}
							break;
					case MEMWRITE:	printf("\tAttempting to write data to Vmem\n");
							switch(MM_write(process,(LOGICADDR)(Rx),(long int)(Sx),(char*)Tx))
							{
								case FAULT:	pageFault(process);break;
								case CRASH:	return;
								default:	break;
							}
							break;
					case MEMCOMPL:	MM_clearSegment(process,(LOGICADDR)(Rx));break;
					default: 	break;
				}
				break;
			case NEWPROC: 	Scheduler_addProcess(createProcess()); break;
			case IOREQ: 	check(IO); break;
			case IOEXEC: 	printf("Process %s doing output stuff!\n",Scheduler_currentProcess()-> name);
					io((char*)Ux); break;
			case IOCOMPL: 	clear(IO);iofl(); break;
			default: 	printf("ERROR! Process %s requested unknown system call, terminating process\n",process->name);
					getchar();
					terminateProcess(process,CRASH);
		}
	}
}
