/* FILE: prog0dvr.c
 * R England, Transy U
 * CS 3074, Winter 2014
 * 
 *		Read and write bit patterns to a bit buffer array.
 */
#include	<stdio.h>
#include	<stdlib.h>
#include	<string.h>
#include	"prog0.h"

/* symbolic constants
 */
#define     QUIT     0
#define     PUT      1
#define     GET      2
#define     TRUE     1
#define     FALSE    0
#define     MAXSTR   256
#define     BYTE_BITS 8

/* function prototypes
 */
void testPut (void);
void testGet (void);
void printBits (unsigned char *s, unsigned count);
int	 getMenuResponse (void);
static void setBitLocal (unsigned char bArray[], int index, int bitValue);


/***
 ***    main program
 ***/
int main (void) {
	int stop = FALSE;
    printf ("\n\nTest Driver Program for Bit Buffer Functions\n\n");

/* loop as long as user wants
 */
	do {
		switch (getMenuResponse ()) {
		case QUIT:		
			stop = TRUE;
			break;
		case PUT:	
			testPut ();
			break;
		case GET:		
			testGet ();
			break;
		default:
			printf ("ERROR: Invalid response. Try again.");
			break;
		}

/* show the current state of the buffer
 */
        printf ("\n\nCurrent buffer contents:\n");
        dumpBB();
	} while (!stop);
	
    printf ("\n\nDone.\n");
	return 0;
}


/** getMenuResponse
 **    display a menu; read response from the keyboard
 **/
int getMenuResponse (void) {
	unsigned response;
    
/* read and return an integer that corresponds to a menu response
 */
    printf("\n\n\nType\t%d for PUT\n"
               "\t%d for GET\n"
               "\t%d for QUIT\t: ",
               PUT, GET, QUIT);
	scanf("%d", &response);
	getchar ();
	return response;
}


/** testPut
 **    try out the putBB function for a bit buffer
 **/
void testPut () {
     char     sourceString[MAXSTR];
     unsigned char sourceBits[MAXSTR] = {0};
     unsigned i = 0;
     unsigned startIndex;
     
/* read an input string of 0's and 1's
 */
     printf ("\nType a bit pattern of fewer than %d bits: ", MAXSTR);
     gets (sourceString);
     if (i > MAXSTR) {
           printf ("\nYIKES!: input string is too long!");
           exit (0);
     }

/* convert the char 0's and 1's to actual bits
 */
     while (sourceString[i] != '\0') {
           if (sourceString[i] == '0') {
               setBitLocal (sourceBits, i, 0);
           }
           else 
           if (sourceString[i] == '1') {
               setBitLocal (sourceBits, i, 1);
           }
           else {
               printf ("WARNING: illegal bit value >%c< in input\n", sourceString[i]);
           }
		   ++i;
     }
 
     printf ("Starting index in bit buffer: ");
     scanf ("%d", &startIndex);
     printf ("Writing %d bits starting at buffer index %d...\n", 
			 i, startIndex);

/* write the string of input bits to the buffer using fn putBB
 */
     if (putBB (sourceBits, startIndex, i))
         printf ("\n\t--> putBB operation was successful");
     else
         printf ("\n\t--> putBB operation was NOT successful");
}


/** testGet
 **    try out the getBB function for a bit buffer
 **/
void testGet () {
     unsigned char sourceBits[MAXSTR] = {0};
     unsigned count;
     unsigned startIndex;
     
/* get the count and the index position for the read from the bit buffer
 */
     printf ("\nHow many bits? : ");
     scanf ("%d", &count);
     printf ("\nStarting index in bit buffer: ");
     scanf ("%d", &startIndex);
     printf ("\nReading %d bits starting at buffer index %d...\n", 
			 count, startIndex);

/* call getBB to retrieve the bits
 */
     if (getBB (sourceBits, startIndex, count)) {
         printf ("\n\t--> getBB of %d bits was successful: ", count);
		 printBits (sourceBits, count);
	 }
     else
         printf ("\n\t--> getBB operation was NOT successful");
}


/** printBits
 **    print out the first <count> bits stored at address <s>
 **/
void printBits (unsigned char *s, unsigned count) {
	 unsigned i = 0;
	 char	  miniBuffer;
	 unsigned bufferIndex = 0;
	 miniBuffer = s[bufferIndex];

/* one at a time, let each bit play the role of the sign bit.
 * determine bit values by seeing if the miniBuffer "looks"
 *    positive or negative (tricky, tricky)
 */
	 while (i < count) {
		 if (miniBuffer < 0)
			 putchar ('1');
		 else
			 putchar ('0');
		 miniBuffer <<= 1;
		 if ((++i % BYTE_BITS) == 0)
			 miniBuffer = s[++bufferIndex];
	 }
}

/* setBitLocal
 *		Set the bit at <index> positions beyond <bArray> to 1 
 *         if <bitValue> is nonzero, set it to 0 otherwise
 */
static void setBitLocal(unsigned char bArray[], int index, int bitValue) {
	int byteIndex = index / BYTE_BITS;
	int bitIndex = index % BYTE_BITS;
	unsigned char mask = (unsigned char) (1 << (BYTE_BITS-1)) >> bitIndex;
	if (bitValue)
		bArray[byteIndex] |= mask;
	else
		bArray[byteIndex] &= ~mask;
}
