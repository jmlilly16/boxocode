#include <stdlib.h>
#include <stdio.h>
#include "Queue.h"

int main()
{
	Queue* Q = initQueue();
	int x=5,y=6,z=7,f;
	int *a,*b,*c;
	a = &x;
	printf("%p\n%d\n",a,*a);
	void* d = (void*)a;
	printf("%p\n",d);
	int* e = (int*)d;
	printf("%p\n",e);
	printf("%d\n",*e);
	
	b = &y;
	c = &z;
	Q_push(Q,a);
	Q_push(Q,b);
	d = Q_pop(Q);
	printf("!!!\n");
	printf("%p\n",d);
	printf("%p\n",e);
	printf("%d\n",*e);
	e = (int*)d;
	printf("%p\n",e);
	f = *e;
	printf("%d\n",f);
	Q_push(Q,c);
	e = (int*)Q_pop(Q);
	printf("%d\n",*e);
	e = (int*)Q_pop(Q);
	printf("%d\n",*e);
	terminateQueue(Q);
	return 0;
}
