//file:LinkedList.c
//Mason Lilly

//This file implements the C definition of a Linked List defined in LinkedList.h

#include <stdlib.h>
#include <stdio.h>
#include "LinkedList.h"

typedef struct LLNode
{
	void* data;
	struct LLNode* next;
}LLNode;

struct LinkedList
{
	LLNode *head,*cursor,*tail;
};

LinkedList* initLinkedList()
{
	LinkedList* list = malloc(sizeof(LinkedList));
	list-> head = malloc(sizeof(LLNode));
	list-> cursor = list-> head;
	list-> tail = list-> head;
	list-> head-> next = 0;
	return list;
}
void terminateLinkedList(LinkedList* list)
{
	LL_moveToFront(list);
	while(LL_remove(list));
	free(list-> head);
	free(list);
}

void LL_insert(LinkedList* list,void* data)
{
	while(LL_move(list));
	list->cursor->next = malloc(sizeof(LLNode));
	list->cursor = list->cursor->next;
	list->cursor->data = data;
	list->cursor->next = 0;
	list->tail = list->cursor;
}
void* LL_get(LinkedList* list)
{
	if(list->cursor->next == 0) return 0;
	else return (void*)list->cursor->next->data;
}
int LL_move(LinkedList* list)
{
	if(list->cursor->next == 0) 
	{return 0;}
	else
	{
		list-> cursor = list->cursor->next;
		return 1;
	}
}
void LL_moveToFront(LinkedList* list)
{
	list-> cursor = list-> head;
}
int LL_remove(LinkedList* list)
{
	if(list->cursor->next == 0) return 0;
	if(list->cursor->next == list->tail) list->tail=list->cursor;
	LLNode* target = list->cursor->next;
	list->cursor->next = list->cursor->next->next;
	free(target);
	return 1;
}
