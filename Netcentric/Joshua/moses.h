/* FILE: moses.h
 *
 *	DESCRIPTION:	header for MOSES virtual O/S environment
 *
 *		Contains constants and declarations used in file
 *		   moses.c/.cpp: a set of simulated user processes and simulated hardware features.
 *    		(See CS 3074 Proj I Winter 2014 assignment description.)
 *		
 *	PROGRAMMED:	R England
 *			Transy U
 *	COURSE:		CS 3074: Netcentric Computing
 *			Winter 2014
 *
 *	VERSION HISTORY:
 *				5 Feb 2010: original for Winter 2010
 *				4 Feb 2012: allow for 64-bits, by doubling size of PSW 
 *							[ALTERNATE VERSION! no separate PC]
 *					
 */

#ifndef	MOSES_H
#define	MOSES_H

/* Variable/Function access types:
 */
#define PUBLIC
#define PRIVATE static
#define BELOW   extern

/* Types for Program Status Word (PSW):
 */
typedef void	(*FNPTR)();
typedef	short	tm_t;
typedef void*   psw_t[2];
BELOW	psw_t	PSW;


/* Registers
 */
typedef	void *REGTYPE;
BELOW   REGTYPE	Rx, Sx, Tx, Ux, Vx;


/* System initialization function
 */
BELOW   void	init_moses (void);

/* I/O functions
 */
BELOW   void	io (char *s);
BELOW   void	iofl (void);
BELOW   void	iolog (void);


/* (End of moses.h)
 */

#endif

