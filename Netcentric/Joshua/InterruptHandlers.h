//file:InterruptHandlers.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This header defines all the functions needed to handle different types of interrupts in Joshua.
//It also defines constants to represent each type of interrupt and system call.
#ifndef INTHNDLR
#define INTHNDLR

#include <stdlib.h>
#include "moses.h"
#include "Scheduler.h"
#include "Semaphore.h"

//These three constants pull double duty as interrupt types and crash types, depending on who's looking at them
#define TERMINATE 5
#define CRASH 6
#define TIMER 8

#define NEWPROC 5
#define IOREQ 8
#define IOEXEC 9
#define IOCOMPL 10

extern Semaphore* IO;

//Interrupts 5,6, and sometimes 8 - the int determines which "flavor" of termination you want.
void terminateProcess(PCB*,int);

//Interrupt 8 (Timer interrupt)
void checkTimeout(PCB*);

//SysCall 5
PCB* createProcess();

//System calls 8,9, and 10 are short enough to be handled entirely within the handleInterrupt function

//Called with each cycle of he Control Loop
//Determines what type of interrupt has occured, then handles it accordingly
void handleInterrupt();
#endif
