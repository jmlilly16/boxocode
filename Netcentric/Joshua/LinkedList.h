//file:LinkedList.h
//MasonLilly

//This file defines a generic LinkedList object, to be implemented in C.

#ifndef LListC
#define LListC

typedef struct LinkedList LinkedList;

LinkedList* initLinkedList();
void terminateLinkedList(LinkedList*);

void LL_insert(LinkedList*, void*);
void* LL_get(LinkedList*);
int LL_move(LinkedList*);
void LL_moveToFront(LinkedList*);
int LL_remove(LinkedList*);

#endif
