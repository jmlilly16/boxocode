//file: Queue.c
//Mason Lilly

//This implements the Queue struct and functions defined in Queue.h

#include <stdlib.h>
#include <stdio.h>
#include "Queue.h"

//Yes, this looks silly. The idea is to wrap the LinkedList class so that only a list defined as a Queue can be used in these functions, and other LinkedList functions can't be used on the list used by this Queue. No, this isn't really necessary for this assignment, it's more of a design exercise for myself.
struct queue			
{
	LinkedList* list;
};

Queue* initQueue()
{
	Queue* q = malloc(sizeof(Queue));
	q-> list = initLinkedList();
	return q;
}
void terminateQueue(Queue* q)
{
	terminateLinkedList(q-> list);
	free(q);
}

void Q_push(Queue* q, void* data)
{
	LL_moveToFront(q-> list);
	while(LL_move(q-> list));
	LL_insert(q-> list,data);
}
void* Q_pop(Queue* q)
{
	LL_moveToFront(q-> list);
	void* data = LL_get(q-> list);
	LL_remove(q-> list);
	return data;
}
