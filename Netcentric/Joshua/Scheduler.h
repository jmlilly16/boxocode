//file:Scheduler.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This function defines a one-instance pseudoobject - the Scheduling Queue. It manages a CycleList of PCBs, and contains functions for adding and removing processes and determining which one gets to execute when.
#ifndef SCHED
#define SCHED
#include "moses.h"
#include "PCB.h"
#include "CycleList.h"

//Collection of functions that manages a CycleList of PCBs.
//Contains the readyNextProcess function, which allocates time slices to processes and facilitates their turn order.

//Initializes and deconstructs the scheduling queue
void Scheduler_init();
void Scheduler_terminate();

//Adds or removes a process to the queue
void Scheduler_addProcess(PCB*);
void Scheduler_removeProcess(PCB*);

//Determine and return the next process;
PCB* Scheduler_readyNextProcess();
//Give the currently active process
PCB* Scheduler_currentProcess();
//Return 1 if the queue is empty
int Scheduler_queueSize();
//Output the current queue to the screen
void Scheduler_printQueue();
#endif
