//file: InterruptHandlers.c
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This file is the implementation of the functions defined in InterruptHandlers.h

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "InterruptHandlers.h"
#include "ProcessStates.h"
#include "moses.h"

//Creates a new process from the values in registers Ux and Vx, returns a Process Control Block to represent it
PCB* createProcess()
{
	PCB* newProcess = malloc(sizeof(PCB));
	newProcess -> name = malloc(sizeof(char));
	strcpy(newProcess -> name,Ux);
	newProcess -> PCounter = (long)(Vx);
	newProcess -> totalTimeLeft = 0;
	newProcess -> timeLeft = 0;
	newProcess -> state = READY;
	//printf("Created and returned the new process \"%s\",\n\tLocated at %lX,\n\tWith first instruction at %lX.\n",newProcess -> name,(unsigned long)newProcess,(unsigned long)(newProcess -> PCounter));
	return newProcess;
}

//Called whenever a process terminates voluntarily, crashes, or timeouts.
void terminateProcess(PCB* process,int crash)
{
	process -> state = TERMINATED;
	Scheduler_removeProcess(process);
	printf("Process %s has ",process -> name);
	switch(crash)
	{
		case TERMINATE:	printf("terminated."); break;
		case CRASH: 	printf("crashed. We can't tell you why."); break;
		case TIMER: 	printf("timed out."); break;
		default: 	printf("ended...somehow."); break;
	}
	printf("\n");
	free(process);
}

//Called upon timer interrupts
//Since time allocation is the job of the scheduler, this just checks to see if the total time budget of a process has been expended
void checkTimeout(PCB* process)
{
	if(process -> totalTimeLeft <= 0) terminateProcess(process,TIMER);
}

//This function is called every cycle of the Control Loop, determines what type of interrupt happened, and handles it accordingly
void handleInterrupt()
{
	printf("Handling CPU interrupt:\n");
	//NOTE: The Interrupt Type constants are not used here because these are physical bits that I can't change, whereas I could mess with the interrupt numbers.
	if(readBit((char*)PSW,5)) {printf("\tTermination Interrupt detected\n");terminateProcess(Scheduler_currentProcess(),TERMINATE);}
	else if(readBit((char*)PSW,6)) {printf("\tCrash Interrupt detected\n");terminateProcess(Scheduler_currentProcess(),CRASH);}
	else if(readBit((char*)PSW,8)) {printf("\tTimer Interrupt detected\n");checkTimeout(Scheduler_currentProcess());}
	else if(readBit((char*)PSW,9))
	{
		printf("\tSystem Call detected, checking type:\n");
		char* typePtr = (char*)PSW;
		unsigned char type = *(typePtr+1);
		type <<= 2;
		type >>= 4;
		printf("\t\tCall Code %d\n",type);
		switch(type)
		{
			case NEWPROC: 	Scheduler_addProcess(createProcess()); break;
			case IOREQ: 	check(IO); break;
			case IOEXEC: 	printf("Process %s doing output stuff!\n",Scheduler_currentProcess()-> name);
					io((char*)Ux); break;
			case IOCOMPL: 	clear(IO);iofl(); break;
			default: 	return;
		}
	}
}
