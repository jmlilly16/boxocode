//Mason Lilly
//Netcentric Computing - CS3074
//Dr England
//Spring 2014

//Joshua is an operating system that runs on the virtual machine Moses.
//To date it implements Programs 2 and 3 of Project I.
//Program 2 features make up the base OS: the loading, scheduling, and termination of processes
//Program 3 features handle IO requests, and the mutual exclusion of critical io variables

//This file contains the Control Loop, the heartbeat of the system which coordinates all other OS functions to cycle processes properly.
//It also contains most of the functions that interface with the Shared Hardware - everything contained in moses.h. 

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "moses.h"
#include "PCB.h"
#include "Scheduler.h"
#include "InterruptHandlers.h"
#include "ProcessStates.h"

//Local handles to the Program Counter and Timer stored in the Program Status Word
static long* pc;
static short* timer;

Semaphore* IO;

//Helper function to set a bit in a bitstream - the PSW in this case
void setBit(char* bitstream, int bitInd, int val)
{
	bitstream += bitInd/8;
	bitInd%=8;
	unsigned char mask = 1 << (7-bitInd);
	*bitstream = val?(*bitstream | mask):(*bitstream & ~mask);
}

//Same as above but for reading bits instead.
//These two functions, along with the static vars above, are the main ways of interfacing with the PSW
int readBit(char* bitstream, int bitInd)
{
	bitstream += bitInd/8;
	bitInd%=8;
	unsigned char mask = 1 << (7-bitInd);
	return *bitstream & mask;
}

//Grabs everything needed out of the PSW, followed by the registers, and stores it in the active process's PCB. Marks it as being paused.
void savePCB()
{
	PCB* process = Scheduler_currentProcess();
	process -> PCounter = *pc;
	process -> timeLeft = *timer;
	process -> registers[0] = Rx;
	process -> registers[1] = Sx;
	process -> registers[2] = Tx;
	process -> registers[3] = Ux;
	process -> registers[4] = Vx;
	process -> state = INTERRUPTED;
	printf("Saved %s's PCB\n",process -> name);
}

//Reloads everything in the active process's PCB into the registers and the PSW and readies it to resume execution.
void loadPCB()
{
	PCB* process = Scheduler_currentProcess();
	process -> state = RUNNING;
	printf("Loading %s's PCB\n",process -> name);
	Rx = process -> registers[0];
	Sx = process -> registers[1];
	Tx = process -> registers[2];
	Ux = process -> registers[3];
	Vx = process -> registers[4];
	*timer = process -> timeLeft;
	*pc = process -> PCounter;
}

//Sets the power level to User Mode and enables interrupts so processes can run safely
void giveControl()
{
	printf("Giving control to user process\n");
	setBit((char*)PSW,1,1);
	setBit((char*)PSW,2,1);
	setBit((char*)PSW,3,1);
}
//Sets the power level to Supervisor Mode and disables interrupts so the OS can do its thing unbothered
void takeControl()
{
	printf("\n\nReceived control of the CPU\n");
	setBit((char*)PSW,1,0);
	setBit((char*)PSW,2,0);
	setBit((char*)PSW,3,0);
}

//Debugging function to echo the state of a process
//Exists purely for aesthetic purposes, has no effect on the OS itself
char* state2Text(PCB* proc)
{
	switch(proc-> state)
	{
		case READY: return (char*)"READY";
		case RUNNING: return (char*)"RUNNING";
		case INTERRUPTED: return (char*)"INTERRUPTED";
		case BLOCKED: return (char*)"BLOCKED";
		case TERMINATED: return (char*)"TERMINATED";
		default: return (char*)"UNDEFINED";
	}
}

//The all-important GO BUTTON.
//Puts the value of whatever's in the Program Counter in the PSW into a function pointer and puts that into the REAL program counter
void kickCPU()
{
	printf("Kicking CPU\n");
	FNPTR cpu = (FNPTR)(*pc);
	cpu();
}

//Where all the fun stuff happens
int main()
{
	//Boot up the virtual machine, the call all the necessary initialization functions.
	init_moses();
	printf("Loading Joshua");
	Scheduler_init();
	IO = initSemaphore(1);

	//Set up the PC and Timer pointers so they will always point at the proper spot in the PSW for easy reference
	pc = (long*)PSW;
	pc++;
	timer = (short*)PSW;
	timer++;

	printf("Joshua initialized. Current PSW:");
	cheatPSW();

	//If the VM was polite, it brought a starting process with it, so let's set that up
	Scheduler_addProcess(createProcess());
	printf("First process, %s, loaded into the scheduler.\n",Scheduler_currentProcess() -> name);
	printf("\n\nJoshua setup complete. Entering Control Loop...\n");
	
	//Control Loop
	//As long as there is something in the Scheduling Queue,
	//this loop will continuously call for processes to be readied by the Scheduler.
	//As each one is readied, the loop will set it up in the CPU, set it free to run,
	//then check up on it once it gets interrupted.
	while(Scheduler_queueSize())
	{
		//if(Scheduler_queueSize()<=3) {printf("Short Queue, danger of fruit flies...Press enter to proceeed.");getchar();}
		printf("There are %d processes to run, loading next process.\n",Scheduler_queueSize());
		Scheduler_readyNextProcess();
		loadPCB();
		giveControl();
		kickCPU();
		takeControl();
		savePCB();
		handleInterrupt();
	}

	printf("\n\n\nAll Processes have terminated.\n");

	//Shut down the system by terminating all the objects
	Scheduler_terminate();
	terminateSemaphore(IO);

	printf("Logging io operations to file.\n");
	iolog();

	printf("\n\nShutting Down.\n\n\n");

	return 0;
}
