//file:SchedulerConstants.h
//Mason Lilly
//Spring 2014 - CS3074: Netcentric Computing
//Dr England

//This header defines constant names for each state a process can be in. Currently all are used except for TERMINATED.

#ifndef SCHED_CONST
#define SCHED_CONST
#define READY 1
#define RUNNING 2
#define INTERRUPTED 3
#define BLOCKED 4
#define TERMINATED -1
#define TIMESLICE 64
#define RUNTIME 2048
#endif
