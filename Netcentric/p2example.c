/* FILE p2example.c 
 *
 *  R England, Transy U
 *  CS 3074, Winter 2014
 *
 *  [related to Netcentric Programming, Project II, Program 1:
 *	Virtual Memory manager version of kernel simulation to run on Moses]
 *
 *  Example: Demo use of 
 *
 *              segPrint : print a specific segment / all segments to the screen
 *                 (similar to segDump, but to screen rather than to file dump.txt) 
 * 
 *              vmemPrint : print current contents of all frames to the screen
 *                 (similar to vmemDump, but to screen rather than to file dump.txt)
 * 
 *              pageR : read an entire logical page from Vdisk into a frame in Vmem
 * 
 *              vmemW : write a specific number of bytes to an address in Vmem
 *
 *  Required additional files:
 *              moses_m.h
 *              moses_m.o
 *
 */

#include	<stdio.h>
#include	<string.h>
#include	"moses_m.h"

#define	NUM_SEGS    8   /* the maximum number of user data               */
                        /*    segments                                   */
#define	NUM_PAGES   16  /* the maximum number of logical pages           */
                        /*    in a user's data segment                   */
#define	NUM_FRAMES  8   /* the number of physical page frames            */
#define	PAGE_SIZE   64  /* the number of bytes in a page                 */

#define	SEG_MASK    (0x1C00)
#define	SEG_BITS    3   /* the number of bits used to represent          */
                        /*    the segment number in a logical            */
                        /*    address                                    */
#define	PAGE_MASK   (0x03C0)
#define	PAGE_BITS   4   /* bits per logical page number in a             */
                        /*    logical address                            */
#define	DISP_MASK   (0x003F)
#define	DISP_BITS   6   /* the number of bits per displacement           */
                        /*    in an address                              */
#define	FRAME_MASK  (0x01C0)
#define	FRAME_BITS  3   /* bits per frame number in a physical           */
                        /*    address                                    */
	
#define BIT_BYTE    8   /* the number of bits in a byte                  */ 
#define STRMAX      256 /* the maximum length of a                       */
                        /*    string variable                            */
                                                        
#define	DISK_ADDR_MASK	   ((unsigned long)(-1)>>(sizeof(REGTYPE)*BIT_BYTE/2))
                        /* mask to isolate the disk address              */

/****
 *** MAIN PROGRAM:
 **/
long main(void) {
    char       name[STRMAX], message[STRMAX];
    VDISKADDR  segDiskAddress;
    VMEMADDR   frameAddress, writeAddress;    
    
    /* (random values, more or less, for purposes of demo) 
     */
    unsigned long  pageNumber = 8, frameNumber = 5, segSize, segNumber = 4;
    unsigned long  temp, displacement = 50, writeLength = 3;

    /* initialize the Moses system,
     * get the name of the first process
     */
    init_moses();
    strcpy (name, (char *) Ux);
    strcpy (message, "howdy!");
    
    /* does this process even *have* a segment number <segNumber>?
     */
    segSize = ((unsigned long *) Sx)[segNumber*2];
    if (segSize == 0) {
        printf("\nNOTICE: process %s has no segment number %ld", name, segSize);
        return 0;
    }
    else {
         /* get the segment disk address
          * [patch: use a temporary variable to simplify the disk address expression]
          */
         temp = (unsigned long)(((REGTYPE *) Sx)[segNumber*2 + 1]);
         segDiskAddress = (VDISKADDR) (temp & DISK_ADDR_MASK);

         /* goto Town with what we Fown
          * Q: What's this Do?
          */
         printf("\n\n\n\nHere is %s's segment %ld, size %ld:", name, segNumber, segSize);
         segPrint(segDiskAddress);
         printf("\n[Press Enter]");
         getchar();
         
         printf ("\n\n\n\nHere's what the memory frames look like initially:");
         vmemPrint();
         printf("\n[Press Enter]");
         getchar();
         
         frameAddress = (VMEMADDR) ((frameNumber << DISP_BITS) + 0);
         pageR(frameAddress, segDiskAddress, pageNumber);
         printf ("\n\n\n\nHere's what the frames look like after"
                 " loading %s's segment %ld page %ld into frame %ld:",
                 name, segNumber, pageNumber, frameNumber);
         vmemPrint();
         printf("\n[Press Enter]");
         getchar();

         writeAddress = (VMEMADDR) ((frameNumber << DISP_BITS) + displacement);
         vmemW(writeAddress, message, writeLength);
         printf ("\n\n\n\nHere's what the frames look like after writing %ld bytes to"
                 " %s's segment %ld page %ld in frame %ld at displacement %ld:",
                 writeLength, name, segNumber, pageNumber, frameNumber, displacement);
         vmemPrint();
         printf("\n[Press Enter]");
         getchar();

    }
    return 0;
}



