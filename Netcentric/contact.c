#include "moses.h"

int main()
{
	init_moses();
	printf("Registers:\nRx: %X\tSx: %X\tTx: %X\tUx: %X\tVx: %X\n",Rx,Sx,Tx,Ux,Vx);
	printf("Name of Process Alpha: %s\n",((char*)Ux));
	short* timer = PSW;
	timer++;
	*timer = 128;
	long* pc = PSW;
	pc++;
	*pc = (long)Vx;
	FNPTR func;
	int i=0;
	for(i=0;i<3;i++)
	{
		printf("Timer before instruction: %d\nProgram Counter before instruction: %X\n",*timer,*pc);
		func = *pc;
		func();
		printf("Timer after instruction: %d\nProgram Counter after instruction: %X\n",*timer,*pc);
		printf("Registers:\nRx: %X\tSx: %X\tTx: %X\tUx: %X\tVx: %X\n",Rx,Sx,Tx,Ux,Vx);
	}
		
	return 0;
}
