// FILE: Tv.h
// R England, Transy U
// CS 2444, Fall 2013
//
//	Interface: Tv class
//
#ifndef TV_H
#define	TV_H

// (comment out next line for normal operation)
//#define	DEBUG

#include	<string>
using namespace std;

class Tv {

//// public member functions:
public:
	Tv (void);			// constructors, destructor
	~Tv (void);

	int getChannel (void) const;	// accessors
    	int getVolume (void) const;
	bool getMute (void) const;

	void setChannelUp (void);	    // mutators
	void setChannelDown (void);
	void setVolumeUp (void);
	void setVolumeDown (void);
	void toggleMute(void);

//// private data
private:
	int channel;
	int volume;
	bool muted;
};

#endif

