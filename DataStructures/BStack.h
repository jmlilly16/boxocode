//File: BStack.h
//Mason Lilly
//Data Structures - CS2444
//Dr England

//This header is for a Bounded Stack that holds up to 5 strings.
//It is part of the assignment Program 3

#ifndef BSTACK
#define BSTACK

#include <string>
using namespace std;

class BStack
{
	public:
		BStack(void);
		~BStack(void);
		
		//Accessors
		int getCount(void) const; //returns the current size of the stack

		//Mutators
		bool pushItem(const string& s); //pushes string at s onto the stack, returning false if no room
		bool popItem(string& s); //copies the top string into s, returning false if the stack is empty

	private:
		static const int MAX_COUNT = 5;
		int count;
		string data[MAX_COUNT];
};

#endif
