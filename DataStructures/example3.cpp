// FILE: example3.cpp
// R England, Transy U
// CS 2444, Fall 2013
//
//	Demo driver: try out the Tv class
//
//	Build command: g++ example3.cpp Tv.cpp -o example2
//
#include	"Tv.h"
#include	<iostream>
using namespace std;

////
///	Main Program
//
int main (void) {
    Tv   sonyPlasma, samsungLcd;
    
// Show what we're starting with
    cout << "Initial state of TVs:" << endl
         << "\n\tSony plasma TV:\n\t\tChannel: " << sonyPlasma.getChannel()
         << "\n\t\tVolume: " << sonyPlasma.getVolume()
	 << "\n\t\tThe TV is " << (sonyPlasma.getMute()?"":" not") << " muted."
         << "\n\n\tSamsung LCD TV:\n\t\tChannel: " << samsungLcd.getChannel()
         << "\n\t\tVolume: " << samsungLcd.getVolume()
	 << "\n\t\tThe TV is " << (samsungLcd.getMute()?"":" not") << " muted." << endl << endl;

// Mess with the channel and volume controls
    for (int i = 0; i < 3; ++i) {
        sonyPlasma.setChannelUp();
        samsungLcd.setVolumeDown();
	sonyPlasma.toggleMute();
    }

// Show what we have now
    cout << "Current state of TVs:" << endl
         << "\n\tSony plasma TV:\n\t\tChannel: " << sonyPlasma.getChannel()
         << "\n\t\tVolume: " << sonyPlasma.getVolume()
	 << "\n\t\tThe TV is " << (sonyPlasma.getMute()?"":" not") << " muted."
         << "\n\n\tSamsung LCD TV:\n\t\tChannel: " << samsungLcd.getChannel()
         << "\n\t\tVolume: " << samsungLcd.getVolume()
	 << "\n\t\tThe TV is " << (samsungLcd.getMute()?"":" not") << " muted." << endl << endl;

// Mess with the channel and volume controls one more time
    for (int i = 0; i < 15; ++i) {
        sonyPlasma.setChannelDown();
        samsungLcd.setVolumeUp();
	sonyPlasma.toggleMute();
	samsungLcd.toggleMute();
    }

// Show what we have now
    cout << "New current state of TVs:" << endl
         << "\n\tSony plasma TV:\n\t\tChannel: " << sonyPlasma.getChannel()
         << "\n\t\tVolume: " << sonyPlasma.getVolume()
	 << "\n\t\tThe TV is " << (sonyPlasma.getMute()?"":" not") << " muted."
         << "\n\n\tSamsung LCD TV:\n\t\tChannel: " << samsungLcd.getChannel()
         << "\n\t\tVolume: " << samsungLcd.getVolume()
	 << "\n\t\tThe TV is " << (samsungLcd.getMute()?"":" not") << " muted." << endl << endl;

	return 0;
}

