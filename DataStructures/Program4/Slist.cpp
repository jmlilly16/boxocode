//File: Slist.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr. England
//Program 4

//This file implements the Singly-linked List described in Slist.h, as covered in class

#include "Slist.h"
using namespace std;

Slist::Slist(void)		//Constuctor
{
	head = new Node;
	head -> next = 0;
	cursor = head;
	tail = head;
}
Slist::~Slist(void)		//Deconstructor
{
	moveToFront();
	while(remove()){}
	delete head;
}

void Slist::insert(const DataType& newData)		//Inserts a node containing newData after the Cursor
{
	Node* newNode = new Node;
	newNode -> data = newData;
	newNode -> next = cursor -> next;
	cursor -> next = newNode;
	if(newNode -> next == 0) tail = newNode;
}
bool Slist::remove(void)				//Removes the element after Cursor
{
	if(cursor -> next == 0) return false;
	Node* trash = cursor -> next;
	cursor -> next = trash -> next;
	if(cursor -> next == 0) tail = cursor;
	delete trash;
	return true;
}
bool Slist::move(void)					//Move cursor down by one
{
	if(cursor == tail) return false;
	cursor = cursor -> next;
	return true;
}
void Slist::moveToBack(void)				
{
	cursor = tail;
}
void Slist::moveToFront(void)
{
	cursor = head;
}

#ifdef P4A
bool Slist::append(const Slist& otherList)		//Adds otherList to the end of this list
{
	if(otherList.isEmpty()) return false;
	moveToBack();
	for(Node* scanner = otherList.head;scanner -> next;scanner = scanner -> next)
	{
		insert(scanner -> next -> data);
		move();
	}
}
bool Slist::rotate(unsigned n)				//Sends n elements to the back of the list
{
	Node* temp;
	for(int i=0;i<n;i++)
	{
		temp = head -> next;
		head -> next = temp -> next;
		temp -> next = tail -> next;
		tail -> next = temp;
		tail = temp;
	}
	return true;
}
#endif
#ifdef P4B
bool Slist::truncate(const DataType& target)
{
	moveToFront();
	if(!(cursor -> next)) return false;
	while(cursor -> next -> data != target)
	{
		if(!move()) return false;
	}
	while(remove()){}
	return true;
}
bool Slist::reverse(void)
{
	if(head -> next == 0) return false;
	reverseFrom(head -> next);
	head -> next -> next = 0;
	Node* temp = head -> next;
	head -> next = tail;
	tail = temp;
	return true;
}	

void Slist::reverseFrom(Node* node)
{
	if(node -> next == 0) return;
	reverseFrom(node -> next);
	node -> next -> next = node;
	return;
}
#endif

bool Slist::get(DataType& data) const
{
	if(cursor -> next == 0) return false;
	data = cursor -> next -> data;
	return true;
}
#ifdef P4A
bool Slist::isEmpty(void) const
{
	if(head -> next) return false;
	else return true;
}
#endif
#ifdef DEBUG
#include <iostream>
using std::cout;
using std::endl;
void Slist::printDEBUG(void) const
{
	cout << "List: ";
	for(Node* temp = head -> next;temp;temp = temp -> next)
	{
		cout << temp -> data << " ";
	}
	cout << endl;
}
#endif
