// FILE: prog4.cpp
// R England, Transy U
// CS 2444, Fall 2013
//
//	Test driver program for Slist class
//

#define DEBUG

#include	"Slist.h"
#include	<iostream>
#include	<string>
using namespace std;

// symbolic constants
const int MAX_LISTS = 3;
const int INSERT = 1;
const int REMOVE = 2;
const int MOVE = 3;
const int MOVETOBACK = 4;
const int MOVETOFRONT = 5;
#ifdef P4A
const int APPEND = 6;
const int ROTATE = 7;
#endif
#ifdef P4B
const int TRUNCATE = 8;
const int REVERSE = 9;
#endif
const int CHANGE_LIST = 10;
const int QUIT = 0;

// prototypes
int menu (void);
void showSlistState (const Slist& s, unsigned label);
unsigned getListIndex (void);

////
///	Main program
//
int main (void) {
	Slist	slist[MAX_LISTS];
	string	s;
    unsigned sIndex = 0, otherIndex, positions;

// tell what this program does

	cout << endl << endl << "TESTING SLIST CLASS OPERATIONS:" << endl << endl;
	
	cout << "Using list " << sIndex << ":" << endl;
	int response = menu();
	while (response != QUIT) {
		
		switch (response) {

// put a new item in a list
		case INSERT:
			cout << "\nString: ";
			cin >> s;
			slist[sIndex].insert(s);
			cout << "Inserted \"" << s << "\" into list " << sIndex << endl;
			break;

// remove an item from a list
		case REMOVE:
			cout << endl;
			if (slist[sIndex].remove()) {
                cout << "Success! Item removed from list " << sIndex << endl;
			}
			else {
                cout << "Failure. NO item removed from list " << sIndex << endl;
			}
			break;

// move to the next item in the list
		case MOVE:
			cout << endl;
			if (slist[sIndex].move()) {
                cout << "Success! Moved to next item in list " << sIndex << endl;
			}
			else {
                cout << "Failure. Did NOT move to next item in list " << sIndex << endl;
			}
			break;

// move to the last item in the list
		case MOVETOBACK:
			cout << endl;
			slist[sIndex].moveToBack();
            cout << "Moved to last item in list " << sIndex << endl;
			break;

// move to the first item in the list
		case MOVETOFRONT:
			cout << endl;
			slist[sIndex].moveToFront();
            cout << "Moved to first item in list " << sIndex << endl;
			break;

#ifdef P4A
// append another list to this list
		case APPEND:
			cout << "\nIndex of list to append to list " << sIndex << ": ";
			otherIndex = getListIndex();
			if (slist[sIndex].append(slist[otherIndex])) {
                cout << "Appending list " << otherIndex 
                     << " to list " << sIndex << " changed the list" << endl;
			}
			else {
                cout << "Appending list " << otherIndex
                     << " to list " << sIndex << " did NOT change the list" << endl;
			}
			break;

// rotate the list
		case ROTATE:
			cout << "\nNumber of positions: ";
            cin >> positions;
			if (slist[sIndex].rotate(positions)) {
                cout << "Rotating list " << sIndex << " by " 
                     << positions << " positions changed the list" << endl;
			}
			else {
                cout << "Rotating list " << sIndex << " by "
                     << positions << " positions did NOT change the list " << endl;
			}
			break;
#endif
#ifdef P4B
// truncate the list
		case TRUNCATE:
			cout << "\nStarting data item: ";
            cin >> s;
			if (slist[sIndex].truncate(s)) {
                cout << "Truncating list " << sIndex << " at " 
                     << s << " changed the list" << endl;
			}
			else {
                cout << "Truncating list " << sIndex << " at " 
                     << s << " did NOT change the list" << endl;
			}
			break;

// reverse the list
		case REVERSE:
			cout << endl;
			if (slist[sIndex].reverse()) {
                cout << "Reversing list " << sIndex << " changed the list" << endl;
			}
			else {
                cout << "Reversing list " << sIndex << " did NOT change the list" << endl;
			}
			break;
#endif
// change to a different list
		case CHANGE_LIST:
			cout << "\nList number: ";
			sIndex = getListIndex();
			break;

// catch invalid menu responses
		default:
			cout << "\nERROR: " << response << " is not a valid response" << endl;
		}

        for (unsigned i = 0; i < MAX_LISTS; ++i) {
            showSlistState (slist[i], i);
        }
        
        cout << endl << endl << "Using list " << sIndex << ":" << endl;
		response = menu();
	}
	
	return 0;
}

// menu
//	prompt for an activity code
//	return the response
//	(does not validate the input)
int menu (void) {
	int	temp;
	cout << "\nType:";
	cout << "\t" << INSERT << " to INSERT a string in this list" << endl;
	cout << "\t" << REMOVE << " to REMOVE a string from this list" << endl;
	cout << "\t" << MOVE << " to move to the NEXT item in this list" << endl;
	cout << "\t" << MOVETOBACK << " to move to the LAST item in this list" << endl;
	cout << "\t" << MOVETOFRONT << " to move to the FIRST item in this list" << endl;
#ifdef P4A
	cout << "\t" << APPEND << " to APPEND a list to this list" << endl;
	cout << "\t" << ROTATE << " to ROTATE this list" << endl;
#endif
#ifdef P4B
	cout << "\t" << TRUNCATE << " to TRUNCATE this list" << endl;
	cout << "\t" << REVERSE << " to REVERSE this list" << endl;
#endif
	cout << "\t" << CHANGE_LIST << " to CHANGE to a different list" << endl;
	cout << "\t" << QUIT << " to quit: ";
	cin >> temp;
	return temp;
}

// showSlistState
//	show the current contents and current element of a linked list
void showSlistState (const Slist& s, unsigned label) {
	string str;
	cout << endl << "   CURRENT CONTENTS OF LIST " << label << ": ";
	s.printDEBUG();
	cout << endl;
	if (s.get(str)) {
        cout << "   GET RETRIEVES: \"" << str << "\"" << endl;
	}
	else {
        cout << "   GET RETRIEVES NOTHING" << endl;
	}
}

// getListIndex
//  read a list index number from the keyboard
unsigned getListIndex (void) {
    unsigned temp;
    do {
        cin >> temp;
    } while (!(temp < MAX_LISTS) && 
             (cout << "ERROR: Must be 0 to " << MAX_LISTS-1 << ". Try again: "));
    return temp;
}
