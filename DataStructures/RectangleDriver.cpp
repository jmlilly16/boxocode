//RectangleDriver.cpp
//Mason Lilly
//This driver program is part of Lab 5

#include <iostream>
#include "Rectangle.h"
using namespace std;

int main()
{
	Rectangle box,quadrilateral,parallelogram;
	
	box.setLength(5.6);
	quadrilateral.setWidth(box.getLength());
	quadrilateral.setLength(8.5);
	
	cout 
		<< "Box has a length of " << box.getLength() << " and a width of " << box.getWidth() << "." << endl
		<< "Quadrilateral has a length of " << quadrilateral.getLength() << " and a width of " << quadrilateral.getWidth() << "." << endl
		<< "Parallelogram has a length of " << parallelogram.getLength() << " and a width of " << parallelogram.getWidth() << "." << endl;

	cout << endl << "Assigning new values..." << endl;
	cout << "Parallelogram's length " << (parallelogram.setLength(-5)?"":"un") << "successfully set to -5." << endl;
	cout << "Quadrilateral's width " << (quadrilateral.setWidth(box.getArea())?"":"un") << "successfully set to Box's area." << endl;
	cout << "Box's width " << (box.setWidth(parallelogram.getLength())?"":"un") << "successfully set to Parallelogram's length." << endl;
	cout << "Parallelogram's width " << (parallelogram.setWidth(10.0)?"":"un") << "successfully set to 10.0." << endl;

	cout << endl << "Here are the final scores: " << endl;
	cout << "Box:";
		cout << "\n\tLength: " << box.getLength();
		cout << "\n\tWidth: " << box.getWidth();
		cout << "\n\tArea: " << box.getArea();
		cout << "\n\tPerimeter: " << box.getPerimeter() << endl;
	cout << "Quadrilateral:";
		cout << "\n\tLength: " << quadrilateral.getLength();
		cout << "\n\tWidth: " << quadrilateral.getWidth();
		cout << "\n\tArea: " << quadrilateral.getArea();
		cout << "\n\tPerimeter: " << quadrilateral.getPerimeter() << endl;
	cout << "Parallelogram:";
		cout << "\n\tLength: " << parallelogram.getLength();
		cout << "\n\tWidth: " << parallelogram.getWidth();
		cout << "\n\tArea: " << parallelogram.getArea();
		cout << "\n\tPerimeter: " << parallelogram.getPerimeter() << endl;
	
	return 0;
}
