//File: BStack.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr England

//This is the implementation of a Bounded Stack, part of Program 3.

#include "BStack.h"
#include <string>
using namespace std;


//(de)constructor
BStack::BStack(void)
{
	count = 0;
}

BStack::~BStack(void)
{
}

//accessor
int BStack::getCount(void) const
{
	return count;
}

//mutators
bool BStack::pushItem(const string& s)
{
	if (count>=5) return false;
	data[count++] = s;
	return true;
}
bool BStack::popItem(string& s)
{
	if (count<=0) return false;
	s = data[--count];
	return true;
}
