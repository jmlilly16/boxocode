//file: hayStack.cpp


#include <iostream>
#include <string>
#include <fstream>
#include "HayStack.h"
using namespace std;

HayStack::HayStack(void)
{
	dim = 0;
}
HayStack::HayStack(fstream& file)
{
	dim = 0;
	if(!file.is_open())
	{
		cout << "Could not open file";
	}
	else
	{
		file >> dim;
		fillWithHay();
		/*for(int i=0;i<dim+2;i++)
		{
			for(int j=0;j<dim+2;j++)
			{
				cout << data[i][j] << " ";
			}
			cout << endl;
		}*/
		cout << endl;
		int row=0;
		for(string line="";getline(file,line);row++)
		{
			//cout << line << endl;
			for(int col=0;col<line.length();col++)
			{
				switch(line[col])
				{
					case 'H':
					case 'N':
					case '?':
						data[row][col/2+1] = line[col];
					default:break;
				}
 			}
		}
		/*for(int i=0;i<dim+2;i++)
		{
			for(int j=0;j<dim+2;j++)
			{
				cout << data[i][j] << " ";
			}
			cout << endl;
		}*/
		
	}	
}

HayStack::~HayStack(void)
{
	
}


void HayStack::fillWithHay(void)
{
	for(int i=0;i<dim+2;i++)
	{
		for(int j=0;j<dim+2;j++)
		{
			data[i][j] = 'H';
		}
	}
}

string HayStack::findPath(int row, int col)
{
	string dirs[4] = {"D","R","L","U"};
	const int shifts[2][4] = {
					{1,0,0,-1},
					{0,1,-1,0}
				};	//increments for each move step (row 0 is vertical, 1 is horizontal)
	string path;
	switch(data[row][col])
	{
		case 'N': data[row][col] = '!'; return "!";
		case '?':
			data[row][col] = ' ';
			for(int i=0;i<4;i++)
			{
				path = findPath(row+shifts[0][i],col+shifts[1][i]);
				if(path != "~") return dirs[i] + path;
			}
			data[row][col] = '?';
		case ' ':
		case 'H': 
		default: return "~";
	}
}

void HayStack::print(ostream& out) const
{
	for(int i=0;i<dim+2;i++)
	{
		for(int j=0;j<dim+2;j++)
		{
			out << data[i][j] << " ";
		}
		out << endl;
	}
}
