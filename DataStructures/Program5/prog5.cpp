//file: prog5.cpp

//Mason Lilly
//Data Structures - CS 2444
//Dr England

//This is the main file for the Needle-in-a-Haystack program. It reads in a file containing a Hay Stack represented by a grid of characters.
//It then searches recursively, from the top left corner, for a path through the Hay to a Needle. Impassable Hay is represented by H, passable squares are represented by ?, and N represents the elusive needle.
//In the recursive function, a string representing the path thus found is built and eventually returned.

#include <iostream>
#include <string>
#include <fstream>
#include "HayStack.h"
using namespace std;



void translatePath(string&);

int main()
{
	fstream file;
	file.open("inHayStack.txt",fstream::in);
	HayStack hayStack(file);
	hayStack.print(cout);
	string path;
	path = hayStack.findPath(1,1);
	if(path == "~")
	{
		cout << "Could not find a path" << endl;
		return 0;
	}
	else
	{
		cout << "Found one! Marked it with a '!'." << endl;
		cout << "Path is ";
		translatePath(path);
		cout << endl;
		hayStack.print(cout);
		
		//give the same output to a file		
		file.close();
		file.open("outHayStack.txt",fstream::out);
		hayStack.print(file);
		return 0;
	}
}

void translatePath(string& path)
{
	char dir = '.',moveDir='.';
	int moveLen = 0;
	while(path.size()>0)
	{
		dir = path.at(0);
		if(moveDir=='.') moveDir=dir;
		//cout << dir;
		if(moveDir != dir)
		{
			if(moveDir == 'D') cout << ", Down ";
			else if(moveDir == 'L') cout << ", Left ";
			else if(moveDir == 'R') cout << ", Right ";
			else if(moveDir == 'U') cout << ", Up ";
			else if(moveDir == '!') cout << "!";
			else cout << "???";
			cout << moveLen;

			if(dir == '!')
			{
				return;
			}
			moveDir = dir;
			moveLen = 0;
		}
		moveLen++;
		path = path.substr(1);
	}
}
