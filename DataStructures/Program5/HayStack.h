//file: HayStack.h

#ifndef HAY
#define HAY

class HayStack
{
	public:
		HayStack(void);
		HayStack(std::fstream&);
		~HayStack(void);

		void fillWithHay(void);
		std::string findPath(int startRow, int startCol);
		void print(std::ostream&) const;
	private:
		static const int MAX_DIM = 30;
		int dim;
		char data[MAX_DIM][MAX_DIM];
};
#endif
