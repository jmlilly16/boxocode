//Rectangle.h
//Mason Lilly
//Rectangle defines a length and a width and provides accessors and mutators for both. It can also calculate Area and Perimeter.

#ifndef RECT
#define RECT

class Rectangle
{
	private:
		double length,width;
	public:
		Rectangle(void);
		~Rectangle(void);
		double getLength(void) const; //accessors
		double getWidth(void) const;
		double getArea(void) const;
		double getPerimeter(void) const;
	
		bool setLength(const double newLength); //mutators, each returns false if a negative value is given
		bool setWidth(const double newWidth);
};
#endif
