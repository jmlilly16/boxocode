// FILE: lab2.cpp
// R England, Transy U
// CS 2444, Fall 2013
//
//       Q: What's the output? Why?
//
#include <iostream>
using namespace std;

// prototype for function stanley
int *stanley (int *creed, int *kevin, int meredith);

////
/// main program
//
int main (void) {
    int  michael = 10;
    int  dwight = 20;
    int *pam, *jim, *ryan;

// start figure 1 trace...
    pam = &michael;
    jim = pam;
    ryan = new int;
    *ryan = 30;
    cout << endl << "FIG1:" << endl;
    cout << "m: " << michael << " &m: " << &michael << endl
         << "d: " << dwight << " &d: " << &dwight << endl
         << "p: " << pam << " *p: " << *pam << endl
         << "j: " << jim << " *j: " << *jim << endl
         << "r: " << ryan << " *r: " << *ryan << endl;
    
// start figure 2 trace...
    delete ryan;
    ryan = jim;
    *jim = 40;
    jim = new int;
    *jim = 50;
    michael += 60;
    pam = &dwight;
    *pam = *jim;
    *jim += 70;
    cout << endl << "FIG2:" << endl;
    cout << "m: " << michael << " &m: " << &michael << endl
         << "d: " << dwight << " &d: " << &dwight << endl
         << "p: " << pam << " *p: " << *pam << endl
         << "j: " << jim << " *j: " << *jim << endl
         << "r: " << ryan << " *r: " << *ryan << endl;
    
// start figure 3 trace...
    jim = stanley (&michael, pam, *jim);
    cout << endl << "FIG3:" << endl;
    cout << "m: " << michael << " &m: " << &michael << endl
         << "d: " << dwight << " &d: " << &dwight << endl
         << "p: " << pam << " *p: " << *pam << endl
         << "j: " << jim << " *j: " << *jim << endl
         << "r: " << ryan << " *r: " << *ryan << endl;
    delete jim;
    jim = 0;
    return 0;
}

// function stanley:
int *stanley (int *creed, int *kevin, int meredith) {
    int *jan = new int;
    *jan = *kevin * meredith + *creed;
    *creed = 80;
    *kevin = 90;
    return jan;
}

