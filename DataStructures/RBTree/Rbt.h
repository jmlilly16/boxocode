//file: Rbt.h
//Mason Lilly
//Data Structures - CS2444
//Dr. England

//Program 8 - Red-Black Tree

//This header defines an implementation of a Red-Black Tree.
//This is a specialized kind of Binary Search Tree that is always balanced.
//Each node of a Red-Black Tree is either a Red node or a Black node, and the isRed variable reflects this.
//All of the extra functionality in a Red-Black Tree occurs in the put function.
//When inserting an element, if a node is found with two red children, the node is "split".
//Splitting a node changes it to red and changes both of its children to black.
//After insertion, as the recursive call is collapsing, each node is checked for two red descendents in a row.
//If a node is found with two consecutive red descendants, it is balanced.

#ifndef RBT
#define RBT

#include "DataType.h"

class Rbt
{
	public:
		Rbt(void);
		~Rbt(void);	

		bool put(const DataType&);
		bool get(const KeyType& seachKey, DataType& foundItem) const;
		void printDEBUG(void) const;
		
	private:
		struct Node
		{
			Node* left;
			Node* right;
			bool isRed;
			DataType data;
			Node(DataType stuff)
			{
				data = stuff;
				left = 0;
				right = 0;
				isRed = true;
			}
		};

		bool put(Node*&,const DataType&);
		bool get(Node* node, const KeyType& searchKey, DataType& foundItem) const;
		void balanceLeft(Node*&);
		void balanceRight(Node*&);
		void deleteNode(Node*);
		void printDEBUG(Node*) const;
		
		Node* root;
};

#endif
