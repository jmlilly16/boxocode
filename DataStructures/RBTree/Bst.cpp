//File:Bst.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr England
//Program 6 - Binary Search Tree

#include <iostream>
#include "Bst.h"
using namespace std;

Bst::Bst(void)
{
	root = 0;
}
Bst::~Bst(void)
{
	deleteNode(root);
}

//Public Wrappers
bool Bst::put(const DataType& data)
{
	return put(root,data);
}
bool Bst::get(const KeyType& target, DataType& foundItem) const
{
	return get(root,target,foundItem);
}
void Bst::printDEBUG(void) const
{
	printDEBUG(root);
}

//Work Happens Here
bool Bst::put(Node*& node, const DataType& data)
{
	if(node == 0)
	{
		node = new Node(data);
		return true;
	}
	if(data == node -> data) return false;
	if(data < node -> data)
	{
		return put(node -> left, data);
	}
	else if(data > node -> data)
	{
		return put(node -> right, data);
	}
}
bool Bst::get(Node* node,const KeyType& target, DataType& foundItem) const
{
	if(node == 0){
	cout << "FLAG!" << endl; return false;}
	if(target == node -> data)
	{
		foundItem = node -> data;
		return true;
	}
	if(target < node -> data) return get(node -> left, target, foundItem);
	else return get(node -> right, target, foundItem);
}
void Bst::deleteNode(Node* node)
{
	if(node -> left != 0)
		deleteNode(node -> left);
	if(node -> right != 0)
		deleteNode(node -> right);
	delete node;
}
void Bst::printDEBUG(Node* node) const
{
	if(node==0)
	{
		cout << "NOTHING!" << endl;
		return;
	}
	cout << node -> data << endl;
	cout << "Left Child of " << node -> data << ":" << endl;
	printDEBUG(node -> left);
	cout << "Right Child of " << node -> data << ":" << endl;
	printDEBUG(node -> right);
}
