//file: Rbt.cpp
//Mason Lilly
//Data Structures - CS2444
// Dr. England

//This is the implementation of the Red-Black Tree described in Rbt.h


#include "Rbt.h"
#include <iostream>
using namespace std;

Rbt::Rbt(void)
{
	root = 0;
}
Rbt::~Rbt(void)
{
	deleteNode(root);
}

bool Rbt::put(const DataType& data)
{
	bool success = put(root, data);
	root -> isRed = false;
	return success;
}
bool Rbt::get(const KeyType& target, DataType& foundItem) const
{
	return get(root,target,foundItem);
}

void Rbt::printDEBUG(void) const
{
	printDEBUG(root);
}

bool Rbt::put(Node*& node, const DataType& data)
{
	if(node == 0)				//Is this the bottom of the tree?
	{
		node = new Node(data);		//Done! Make a node.
		return true;
	}
	if(node -> data == data)		//Already in the tree?
	{
		return false;
	}
	
	if(node -> left && node -> right && node -> left -> isRed && node -> right -> isRed)
	{
		node -> isRed = true;								//Split!
		node -> left -> isRed = node -> right -> isRed = false;
	}

	if(!put(((data < node -> data)? node->left:node->right),data))	//Go right or left
	{
		return false;	//If that call failed, the tree was unchanged, don't need to do the rest.
	}
	//Check the balance on the way out
	if(node -> left && node -> left -> isRed)
	{
		if(node -> left -> left && node -> left -> left -> isRed)
			balanceRight(node);
		else if(node -> left -> right && node -> left -> right -> isRed)
		{
			balanceLeft(node -> left);
			balanceRight(node);
		}
	}
	else if(node -> right && node -> right -> isRed)
	{
		
		if(node -> right -> right && node -> right -> right -> isRed)
		{
			cout << "FLAG!" << endl;
			balanceLeft(node);
		}
		else if(node -> right -> left && node -> right -> left -> isRed)
		{
			balanceRight(node -> right);
			balanceLeft(node);
		}
	}
	return true;
}

void Rbt::balanceRight(Node*& node)
{
	Node* anchor = node -> left;
	node -> left = anchor -> right;
	anchor -> right = node;
	node = anchor;
	node -> isRed = false;
	node -> left -> isRed = true;
	node -> right -> isRed = true;
}
void Rbt::balanceLeft(Node*& node)
{
	Node* anchor = node -> right;
	node -> right = anchor -> left;
	anchor -> left = node;
	node = anchor;
	node -> isRed = false;
	node -> left -> isRed = true;
	node -> right -> isRed = true;
}

bool Rbt::get(Node* node,const KeyType& target, DataType& foundItem) const
{
	if(node == 0) return false;
	if(target == node -> data)
	{
		foundItem = node -> data;
		return true;
	}
	if(target < node -> data) return get(node -> left, target, foundItem);
	else return get(node -> right, target, foundItem);
}

void Rbt::deleteNode(Node* node)	//Postorder traversal to clean up the data
{
	if(node)
	{
		if(node -> left)
			deleteNode(node -> left);
		if(node -> right)
			deleteNode(node -> right);
		delete node;			//Nothing left behind? Show yourself out.
	}
}

void Rbt::printDEBUG(Node* node) const
{
	if(node==0)
	{
		cout << "NOTHING!" << endl;
		return;
	}
	cout << (node -> isRed?"RED":"BLACK") << ":" << node -> data << endl;
	printDEBUG(node -> left);
	printDEBUG(node -> right);
}
