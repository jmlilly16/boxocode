//file RbtT.gen
//Mason Lilly
//Data Structures - CS2444
//Dr. England
//
//This file (gen for generic) defines a Red-Black Tree using a template.

#include <iostream>
using namespace std;

template <class K,class D>
class Rbt
{
	public:
		Rbt(void);
		~Rbt(void);
		
		bool put(const D);

		bool get(const K,D&) const;
		void printDEBUG(void) const;

	private:
		struct Node
		{
			D data;
			Node* left;
			Node* right;
			bool isRed;
			Node(D d)
			{
				data = d;
				left = 0;
				right = 0;
				isRed = true;
			}
		};
		bool put(Node*&,const D);
		void balanceLeft(Node*&);
		void balanceRight(Node*&);
		void deleteNode(Node*);
		
		bool get(Node*,const K,D&) const;
		void printDEBUG(Node*) const;

		Node* root;
};

//--------------
//PUBLIC

template <class K, class D>
Rbt<K,D>::Rbt()
{
	root = 0;
}

template <class K, class D>
Rbt<K,D>::~Rbt()
{
	deleteNode(root);
}

template <class K, class D>
bool Rbt<K,D>::put(const D item)
{
	bool success = put(root,item);
	if(root) root -> isRed = false;
	return success;
}

template <class K, class D>
bool Rbt<K,D>::get(const K target, D& foundItem) const
{
	return get(root, target, foundItem);
}

template <class K, class D>
void Rbt<K,D>::printDEBUG() const
{
	printDEBUG(root);
}

//-------------
//PRIVATE

template <class K, class D>
bool Rbt<K,D>::put(Node*& node, const D item)
{
	if(node == 0)
	{
		node = new Node(item);	//Hit the bottom of the tree! Make a node and head home.
		return true;
	}
	if(node -> data == item)
	{
		return false;		//That's already in the tree!
	}
	
	if(node -> left && node -> left -> isRed && node -> right && node -> right -> isRed) //Since we're still going, split this node?
	{
		node -> isRed == true;
		node -> left -> isRed = false;
		node -> right -> isRed = false;
	}
	
	Node* destination = (item < node -> data)?node -> left:node -> right;
	bool success = put(destination, item);
	
	if(node -> left && node -> left -> isRed)
	{
		if(node -> left -> left && node -> left -> left -> isRed)
		{
			balanceRight(node);
		}
		else if(node -> left -> right && node -> left -> right -> isRed)
		{
			balanceLeft(node -> left);
			balanceRight(node);
		}
	}
	if(node -> right && node -> right -> isRed)
	{
		if(node -> right -> left && node -> right -> left -> isRed)
		{
			balanceRight(node -> right);
			balanceLeft(node);
		}
		else if(node -> right -> right && node -> right -> right -> isRed)
		{
			balanceLeft(node);
		}
	}
	return success;
}

template <class K, class D>
void Rbt<K,D>::balanceRight(Node*& node)
{
	Node* anchor = node -> left;
	node -> left = anchor -> right;
	anchor -> right = node;
	node = anchor;
}

template <class K, class D>
void Rbt<K,D>::balanceLeft(Node*& node)
{
	Node* anchor = node -> right;
	node -> right = anchor -> left;
	anchor -> left = node;
	node = anchor;
}

template <class K, class D>
void Rbt<K,D>::deleteNode(Node* node)
{
	if(node -> left) deleteNode(node -> left);
	if(node -> right) deleteNode(node -> right);
	delete node;
}


template <class K, class D>
bool Rbt<K,D>::get(Node* node, const K target, D& foundItem) const
{
	if(node == 0) return false;
	if(node -> data == target)
	{
		foundItem = node -> data;
		return true;
	}
	if(node -> data > target) return get(node -> left,target,foundItem);
	else return get(node -> right,target,foundItem);
}

template <class K, class D>
void Rbt<K,D>::printDEBUG(Node* node) const
{
	if(node == 0)
	{
		cout << "NOTHING!" << endl;
		return;
	}
	cout << (node -> isRed)?"RED:":"BLACK:" << node -> data << endl;
	printDEBUG(node -> left);
	printDEBUG(node -> right);
}
