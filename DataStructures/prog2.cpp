//prog2.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr England
//Due 9/24/13

//This program takes in the labels and colors of three balloons from the user
//	Then, using pointers, it performs a faux bubble sort to put them in
//	alphabetical order. The balloons are displayed in order before and
//	after the sort.

#include <iostream>
#include <string>
using namespace std;

struct Balloon
{
	string text;
	string color;
};

//Prompts the user for text and color of a balloon, stores it in a new Balloon instance, and returns its address.
//position should be text representing the balloon being entered (e.g. "Front")
Balloon* enterBalloon(string position)
{
	Balloon* newBalloon = new Balloon;
	cout << endl << position << " balloon's text: ";
	cin >> newBalloon -> text;
	cout << position << " balloon's color: ";
	cin >> newBalloon -> color;
	return newBalloon;
}

//Swaps two balloons between pointers
void swapBalloons(Balloon*& b1, Balloon*& b2)
{
	Balloon *temp = b1;
	b1 = b2;
	b2 = temp;
}


int main()
{
	Balloon *front, *middle, *back;
		
	cout << "Enter the text and color of three balloons, and I'll sort them: " << endl;
	//Get input	
	front = enterBalloon("Front");
	middle = enterBalloon("Middle");
	back = enterBalloon("Back");

	//Echo the initial balloons
	cout << endl << endl << "Here are the balloons you just entered:" << endl;
	cout << "\tThe front balloon is " << front -> color << " and it says " << front -> text << "." << endl;
	cout << "\tThe middle balloon is " << middle -> color << " and it says " << middle -> text << "." << endl;
	cout << "\tThe back balloon is " << back -> color << " and it says " << back -> text << "." << endl;	

	//Sort 'em
	if(front -> text > middle -> text) swapBalloons(front,middle);
	if(front -> text > back -> text) swapBalloons(front,back); //front is now guaranteed to be correct
	if(middle -> text > back -> text) swapBalloons(middle, back); //now they're all correct

	//Output the new order
	cout << endl << endl << "Here are the balloons after being sorted:" << endl;
	cout << "\tThe front balloon is " << front -> color << " and it says " << front -> text << "." << endl;
	cout << "\tThe middle balloon is " << middle -> color << " and it says " << middle -> text << "." << endl;
	cout << "\tThe back balloon is " << back -> color << " and it says " << back -> text << "." << endl;	

	//Garbage Collection	
	delete front, middle, back;
	front = middle = back = 0;
	return 0;
}
