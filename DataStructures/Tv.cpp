// FILE: Tv.cpp
// R England, Transy U
// CS 2444, Fall 2013
//
//	Implementation: Tv class
//
#include "Tv.h"
#include <iostream>
using namespace std;

// symbolic constants
const int MAX_CHANNEL = 13;
const int MAX_VOLUME = 10;
const int MIN_CHANNEL = 1;
const int MIN_VOLUME = 0;
const int INIT_CHANNEL = 1;
const int INIT_VOLUME = 5;

//// public member functions ////

// constructor
Tv::Tv (void) {
	channel = INIT_CHANNEL;
	volume = INIT_VOLUME;
	muted = false;
#ifdef DEBUG
	cout << "(Howdy from the Tv constructor!)" << endl;
#endif
}

// destructor
Tv::~Tv (void) {
#ifdef DEBUG
	cout << "(Howdy from the Tv destructor!)" << endl;
#endif
}

//// accessors

// getChannel, getVolume
//    return the current channel / current volume
int Tv::getChannel (void) const { 
	return channel;
}
int Tv::getVolume (void) const {
	return volume;
}
bool Tv::getMute (void) const
{
	return muted;
}

//// mutators

// setChannelUp, setChannelDown
//    increment / decrement the current channel, with wraparound
void Tv::setChannelUp (void) {
	++channel;
	if (channel > MAX_CHANNEL) {
		channel = MIN_CHANNEL;
	}
}
void Tv::setChannelDown (void) {
	--channel;
	if (channel < MIN_CHANNEL) {
		channel = MAX_CHANNEL;
	}
}

// setVolumeUp, setVolumeDown
//    increment / decrement the current Volume, without wraparound
void Tv::setVolumeUp (void) {
	if (volume < MAX_VOLUME) {
		++volume;
	}
}
void Tv::setVolumeDown (void) {
	if (volume > MIN_VOLUME) {
		--volume;
	}
}
void Tv::toggleMute(void)
{
	muted = !muted;
}
