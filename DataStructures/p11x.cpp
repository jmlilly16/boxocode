//file: p11x.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr England
//Extra Credit Problems

//Boggle

//This program takes in a 5x5 matrix of letters from a file, then 6 words.
//It then analyzes the matrix to see if each word can be found within the matrix.
//Once it's finished, it outputs YES and NO for each word to indicated if it was found.

#include <iostream>
#include <fstream>
//#define DEBUG
using namespace std;

const int GRID_SIZE = 5;
const int NUM_WORDS = 6;
const int WORD_LENGTH = 10;

string *board[GRID_SIZE][GRID_SIZE];	//cheap workaround to keep from having to pass this thing

bool testWord(const string target);
bool matchesWord(string targetWord,int x,int y,int letterCount,string* letters[]);

int main()
{
	string line;
	string words[NUM_WORDS];
	string *lettersUsed[WORD_LENGTH];
	ifstream fileIn("BOGGLE.DAT");
	if(fileIn.is_open())
	{
		for(int i=0;i<GRID_SIZE;i++)
		{
			getline(fileIn,line);
			for(int j=0;j<GRID_SIZE;j++)
			{
				board[i][j] = new string;
				*board[i][j] = line.substr(j,1);
			}
		}
		getline(fileIn,line);
		for(int i=0;i<NUM_WORDS;i++)
		{
			getline(fileIn,line);
			words[i] = line;
		}
		
		for(int i=0;i<NUM_WORDS;i++)
		{
			cout << (testWord(words[i])?"YES":"NO") << endl;
		}
	
		fileIn.close();
	}
	else cout << "File could not be opened";
	return 0;
}

bool testWord(const string target)
{
	string *usedLetters[WORD_LENGTH];
	int usedCount;
	for(int i=0;i<GRID_SIZE;i++)
	{
		for(int j=0;j<GRID_SIZE;j++)
		{
			usedCount = 0;
			if(matchesWord(target,i,j,usedCount,usedLetters))
				return true;
		}
	}
	return false;
}

bool matchesWord(string target,int x, int y, int letterCount, string* letters[])
{
	//cout << "Testing square " << x << "," << y << " for letter " << letterCount+1 << " of the word " << target << endl;
	if(letterCount == target.size()) return true;
	if(x<0||x>GRID_SIZE-1||y<0||y>GRID_SIZE-1) return false;
	for(int i=0;i<letterCount;i++)
	{
		if(letters[i] == board[x][y])
			return false;
	}
	if(*board[x][y] == target.substr(letterCount,1))
	{
		letters[letterCount++] = board[x][y];
		return (
			matchesWord(target,x-1,y-1,letterCount,letters) ||
			matchesWord(target,x,y-1,letterCount,letters) ||
			matchesWord(target,x+1,y-1,letterCount,letters) ||
			matchesWord(target,x-1,y,letterCount,letters) ||
			matchesWord(target,x+1,y,letterCount,letters) ||
			matchesWord(target,x-1,y+1,letterCount,letters) ||
			matchesWord(target,x,y+1,letterCount,letters) ||
			matchesWord(target,x+1,y+1,letterCount,letters));
	}
	return false;
}
