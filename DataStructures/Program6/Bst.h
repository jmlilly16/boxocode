//File: Bst.h
//Mason Lilly
//Data Structures - CS2444
//Dr England
//Program 6 - Binary Search Tree

#ifndef BST
#define BST
#include "DataType.h"

class Bst
{
	public:
		Bst(void);
		~Bst(void);
		bool put(const DataType& newItem);
		bool get(const KeyType& seachKey, DataType& foundItem) const;
		void printDEBUG(void) const;
	private:
		struct Node
		{
			DataType data;
			Node *left,*right;
			Node(DataType d)
			{
				data = d;
				left = 0;
				right = 0;
			}
		};
		Node* root;
		bool put(Node*& node, const DataType& newItem);
		bool get(Node* node, const KeyType& searchKey, DataType& foundItem) const;
		void printNode(Node* node) const;
		void deleteNode(Node* node);
		void printDEBUG(Node* node) const;
};

#endif
