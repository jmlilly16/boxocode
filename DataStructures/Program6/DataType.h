// FILE: DataType.h

// R England, Transy U
// CS 2444, Fall 2013

//

//	Define a common data type and key type for testing a Bst implementation

//


#ifndef DATATYPE_H

#define DATATYPE_H


// (comment out the next line to use simple int data type while debugging)

#define USE_MP3


#ifdef  USE_MP3

#include "Mp3.h"

typedef Code	KeyType;

typedef Mp3   	DataType;


#else

typedef int KeyType;

typedef int DataType;


#endif

#endif
