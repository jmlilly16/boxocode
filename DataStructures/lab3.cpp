//lab3.cpp
//Mason Lilly
//Data Structures - CS2444
//Dr England

#include <iostream>
#include <string>
using namespace std;

struct FastFood
{
	string name;
	int calories;
};

int main()
{
	FastFood *theGood = new FastFood, *theBad = new FastFood;
	
	cout << "What is the name of the first Fast Food? ";
	cin >> theGood -> name;
	cout << "How many calories does it contain? ";
	cin >> theGood -> calories;
	cout << endl << "What is the name of the second Fast Food? ";
	cin >> theBad -> name;
	cout << "How many calories does it contain? ";
	cin >> theBad -> calories;

	cout << endl << "As you entered them, the first fast food is a " << theGood -> name << " containing " << theGood -> calories << " calories," << endl;
	cout << "and the second fast food is a " << theBad -> name << " containing " << theBad -> calories << " calories." << endl;
	
	if(theGood -> calories > theBad -> calories)
	{
		FastFood *temp = theGood;
		theGood = theBad;
		theBad = temp;
	}

	cout << endl << "After sorting, the first fast food is a " << theGood -> name << " containing " << theGood -> calories << " calories," << endl;
	cout << "and the second fast food is a " << theBad -> name << " containing " << theBad -> calories << " calories." << endl;
	delete theGood,theBad;
	theGood = theBad = 0;
	return 0;
}
