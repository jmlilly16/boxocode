//Rectangle.cpp
//Mason Lilly
//Rectangle Class: Implementation for the Rectangle described in Rectangle.h

#include "Rectangle.h"

Rectangle::Rectangle(void) //Constructor
{
	length=0.0;
	width=0.0;
}
Rectangle::~Rectangle(void) //Destructor
{}

//Accessors
double Rectangle::getLength(void) const
{
	return length;
}
double Rectangle::getWidth(void) const
{
	return width;
}
double Rectangle::getArea(void) const
{
	return length*width;
}
double Rectangle::getPerimeter(void) const
{
	return 2*length+2*width;
}

//Mutators
bool Rectangle::setLength(double newLength)
{
	if(newLength<0) return false;
	length=newLength;
	return true;
}
bool Rectangle::setWidth(double newWidth)
{
	if(newWidth<0) return false;
	width=newWidth;
	return true;
}
