//prog1.cpp
//Mason Lilly
//Dr. England
//CS 2444
//
//Takes in names of different kinds of fruit along with whether or not they are an apple,
//then sorts them using the Polish Flag sorting algorithm.

#include <iostream>
#include <string>

using namespace std;

const int MAX_FRUITS = 20;
const string SENTINEL = "X";

struct Fruit
{
	string name;
	bool isApple;
};

bool readFruit(Fruit& f) //Reads user input into a given Fruit address
{
	cout << "Fruit name?: ";
	cin >> f.name;
	if (f.name == SENTINEL) return false; //Store the response as a fruit name, but exit if it's the sentinel.
  
	cout << "Is this an apple? (Y/N): ";
	char appleness; //Intermediary between response and the "isApple" boolean
	do //Loop to ensure proper input. Quitting isn't an option now, since we've stored a name already.
	{
		cin >> appleness;
		appleness = toupper(appleness);
	}while(!(appleness=='Y'||appleness=='N')&&cout<<"Input must be Y or N: "); //Short-circuit. If appleness is other than Y/N, the cout statement is executed and is automatically true.
	//while((appleness=='Y')?(!(f.isApple=true)):((appleness=='N')?(f.isApple=false):cout << "Input must be Y or N: "));
	if(appleness == 'Y') f.isApple = true;
	else f.isApple = false;
  
	return true;
}

void swapFruits(Fruit& f1, Fruit& f2)
{
	Fruit temp = f1;
	f1 = f2;
	f2 = temp;
}

  
void polishSort(Fruit f[], int length) //Here's how many Polish people it takes to sort an array (Apples go on the bottom)
{
	int low=0,high=length-1; //Start at opposite ends of the array
	while(true)
	{
		while(low<high&&f[low].isApple) //Scan the lower index up until it finds a non-Apple
		{				//All the while we watch for the indices to pass each other
			low++;
		}
		while(low<high&&!f[high].isApple)//Similarly scan the high index down until it finds an Apple
		{
			high--;
		}
		if(low>=high) break;//Check to make sure the last movement didn't cross the indices
		swapFruits(f[low],f[high]);
	}
}

void printArray(Fruit f[], int length)
{
	for(int i=0;i<length;i++)
	{
		cout << f[i].name << (f[i].isApple?" is":" is not") << " an apple." << endl;
	}
}

int main()
{
	Fruit fruits[MAX_FRUITS],tempFruit; //tempFruit is passed to readFruit to receive the user's input
	int numFruits = 0; //Serves both as the index for storing fruits and a record of how many we've been given

	cout << "Enter some names of of fruit types. (" << SENTINEL << " to quit)" << endl;
	while(numFruits<MAX_FRUITS&&readFruit(tempFruit))	//Read: While the array hasn't been filled and the user keeps giving you fruits
	{
		fruits[numFruits++] = tempFruit;
	}

	cout << "Original Array:" << endl << endl;
	printArray(fruits,numFruits);

	polishSort(fruits,numFruits);

	cout << "Sorted Array:" << endl << endl;
	printArray(fruits,numFruits);
	return 0;
}
