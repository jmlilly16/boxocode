// FILE: prog3.cpp
// R England, Transy U
// CS 2444, Fall 2013
//
//	Test driver program for BStack class, Program 3 assignment:
//		Try out "pushItem" and "popItem" operations 
//         on bounded stacks of strings.
//
#include	"BStack.h"
#include	<iostream>
#include	<string>
using namespace std;

// symbolic constants
const int BSTACK_COUNT = 3;
const int PUSH = 1;
const int POP = 2;
const int SHOW_COUNT = 3;
const int BSTACK_CHANGE = 4;
const int QUIT = 0;

// prototypes
int menu (void);
int getStackNumber (void);

////
///	Main program
//
int main (void) {
	BStack	bStacks[BSTACK_COUNT];
	string	st;
	unsigned bNum = 0;

	cout << "\n\nTESTING BSTACK CLASS OPERATIONS:" << endl;
	cout << "\nNow using stack " << bNum << endl;
	int response = menu();
	while (response != QUIT) {
		switch (response) {

// push a new item onto a stack
		case PUSH:
			cout << "\nString: ";
			cin >> st;
			if (!(bStacks[bNum].pushItem(st))) {
				cout << "String NOT pushed --- stack " 
				     << bNum << " is full" << endl;
			}
			else {
				cout << "Pushed \"" << st << "\" onto stack "
				     << bNum << endl;
			}
			break;

// pop an item from a stack
		case POP:
			cout << endl;
			if (!(bStacks[bNum].popItem(st))) {
				cout << "String NOT popped --- stack "
				     << bNum << " is empty" << endl;
			}
			else {
				cout << "Popped \"" << st << "\" from stack "
				     << bNum << endl;

			}
			break;

// see how many items are currently in each stack
		case SHOW_COUNT:
			cout << endl;
			for (int i = 0; i < BSTACK_COUNT; ++i) {
				cout << "Items in stack " << i << ": " 
				     << bStacks[i].getCount() << endl;
			}
			break;

// use a different stack
		case BSTACK_CHANGE:
			bNum = getStackNumber();
			cout << "\nNow using stack " << bNum << endl;
			break;

// catch invalid menu responses
		default:
			cout << "\nERROR: " << response << " is not a valid response" << endl;
		}
        response = menu();
	}

	return 0;
}

// getStackNumber
//    prompt for a stack index number
//    return the response
//    (validates the input)	
int getStackNumber (void) {
	unsigned temp;
	do {
		cout << "\nNew stack Number: ";
		cin >> temp;
	} while (!(temp < BSTACK_COUNT) && 
		 cout << "\nInvalid stack number. Must be between 0 and " 
		      << BSTACK_COUNT-1 << ": ");
	return temp;
}

// menu
//    prompt for an activity code
//    return the response
//    (does not validate the input)
int menu (void) {
	int	temp;
	cout << "\nType:";
	cout << "\t" << PUSH << " to push a string onto a stack" << endl;
	cout << "\t" << POP << " to pop a string from a stack" << endl;
	cout << "\t" << SHOW_COUNT << " to see how many items are stored in all stacks" << endl;
	cout << "\t" << BSTACK_CHANGE << " to select a different stack" << endl;
	cout << "\t" << QUIT << " to quit: ";
	cin >> temp;
	return temp;
}
