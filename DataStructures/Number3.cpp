//Mason Lilly
//This program is meant to extrapolate on a graph theory problem Diana was working on
//It takes a set of integers and for each element creates a subset including all the elements that share a factor with the root element.

#include <iostream>
using namespace std;

class Subset
{
	public int root;
	public int matches[100],numMatches;
}

int main()
{
	int numElements;
	cout << "How many elements" << endl;
	cin >> numElements;
	int elements[numElements];
	Subset subsets[numElements];
	for(int i=0;i<numElements;i++)
	{
		elements[i] = i+1;
		subsets[i] = new Subset;
		subsets[i].root = i+1;
	}
	int factors[25],numFactors;
	for(int i=0;i<numElements;i++)
	{
		findFactors(i+1,factors,numFactors);
		
	}
	return 0;
}
