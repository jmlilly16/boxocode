#file: jmlilly16.s
#auth: Mason Lilly
#Computer Organization - Dr. Kenny Moorman
#Winter 2016
#Assignment 2 - Rearrange-inator

#This program implements and demonstrates the 'arrange' function
#arrange accepts the address of an array and two indices in the array
#The element at the first array index is assigned to be the "guard".
#The elements between the two indices are then rearranged such that
#elements less than the guard are before it, and elements greater than the guard are after it

.data
list:		.space	1024
limitquery:	.asciiz "Enter an array size\n"
arrayquery:	.asciiz "Enter array elements:\n"
startquery:	.asciiz "Enter a start index\n"
endquery:	.asciiz "Enter an end index (must not be less than the start index)\n"
arraycontents:	.asciiz "Current contents of array:\n"
inputsym:	.asciiz ">>"
queryintbad1:	.asciiz "Err: Input must be between "
queryintbad2: 	.asciiz " and "
endl:		.asciiz "\n"
blank:		.asciiz " "
sortdone:	.asciiz "Sorting complete\n"
pivotwas:	.asciiz "Pivot was: "
pivotpos:	.asciiz "Pivot is now in position: "
queryrange1:	.asciiz "(Range: ["
queryrange2:	.asciiz ","
queryrange3:	.asciiz "])"

.text
.globl 	main

main:	addi $sp,$sp,-36
	sw $s0,0($sp)
	sw $s1,4($sp)
	sw $s2,8($sp)
	sw $s3,12($sp)
	sw $s4,16($sp)
	sw $s5,20($sp)
	sw $s6,24($sp)
	sw $s7,28($sp)
	sw $ra,32($sp)

	la $a0,endl
	li $v0,4
	syscall

	#query a size limit
	la $a0,limitquery
	li $a1,1
	li $a2,256
	jal queryint
	move $s0,$v0

	#input an array
	la $a0,arrayquery
	li $v0,4
	syscall			#give the initial array prompt

	li $s7,0		#set up iterator
	arrayinloop:
		slt $t1,$s7,$s0		#check array size limit
		beq $t1,$zero,arrayindone
		#loop body

		la $a0,inputsym
		li $v0,4
		syscall			#print input prompt
		li $v0,5
		syscall			#acquire input
		move $s6,$v0

		la $t2,list		#get list baseaddr
		sll $t3,$s7,2		#shift iterator
		add $t2,$t2,$t3		#offset baseaddr
		sw $s6,0($t2)		#store input into list

		#loop finish
		addi $s7,$s7,1
		j arrayinloop
	arrayindone:

	#announce array contents
	la $a0,arraycontents
	li $v0,4
	syscall
	#output the whole array
	la $a0,list
	move $a1,$s0
	jal arrayout

	#input start and end points
	la $a0,startquery
	li $a1,0
	addi $a2,$s0,-1
	jal queryint			#query start point
	move $s1,$v0

	la $a0,endquery
	move $a1,$s1
	addi $a2,$s0,-1
	jal queryint			#query end point
	move $s2,$v0

	#pass everything to a function
	la $a0,list
	move $a1,$s1
	move $a2,$s2
	jal arrange			#magic!
	move $s7,$v0
	move $s6,$v1

	#report successful sort
	la $a0,endl
	li $v0,4
	syscall
	la $a0,sortdone
	li $v0,4
	syscall
	#announce pivot
	la $a0,pivotwas
	li $v0,4
	syscall
	move $a0,$s7
	li $v0,1
	syscall
	la $a0,endl
	li $v0,4
	syscall
	#announce pivot location
	la $a0,pivotpos
	li $v0,4
	syscall
	move $a0,$s6
	li $v0,1
	syscall
	la $a0,endl
	li $v0,4
	syscall
	syscall

	#announce array contents
	la $a0,arraycontents
	li $v0,4
	syscall
	#output the whole array again
	la $a0,list
	move $a1,$s0
	jal arrayout
	la $a0,endl
	li $v0,4
	syscall

	#main function cleanup
	lw $s0,0($sp)
	lw $s1,4($sp)
	lw $s2,8($sp)
	lw $s3,12($sp)
	lw $s4,16($sp)
	lw $s5,20($sp)
	lw $s6,24($sp)
	lw $s7,28($sp)
	lw $ra,32($sp)
	addi $sp,$sp,36
	jr $ra
#/main

#queryint accepts the address of a string on a0, a lower bound on a1, and an upper bound on a2
#The function uses the string given to prompt the user for an integer
#If the integer received is not within the range [lower,upper], an error is reported and the query is reattempted.
#If the upper bound given is less than the lower bound, the two are swapped.
queryint:
	addi $sp,$sp,-36
	sw $ra,0($sp)
	sw $s0,4($sp)
	sw $s1,8($sp)
	sw $s2,12($sp)
	sw $s3,16($sp)
	sw $s4,20($sp)
	sw $s5,24($sp)
	sw $s6,28($sp)
	sw $s7,32($sp)

	move $s0,$a0	#query text address
	move $s1,$a1	#lower bound
	move $s2,$a2	#upper bound

	#if the bounds are not "in order", they are swapped
	slt $t0,$s2,$s1
	bne $t0,$zero,boundswap
	j bounddone
boundswap:
	move $t0,$s1
	move $s1,$s2
	move $s2,$t0
bounddone:	#lower bound is now less than or equal to the higher bound

tryquery:			#query and get input
	move $a0,$s0
	li $v0,4
	syscall			#print the prompt
	la $a0,queryrange1
	li $v0,4
	syscall
	move $a0,$s1
	li $v0,1
	syscall
	la $a0,queryrange2
	li $v0,4
	syscall
	move $a0,$s2
	li $v0,1
	syscall
	la $a0,queryrange3
	li $v0,4
	syscall
	la $a0,endl
	li $v0,4
	syscall
	la $a0,inputsym
	li $v0,4
	syscall
	li $v0,5
	syscall
	move $s7,$v0

	#check input bounds
	slt $t1,$s7,$s1		#if the input is less than the lower bound,
	bne $t1,$zero,querybad	#there's a problem.
	slt $t1,$s2,$s7		#if not, and it's not greater than the upper bound,
	beq $t1,$zero,querygood	#then it's good!

querybad:			#print complaint string
	la $a0,queryintbad1
	li $v0,4
	syscall	#"input must be between "
	move $a0,$s1
	li $v0,1
	syscall	#<lower bound>
	la $a0,queryintbad2
	li $v0,4
	syscall	#" and "
	move $a0,$s2
	li $v0,1
	syscall #<upper bound>
	la $a0,endl
	li $v0,4
	syscall #"/n"
	j tryquery	#go back to the do the query again

querygood:		#save input to return value
	move $v0,$s7

	lw $s7,32($sp)
	lw $s6,28($sp)
	lw $s5,24($sp)
	lw $s4,20($sp)
	lw $s3,16($sp)
	lw $s2,12($sp)
	lw $s1,8($sp)
	lw $s0,4($sp)
	lw $ra,0($sp)
	addi $sp,$sp,36
	jr $ra
#/queryint

#arrayout takes in an array base address on a0 and an array size on a1
#On successful execution, it prints a1 elements from a0, one per line
#If a1 is nonpositive, nothing is printed.
arrayout:
	addi $sp,$sp,-36
	sw $ra,0($sp)
	sw $s0,4($sp)
	sw $s1,8($sp)
	sw $s2,12($sp)
	sw $s3,16($sp)
	sw $s4,20($sp)
	sw $s5,24($sp)
	sw $s6,28($sp)
	sw $s7,32($sp)

	move $s0,$a0
	move $s1,$a1

	li $s7,0
	arrayoutloop:
		slt $t1,$s7,$s1
		beq $t1,$zero,arrayoutdone

		#loop body
		sll $t1,$s7,2
		add $t1,$t1,$s0
		lw $a0,0($t1)
		li $v0,1
		syscall		#print the number
		la $a0,blank
		li $v0,4
		syscall		#print a space
		#loop finish
		addi $s7,$s7,1
		j arrayoutloop
	arrayoutdone:

	#print a blank line
	la $a0,endl
	li $v0,4
	syscall

	lw $s7,32($sp)
	lw $s6,28($sp)
	lw $s5,24($sp)
	lw $s4,20($sp)
	lw $s3,16($sp)
	lw $s2,12($sp)
	lw $s1,8($sp)
	lw $s0,4($sp)
	lw $ra,0($sp)
	addi $sp,$sp,36

	jr $ra
#/arrayout


#arrange takes an array base address on a0, a lower bound on a1, and an upper bound on a2
#Preconditions:
#The lower bound must be strictly less than the upper bound,
#and both bounds must be in the range [0,n) where n is the size of the array
#When the function returns, the elements in the range [lower,upper] will be rearranged
#The element in the first position prior to rearrangement becomes the pivot
#After rearrangement, everything that is in the range and less than the pivot will be before it,
#and everything greater than the pivot will be after it.
arrange:
	addi $sp,$sp,-36
	sw $ra,0($sp)
	sw $s0,4($sp)
	sw $s1,8($sp)
	sw $s2,12($sp)
	sw $s3,16($sp)
	sw $s4,20($sp)
	sw $s5,24($sp)
	sw $s6,28($sp)
	sw $s7,32($sp)

	move $s0,$a0
	move $s1,$a1
	move $s2,$a2

	#load the value of the pivot into s3, to be used throughout the function.
	sll $t0,$s1,2
	add $t0,$t0,$s0
	lw $s3,0($t0)

	#initialize two iterators - one at the beginning of the range, one at the end
	addi $s7,$s1,1		#left iterator (starts to the right of the guard)
	move $s6,$s2		#right iterator (starts at the upper bound)

	arrangeloop:
		slt $t0,$s7,$s6
		beq $t0,$zero,arrangeloopd

		#loop body
		#increment the left iterator until it either crosses the right iterator or finds an element greater than the pivot
		leftiteratorloop:
			sll $t0,$s7,2
			add $t0,$t0,$s0
			lw $t0,0($t0)		#dereference iterator
			slt $t0,$s3,$t0		#compare element to pivot
			bne $t0,$zero,leftiteratordone	#if it's greater than the pivot, freeze this iterator
			addi $s7,$s7,1		#otherwise, increment it
			slt $t0,$s7,$s6		#if it's no longer less than the right iterator,
			beq $t0,$zero,arrangeloopd	#then the iterators have crossed. Finish the algorithm.
			j leftiteratorloop
		leftiteratordone:
		#increment the right iterator until it either crosses the left iterator or finds an element less than the pivot
		rightiteratorloop:
			sll $t0,$s6,2
			add $t0,$t0,$s0
			lw $t0,0($t0)
			slt $t0,$t0,$s3
			bne $t0,$zero,rightiteratordone
			addi $s6,$s6,-1
			slt $t0,$s7,$s6
			beq $t0,$zero,arrangeloopd
			j rightiteratorloop
		rightiteratordone:

		#swap the elements at the two iterators
		sll $t3,$s7,2
		add $t3,$t3,$s0
		lw $t4,0($t3)	#temp
		sll $t5,$s6,2
		add $t5,$t5,$s0
		lw $t6,0($t5)
		sw $t6,0($t3)
		sw $t4,0($t5)

		#loop finish
		addi $s7,$s7,1
		addi $s6,$s6,-1			#move the iterators inward
		j arrangeloop
	arrangeloopd:

	#Test the element where the two iterators crossed
	sll $t3,$s6,2	#The right element is the better bet. It could be that the two iterators started on the same element and the left one has already moved one place, but the right one will always be correct
	add $t3,$t3,$s0
	lw $t4,0($t3)

	#if it's less than the pivot, swap it with the pivot
	slt $t0,$t4,$s3
	bne $t0,$zero,pivotswapleft
	#if it's greater than the pivot, swap the element before it with the pivot
	slt $t0,$s3,$t4
	bne $t0,$zero,pivotswapright
	#if it's equal to the pivot, we're done.
	j pivotdone
pivotswapleft:
	sll $t5,$s1,2
	add $t5,$t5,$s0
	lw $t6,0($t5)
	sw $t6,0($t3)
	sw $t4,0($t5)
	j pivotdone
pivotswapright:
	addi $s6,$s6,-1
	sll $t3,$s6,2
	add $t3,$t3,$s0
	lw $t4,0($t3)
	sll $t5,$s1,2
	add $t5,$t5,$s0
	lw $t6,0($t5)
	sw $t4,0($t5)
	sw $t6,0($t3)
pivotdone:
	#list should now be in order! Yay!

	move $v0,$s3				#report the pivot back in case anyone's interested
	move $v1,$s6				#also where the pivot ended up, in case there's multiples of it

	lw $s7,32($sp)
	lw $s6,28($sp)
	lw $s5,24($sp)
	lw $s4,20($sp)
	lw $s3,16($sp)
	lw $s2,12($sp)
	lw $s1,8($sp)
	lw $s0,4($sp)
	lw $ra,0($sp)
	addi $sp,$sp,36
	jr $ra
#/arrange
