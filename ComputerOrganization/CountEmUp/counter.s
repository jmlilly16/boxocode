	.data
list: 	.space 1024	#the list entered by the user. Max 256 words
	.globl list

query:	.asciiz "How many numbers would you like to enter? (Max 256)\n"
ioerror:.asciiz "Value is out of range\n"
endl:	.asciiz "\n"
iprompt:.asciiz "Enter one number per line (blank line defaults to 0):\n"
prsym:	.asciiz ">>"	#prompt symbol
col:	.asciiz ": "

	.text
	.globl main
	#register map: $s0: limit; $s1 count $s2: iterator, $s3: inner iterator, $s4: current word
main:	#print the start query
	li $v0,4
	la $a0,query
	syscall
	li $v0,4
	la $a0,prsym
	syscall

	#get the start input
	li $v0, 5
	syscall

	#store it in limit
	move $s0,$v0

	#check if it's in bounds
	slt $t0,$zero,$s0	#limit must be greater than 0
	slti $t1,$s0,257	#limit must be less than 257 (lt-or-eq 256)
	and $t0,$t0,$t1		#combine the conditions
	bne $t0,$zero,limgood

	#limit out of bounds. Output error message and go back.
	la $a0,ioerror
	li $v0,4
	syscall
	j main

limgood:#get input until all numbers have been stored

	#1) prompt user
	li $v0,4
	la $a0,iprompt
	syscall

	#2) set up a loop
	li $s2,0	#initialize iterator to 0
iptloop:slt $t0,$s2,$s0	#is the iterator less than the limit?
	beq $t0,$zero,iptdone	#if so, done

	#3) loop body
	#a) print prompt symbol
	li $v0,4
	la $a0,prsym
	syscall

	#b) get input
	li $v0,5
	syscall
	move $t2,$v0

	#c) store input in list
	#i) calculate address
	sll $t0,$s2,2	#multiply iterator * 4 for address offset
	la $t1,list	#load address
	add $t0,$t0,$t1	#add offset to address

	#ii) store value
	sw $t2,0($t0)

	#4) finish loop
	addi $s2,$s2,1	#increment iterator
	j iptloop
iptdone:#loop end

	#one line break output for good measure
	la $a0,endl
	li $v0,4
	syscall

	#---Count, Output---
	#loop over the list
	#for each element, loop over the rest of the list to see how many times it occurs

	#1) set up outer loop to scan the main list
	li $s2,0	#initialize iterator
oloopo:	slt $t0,$s2,$s0 #"output loop outer"
	beq $t0,$zero,oloopod

	#2) loop body
	#a) load a word from the list
	#i) calculate address
	la $t0,list	#get base address
	sll $t1,$s2,2	#shift offset
	add $t0,$t0,$t1	#apply offset

	#ii) load word
	lw $s4,0($t0)	#load word from memory

	#b) look through the rest of the list
		#1) initialize count ($s1) to 1, since we've implicitly found one copy
		li $s1,1

		#2) set up inner loop
		addi $s3,$s2,1	#inner iterator should start one *after* where we currently are
	oloopi:	slt $t1,$s3,$s0	#stands for "output loop inner"
		beq $t1,$zero,oloopid

		#3) loop body
		#a) load word from list
		#i) calculate address
		la $t0,list	#get base address
		sll $t1,$s3,2	#calculate offset
		add $t0,$t0,$t1	#apply offset

		#ii) load word
		lw $t0,0($t0)	#We no longer need the address, so it's safe to overwrite it

		#b) check if the two are equal
		bne $t0,$s4,oloopie

		#c) if they are, increment the count
		addi $s1,$s1,1

		#d) if they are not, or after you're done, increment the iterator and continue with the loop
	oloopie:addi $s3,$s3,1	#"output loop inner end"
		j oloopi

	oloopid:#done checking the histogram

	#c) output the entry and the count
	#i) output the current word
	li $v0,1
	move $a0,$s4
	syscall

	#ii) output the colon
	la $a0,col
	li $v0,4
	syscall

	#iii) output the count
	move $a0,$s1
	li $v0,1
	syscall

	#iv) output an endline
	la $a0,endl
	li $v0,4
	syscall

	addi $s2,$s2,1
	j oloopo
	#output the histogram
	#read through each element of the histogram and output it with its corresponding frequency
oloopod:jr $ra
