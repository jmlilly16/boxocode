//FILE: StringDFA.cpp
//AUTHOR: Mason Lilly
//DLM: 2/22/15

//This file implements the functions for the StringDFA object defined in StringDFA.h

#include "StringDFA.h"

#include <iostream>
#include <typeinfo>

using namespace std;

StringDFA::StringDFA(vector<DFAConn> connections, int start, vector<int> acceptingStates, int error)
	:startState(0),errorState(0),currentState(0)
{
	cout << "Constructing a StringDFA at location " << this << endl;
	cout << "Has these variables:" << endl;
	cout << "\tmap<int,State> states at " << &states << endl;
	cout << "\tState* currentState at " << &currentState << endl;
	cout << "\tState startState at " << &startState << endl;
	cout << "\tState errorState at " << &errorState << endl;
	State s(start);
	State e(error);
	cout << "Made a state at "<<&s<<endl;
	cout << "It has a vector<pair<char,State>> called transitions at " << &s.transitions << endl;
	pair<char,State> s1('x',e);
	cout << "Make a transition pair at " << &s1 << endl;
	cout << "Its target state is at " << &s1.second << endl;
	pair<int,State> p(start,s);
	cout << "Made a pair at " << &p << endl;
	cout << "The pair's two elements are a " << typeid(p.first).name() << " at " << &p.first << " with a value of " << p.first << endl;
	cout << "\tand a " << typeid(p.second).name() << " at " << &p.second << endl;
	
	auto p1 = states.insert(p);
	cout << "states.insert(p) returned a " << typeid(p1).name() << " at " << &p1 << endl; 
	cout << "Its first element is a " << typeid(p1.first).name() << " at " << &(p1.first) << endl;
	cout << "THAT points to a " << typeid(*(p1.first)).name() << " at " << &(*(p1.first)) << endl;
	cout << "Its first element is a " << typeid((*(p1.first)).first).name() << " at " << &((*(p1.first)).first) << " with a value of " << (*(p1.first)).first << endl;
	cout << "Its second element is a " << typeid((*(p1.first)).second).name() << " at " << &((*p1.first).second) << endl;

	states.insert(pair<int,State>(error,e));
	startState = &states[start];
	errorState = &states[error];
	currentState = startState;
	cout << endl << "\tstartState now equals " << startState << endl;
	cout << "\terrorState now equals " << errorState << endl;
	cout << "\tcurrentState now equals " << currentState << endl;
	for(DFAConn d: connections)
	{
		states.insert(pair<int,State>(d.origin,State(d.origin)));
		states.insert(pair<int,State>(d.dest,State(d.dest)));
		//cout << "Processing connection from " << d.origin << " through " << d.trigger << " to " << d.dest << endl;
		switch(d.type)
		{
			case DFAConn::SINGLE:
				//cout << "\tConnection is a single character." << endl;
				addTransition(Transition(d.origin,d.trigger,d.dest));
				break;
			case DFAConn::NUM:
				for(int i=48;i<58;i++)
					addTransition(Transition(d.origin,i,d.dest));
				break;
			case DFAConn::ALPHANUM:
				for(int i=48;i<58;i++)
					addTransition(Transition(d.origin,i,d.dest));
			case DFAConn::ALPHA:
				for(int i=65;i<91;i++)
					addTransition(Transition(d.origin,i,d.dest));
				for(int i=97;i<123;i++)
					addTransition(Transition(d.origin,i,d.dest));
		}
	}
	for(int i: acceptingStates)
		states[i].accept = true;
}

void StringDFA::addTransition(Transition transition)
{
	/*cout << "\tAdding a transition from " << transition.origin << " through " << transition.symbol << " to " << transition.dest << endl;
	cout << "\tstates["<<transition.origin<<"].id = " << states[transition.origin].id << endl;
	cout << "\tstates["<<transition.origin<<"].transitions['"<<transition.symbol<<"'].id = " << states[transition.origin].transitions[transition.symbol].id << endl;
	cout << "\tstates["<<transition.dest<<"].id = " << states[transition.dest].id << endl;*/
	states[transition.origin].transitions.push_back(pair<char,State>(transition.symbol,states[transition.dest]));
	/*cout << "\tstates["<<transition.origin<<"].transitions['"<<transition.symbol<<"'].id = " << states[transition.origin].transitions[transition.symbol].id << endl;*/
}

void StringDFA::transition(char symbol)
{
	//cout << "Transitioning on symbol " << symbol << endl;
	//cout << "Current state = " << currentState << ". *Current State = " << currentState->id << endl;
	for(auto& i : currentState->transitions)
	{
		//cout << "Current state has transition " << i.first << "->" << i.second.id << endl;
		if(i.first==symbol)
		{
			currentState = &(i.second);
			return;
		}
	}
	currentState = errorState;
}

bool StringDFA::isError()
{
	return currentState==errorState;
}
bool StringDFA::isAccept()
{
	return currentState->accept;
}
int StringDFA::getState()
{
	return currentState->id;
}
void StringDFA::start()
{
	currentState = startState;
}
void StringDFA::print()
{
	//cout << "Current state: " << currentState << ": " << currentState->id << endl;
	for(auto& i: currentState->transitions)
	{
		//cout << "bing" << endl;
	}
	for(auto& i: states)
	{
		//cout << "State at " << i.first << "("<<&i.second<<"): " << endl;
		//cout << "\tid="<< i.second.id << endl;
		for(auto& j: i.second.transitions)
		{
			//cout << "\t'"<<j.first<<"': "<<j.second.id<<endl;
		}
	}
	//cout << endl << "Start State: " << startState.id << endl << "Error State: " << errorState.id << endl;
}
