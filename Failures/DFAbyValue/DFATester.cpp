#include "StringDFA.h"

#include <iostream>

using namespace std;

void printDFA(StringDFA dfa)
{
	cout << "State: " << dfa.getState() << endl;
	cout << "DFA isAccept(): " << dfa.isAccept() << endl;
	cout << "DFA isError(): " << dfa.isError() << endl;
	cout << endl;
}

int main()
{
	/*vector<DFAConn> setupConns;
	setupConns.push_back(DFAConn(0,'n',1));
	setupConns.push_back(DFAConn(1,'e',2));
	setupConns.push_back(DFAConn(1,'o',3));
	setupConns.push_back(DFAConn(2,'w',4));
	setupConns.push_back(DFAConn(3,'t',5));
	vector<int> setupAccepts;
	setupAccepts.push_back(4);
	setupAccepts.push_back(5);

	StringDFA dfa(setupConns,0,setupAccepts,-1);

	dfa.print();

	cout << "Initial:" << endl;
	printDFA(dfa);
	dfa.start();
	cout << "After start(): " << endl;
	printDFA(dfa);
	dfa.transition('f');
	cout << "After transition(f):" << endl;
	printDFA(dfa);
	dfa.start();
	cout << "After start(): " << endl;
	printDFA(dfa);
	dfa.transition('n');
	cout << "After transition(n):" << endl;
	printDFA(dfa);
	dfa.transition('f');
	cout << "After transition(f):" << endl;
	printDFA(dfa);
	dfa.start();
	cout << "After start(): " << endl;
	printDFA(dfa);
	dfa.transition('n');
	cout << "After transition(n):" << endl;
	printDFA(dfa);
	dfa.transition('e');
	cout << "After transition(e):" << endl;
	printDFA(dfa);
	dfa.transition('t');
	cout << "After transition(t):" << endl;
	printDFA(dfa);
	dfa.start();
	cout << "After start(): " << endl;
	printDFA(dfa);
	dfa.transition('n');
	cout << "After transition(n):" << endl;
	printDFA(dfa);
	dfa.transition('o');
	cout << "After transition(o):" << endl;
	printDFA(dfa);
	dfa.transition('t');
	cout << "After transition(t):" << endl;
	printDFA(dfa);*/

	vector<DFAConn> setupConns;
	setupConns.push_back(DFAConn(0,'a',1));
	setupConns.push_back(DFAConn(0,'b',2));
	setupConns.push_back(DFAConn(0,'c',3));
	setupConns.push_back(DFAConn(0,'d',4));
	setupConns.push_back(DFAConn(0,'e',5));
	setupConns.push_back(DFAConn(0,'f',6));

	vector<int> setupAccepts;
	setupAccepts.push_back(1);
	setupAccepts.push_back(2);
	setupAccepts.push_back(3);
	setupAccepts.push_back(4);
	setupAccepts.push_back(5);
	setupAccepts.push_back(6);
	
	StringDFA dfa(setupConns,0,setupAccepts);
	
	dfa.start();
	dfa.print();
	printDFA(dfa);
	dfa.transition('e');
	cout << "After transition(e):" << endl;
	printDFA(dfa);
	
	return 0;
}
