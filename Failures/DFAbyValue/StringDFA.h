//FILE: StringDFA.h
//AUTHOR: Mason Lilly
//DLM: 2/22/15

//This file defines a class to represent a Discrete Finite Automaton for processing strings.

#ifndef StrDFA
#define StrDFA

#include <string>
#include <vector>
#include <map>
#include <iostream>
using namespace std;

struct DFAConn
{
	enum CharSet {SINGLE,ALPHA,NUM,ALPHANUM};
	int origin;
	char trigger;
	int dest;
	CharSet type;
	
	DFAConn(int origin, char trigger, int dest, CharSet type)
		:origin(origin), trigger(trigger), dest(dest), type(type)
	{}
	DFAConn(int origin, char trigger, int dest)
		:origin(origin), trigger(trigger), dest(dest)
	{
		type = SINGLE;
	}
};

class StringDFA
{
	struct State;
	struct State
	{
		vector<pair<char,State>> transitions;
		bool accept;
		int id;
		State(int id)
			:accept(false),id(id)
		{
			cout << "Constructing a State at " << this << ":" << endl;
			cout << "It has these variables: " << endl;
			cout << "\tvector<pair<char,State>> transitions at " << &transitions << endl;
			cout << "\tbool accept at " << &accept << endl;
			cout << "\tint id at " << &id << endl;
		}
		State()
			:accept(false),id(-2)
		{/*cout << "Constructed state "<<id<<" at location: " << this << endl << "It has a transition list at location " << &transitions << endl;*/}
		bool operator==(State other){return this->id==other.id;}
	};
	struct Transition
	{
		int origin;
		char symbol;
		int dest;
		Transition(int origin, char symbol, int dest)
			:origin(origin), symbol(symbol), dest(dest)
		{}
	};
	map<int,State> states;
	
	State* currentState;
	State* startState;
	State* errorState;

	void addTransition(Transition);
	
	public:
	StringDFA(vector<DFAConn> connections, int startState, vector<int> acceptingStates, int errorState=-1);
	void transition(char symbol);
	bool isAccept();
	bool isError();
	void start();
	int getState();
	void print();
};

#endif
