//FILE: StringDFA.cpp
//AUTHOR: Mason Lilly
//DLM: 2/22/15

//This file implements the functions for the StringDFA object defined in StringDFA.h

#include "StringDFA.h"

StringDFA::StringDFA(vector<DFAConn> connections, int startState, vector<int> acceptingStates)
	:startState(startState)
{
	for(DFAConn d: connections)
	{
		states[d.origin] = false;
		states[d.dest] = false;
		switch(d.type)
		{
			case DFAConn.SINGLE:
				transitions.add(Transition(d.origin,d.trigger,d.dest));
				break;
			case DFAConn.NUM:
				for(int i=48;i<58;i++)
					transitions.add(Transition(d.origin,i,d.dest));
				break;
			case DFAConn.ALPHANUM:
				for(int i=48;i<58;i++)
					transitions.add(Transition(d.origin,i,d.dest));
			case DFAConn.ALPHA:
				for(int i=65;i<91;i++)
					transitions.add(Transition(d.origin,i,d.dest));
				for(int i=97;i<123;i++)
					transitions.add(Transition(d.origin,i,d.dest));
		}
	}
	for(int i: acceptingStates)
	{
		states[i] = true;
	}
}
