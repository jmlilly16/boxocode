//FILE: StringDFA.h
//AUTHOR: Mason Lilly
//DLM: 2/22/15

//This file defines a class to represent a Discrete Finite Automaton for processing strings.

#ifndef StrDFA
#define StrDFA

#include <string>
using namespace std;

struct DFAConn
{
	enum CharSet {SINGLE,ALPHA,NUM,ALPHANUM};
	int origin;
	char trigger;
	int dest;
	CharSet type;
	
	DFAConn(int origin, char trigger, int dest, CharSet type)
		:origin(origin), trigger(trigger), dest(dest), type(type)
	{}
	DFAConn(int origin, char trigger, int dest)
		:origin(origin), trigger(trigger), dest(dest)
	{
		type = SINGLE;
	}
};

class StringDFA
{
	struct Transition
	{
		int origin;
		char symbol;
		int dest;
	};
	map<int,bool> states;
	vector<Transition> transitions;
	
	int currentState;
	int startState;
	
	public:
	StringDFA(vector<DFAConn> connections, int startState, vector<int> acceptingStates);
	void transition(char symbol);
	bool isAccept();
}
