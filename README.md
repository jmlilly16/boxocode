This repository is an archive of all the coding work I did while at Transylvania University and contains some good code samples. Except for my capstone project, which is in my TransyTour repository, each course is represented by a folder.

CompilerConstruction contains the "biggest" single project completed for a course; it consists of a fully functional compiler/executor setup for the TRANSY language, a BASIC-like language created for the course.

Other good code samples can be found in the Graphics, NetCentric, and MachineIntelligence folders.

Unfortunately, my Java code which I wrote for my Robotics class my freshman year has been lost.