#include "nesting.h"

#include <iostream>
using namespace std;

int main()
{
	nesting<5> foo;
	nesting<7> bar;
	
	foo[3] = 4;
	cout << foo[3] << endl;
}
