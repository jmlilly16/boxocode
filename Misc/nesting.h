#ifndef NESTING_H
#define NESTING_H

template <unsigned N>
class nesting : public nesting<N-1>
{
	int data;
	public:
	int& operator[](unsigned index);
};

template <unsigned N>
int& nesting<N>::operator[](unsigned index)
{
	if(index > N)
		throw "Out of Range!";
	if(index == 1)
		return data;
	return nesting<N-1>::operator[](index - 1);
}

template<>
class nesting<1>
{
	int data;
	public:
	int& operator[](unsigned index)
	{
		if(index > 1)
			throw "Out of Range!";
		if(index == 1)
			return data;
	}
};

#endif
