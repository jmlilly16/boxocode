;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;                                          
;;;;  Micro ELI
;;;;
;;;;  Common Lisp implementation and
;;;;  modifications by:
;;;;
;;;;  Bill Andersen (waander@cs.umd.edu)
;;;;  Department of Computer Science
;;;;  University of Maryland
;;;;  College Park, MD  20742
;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(require :cd-functions)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Global Variables
;;;

(defvar *stack* nil)      ;request packet stack

(defvar *concept*)        ;globals set by request assignments
(defvar *sentence*)
(defvar *cd-form*)
(defvar *word*)
(defvar *part-of-speech*)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Data Structures
;;;

(defun top-stack () (car *stack*))

(defun add-stack (packet)
  (and packet (push packet *stack*))
  packet)

(defun pop-stack () (pop *stack*))

(defun init-stack () (setq *stack* nil))

(defun empty-stack-p () (null *stack*))

(defun load-def ()
  "Adds a word's request packet to the stack. 
Word definitions are stored under the property
DEFINITION."
  (let ((packet (get *word* 'defintion)))
    (cond (packet (add-stack packet))
          (t (user-trace " - not in dictionary~%")))))

(defun req-clause (key l)
  "Extracts clauses from requests of the form:
((test ...) (assign ...) (next-packet ...))"
  (let ((x (assoc key l)))
    (and x (cdr x))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Top Level Functions
;;;

(defun process-text (text)
  "Process a list of sentences, parsing each one, and
printing the result."
  (dolist (sentence text)
    (user-trace "~2%Input is ~s~%" sentence)
    (let ((cd (parse sentence)))
      (user-trace "~2%CD form is ~s" cd)))
  (values))

(defun parse (sentence)
  "Takes a sentence in list form and returns the
conceptual analysis for it."
  (setq *concept* nil)
  (init-stack)
  (do ((*word* nil)
       (*sentence* (cons '*start* sentence)))
      ((null (setq *word* (pop *sentence*)))
       (remove-variables *concept*))
    (user-trace "~2%Processing word ~s" *word*)
    (load-def)
    (run-stack)))

(defun run-stack ()
  "If some request in the packet on the top of the stack
can be triggered, that packet is removed from the stack,
and the request is saved and executed.  When the top packet
contains no triggerable requests, the packets in the
requests which were triggered and saved are added to the
stack."
  (do ((request (check-top) (check-top))
       (triggered nil))
      ((null request) (add-packets triggered))
    (pop-stack)
    (do-assigns request)
    (push request triggered)))

(defun check-top ()
  "Returns the first request with a true test from the
top packet in the stack."
  (unless (empty-stack-p)
    (dolist (request (top-stack))
       (when (or (null request)
                 (is-triggered request))
         (return request)))))

(defun is-triggered (request)
  "Returns T if a request has no test or if the test
evaluates to T."
  (let ((test (req-clause 'test request)))
    (or (null test) (eval (car test)))))

(defun do-assigns (request)
  "Sets the global variables given in the ASSIGN clause
of a request."
  (do ((assignments (req-clause 'assign request)
                    (cddr assignments)))
      ((null assignments))
    (reassign (first assignments) 
              (second assignments))))

(defun reassign (var val)
  "Reassigns var to val and prints a message."
  (when (set var (eval val))
    (user-trace "~&  ~s = ~s~%" var (eval var))))

(defun add-packets (requests)
  "Takes a list of requests and add their NEXT-PACKETs
to the stack."
  (dolist (request requests)
    (add-stack (req-clause 'next-packet request))))

;; NOTE: 
;;   This function has been modified to handle a list of CD forms
;; instead of a single form.  The reason for this is that some
;; concepts cannot be expressed with a single CD.  For example, 
;; "buying" is an ATRANS of something to the actor *and* and
;; ATRANS of money from the actor to the seller.
;;   The parser is not affected since all it does is control the
;; request firing which in turn makes variable assignments.

(defun remove-variables (cd-form)
  "Takes a parsed CD from Micro-ELI and returns a copy
of the pattern with the variables replaced by values.
Roles with NIL fillers are left out of the result.  This
works like INSTANTIATE in Micro-SAM and Micro-POLITICS
except that the values are derived from global variables
rather than binding lists."
  (cond ((symbolp cd-form) cd-form)
        ((is-var cd-form)
         (remove-variables (eval (name-var cd-form))))
        (t (replace-list cd-form))))

;; REPLACE-LIST is just an auxiliary function for 
;; REMOVE-VARIABLES.
(defun replace-list (cd-form)
  (cond ((null cd-form) nil)
        ((atom (header-cd cd-form))
         (cons (header-cd cd-form)
               (let (result)
                 (dolist (pair (roles-cd cd-form))
                   (let ((val (remove-variables (filler-pair pair))))
                     (when val (push `(,(role-pair pair) ,val)
                      result))))
                 (nreverse result))))
        (t (cons (replace-list (car cd-form))
                 (replace-list (cdr cd-form))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;;  Dictionary Functions
;;;

(defmacro defword (&body def)
  `(progn (setf (get ',(car def) 'defintion) ',(cdr def))
          ',(car def)))

;; Example vocabulary items...

;;We've left in the "development" versions of tricky verbs, to help illustrate some of the problems encountered.
;;This is the version initially included, which only handles physical movement.
;;In order to understand the sentence "Mason went crazy", a non-physical interpretation of the word needed to be added.
(defword went
  ((assign *part-of-speech* 'verb
           *cd-form* '(ptrans (actor  ?go-var1)
                              (object ?go-var1)
                              (to     ?go-var2)
                              (from   ?go-var3))
           go-var1 *subject*
           go-var2 nil
           go-var3 nil)
   (next-packet
    ((test (equal *word* 'to))
     (next-packet
      ((test (equal *part-of-speech* 'noun-phrase))
       (assign go-var2 *cd-form*))))
    ((test (equal *word* 'home))
     (assign go-var2 '(house))))))

;;Here is the first draft at a "mental go" interpretation. It uses the mtrans CD, then seeks an adjective to describe the actor's new mental state.
(defword went
  ((assign *part-of-speech* 'verb
           *cd-form* '(mtrans (actor  ?get-var1)
                              (object ?get-var1)
                              (to     ?get-var2)
                              (from   ?get-var3))
           get-var1 *subject*
           get-var2 nil
           get-var3 nil)
   (next-packet
      ((test (equal *part-of-speech* 'adjective))
       (assign get-var2 *cd-form*))))
    ((test (equal *word* 'crazy))
     (assign get-var2 '(crazy))))

;;You can only define a word once, so both interpretations need to be in one definition.
;;This iteration immediately waits for information from the next word, then picks either mtrans or ptrans as its interpretation.
(defword went
	((test (equal *word* 'went))
	(next-packet
		((test (equal *part-of-speech* 'adjective))
		(assign *part-of-speech* 'verb
			*cd-form* '(mtrans 	(actor ?get-var1)
						(object ?get-var1)
						(to	?get-var2)
						(from	?get-var3))
			get-var1 *subject*
			get-var2 '(crazy)
			get-var3 nil)
		)
		((test (equal *word* 'to))
		(assign *part-of-speech* 'verb
			*cd-form* '(ptrans 	(actor ?go-var1)
						(object ?go-var1)
						(to	?go-var2)
						(from	?go-var3))
			go-var1 *subject*
			go-var2 '(store)
			go-var3 nil)
		)
	))
)

;;When we learned that we could add our own CDs to the program, we decided to redefine the mental sense of "went".
;;Mtrans sounds appropriate in the name, but it is not faithful to the original sense of mtrans, which deals fairly strictly with communication.
;;Specifically, because all of Schank's basic CDs deal specifically with things "observable on a movie screen",
;;which means anything concerning mental patterns or states, which includes sanity, is inexpressible using Schank's CDs.
;;We decided to implement an S-Trans CD, or state-transition.
;;This CD could be used to represent any action that results in the changing of some state description of an object,
;;such as getting angry (emotional state), turning on a blender (operational state), painting a fence (physical state (color)), getting married (legal state), or, in this case, going crazy (mental state).
;;Of course, in this system, it might make just as much sense to interpret ptrans as simply a physical strans, and atrans as an strans concerning ownership. While this is an intriguing idea, however, exploring and implementing it further would require more time than we have, and the program works fine as it is.

;;This iteration uses the new strans CD, and waits to report its CD form until the program finds a part of speech that indicates the proper one.
;;This funciton ultimately doesn't work, however, because of how it interacts with the preposition "to".
;;When "to" is loaded and the stack is run, its request tests for a 'noun-phrase.
;;This is supposed to detect the 'noun-phrase reported by the following "the".
;;Instead, when "to"'s request is tested, the *part-of-speech* variable is already 'noun-phrase from when "Lorin" was parsed.
;;Thus, (person (name (Lorin))) is reported as the noun-phrase found by "to", and we get the interpretation that Lorin went to the Lorin.
(defword went
	((test (equal *part-of-speech* 'adjective))
	(assign *part-of-speech* 'verb
		state-var2 *cd-form*
		*cd-form* '(strans	(actor	?state-var1)
					(object ?state-var1)
					(to	?state-var2)
					(from	?state-var3))
		state-var1 *subject*
		state-var3 nil)
	)
	((test (equal *part-of-speech* 'preposition))
	(assign *part-of-speech* 'verb
		go-var2 *cd-form*
		*cd-form* '(ptrans	(actor	?go-var1)
					(object	?go-var1)
					(to	?go-var2)
					(from	?go-var3))
		go-var1 *subject*
		go-var3 nil
	))
)

;;**This is the version that actually gets run.**
;;To circumvent the problem with the previous definition, "went" needs to immediately inform the program that it is a verb.
;;(We discovered that reporting part-of-speech immediately is generally good manners to the rest of the words, anyway).
;;This prevents "to" from mistakenly picking up "Lorin".
;;However, setting *part-of-speech* to 'verb immediately prompts the remaining request from *start* (the one that's looking for the sentence's concept) to trigger and set the sentence concept to whatever is in *cd-form* at the time.
;;At this point, it's still (person (name (Lorin))).
;;So, we need to put SOMETHING into *cd-form*, without knowing yet what.
;;Thus, this definition sets cd-form to a variable name, and then later fills it with the proper CD form, itself containing more variables, then fills those variables as normal.
(defword went
	((assign	*part-of-speech* 'verb
			*cd-form* ?went-form)
	(next-packet
		((test (equal *part-of-speech* 'adjective))
		(assign went-form '(strans	(actor	?state-var1)
						(object ?state-var1)
						(to	?state-var2)
						(from	?state-var3))
			state-var1 *subject*
			state-var2 *cd-form*
			state-var3 nil)
		)
		((test (equal *part-of-speech* 'preposition))
		(assign went-form '(ptrans	(actor	?go-var1)
						(object ?go-var1)
						(to	?go-var2)
						(from 	?go-var3))
			go-var1 *subject*
			go-var2 *cd-form*
			go-var3 nil)
		)
	))
)

;;"Passed" was the other word that required us to make up new rules and posed some programming difficulty.
;;The first definition, as in "ben passed the store", is easy to interpret.
;;This first definition interprets all uses as ptrans.

(defword passed
  ((assign *part-of-speech* 'verb
           *cd-form* '(ptrans (actor  ?go-var1)
                              (object ?go-var1)
                              (to     ?go-var2)
                              (from   ?go-var3))

           go-var1 *subject*
           go-var2 nil
           go-var3 nil
	)
   (next-packet
    ((test (equal *word* 'the))
     (next-packet
      ((test (equal *part-of-speech* 'noun-phrase))
       (assign go-var3 *cd-form*))))
	((test (equal *word* 'store))
		(assign go-var3 '(store)))
	((test (equal *word* 'test))
     		(assign go-var1 '(test)
 		go-var3 *subject*))
       )
      )
     )

;;Obviously, though, passing a test doesn't involve movement. We added a second interpretation, calling it mtrans.
(defword passed
	((test (equal *word* 'passed))
	(next-packet
		((test (equal *word* 'the))
     			(next-packet
     				 ((test (equal *part-of-speech* 'noun-phrase))
      					 (assign go-var3 *cd-form*))))
		((test (equal *word* 'test))
		(assign *part-of-speech* 'verb
			*cd-form* '(mtrans 	(actor ?get-var1)
						(object ?get-var1)
						(to	?get-var2)
						(from	?get-var3))
			get-var1 *subject*
			get-var2 nil
			get-var3 '(test))
		)
		((test (equal *word* 'store))
		(assign *part-of-speech* 'verb
			*cd-form* '(ptrans 	(actor ?go-var1)
						(object ?go-var1)
						(to	?go-var2)
						(from	?go-var3))
			go-var1 *subject*
			go-var2 nil
			go-var3 '(store))
		)
	))
)

;;While mtrans is the closest fit among Schank's CDs to refer to taking an examination,
;;as was the problem with "went", calling passing a test a form of communication doesn't really cut it.
;;Stating that someone passed a test specifies neither what information was communicated, nor whom it was communicated to.
;;Rather, this form of "pass" seems only to occur in reference to a test of some form. The implication is that one took the test, and did so successfully.
;;"Taking" a test doesn't fit into any available CD either. Nothing is being transferred between entities, so it can't be atrans.
;;Mtrans doesn't work for previously stated reasons. Nor is it mbuild, since mbuild happens internally and what we have is clearly an interaction between Kelsey and the test.
;;Strans doesn't really work, either. By passing the test, nothing has been changed about the test - any number of people can come take and pass or fail it in the future. Nor has Kelsey changed a property intrinsic to herself by passing the test.
;;What the statement "Kelsey passed the test" tells us is that Kelsey did some action specified by the test. It's not specified what - it could be a fitness test, a musical demonstration, a verbal examination, etc.
;;Therefore, the specific action Kelsey performed is indeterminable. She "did the thing", whatever actions that entails, and "passed", when "the thing" is a "test", implies that she did it well.
;;The sentence tells us very little about what was done, only HOW it was done.
;;If we want to know more about what Kelsey did, we have to rely on prior or upcoming knowledge about the test.
;;To account for this, and the many other verbs that exist that describe "doing", such as "he botched the recipe", "I worked the fields", "she used the internet", or "John did his job",
;;we implemented a "perform" CD, which takes as its parameters the actor doing the action, the name of a script that describes what was done, and an outcome variable that in this case stores whether "taking the test" was successful.
;;In general, use of perform should be interpreted as something to the effect of "(agent) did with (object) what you usually do with a (object)".

;;In this and all following iterations, the test for whether to invoke the perform CD is simply whether the last word read was "test" or something else.
;;Without making the system much more complicated, this was the only way to do it. "Test" and "store" are no different in part-of-speech, the only piece of classification information that gets passed between requests through global variables.
;;The way to make it work more generally would be to subclassify the nouns; i.e., label "store" as a location and know that that implies a ptrans, and label "test" as an "activity" or something, and know that this invokes a script.
;;Needless to say, this would have taken way longer than we had.

;;Now, as for this particular implementation, the first packet level necessarily assigns part-of-speech and a placeholder cd variable similar to "went". We tried to make the first next-packet level west to wait for a noun-phrase, then immediately insert another set of tests to determine which variety of noun-phrase was found, but by the time those requests were checked the program had moved on to the next word (or specifically, it had terminated because it was out of words).
;;What you see here, then, is a poorly-fated attempt at including two assign blocks joined by a hard if structure to do the test immediately once a noun-phrase was found.
;;We quickly realized that micro-eli looks specifically for blocks labelled "assign", so this wasn't going to work.
(defword passed
	((assign	*part-of-speech* 'verb
			*cd-form*	?passed-form
	)
	(next-packet
		((test (equal *part-of-speech* 'noun-phrase))
		(if 	(equal *word* 'test)
			(assign	passed-form	'(perform	(actor	?perf-var1)
								(script ?perf-var2)
								(outcome ?perf-var3))
				perf-var1	*subject*
				perf-var2	*cd-form*
				perf-var3	'(success)
			)
			(assign	passed-form	'(ptrans	(actor	?go-var1)
								(object	?go-var1)
								(to	?go-var2)
								(to	?go-var3))
				go-var1		*subject*
				go-var2		nil
				go-var3		*cd-form*
			)
		))
	))
)

;;I'm too embarrassed to explain this one.
(defword passed
	((assign	*part-of-speech* 'verb
			*cd-form*	?passed-form
	)
	(next-packet
		((test (equal *part-of-speech* 'noun-phrase))
		(assign passed-form	(if (equal *word* 'test) '(perform	(actor	?perf-var1)
										(script ?perf-var2)
										(outcome ?perf-var3))
								 '(ptrans	(actor	?go-var1)
										(object	?go-var1)
										(to	?go-var2)
										(from	?go-var3)))
			(if (equal *word* 'test) perf-var1 go-var1)
				*subject*
			(if (equal *word* 'test) perf-var2 go-var2)
				(if (equal *word* 'test) *cd-form* nil)
			(if (equal *word* 'test) perf-var3 go-var3)
				(if (equal *word* 'test) '(success) *cd-form))
		)
	))
)

;;Finally, here, I remembered my 10th grade "Compound Conditional" lessons and made two tests, one for a noun phrase that ends in "test" and one for a generic noun phrase.
(defword passed
	((assign 	*part-of-speech* 'verb
			*cd-form*	?passed-form
	)
	(next-packet
		((test (and (equal *part-of-speech* 'noun-phrase) (equal *word* 'test)))
		(assign	passed-form	'(perform	(actor	?perf-var1)
							(script ?perf-var2)
							(outcome ?perf-var3))
			perf-var1	*subject*
			perf-var2	*cd-form*
			perf-var3	'(success)
		))
		((test (equal *part-of-speech* 'noun-phrase))
		(assign	passed-form	'(ptrans	(actor	?go-var1)
							(object	?go-var1)
							(to	?go-var2)
							(from	?go-var3))
			go-var1		*subject*
			go-var2		nil
			go-var3		*cd-form*
		))
	))
)

;;This is an example of a more typical verb. No double-meanings, just one CD that gets set and then requests a preposition to fill a variable.
(defword walked
  ((assign *part-of-speech* 'verb
           *cd-form* '(ptrans (actor  ?go-var1)
                              (object ?go-var1)
                              (to     ?go-var2)
                              (from   ?go-var3))

           go-var1 *subject*
           go-var2 nil
           go-var3 nil
	)
   (next-packet
   	((test (equal *part-of-speech* 'preposition))
        (assign go-var2 *cd-form*))
   ))
)

;;Similar to walked. Were this being written for a more conscientious knowledge-representation system, we might include an
;;?instrument variable that would acknowledge that this verb implies a car was involved. But for our purposes they work the same.
(defword drove
  ((assign *part-of-speech* 'verb
           *cd-form* '(ptrans (actor  ?go-var1)
                              (object ?go-var1)
                              (to     ?go-var2)
                              (from   ?go-var3))

           go-var1 *subject*
           go-var2 nil
           go-var3 nil
	)
     (next-packet
      ((test (equal *part-of-speech* 'preposition))
       (assign go-var2 *cd-form*)))
     )
)
     
;;Sets up an atrans CD, which knows initially that an actor transferred something to itself, and looks for a noun phrase to determine what that something is.
(defword bought
  ((assign *part-of-speech* 'verb
           *cd-form* '(atrans (actor  ?go-var1)
                              (object ?go-var2)
                              (to     ?go-var1)
                              (from   ?go-var3))

           go-var1 *subject*
           go-var2 nil
           go-var3 nil
	)
   (next-packet
      ((test (equal *part-of-speech* 'noun-phrase))
       (assign go-var2 *cd-form*))
   ))
)

;;The only real pronoun in this program. It's also not used in any of our sentences.
;;Since Micro-ELI deals only with short sentences, independent of each other, there's no functional difference between a pronoun and any proper noun.
(defword he
  ((assign *part-of-speech* 'noun-phrase
           *cd-form* '(person))))

;;See "bought"
(defword got
  ((assign *part-of-speech* 'verb
           *cd-form* '(atrans (actor  ?get-var3)
                              (object ?get-var2)
                              (to     ?get-var1)
                              (from   ?get-var3))
           get-var1 *subject*
           get-var2 nil
           get-var3 nil)
   (next-packet
    ((test (eq *part-of-speech* 'noun-phrase))
     (assign get-var2 *cd-form*)))))

;;This is another "going places" word, which looks for a location that was left by the actor.
;;One thing of note here is that the next-packet's test is for a noun, rather than a preposition.
;;An ideal system would be able to handle "Mary left home", "The bus left the station", and "The flight left from Heathrow".
;;Here, though, since the program only has to interpret "John left school", only the single-noun interpretation is checked for.
(defword left
  ((assign *part-of-speech* 'verb
           *cd-form* '(ptrans (actor  ?get-var1)
                              (object ?get-var1)
                              (to     ?get-var2)
                              (from   ?get-var3))
           get-var1 *subject*
           get-var2 nil
           get-var3 nil)
   (next-packet
    ((test (eq *part-of-speech* 'noun))
     (assign get-var3 *cd-form*)))))

;;This word and its companion, "the", are responsible for turning nouns into noun-phrases.
(defword a
  ((test (equal *part-of-speech* 'noun))
   (assign *part-of-speech* 'noun-phrase
           *cd-form* (append *cd-form* *predicates*)
           *predicates* nil)))
;;The program's only preposition. Essentially its only function is to look for a noun phrase and report it.
;;We do break our own rule here and wait to announce the word's part of speech until its content has been filled.
;;No particular reason for that, just that it works and there's no time to change it now.
(defword to
	((test (equal *part-of-speech* 'noun-phrase))
	(assign *part-of-speech* 'preposition
		*cd-form* (append *cd-form* *predicates*)
           	*predicates* nil)
	)
)

(defword the
  ((test (equal *part-of-speech* 'noun))
   (assign *part-of-speech* 'noun-phrase
           *cd-form* (append *cd-form* *predicates*)
           *predicates* nil)))
;;Just as "he" functions as a proper noun, "his" functions essentially as an article.
(defword his
  ((test (equal *part-of-speech* 'noun))
   (assign *part-of-speech* 'noun-phrase
           *cd-form* (append *cd-form* *predicates*)
           *predicates* nil)))

;;Nouns! Good old nouns. They don't ask for anything in life.
(defword kite
  ((assign *part-of-speech* 'noun
           *cd-form* '(kite))))

(defword store
  ((assign *part-of-speech* 'noun
           *cd-form* '(store))))
(defword test
  ((assign *part-of-speech* 'noun
           *cd-form* '(test))))
(defword class
  ((assign *part-of-speech* 'noun
           *cd-form* '(class))))
(defword game
  ((assign *part-of-speech* 'noun
           *cd-form* '(game))))
(defword school
  ((assign *part-of-speech* 'noun
           *cd-form* '(school))))

(defword jack
  ((assign *cd-form* '(person (name (jack)))
           *part-of-speech* 'noun-phrase)))
(defword Lorin
  ((assign *cd-form* '(person (name (Lorin)))
           *part-of-speech* 'noun-phrase)))
(defword Ben
  ((assign *cd-form* '(person (name (Ben)))
           *part-of-speech* 'noun-phrase)))
(defword Kelsey
  ((assign *cd-form* '(person (name (Kelsey)))
           *part-of-speech* 'noun-phrase)))
(defword Mason
  ((assign *cd-form* '(person (name (Mason)))
           *part-of-speech* 'noun-phrase)))
(defword Skyler
  ((assign *cd-form* '(person (name (Skyler)))
           *part-of-speech* 'noun-phrase)))
(defword Michael
  ((assign *cd-form* '(person (name (Michael)))
           *part-of-speech* 'noun-phrase)))
(defword John
  ((assign *cd-form* '(person (name (John)))
           *part-of-speech* 'noun-phrase)))
(defword Jane
  ((assign *cd-form* '(person (name (Jane)))
           *part-of-speech* 'noun-phrase)))

;;Oh, yeah, and our one adjective.
;;It would have been nice to play with these guys some more.
;;There's an interesting problem that arises from the use of an adjective in "went", however.
;;An adjective ought to check ahead of it for a noun to modify. In most cases, anyway.
;;If used after "went", however, there is no noun to be modified.
;;Now, technically, in "went crazy", "crazy" is an adverb, not an adjective, since it modifies went.
;;This means the grammatical role of a word like crazy can change based on what words have come BEFORE,
;;something that no other word has to do in our wordset.
;;Since none of our sentences need to actually use adjectives to modify nouns, this lets us essentially treat adjectives as adverbs
(defword crazy
	((assign 	*part-of-speech* 'adjective
			*cd-form* '(crazy))
	)
)

(defword *start*
  ((assign *part-of-speech* nil
           *cd-form* nil
           *subject* nil
           *predicates* nil)
   (next-packet
    ((test (equal *part-of-speech* 'noun-phrase))
     (assign *subject* *cd-form*)
     (next-packet
      ((test (equal *part-of-speech* 'verb))
       (assign *concept* *cd-form*)))))))

(setq kite-text
      '((jack went to the store)
        (he got a kite)
        (he went home)))
(setq class-text
      '((Lorin went to the store)
	(Ben passed the store)
	(Kelsey passed the test)
	(Mason went crazy)
	(Skyler walked to his class)
	(Michael bought the game)
	(John left school)
	(Jane drove to the school)))

(provide :micro-eli)
