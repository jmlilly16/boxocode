TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

INCLUDEPATH += ../../source/

SOURCES += \
    source/NDFATester.cpp

HEADERS += \
    ../../source/NDFA.h \
    ../../source/NDFARunner.h \
    ../../source/Symbol.h \
    ../../source/DFA.h \
    ../../source/NDFABuilder.h

TARGET = NDFATester
