#include "NDFA.h"
#include "NDFARunner.h"
#include "NDFABuilder.h"

#include <iostream>
#include <string>
#include <sstream>
using namespace std;

enum CMD: char
{
	NEWNDFA = 'n',
	LISTNDFAS = 'M',
	WHICHNDFA = 'm',
	SWITCHNDFA = 'b',
	REMOVENDFA = 'N',

	ADDSTATE = 'a',
	CONTAINSSTATE = 's',
	LISTSTATES = 'S',
	SETSTART = 'd',
	WHATSTART = 'D',
	SETFINAL = 'f',
	ISFINAL = 'F',
//	REMOVESTATE = 'r',

	ADDTRANSITION = 't',
	ADDEPSILON = 'e',
	LISTTRANSITIONS = 'T',
	REMTRANSITION = 'y',
	REMEPSILON = 'w',
	REMEPSILONS = 'W',
	REMTRANSITIONS = 'Y',
	CLEARTRANSITIONS = 'c',

	DELTA = ':',
	EXPANDEPSILON = 'E',
	ACCEPT = '>',

	ISPROPER = 'P',
	MAKEPROPER = 'p',

	UNION = 'u',
	INTERSECT = 'i',
	COMPLEMENT = 'o',
	CLOSURE = '*',
	POSCLOSURE = '+',
	CONCAT = '.',

	MAKEDETER = '!',
	MINIMIZE = '@',

	EXIT = 'Q',
	HELP = '?'
};

char cmd;
char args[64];
stringstream argstr;

map<NDFA<char>*,string> ndfas;
NDFA<char>* cur_ndfa =  nullptr;

void newNDFA();
void listNDFAs();
void whichNDFA();
void switchNDFA();
void removeNDFA();
void addState();
void containsState();
void listStates();
void setStart();
void whatStart();
void setFinal();
void isFinal();
void addTransition();
void addEpsilon();
void listTransitions();
void removeEpsilon();
void removeEpsilons();
void removeTransition();
void removeTransitions();
void clearTransitions();
void delta();
void expandEpsilon();
void isProper();
void makeProper();
void Union();
void intersect();
void complement();
void closure();
void posclosure();
void concat();
void makeDeterministic();
void minimize();
void accept();
void printHelp();

void cleanup();

template <typename T>
bool readArg(T& arg);

int main()
{
	cout << "Welcome" << endl;
	while(cmd != EXIT)
	{
		while(true)
		{	//get input, at least one non-newline character
			cout << (cur_ndfa!=nullptr?ndfas[cur_ndfa]:"") << ":" << flush;
			cin.get(cmd);
			if(cmd == '\n')
				continue;
			cin.getline(args,64);
			argstr.str(string(args));
			argstr.clear();
			break;
		}
		switch(cmd)
		{	//delegate command
			case NEWNDFA:
				newNDFA();
				break;
			case LISTNDFAS:
				listNDFAs();
				break;
			case WHICHNDFA:
				whichNDFA();
				break;
			case SWITCHNDFA:
				switchNDFA();
				break;
			case REMOVENDFA:
				removeNDFA();
				break;
			case ADDSTATE:
				addState();
				break;
			case CONTAINSSTATE:
				containsState();
				break;
			case LISTSTATES:
				listStates();
				break;
			case SETSTART:
				setStart();
				break;
			case WHATSTART:
				whatStart();
				break;
			case SETFINAL:
				setFinal();
				break;
			case ISFINAL:
				isFinal();
				break;
//			case REMOVESTATE:
//				removeState();
//				break;
			case ADDTRANSITION:
				addTransition();
				break;
			case ADDEPSILON:
				addEpsilon();
				break;
			case LISTTRANSITIONS:
				listTransitions();
				break;
			case REMEPSILON:
				removeEpsilon();
				break;
			case REMEPSILONS:
				removeEpsilons();
				break;
			case REMTRANSITION:
				removeTransition();
				break;
			case REMTRANSITIONS:
				removeTransitions();
				break;
			case CLEARTRANSITIONS:
				clearTransitions();
				break;
			case DELTA:
				delta();
				break;
			case EXPANDEPSILON:
				expandEpsilon();
				break;
			case ACCEPT:
				accept();
				break;
			case ISPROPER:
				isProper();
				break;
			case MAKEPROPER:
				makeProper();
				break;
			case UNION:
				Union();
				break;
			case INTERSECT:
				intersect();
				break;
			case COMPLEMENT:
				complement();
				break;
			case HELP:
				printHelp();
				break;
			case EXIT:
				cleanup();
				break;
			default:
				cout << "Unrecognized command" << endl;
				break;
		}
	}
    return 0;
}

void newNDFA()
{
	string name;
	if(readArg(name))
	{
		for(auto n: ndfas)
		if(n.second==name)
		{
			cout << "Name is already in use" << endl;
			return;
		}
		ndfas[new NDFA<char>] = name;
		cout << "Created \"" << name << "\"" << endl;
	}
}

void listNDFAs()
{
	if(!ndfas.size())
	{
		cout << "There are currently no NDFAs in memory - use 'n' to make one" << endl;
		return;
	}
	cout << "Currently loaded NDFAs:" << endl;
	for(auto i: ndfas)
		cout << i.second << endl;
}

void whichNDFA()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is currently selected. Use 'b' to select one." << endl;
		return;
	}
	cout << "Currently selected NDFA:" << endl;
	cout << ndfas[cur_ndfa] << endl;
}

void switchNDFA()
{
	if(!ndfas.size())
	{
		cout << "There are currently no NDFAs in memory - use 'n' to make one" << endl;
		return;
	}
	string name;
	if(readArg(name))
	{
		for(auto n: ndfas)
			if(n.second==name)
			{
				cur_ndfa = n.first;
				return;
			}
		cout << "Name is not defined" << endl;
	}
}

void removeNDFA()
{
	if(!ndfas.size())
	{
		cout << "There are no NDFAs to remove" << endl;
		return;
	}
	string name;
	if(readArg(name))
	{
		for(auto n: ndfas)
			if(n.second==name)
			{
				if(cur_ndfa==n.first)
					cur_ndfa=nullptr;
				delete n.first;
				ndfas.erase(n.first);
				return;
			}
		cout << "Name is not defined" << endl;
	}
}

void addState()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel stateName;
	if(readArg(stateName))
	{
		if(cur_ndfa->addState(stateName))
			cout << "Added State " << stateName << endl;
		else
			cout << "Could not add State " << stateName << " - state already exists" << endl;
	}
}

void containsState()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
		cout << ndfas[cur_ndfa] << " " << (cur_ndfa->containsState(name)?"contains":"does not contain") << " state " << name << endl;
}

void listStates()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	auto states = cur_ndfa->listStates();
	cout << "States in " << ndfas[cur_ndfa] << ":" << endl;
	for(auto s: states)
		cout << s << endl;
}

void setStart()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
	{
		if(cur_ndfa->containsState(name))
		{
			cur_ndfa->setStartState(name);
			cout << "Set start state to " << name << endl;
		}
		else
			cout << "Could not set start state to " << name << " - state does not exist" << endl;
	}
}

void whatStart()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	cout << "Start state is " << cur_ndfa->getStartState() << endl;
}

void isFinal()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
	{
		if(cur_ndfa->containsState(name))
			cout << "State " << name << (cur_ndfa->isStateFinal(name)?" is ":" is not ") << "final" << endl;
		else
			cout << "State " << name << " does not exist" << endl;
	}
}

void setFinal()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	bool value;
	bool good = readArg(name) && readArg(value);
	if(good)
	{
		if(cur_ndfa->containsState(name))
		{
			cur_ndfa->setStateIsFinal(name,value);
			cout << "State " << name << " is now " << (value?"final":"not final") << endl;
		}
		else
			cout << "State " << name << " does not exist" << endl;
	}
}

void addTransition()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from,to;
	char symbol;
	if(readArg(from) && readArg(symbol) && readArg(to))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		if(!cur_ndfa->containsState(to))
		{
			cout << "State " << to << " does not exist" << endl;
			return;
		}
		if(cur_ndfa->addTransition(from,symbol,to))
			cout << "Added transition from " << from << " to " << to << " on symbol " << symbol << endl;
		else
			cout << "Could not add transition - transition already exists" << endl;
	}
}

void addEpsilon()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from,to;
	if(readArg(from) && readArg(to))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		if(!cur_ndfa->containsState(to))
		{
			cout << "State " << to << " does not exist" << endl;
			return;
		}
		if(cur_ndfa->addTransition(from,Symbol<char>::epsilon,to))
			cout << "Added epsilon transition from " << from << " to " << to << endl;
	}
}

void listTransitions()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
	{
		if(!cur_ndfa->containsState(name))
		{
			cout << "State " << name << " does not exist" << endl;
			return;
		}
		auto transitions = cur_ndfa->listTransitions(name);
		cout << "Transitions from State " << name << ":" << endl;
		for(auto t: transitions)
		{
			if(t.first.isEpsilon)
				cout << "Epsilon -> " << flush;
			else
				cout << t.first.symbol << " -> " << flush;
			for(auto s: t.second)
			{
				cout << s << " " << flush;
			}
			cout << endl;
		}
	}
}

void removeTransition()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from;
	char symbol;
	StateLabel to;
	if(readArg(from) && readArg(symbol) && readArg(to))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		if(cur_ndfa->removeTransition(from,symbol,to))
			cout << "Removed transition from " << from << " on symbol " << symbol << " to " << to << endl;
		else
			cout << "Could not remove transition - transition does not exists" << endl;
	}
}

void removeEpsilon()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from,to;
	if(readArg(from) && readArg(to))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		if(!cur_ndfa->containsState(to))
		{
			cout << "State " << to << " does not exist" << endl;
			return;
		}
		if(cur_ndfa->removeTransition(from,Symbol<char>::epsilon,to))
			cout << "Removed epsilon transition from " << from << " to " << to << endl;
		else
			cout << "Transition does not exist" << endl;
	}
}

void removeEpsilons()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from;
	if(readArg(from))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		cur_ndfa->removeTransitions(from,Symbol<char>::epsilon);
		cout << "Removed all epsilon transitions from " << from << endl;
	}
}

void removeTransitions()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	char symbol;
	if(readArg(name) && readArg(symbol))
	{
		if(!cur_ndfa->containsState(name))
		{
			cout << "State " << name << " does not exist" << endl;
			return;
		}
		cur_ndfa->removeTransitions(name,symbol);
		cout << "Removed all transitions from " << name << " on symbol " << symbol << endl;
	}
}

void clearTransitions()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
	{
		if(!cur_ndfa->containsState(name))
		{
			cout << "State " << name << " does not exist" << endl;
			return;
		}
		cur_ndfa->clearTransitions(name);
		cout << "Removed all transitions from State " << name << endl;
	}
}

void delta()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel from;
	set<StateLabel> to;
	char symbol;
	if(readArg(from) && readArg(symbol))
	{
		if(!cur_ndfa->containsState(from))
		{
			cout << "State " << from << " does not exist" << endl;
			return;
		}
		if(cur_ndfa->delta(from,symbol,to))
		{
			cout << "Delta from " << from << " on symbol " << symbol << " yields {" << flush;
			for(auto t: to)
				cout << t << " " << flush;
			cout << "}" << endl;
		}
		else
			cout << "Delta from " << from << " on symbol " << symbol << " yields nothing" << endl;
	}
}

void expandEpsilon()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	StateLabel name;
	if(readArg(name))
	{
		if(!cur_ndfa->containsState(name))
		{
			cout << "State " << name << " does not exist" << endl;
			return;
		}
		set<StateLabel> expand = cur_ndfa->expandEpsilon(name);
		cout << "Epsilon expansion of State " << name << " yields: {" << flush;
		for(auto e: expand)
			cout << e << " " << flush;
		cout << "}" << endl;
	}
}

void accept()
{
	if(!cur_ndfa)
	{
		cout << "No NDFA is selected" << endl;
		return;
	}
	string input;
	if(readArg(input))
	{
		NDFARunner<char> runner(*cur_ndfa);
		if(runner.accept(input.data(),input.size()))
			cout << "String was accepted" << endl;
		else
			cout << "String was not accepted" << endl;
	}
}

void isProper()
{
	string name;
	if(readArg(name))
	{
		for(auto n: ndfas)
		if(n.second==name)
		{
			cout << name << " is " << (NDFA<char>::IsProper(*n.first)?"":"not") << " proper" << endl;
			return;
		}
		cout << name << " does not name an NDFA" << endl;
	}
}

void makeProper()
{
	string name;
	if(readArg(name))
	{
		for(auto n: ndfas)
		if(n.second==name)
		{
			NDFA<char>::MakeProper(*n.first);
			cout << "Properized " << name << endl;
			return;
		}
		cout << name << " does not name an NDFA" << endl;
	}
}

void Union()
{
	string name1, name2, name3;
	if(readArg(name1) && readArg(name2) && readArg(name3))
	{
		NDFA<char>* ndfa1=nullptr;
		NDFA<char>* ndfa2=nullptr;
		NDFA<char>* result;
		for(auto n: ndfas)
		{
			if(n.second==name1)
				ndfa1=n.first;
			if(n.second==name2)
				ndfa2=n.first;
			if(n.second==name3)
			{
				cout << name3 << " is already in use" << endl;
				return;
			}
		}
		bool goodNDFAs = true;
		if(!ndfa1)
		{
			cout << "First name does not name an NDFA" << endl;
			goodNDFAs = false;
		}
		if(!ndfa2)
		{
			cout << "Second name does not name an NDFA" << endl;
			goodNDFAs = false;
		}
		if(!goodNDFAs)
			return;

		result = new NDFA<char>;
		NDFABuilder<char>::Union(*ndfa1,*ndfa2,*result);
		ndfas[result] = name3;
	}
}

void intersect()
{
	string name1, name2, name3;
	if(readArg(name1) && readArg(name2) && readArg(name3))
	{
		NDFA<char>* ndfa1=nullptr;
		NDFA<char>* ndfa2=nullptr;
		NDFA<char>* result;
		for(auto n: ndfas)
		{
			if(n.second==name1)
				ndfa1=n.first;
			if(n.second==name2)
				ndfa2=n.first;
			if(n.second==name3)
			{
				cout << name3 << " is already in use" << endl;
				return;
			}
		}
		bool goodNDFAs = true;
		if(!ndfa1)
		{
			cout << "First name does not name an NDFA" << endl;
			goodNDFAs = false;
		}
		if(!ndfa2)
		{
			cout << "Second name does not name an NDFA" << endl;
			goodNDFAs = false;
		}
		if(!goodNDFAs)
			return;

		result = new NDFA<char>;
//		NDFABuilder<char>::Intersection(*ndfa1,*ndfa2,*result);
		ndfas[result] = name3;
	}
}

void complement()
{
	string source,dest;
	if(readArg(source),readArg(dest))
	{
		NDFA<char>* orig=nullptr;
		NDFA<char>* result;
		for(auto n: ndfas)
		{
			if(n.second==source)
				orig=n.first;
			if(n.second==dest)
			{
				cout << dest << " is already in use" << endl;
				return;
			}
		}
		if(!orig)
		{
			cout << source << " does not name an NDFA" << endl;
			return;
		}

		result = new NDFA<char>;
		NDFABuilder<char>::Complement(*orig,*result);
		ndfas[result] = dest;
	}
}

void printHelp()
{
	cout << (char)NEWNDFA << " <string> - Create a new NDFA" << endl <<
			(char)LISTNDFAS << " - List all NDFAs in memory" << endl <<
			(char)WHICHNDFA << " - Show the current NDFA"  << endl <<
			(char)SWITCHNDFA << " <string> - Switch to NDFA named <string>"  << endl <<
			(char)REMOVENDFA << " <string> - Remove NDFA named <string>"  << endl <<
			(char)ADDSTATE << " <int> - Add a state"  << endl <<
			(char)CONTAINSSTATE << " <int> - See if current NDFA contains state"  << endl <<
			(char)LISTSTATES << " - List all states in the current NDFA"  << endl <<
			(char)SETSTART << " <int> - Set the start state of the current NDFA to <int>"  << endl <<
			(char)WHATSTART << " <int> - Show the start state of the current NDFA"  << endl <<
			(char)SETFINAL << " <int> <bool> - Set whether state <int> is final"  << endl <<
			(char)ISFINAL << " <int> - Show whether state <int> is final"  << endl <<
			(char)ADDTRANSITION << " <int> <char> <int> - Add a transition from state <int> on symbol <char> to state <int>" << endl <<
			(char)ADDEPSILON << " <int> <int> - Add an epsilon transition from <int> to <int>" << endl <<
			(char)LISTTRANSITIONS << " <int> - List all transitions on state <int>"  << endl <<
			(char)REMEPSILON << " <int> <int> - Remove the epsilon transition, if any, from <int> to <int>" << endl <<
			(char)REMEPSILONS << " <int> - Remove all epsilon transitions from <int>" << endl <<
			(char)REMTRANSITION << " <int> <char> <int> - Remove the transition, if any, from state <int> on symbol <char> to state <int>" << endl <<
			(char)REMTRANSITIONS << " <int> <char> - Remove all transitions from state <int> on symbol <char>" << endl <<
			(char)CLEARTRANSITIONS << " <int> - Clear all transitions from state <int>" << endl <<
			(char)DELTA << " <int> <char> - Show the result of a transition from state <int> on symbol <char>" << endl <<
			(char)EXPANDEPSILON << " <int> - Show the epsilon expansion of state <int>" << endl <<
			(char)ACCEPT << " <string> - Try to accept <string>" << endl <<
			(char)ISPROPER << " <string> - Test whether <string> is a proper NDFA (no broken transitions, no unreachable states)" << endl <<
			(char)MAKEPROPER << " <string> - Make <string> proper: Add states such that no transitions are broken, remove states that are unreachable" << endl <<
			(char)UNION << " <string1> <string2> <string3> - Make a new NDFA consisting of the union of NDFAs <string1> and <string2> and store it in <string3>" << endl <<
			(char)INTERSECT << "<string1> <string2> <string3> - Make a new NDFA consisting of the intersection of NDFAs <string1> and <string2> and store it in <string3>" << endl <<
			(char)CLOSURE << "<string1> <string2> - Make a new NDFA consisting of the closure (repetition 0 or more times) of <string1> and store it in <string2>" << endl <<
			(char)POSCLOSURE << "<string1> <string2> - Make a new NDFA consisting of the plus-closure (repetition 1 or more times) of <string1> and store it in <string2>" << endl <<
			(char)COMPLEMENT << "<string1> <string2> - Make a new NDFA consisting of the complement of <string1> and store it in <string2>" << endl <<
			(char)CONCAT << "<string1> <string2> <string3> - Make a new NDFA consisting of the concatenation of <string1> and <string2> and store it in <string3>" << endl <<
			(char)EXIT << " - Quit" << endl <<
			(char)HELP << " - Print help" << endl;
}

void cleanup()
{
	for(auto n: ndfas)
		delete n.first;
}

template <typename T>
bool readArg(T& arg)
{
	argstr >> arg;
	if(argstr.fail())
	{
		cout << "Syntax Error" << endl;
		return false;
	}
	return true;
}
