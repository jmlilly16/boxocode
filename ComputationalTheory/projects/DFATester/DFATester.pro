TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11

TARGET = DFATester

INCLUDEPATH += ../../source/

HEADERS += ../../source/DFA.h \
    ../../source/DFARunner.h

SOURCES += source/DFATester.cpp

DESTDIR = build
OBJECTS_DIR = build/.obj
