#include "DFA.h"
#include "DFARunner.h"

#include <iostream>
using namespace std;

int main()
{
	DFA<char> dfa;
    dfa.addState(0);
    dfa.setStartState(0);
    dfa.addTransition(0,'a',0);
	dfa.addState(1);
    dfa.setStateIsFinal(1,true);
    dfa.addTransition(0,'b',1);
    dfa.addTransition(1,'a',0);
    dfa.setTransition(1,'a',1);
    dfa.addTransition(1,'b',0);

    dfa.addState(2);
    dfa.addTransition(2,'a',1);
    dfa.addTransition(2,'b',1);
    dfa.addTransition(1,'c',2);

    dfa.containsState(1);
    dfa.listStates();
    dfa.listTransitions(2);
    dfa.isStateFinal(2);

    dfa.removeTransitions(1,'c');
    dfa.clearTransitions(2);
    dfa.removeState(2);

//	cout << "DFA has these states: " << flush;
//	for(auto i: dfa.listStates())
//		cout << i << " " << flush;
//	cout << endl;

	DFARunner<char> foo(dfa);
    string str = "aaaabababaaa";
    if(foo.accept(str.data(),12))
	{
		cout << "Win!" << endl;
	}
	else
	{
		cout << "No win :(" << endl;
	}
	return 0;
}
