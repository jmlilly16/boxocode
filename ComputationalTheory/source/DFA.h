//file: DFA.h
//auth: Mason Lilly

#ifndef DFA_H
#define DFA_H

#include "NDFA.h"

#include <queue>
using std::queue;

template <class A>
class DFA : public NDFA<A>
{
    private:
	using NDFA<A>::states;
    public:
    bool addTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to);
	bool addTransition(const StateLabel& from, A symbol, const StateLabel& to);
    bool setTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to);
    bool setTransition(const StateLabel& from, A symbol, const StateLabel& to);

    bool delta(const StateLabel& from, Symbol<A> symbol, StateLabel& to) const;
	bool delta(const StateLabel& from, A symbol, StateLabel& to) const;

	static bool IsFullySpecified(const NDFA<A>& dfa);
	static void MakeFullySpecified(const NDFA<A>& dfa, NDFA<A>& result);
	static DFA MakeFullySpecified(const NDFA<A>& dfa);

	static bool IsDeterministic(const NDFA<A>& ndfa);
	static void MakeDeterministic(const NDFA<A>& ndfa, DFA& result);
	static DFA MakeDeterministic(const NDFA<A>& ndfa);
};

template <class A>
bool DFA<A>::addTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to)
{
	if(symbol.isEpsilon)
        return false;
    auto& t = states.at(from).transitions;
    if(t.count(symbol))
        return false;
    set<StateLabel> target;
    target.insert(to);
    t[symbol] = target;
    return true;
}

template <class A>
bool DFA<A>::addTransition(const StateLabel &from, A symbol, const StateLabel &to)
{
	return addTransition(from,Symbol<A>(symbol),to);
}

template <class A>
bool DFA<A>::setTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to)
{
	if(symbol.isEpsilon)
        return false;
    auto& t = states.at(from).transitions;
    t[symbol].clear();
    t[symbol].insert(to);
    return true;
}

template <class A>
bool DFA<A>::setTransition(const StateLabel& from, A symbol, const StateLabel& to)
{
    return setTransition(from,Symbol<A>(symbol),to);
}

template <class A>
bool DFA<A>::delta(const StateLabel& from, Symbol<A> symbol, StateLabel& to) const
{
    set<StateLabel> target;
	bool result = NDFA<A>::delta(from,symbol,target);
    to = *(target.begin());
    return result;
}

template <class A>
bool DFA<A>::delta(const StateLabel& from, A symbol, StateLabel& to) const
{
    return delta(from,Symbol<A>(symbol),to);
}

template <class A>
bool DFA<A>::IsDeterministic(const NDFA<A>& ndfa)
{
	for(auto s: ndfa.listStates())
	for(auto t: ndfa.listTransitions(s))
	if(t.first == Symbol<A>::epsilon || t.second.size() > 1)
		return false;
	return true;
}

template <class A>
void DFA<A>::MakeDeterministic(const NDFA<A>& ndfa, DFA& dfa)
{
	typedef set<StateLabel> CompositeLabel;
	queue<CompositeLabel> stateQueue;
	CompositeLabel currentState;
	set<StateLabel> to;
	map<Symbol<A>,CompositeLabel> newStates;
	map<CompositeLabel,StateLabel> aliases;
	StateLabel next = 0;

	dfa.clear();
	currentState.clear();
	currentState.insert(ndfa.getStartState());
	ndfa.expandEpsilon(currentState);
	aliases[currentState] = next++;
	stateQueue.push(currentState);
	while(!stateQueue.empty())
	{
		currentState = stateQueue.front();
		stateQueue.pop();
		ndfa.expandEpsilon(currentState);
		newStates.clear();

		//compile a map of all transitions reachable on each symbol from the current composite state
		for(auto i: currentState)
		for(auto j: ndfa.listTransitions(i))
		{
			to.clear();
			ndfa.delta(i,j.first,to);
			for(auto k: to)
				newStates[j.first].insert(k);
		}

		//make an alias for any new composite states that were created, and add transitions to the dfa.
		//also add any new composite states to the queue to be expanded later.
		for(auto i: newStates)
		{
			if(!aliases.count(i.second))
			{
				aliases[i.second] = next++;
				dfa.addState(aliases[i.second]);
				stateQueue.push(i.second);
			}
			dfa.addTransition(aliases[currentState],i.first,aliases[i.second]);
		}
	}
}

template <class A>
DFA<A> DFA<A>::MakeDeterministic(const NDFA<A>& ndfa)
{
	DFA result;
	MakeDeterministic(ndfa,result);
	return result;
}

#endif
