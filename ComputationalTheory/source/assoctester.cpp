#include "assoc.h"

#include <typeinfo>
#include <iostream>
using namespace std;

int main()
{
	struct foo
	{
		char bar;
	};
	assoc<foo,double> test;
	foo tim;
	tim.bar = 'a';
	test.set(tim,10.0);
	test[tim] = 0.5;
	cout << "test[0] is a " << typeid(test[tim]).name() << endl;
	return 0;
}
