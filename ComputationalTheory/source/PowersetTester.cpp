#include "PowerSet.h"
#include <iostream>

using namespace std;

void printSet(const set<int>& x)
{
	cout << "{";
	for(int i: x)
		cout << i << " " << flush;
	cout << "}";
	cout << endl;
}

int main()
{
	set<int> foo = {1,5,3,6,8,7,4,2};
	set<set<int>> bar = powerset(foo);
	for(auto b: bar)
	{
		printSet(b);
	}
	return 0;
}
