#ifndef DFARUNNER_H
#define DFARUNNER_H

#include "DFA.h"

#include <vector>
using std::vector;

template <class A>
class DFARunner
{
	private:
	const DFA<A>& dfa;
	StateLabel state;

	public:
	DFARunner(const DFA<A>& dfa);
	bool accept(const A* str, size_t l);
	bool accept(const vector<A>& str);
};

template <class A>
DFARunner<A>::DFARunner(const DFA<A>& dfa)
	:dfa(dfa)
{}

template <class A>
bool DFARunner<A>::accept(const A* str, size_t l)
{
	int i = 0;
	state = dfa.getStartState();
	while(i<l)
	{
		if(!dfa.delta(state,str[i++],state))
			return false;
	}
	return dfa.isStateFinal(state);
}

template <class A>
bool DFARunner<A>::accept(const vector<A>& str)
{
	state = dfa.getStartState();
	for(auto i: str)
	if(!dfa.delta(state,i,state))
		return false;
	return dfa.isStateFinal(state);
}

#endif // DFARUNNER_H
