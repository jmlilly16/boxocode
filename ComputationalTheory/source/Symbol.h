#ifndef SYMBOL_H
#define SYMBOL_H

template <class AlphabetType>
class Symbol
{
    public:
	const AlphabetType symbol;
	const bool isEpsilon;
	Symbol(AlphabetType symbol, bool isEpsilon = false);
    bool operator==(const Symbol& other) const;
	bool operator<(const Symbol& other) const;

	const static Symbol epsilon;
};

template <class A>
Symbol<A>::Symbol(A symbol, bool epsilon)
    :symbol(symbol),
	isEpsilon(epsilon)
{}

template <class A>
bool Symbol<A>::operator==(const Symbol& other) const
{
	return (isEpsilon && other.isEpsilon) || symbol == other.symbol;
}

template <class A>
bool Symbol<A>::operator<(const Symbol& other) const
{
	return symbol < other.symbol;
}

template <class A>
const Symbol<A> Symbol<A>::epsilon = Symbol<A>(A(),true);

#endif // SYMBOL_H
