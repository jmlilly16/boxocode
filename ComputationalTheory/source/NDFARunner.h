#ifndef NDFARUNNER_H
#define NDFARUNNER_H

#include "NDFA.h"

#include <vector>
using std::vector;

template <class Alphabet>
class NDFARunner
{
	private:
	const NDFA<Alphabet>& ndfa;
	set<StateLabel> states;

	public:
	NDFARunner(const NDFA<Alphabet>& ndfa);
	bool accept(const vector<Alphabet>& str);
	bool accept(const Alphabet* str, size_t l);
};

template <class A>
NDFARunner<A>::NDFARunner(const NDFA<A>& ndfa)
	:ndfa(ndfa)
{}

template <class A>
bool NDFARunner<A>::accept(const vector<A>& str)
{
	set<StateLabel> to,next;
	states.clear();
	states.insert(ndfa.getStartState());
	for(auto symbol: str)
	{
		ndfa.expandEpsilon(states);
		next.clear();
		for(auto state: states)
		{
			to.clear();
			ndfa.delta(state,symbol,to);
			for(auto k: to)
				next.insert(k);
		}
		if(next.empty())
			return false;
		states = next;
	}
	ndfa.expandEpsilon(states);

	for(auto s: states)
	if(ndfa.isStateFinal(s))
		return true;
	return false;
}

template <class A>
bool NDFARunner<A>::accept(const A* str, size_t l)
{
	int i = 0;
	set<StateLabel> to,next;

	states.insert(ndfa.getStartState());

	while(i<l)
	{
		ndfa.expandEpsilon(states);
		next.clear();
		for(auto state: states)
		{
			to.clear();
			ndfa.delta(state,str[i],to);
			for(auto k: to)
				next.insert(k);
		}
		if(next.empty())
			return false;
		states = next;
		i++;
	}
	ndfa.expandEpsilon(states);

	for(auto state: states)
	if(ndfa.isStateFinal(state))
		return true;
	return false;
}

#endif // NDFARUNNER_H
