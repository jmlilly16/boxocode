#ifndef NDFA_H
#define NDFA_H

#include "Symbol.h"

#include <map>
using std::map;
#include <set>
using std::set;

typedef int StateLabel;

template <class A>
class NDFA
{
	public:
	typedef A alphType;

	struct StateData
	{
		bool isFinal;
		map<Symbol<A>,set<StateLabel>> transitions;
		StateData();
		~StateData();
	};

	protected:
	map<StateLabel,StateData> states;
	StateLabel startState;

	public:
	NDFA();
	virtual ~NDFA();

	bool addState(const StateLabel& name);
	bool containsState(const StateLabel& name) const;
	set<StateLabel> listStates() const;
	bool removeState(StateLabel name);

	void setStartState(const StateLabel& name);
	void setStateIsFinal(const StateLabel& name, bool value);
	StateLabel getStartState() const;
	bool isStateFinal(const StateLabel& name) const;

	virtual bool addTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to);
	bool addTransition(const StateLabel& from, A symbol, const StateLabel& to);
	bool removeTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to);
	bool removeTransition(const StateLabel& from, A symbol, const StateLabel& to);
	bool removeTransitions(const StateLabel& name, Symbol<A> symbol);
	bool removeTransitions(const StateLabel &name, A symbol);
	const map<Symbol<A>,set<StateLabel>>& listTransitions(const StateLabel& name) const;
	void clearTransitions(const StateLabel& name);

	bool delta(const StateLabel& from, Symbol<A> symbol, set<StateLabel>& to) const;
	bool delta(const StateLabel& from, A symbol, set<StateLabel>& to) const;

	void expandEpsilon(set<StateLabel>& current) const;
	set<StateLabel> expandEpsilon(const StateLabel& name) const;
	set<A> getAlphabet() const;

	void clear();

	static bool IsProper(const NDFA& ndfa);
	static void MakeProper(NDFA& ndfa);
};

template <class A>
NDFA<A>::StateData::StateData()
	:isFinal(false)
{}

template <class A>
NDFA<A>::StateData::~StateData()
{}

template <class A>
NDFA<A>::NDFA()
{}

template <class A>
NDFA<A>::~NDFA()
{}

template <class A>
bool NDFA<A>::addState(const StateLabel &name)
{
	if(states.count(name))
		return false;
	states[name] = StateData();
	return true;
}

template <class A>
bool NDFA<A>::containsState(const StateLabel &name) const
{
	return states.count(name);
}

template <class A>
set<StateLabel> NDFA<A>::listStates() const
{
	set<StateLabel> result;
	for(auto i: states)
		result.insert(i.first);
	return result;
}

template <class A>
bool NDFA<A>::removeState(StateLabel name)
{
	return states.erase(name);
}

template <class A>
void NDFA<A>::setStartState(const StateLabel& name)
{
	startState = name;
}

template <class A>
void NDFA<A>::setStateIsFinal(const StateLabel &name, bool value)
{
	states.at(name).isFinal = value;
}

template <class A>
StateLabel NDFA<A>::getStartState() const
{
	return startState;
}

template <class A>
bool NDFA<A>::isStateFinal(const StateLabel &name) const
{
	return states.at(name).isFinal;
}

template <class A>
bool NDFA<A>::addTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to)
{
	if(states.at(from).transitions[symbol].count(to))
		return false;
	states.at(from).transitions[symbol].insert(to);
	return true;
}

template <class A>
bool NDFA<A>::addTransition(const StateLabel &from, A symbol, const StateLabel &to)
{
	return addTransition(from,Symbol<A>(symbol),to);
}

template <class A>
const map<Symbol<A>,set<StateLabel> >& NDFA<A>::listTransitions(const StateLabel& name) const
{
	return states.at(name).transitions;
}

template <class A>
bool NDFA<A>::removeTransitions(const StateLabel &name, Symbol<A> symbol)
{
	return states.at(name).transitions.erase(symbol);
}

template <class A>
bool NDFA<A>::removeTransitions(const StateLabel &name, A symbol)
{
	return removeTransitions(name,Symbol<A>(symbol));
}

template <class A>
bool NDFA<A>::removeTransition(const StateLabel& from, Symbol<A> symbol, const StateLabel& to)
{
	return states.at(from).transitions[symbol].erase(to);
}

template <class A>
bool NDFA<A>::removeTransition(const StateLabel& from, A symbol, const StateLabel& to)
{
	return removeTransition(from,Symbol<A>(symbol),to);
}

template <class A>
void NDFA<A>::clearTransitions(const StateLabel &name)
{
	states.at(name).transitions.clear();
}

template <class A>
bool NDFA<A>::delta(const StateLabel &from, A symbol, set<StateLabel> &to) const
{
	return delta(from,Symbol<A>(symbol),to);
}

template <class A>
bool NDFA<A>::delta(const StateLabel &from, Symbol<A> symbol, set<StateLabel> &to) const
{
	if(!(states.at(from).transitions.count(symbol)))
		return false;
	to = states.at(from).transitions.at(symbol);
	return true;
}

template <class A>
void NDFA<A>::expandEpsilon(set<StateLabel>& current) const
{
	set<StateLabel> expand,to;
	int prevSize;
	const Symbol<A>& epsilon = Symbol<A>::epsilon;

	do
	{
		prevSize = current.size();
		expand.clear();
		for(auto i: current)
		{
			to.clear();
			if(delta(i,epsilon,to))
			for(auto j: to)
				expand.insert(j);
		}
		for(auto i: expand)
			current.insert(i);
	}while(prevSize<current.size());
}

template <class A>
set<StateLabel> NDFA<A>::expandEpsilon(const StateLabel& name) const
{
	set<StateLabel> result;
	result.insert(name);
	expandEpsilon(result);
	return result;
}

template <class A>
set<A> NDFA<A>::getAlphabet() const
{
	set<A> result;
	for(auto s: states)
	for(auto t: s.second.transitions)
	if(t.first != Symbol<A>::epsilon)
		result.insert(t.first.symbol);
}

template <class A>
void NDFA<A>::clear()
{
	states.clear();
}

template <class A>
bool NDFA<A>::IsProper(const NDFA& ndfa)
{
	set<StateLabel> frontier,next,seen;
	size_t prevsize;
	seen.clear();
	frontier.insert(ndfa.getStartState());

	do
	{
		prevsize = seen.size();
		next.clear();
		ndfa.expandEpsilon(frontier);
		for(auto f: frontier)
		{
			if(!ndfa.containsState(f))
				return false;
			for(auto t: ndfa.listTransitions(f))
			for(auto s: t.second)
				next.insert(s);
			seen.insert(f);
		}
		frontier = next;
	}
	while(seen.size()>prevsize);
	if(seen.size()<ndfa.listStates().size())
		return false;
	return true;
}

template <class A>
void NDFA<A>::MakeProper(NDFA& ndfa)
{
	set<StateLabel> frontier,next,seen;
	size_t prevsize;
	seen.clear();
	frontier.insert(ndfa.getStartState());

	do
	{
		prevsize = seen.size();
		next.clear();
		ndfa.expandEpsilon(frontier);
		for(auto f: frontier)
		{
			if(!ndfa.containsState(f))
				ndfa.addState(f);
			for(auto t: ndfa.listTransitions(f))
			for(auto s: t.second)
				next.insert(s);
			seen.insert(f);
		}
		frontier = next;
	}
	while(seen.size()>prevsize);

	for(auto s: ndfa.listStates())
	if(!seen.count(s))
		ndfa.removeState(s);
}

#endif // NDFA_H
