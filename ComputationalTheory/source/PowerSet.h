//file PowerSet.h
//Auth: Mason Lilly

//Defines the powerset function, which creates the mathematical power set (set of all subsets) of a given std::set object

#ifndef POWERSET
#define POWERSET

#include <set>

using namespace std;

//This function takes in a set and returns the set of all subsets of that set
//E.g., passing in {2,5,6} would yield {{},{2},{5},{6},{2,5},{2,6},{5,6},{2,5,6}}
//Note that an empty set is always included
template <class E>
set<set<E>> powerset(const set<E>& x)
{
	set<set<E>> result;	//Powerset to be built
	result.insert(set<E>());//Seed the result with the empty set
	for(E e: x)		//For each e in x...
	{
		set<set<E>> tempSets;	//...make a new set of sets...
		for(const set<E>& s: result)
		{
			set<E> tempSet = s;
			tempSet.insert(e);	//...which is the union of e and every set already in result...
			tempSets.insert(tempSet);
		}
		for(const set<E>& s: tempSets)	//...then add those new sets to result.
			result.insert(s);
	}
	return result;
}

#endif
